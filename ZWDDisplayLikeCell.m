//
//  ZWDDisplayLikeCell.m
//  yanyu
//
//  Created by Case on 15/3/25.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "ZWDDisplayLikeCell.h"
#import "CDFMeViewController.h"




@interface ZWDDisplayLikeCell ()
{
    NSArray *_likeList;
    NSInteger _dataTag;
}
@property (nonatomic ,strong) UIButton *likeButton;

@end

@implementation ZWDDisplayLikeCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)showData:(NSArray *)array{
    
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    
    _likeList = [NSArray arrayWithArray:array];
    _dataTag = -1;
    /*
     (
     {
     uid = 14633085;
     username = ceshi222;
     }
     )
     */
    
    NSMutableString *strOld = [NSMutableString new];
    //[strOld appendString:@"👍  "];
    for (int i = 0; i < array.count; i++) {
        NSDictionary *dict = array[i];
        if (i == 0) {
            [strOld appendFormat:@"%@",dict[@"username"]];
            
        }else{
            [strOld appendFormat:@" , %@",dict[@"username"]];
        }
    }
    
    NSMutableString *str = [NSMutableString new];
    //[str appendString:@"👍  "];
    for (int i = 0; i < array.count; i++) {
        NSDictionary *dict = array[i];
        if (i == 0) {
            [str appendString:[NSString stringWithFormat:@"<a href=\"title\">[%d赞] </a>",array.count]];
            [str appendFormat:@"<a href=\"home.php?mod=space&uid=%@\">%@</a>",dict[@"uid"],dict[@"username"]];

        }else{
            [str appendFormat:@"<a href=\"home.php?mod=space&uid=%@\"> , %@</a>",dict[@"uid"],dict[@"username"]];
        }
    }
   /*
    NSLog(@"%f",self.bounds.size.height);
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(13, 0, self.bounds.size.width - 25, [self heightForString:str fontSize:12 andWidth:self.bounds.size.width - 25])];
    label.text = str;
    label.textColor = TITLEGRAY;
    label.font = [UIFont systemFontOfSize:12];
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.numberOfLines = 0;
    [self addSubview:label];
    */
    //btn_heart_hl@2x     btn_heart@2x
    
    
    /*
    //分割线
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 50, self.bounds.size.width, 0.5)];
    lineView.backgroundColor = [UIColor colorWithWhite:200.0f/255.0f alpha:.5];
    [self addSubview:lineView];
    /////
     */
    
    DTAttributedLabel *label = [[DTAttributedLabel alloc]initWithFrame:CGRectMake(17, 13, self.bounds.size.width - 34, [self heightForString:strOld fontSize:FONT andWidth:self.bounds.size.width - 34]+30)];
    label.delegate = self;
    label.backgroundColor = [UIColor clearColor];
    label.attributedString = [self formatHTMLString:str atIndexPath:nil];
    [self addSubview:label];
    //heart_disabled@2x
    UIImageView *likeImage = [[UIImageView alloc]initWithFrame:CGRectMake(8, 3, 20, 20)];
    likeImage.contentMode = UIViewContentModeScaleAspectFit;
    likeImage.backgroundColor = [UIColor clearColor];
    likeImage.image = [UIImage imageNamed:@"heart_disabled"];
    //[self addSubview:likeImage];
    
    UILabel *countLabel = [[UILabel alloc]initWithFrame:CGRectMake(8, 3, 40, 20)];
    countLabel.text = [NSString stringWithFormat:@"[%d赞] ",array.count];
    countLabel.textColor = RGBACOLOR(238, 49, 61, 1.f);
    //[self addSubview:countLabel];
    
    self.backgroundColor = RGBACOLOR(243, 243, 243, 1.0f);
//    self.backgroundColor = [UIColor whiteColor];
//    UIView *lineTop = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 1)];
//    lineTop.backgroundColor = [Common colorWithHex:@"#CDCDCD" alpha:1];
//    [self addSubview:lineTop];
//    UIView *lineBottom = [[UIView alloc]initWithFrame:CGRectMake(0, self.bounds.size.height - 1, self.bounds.size.width, 1)];
//    lineBottom.backgroundColor = [Common colorWithHex:@"#CDCDCD" alpha:1];
//    [self addSubview:lineBottom];
    

    UIView *line1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0.5, label.bounds.size.width, 0.5)];
    line1.backgroundColor = RGBACOLOR(240.f, 238.f, 236.f, 1.f);
    //[label addSubview:line1];
    UIView *line2 = [[UIView alloc]initWithFrame:CGRectMake(0, label.bounds.size.height - 0.5, label.bounds.size.width, 0.5)];
    line2.backgroundColor = RGBACOLOR(240.f, 238.f, 236.f, 1.f);
    //[label addSubview:line2];
    
    
    
    
    self.likeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.likeButton.frame = CGRectMake(KDeviceSizeWidth/2 - 50, self.bounds.size.height - 50, 100, 50);
   // self.likeButton.center = CGPointMake(self.center.x, 25);
    [self.likeButton setImage:[UIImage imageNamed:@"btn_heart"] forState:UIControlStateNormal];
    [self.likeButton setImage:[UIImage imageNamed:@"btn_heart_hl"] forState:UIControlStateHighlighted];
    [self.likeButton setImage:[UIImage imageNamed:@"btn_heart_hl"] forState:UIControlStateDisabled];
    self.likeButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    //[self addSubview:self.likeButton];
    [self.likeButton addTarget:self action:@selector(likeButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if (array.count == 0) {
        //没有人点赞
        UILabel *newLbale = [[UILabel alloc]initWithFrame:label.frame];
        newLbale.text = @"还没有人点赞，赞一个吧!";
        newLbale.textColor = TITLEGRAY;
        [self addSubview:newLbale];
        
        
    }
}
#pragma mark - 点赞按钮点击
- (void)likeButtonClick:(UIButton *)button{
    
    
}

- (UIView *)attributedTextContentView:(DTAttributedTextContentView *)attributedTextContentView viewForLink:(NSURL *)url identifier:(NSString *)identifier frame:(CGRect)frame{
    if (_dataTag == -1) {
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeSystem];
//        NSLog(@"%@",identifier);
        [button1 setTitle:[NSString stringWithFormat:@"[%d赞] ",_likeList.count] forState:UIControlStateNormal];
        button1.frame = frame;
        button1.titleLabel.font = [UIFont fontWithName:@"Arial" size:FONT];
//        button1.backgroundColor = RGBACOLOR(243, 243, 243, 1.0f);
        button1.backgroundColor = [UIColor whiteColor];
        [button1 setTitleColor:RGBACOLOR(249, 51, 0, 1.f) forState:UIControlStateNormal];
        //／／RGBACOLOR(238, 49, 61, 1.f)
        _dataTag++;
        return button1;
    }
    
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    button.frame = frame;
    
    [button setTitle:[NSString stringWithFormat:@"%d",_dataTag] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(userNameClick:) forControlEvents:UIControlEventTouchUpInside];
    _dataTag ++;
    
    return button;
    
}
- (void)userNameClick:(UIButton *)button{
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CDFMeViewController *vc = [s instantiateViewControllerWithIdentifier:@"userViewController"];
//    NSLog(@"%d",button.tag);
    vc.uid = [_likeList[[button.titleLabel.text intValue]] objectForKey:@"uid"];
    [self.navVC pushViewController:vc animated:YES];
}
#pragma mark - DTAttributedLabel
- (NSAttributedString *)formatHTMLString:(NSString *)html atIndexPath:(NSIndexPath *)indexPath
{
    if (html == nil) {
        return nil;
    }
    
//    NSLog(@"%@",html);
    NSData *data = [html dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *options = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    [NSValue valueWithCGSize:CGSizeMake(306, 2000)], DTMaxImageSize,
                                    [NSNumber numberWithFloat:1.5], DTDefaultLineHeightMultiplier,
                                    [NSNumber numberWithFloat:FONT], DTDefaultFontSize,
                                    @"Arial", DTDefaultFontFamily,
                                    @"Gray", DTDefaultLinkColor,
                                    @"none", DTDefaultLinkDecoration,
                                    nil];
//    NSLog(@"%@",options);
    NSAttributedString *string = [[NSAttributedString alloc] initWithHTMLData:data options:options documentAttributes:NULL];
//    NSLog(@"%@",string);
    
    return string;
}


- (float) heightForString:(NSString *)value fontSize:(float)fontSize andWidth:(float)width
{
    CGSize sizeToFit = [value sizeWithFont:[UIFont systemFontOfSize:fontSize]
                         constrainedToSize:CGSizeMake(width -16.0, CGFLOAT_MAX)
                             lineBreakMode:NSLineBreakByWordWrapping];
    //此处的换行类型（lineBreakMode）可根据自己的实际情况进行设置
    return sizeToFit.height + 16.0;
}
@end
