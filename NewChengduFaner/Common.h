//
//  Common.h
//  通用方法，依赖 Reachability
//
//  Created by dev@huaxi100.com on 13-3-4.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"
//#import "APService.h"

@interface Common : NSObject

/**
 *  填充指定context区域线性渐变
 *
 *  @param context    目标context
 *  @param rect       需要填充的区域
 *  @param startColor 填充起始颜色
 *  @param endColor   填充终止颜色
 *  @param startPoint 填充起始点
 *  @param endPoint   填充终止点
 */
void drawLinearGradient(CGContextRef context,
                        CGRect rect,
                        CGColorRef startColor,
                        CGColorRef  endColor,
                        CGPoint startPoint,
                        CGPoint endPoint);

/**
 *  填充指定路径区域线性渐变
 *
 *  @param context    目标context
 *  @param path       需要填充的路径
 *  @param startColor 填充起始颜色
 *  @param endColor   填充终止颜色
 *  @param startPoint 填充起始点
 *  @param endPoint   填充终止点
 */
void drawLinearGradientForPath(CGContextRef context,
                               CGPathRef path,
                               CGColorRef startColor,
                               CGColorRef  endColor,
                               CGPoint startPoint,
                               CGPoint endPoint);


/**
 *  画线
 *
 *  @param context    目标context
 *  @param startPoint 起始点
 *  @param endPoint   终止点
 *  @param color      颜色
 */
void draw1PxStroke(CGContextRef context, CGPoint startPoint, CGPoint endPoint,
                   CGColorRef color);

/**
 *  创建等半径圆角矩形
 *
 *  @param rect   矩形范围
 *  @param radius 圆角半径
 *
 *  @return CGMutablePathRef
 */
CGMutablePathRef createRoundedRectForRect(CGRect rect, CGFloat radius);

/**
 *  创建不等半径圆角矩形
 *
 *  @param rect        矩形范围
 *  @param topLeft     上左半径
 *  @param topRight    上右半径
 *  @param bottomLeft  下左半径
 *  @param bottomRight 下右半径
 *
 *  @return CGMutablePathRef
 */
CGMutablePathRef createRoundedRectForRectWithEach(CGRect rect, CGFloat topLeft,CGFloat topRight, CGFloat bottomLeft, CGFloat bottomRight);

/**
 *  将 Dictionary 转化为 Data 存入 NSUserDefaults
 *
 *  @param dict k-v对象
 *  @param key  存入 NSUserDefaults 的主键值
 *
 *  @return 是否成功
 */
+ (BOOL)archive:(id)dict withKey:(NSString *)key;

/**
 *  将 NSUserDefaults 里的 Data 转化为 Dictionary
 *
 *  @param key 主键值
 *
 *  @return NSDictionary
 */
+ (id)unarchiveForKey:(NSString *)key;
/**
 *  将16进制的颜色值转化为object-c的颜色值
 *
 *  @param _hex 颜色的16进制格式，如 “#A32B9F”
 *  @param _a   颜色的透明度
 *
 *  @return UIColor
 */
+ (UIColor *) colorWithHex:(NSString *)_hex alpha:(float)_a;

/**
 *  格式化日期
 *
 *  @param timeInterval   时间戳
 *  @param format 日期格式
 *
 *  @return 格式化以后的日期字符串
 */
+ (NSString *)formatDate:(NSTimeInterval)timeInterval ;

/**
 *  缩放图片尺寸
 *
 *  @param image 图片对象
 *  @param size  容器尺寸
 *
 *  @return UIImage
 */
+ (UIImage *)resizeImage:(UIImage *)image fitSize:(CGSize)size;

/**
 *  将 Dictionary 转化为 JSON 字符串
 *
 *  @param dictionary k-v对象
 *
 *  @return 字符串
 */
+ (NSString*)dictionaryToJSON:(NSDictionary*)dictionary;

/**
 *  获取网络状态
 *
 *  @return 网络状态
 */
+ (NetworkStatus) checkReachability;

/**
 *  获取缩略图链接
 *
 *  @param imageUrl 原图片链接
 *  @param size     缩略尺寸
 *  @param retina   是否显示视网膜屏图（retina屏有效）
 *
 *  @return 缩略图链接
 */
+ (NSString *)getThumbImage:(NSString *)imageUrl size:(CGSize)size withRetinaDisplayDetect:(BOOL)retina;


+ (NSString *)getFormhash;

/**
 *  获取系统版本
 *
 *  @return 系统版本号
 */
+ (CGFloat)systemVersion;

+ (NSString *)bundleVersion;

/**
 *  根据uid返回头像地址
 *
 *  @param uid id
 *  @param type NSString
 *
 *  @return NSString
 */
+ (NSString *)formatAvatarWithUID:(id)uid type:(NSString *)type;

+ (void)saveDefaultCookies;

+ (NSArray *)getDefaultCookies;

/**
 *  添加默认的cookie到请求对象里面
 */
+ (void)appendDefaultCookiesForRequest;

+ (void)login:(id)responseObject;

/**
 *  退出登录
 */
+ (void)logout;

/**
 *  设置自定义的cookie到请求对象里面
 *
 *  @param cookies nsarray
 */
+ (void)appendCookies:(NSArray *)cookies;

/**
 *  将byte转化为 1.2KB 这种格式的字符串。
 *
 *  @param bytes long long
 *
 *  @return string
 */
+ (NSString *)formatBytes:(long long)bytes;

/**
 *  修复摄像头拍照的照片方向
 *
 *  @param image UIImage
 *
 *  @return UIImage
 */
+ (UIImage *)fixRotation:(UIImage *)image;

+ (NSString *)decodeURIComponent:(NSString *)string;

+ (NSDictionary *)parseUrlQueryString:(NSString *)query;

+ (NSDictionary *)decodeURL:(NSString *)url;

+ (CGSize) fitSize: (CGSize)thisSize inSize: (CGSize) aSize;

+ (void)showAvator:(UIImageView*)imageView uid:(id)uid type:(NSString *)type;

+ (void)writeToFile:(NSString *)key data:(NSArray *)data;
+ (NSArray *)getDataFromFile:(NSString *)key;
+ (BOOL)isDown:(NSString *)key;
@end
