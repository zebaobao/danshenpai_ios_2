//
//  Common.m
//  通用方法
//
//  Created by dev@huaxi100.com on 13-3-4.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "Common.h"

@implementation Common

void drawLinearGradient(CGContextRef context,
                        CGRect rect,
                        CGColorRef startColor,
                        CGColorRef  endColor,
                        CGPoint startPoint,
                        CGPoint endPoint)
{
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGFloat locations[] = { 0.0, 1.0 };
    
    NSArray *colors = [NSArray arrayWithObjects:(__bridge id)(startColor), (__bridge id)(endColor), nil];
    
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef) colors, locations);
    
    CGContextSaveGState(context);
    CGContextAddRect(context, rect);
    CGContextClip(context);
    CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
    
    
    
    CGContextRestoreGState(context);

    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
}

void drawLinearGradientForPath(CGContextRef context,
                        CGPathRef path,
                        CGColorRef startColor,
                        CGColorRef  endColor,
                        CGPoint startPoint,
                        CGPoint endPoint)
{
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGFloat locations[] = { 0.0, 1.0 };
    
    NSArray *colors = [NSArray arrayWithObjects:(__bridge id)(startColor), (__bridge id)(endColor), nil];
    
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef) colors, locations);
    
    CGContextSaveGState(context);
    CGContextAddPath(context, path);
    CGContextClip(context);
    CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
    CGContextRestoreGState(context);
    
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
}

void draw1PxStroke(CGContextRef context, CGPoint startPoint, CGPoint endPoint,
                   CGColorRef color)
{
    
    CGContextSaveGState(context);
    
    //禁用抗锯齿
    CGContextSetAllowsAntialiasing(context,NO);
    
    CGContextSetLineCap(context, kCGLineCapSquare);
    CGContextSetStrokeColorWithColor(context, color);
    CGContextSetLineWidth(context, .5);
    CGContextMoveToPoint(context, (int)startPoint.x + .25, (int)startPoint.y + .25);
    CGContextAddLineToPoint(context, (int)endPoint.x + .25, (int)endPoint.y + .25);
    CGContextStrokePath(context);
    
    CGContextRestoreGState(context);
}

CGMutablePathRef createRoundedRectForRect(CGRect rect, CGFloat radius)
{
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, CGRectGetMidX(rect), CGRectGetMinY(rect));
    CGPathAddArcToPoint(path, NULL, CGRectGetMaxX(rect), CGRectGetMinY(rect), CGRectGetMaxX(rect), CGRectGetMaxY(rect), radius);
    CGPathAddArcToPoint(path, NULL, CGRectGetMaxX(rect), CGRectGetMaxY(rect), CGRectGetMinX(rect), CGRectGetMaxY(rect), radius);
    CGPathAddArcToPoint(path, NULL, CGRectGetMinX(rect), CGRectGetMaxY(rect), CGRectGetMinX(rect), CGRectGetMinY(rect), radius);
    CGPathAddArcToPoint(path, NULL, CGRectGetMinX(rect), CGRectGetMinY(rect), CGRectGetMaxX(rect), CGRectGetMinY(rect), radius);
    CGPathCloseSubpath(path);
    
    return path;
}

CGMutablePathRef createRoundedRectForRectWithEach(CGRect rect, CGFloat topLeft,CGFloat topRight, CGFloat bottomLeft, CGFloat bottomRight)
{
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, CGRectGetMidX(rect), CGRectGetMinY(rect));
    CGPathAddArcToPoint(path, NULL, CGRectGetMaxX(rect), CGRectGetMinY(rect), CGRectGetMaxX(rect), CGRectGetMaxY(rect), topRight);
    CGPathAddArcToPoint(path, NULL, CGRectGetMaxX(rect), CGRectGetMaxY(rect), CGRectGetMinX(rect), CGRectGetMaxY(rect), bottomRight);
    CGPathAddArcToPoint(path, NULL, CGRectGetMinX(rect), CGRectGetMaxY(rect), CGRectGetMinX(rect), CGRectGetMinY(rect), bottomLeft);
    CGPathAddArcToPoint(path, NULL, CGRectGetMinX(rect), CGRectGetMinY(rect), CGRectGetMaxX(rect), CGRectGetMinY(rect), topLeft);
    CGPathCloseSubpath(path);
    
    return path;
}

+ (BOOL)archive:(id)dict withKey:(NSString *)key
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = nil;
    if (dict) {
        data = [NSKeyedArchiver archivedDataWithRootObject:dict];
    }
    [defaults setObject:data forKey:key];
    return [defaults synchronize];
}

+ (id)unarchiveForKey:(NSString *)key
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:key];
    id userDict = nil;
    if (data) {
        userDict = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    return userDict;
}

+ (UIColor *)colorWithHex:(NSString *)_hex alpha:(float)_a
{
    unsigned int red, green, blue;
    NSRange range;
    range.length =2;
    
    range.location =1;
    [[NSScanner scannerWithString:[_hex substringWithRange:range]]scanHexInt:&red];
    range.location =3;
    [[NSScanner scannerWithString:[_hex substringWithRange:range]]scanHexInt:&green];
    range.location =5;
    [[NSScanner scannerWithString:[_hex substringWithRange:range]]scanHexInt:&blue];
    
    return [UIColor colorWithRed:(float)(red/255.0f)green:(float)(green/255.0f)blue:(float)(blue/255.0f)alpha:_a];
}

+ (NSString *)formatDate:(NSTimeInterval)timeInterval {
    NSDate *myDate = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [NSLocale currentLocale];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm";
    NSString *dateStr = [dateFormatter stringFromDate:myDate];//目标时间, 字符串格式
    NSDate *nowDate = [NSDate date];
    NSString *nowDateStr = [dateFormatter stringFromDate:nowDate]; //当前的时间, 字符串格式
    NSTimeInterval nowTime = [[NSDate date] timeIntervalSince1970]; //当前时间到1970年的秒数
    long long int nowtime = (long long) nowTime;
    long long int cha = nowtime - timeInterval;
    //对时间进行优化显示
    if ([[dateStr substringWithRange:NSMakeRange(0, 4)] isEqualToString:[nowDateStr substringWithRange:NSMakeRange(0, 4)]]) {
        if ([[dateStr substringWithRange:NSMakeRange(5, 5)] isEqualToString:[nowDateStr substringWithRange:NSMakeRange(5, 5)]]) {
            if (cha / 3600.0 <= 1) {
                dateStr = [NSString stringWithFormat:@"%ld分钟之前", (long int)cha/60];
            }else if (cha / 3600.0<= 2 && cha / 3600.0 > 1) {
                dateStr = @"1小时前";
            }else if (cha / 3600.0<=3 && cha / 3600.0 > 2) {
                dateStr = @"2小时前";
            } else {
                dateStr = [NSString stringWithFormat:@"今天%@", [dateStr substringWithRange:NSMakeRange(11, 5)]];
//                NSLog(@"今天 %@", [dateStr substringWithRange:NSMakeRange(11, 5)]);
            }
        }else if ([[nowDateStr substringWithRange:NSMakeRange(8, 2)] integerValue] - [[dateStr substringWithRange:NSMakeRange(8, 2)] integerValue] == 1) {
//            NSLog(@"昨天%@", [dateStr substringWithRange:NSMakeRange(11, 5)]);
            dateStr = [NSString stringWithFormat:@"昨天%@", [dateStr substringWithRange:NSMakeRange(11, 5)]];
        } else {
//            NSLog(@"%@", [dateStr substringWithRange:NSMakeRange(5, 11)]);
//            NSLog(@"%@", [dateStr substringWithRange:NSMakeRange(5, 5)]);
//            dateStr = [NSString stringWithFormat:@"%@", [dateStr substringWithRange:NSMakeRange(5, 11)]]; //如果是前天或者更早的日期, 显示日期和具体时间
            dateStr = [NSString stringWithFormat:@"%@", [dateStr substringWithRange:NSMakeRange(5, 5)]];//如果是前天或者更早的日期, 只显示日期, 不显示具体时间
        }
    }
    return dateStr;
}

+ (UIImage *)resizeImage:(UIImage *)image fitSize:(CGSize)size
{
	if (image.size.width > size.width || image.size.height > size.height) {
		CGSize newSize = CGSizeZero;
		if (image.size.width / image.size.height > size.width / size.height) {
			newSize.width = size.width;
			newSize.height = image.size.height * (size.width / image.size.width);
		} else {
			newSize.width = image.size.width * (size.height / image.size.height);
			newSize.height = size.height;
		}
        //		if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        //			UIGraphicsBeginImageContextWithOptions(newSize, NO, [[UIScreen mainScreen] scale]);
        //		} else {
		UIGraphicsBeginImageContext(newSize);
        //		}
        
		//Draw the image into the rect
		[image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
		UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
        
		return newImage;
	} else {
		return image;
	}
}

+ (NSString *)dictionaryToJSON:(NSDictionary *)dictionary
{
    if (!dictionary) {
        return @"";
    }
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary
                                                       options:0
                                                         error:&error];
    
    if (!jsonData) {
        return [NSString stringWithFormat:@"JSON error: %@", error];
    } else {
        
        NSString *JSONString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
        return JSONString;
    }
}

+ (NetworkStatus) checkReachability
{
    Reachability *r = [Reachability reachabilityForInternetConnection];
    //NSLog(@"network status : %d",[r currentReachabilityStatus]);
    return [r currentReachabilityStatus];
}

+ (NSString *)getThumbImage:(NSString *)imageUrl size:(CGSize)size withRetinaDisplayDetect:(BOOL)retina
{
//    NSLog(@"%@",imageUrl);
    NetworkStatus status = [Common checkReachability];
    NSInteger q = 90;
    /*
    if (status == ReachableViaWWAN) {
        q = 90;
    }else{
        if([UIScreen mainScreen].scale == 2 && retina){
            size.width*=2;
            size.height*=2;
        }
    }
     */
	
    if([UIScreen mainScreen].scale == 2 && retina){
        size.width*=2;
        size.height*=2;
    }
    
	CFStringRef urlEncode = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)imageUrl, nil, (CFStringRef)@":/.?&=;+!@$()~", kCFStringEncodingUTF8);
	NSString *retValue = [NSString stringWithFormat:@"%@thumb/thumb.php?w=%d&h=%d&q=%d&zc=1&src=%@", kBaseURL, (int)size.width, (int)size.height,q, urlEncode];
	CFRelease(urlEncode);
//    NSLog(@"567567567567:%@", retValue);
	return retValue;
}

+ (NSString *)getFormhash
{
    NSString *formhash = [Common unarchiveForKey:kOnlineInfo][@"data"][@"formhash"];
    return formhash ? formhash : @"";
}

+ (CGFloat)systemVersion
{
    static CGFloat version;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        version = [[UIDevice currentDevice].systemVersion doubleValue];
    });
    return version;
}

+ (NSString *)bundleVersion
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
}

+ (void)login:(id)responseObject
{
    NSMutableDictionary *loginInfo = [NSMutableDictionary dictionaryWithDictionary:[Common unarchiveForKey:kLoginInfo]];
    id uid = responseObject[@"Variables"][@"member_uid"];
    
    //读取cookie
    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:[NSURL URLWithString:kBaseURL]];
    //保存cookie和登录信息
    NSDictionary *loginInfoData = @{@"cookie": cookies,@"data":responseObject[@"Variables"]};
    [loginInfo setObject:loginInfoData forKey:uid];
    [Common archive:loginInfo withKey:kLoginInfo];
    
    //设置登录信息为当前登录用户
    [Common archive:loginInfoData withKey:kOnlineInfo];
//    [APService setAlias:uid callbackSelector:nil object:nil];
    [XGPush setTag:[NSString stringWithFormat:@"%@",uid]];
    
    //发送登录通知
    [[NSNotificationCenter defaultCenter] postNotificationName:kLOGIN_NOTIFICATION object:nil userInfo:loginInfoData];
    
//    NSLog(@"%@ login", uid);
}

+ (void)logout {
    // 清除cookie
	for (NSHTTPCookie *cookie in [NSHTTPCookieStorage sharedHTTPCookieStorage].cookies) {
		[[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
	}
    [Common archive:nil withKey:kOnlineInfo];
    //发送退出登录通知
    [[NSNotificationCenter defaultCenter] postNotificationName:kLOGOUT_NOTIFICATION object:nil];
}

+ (NSString *)formatAvatarWithUID:(id)uid type:(NSString *)type
{
    return [NSString stringWithFormat:@"http://bang.dahe.cn/uc_server/avatar.php?uid=%@&size=%@", uid, type];
}

+ (void)saveDefaultCookies
{
    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:[NSURL URLWithString:kBaseURL]];
    [Common archive:@{kDefaultCookies: cookies} withKey:kDefaultCookies];
}

+ (NSArray *)getDefaultCookies
{
    return [NSArray arrayWithArray:[[Common unarchiveForKey:kDefaultCookies] objectForKey:kDefaultCookies]];
}

+ (void)appendDefaultCookiesForRequest
{
    NSArray *cookies = [[Common unarchiveForKey:kOnlineInfo] objectForKey:@"cookie"];
    //NSLog(@"%@",cookies);
    [Common appendCookies:cookies];
}

+ (void)appendCookies:(NSArray *)cookies
{
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookies:cookies forURL:[NSURL URLWithString:kBaseURL] mainDocumentURL:[NSURL URLWithString:kBaseURL]];
}

+ (NSString *)formatBytes:(long long)bytes
{
    if (bytes == 0) {
        return @"0kb";
    }
    return [NSString stringWithFormat:@"%.2fkb",bytes * 1.0f / 1024];
}

+ (UIImage *)fixRotation:(UIImage *)image
{
    if (image.imageOrientation == UIImageOrientationUp) return image;
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (image.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, image.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, image.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (image.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, image.size.width, image.size.height,
                                             CGImageGetBitsPerComponent(image.CGImage), 0,
                                             CGImageGetColorSpace(image.CGImage),
                                             CGImageGetBitmapInfo(image.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (image.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.height,image.size.width), image.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.width,image.size.height), image.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

+ (CGSize) fitSize: (CGSize)thisSize inSize: (CGSize) aSize
{
	CGFloat scale;
	CGSize newsize = thisSize;
	
	if (newsize.height && (newsize.height > aSize.height))
	{
		scale = aSize.height / newsize.height;
		newsize.width *= scale;
		newsize.height *= scale;
	}
	
	if (newsize.width && (newsize.width >= aSize.width))
	{
		scale = aSize.width / newsize.width;
		newsize.width *= scale;
		newsize.height *= scale;
	}
	if(thisSize.width < aSize.width){
		newsize.width = newsize.width / [UIScreen mainScreen].scale;
		newsize.height = newsize.height / [UIScreen mainScreen].scale;
	}
	return newsize;
}

+ (NSString *)decodeURIComponent:(NSString *)string
{
	NSMutableString *decodedString = [NSMutableString stringWithString:string];
    [decodedString replaceOccurrencesOfString:@"+"
                                   withString:@" "
                                      options:NSLiteralSearch
                                        range:NSMakeRange(0, [decodedString length])];
    
    return [decodedString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

+ (NSDictionary *)parseUrlQueryString:(NSString *)query
{
	NSArray *keyandvalue = [query componentsSeparatedByString:@"&"];
	NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:keyandvalue.count];
	for (NSString *value in keyandvalue) {
		NSArray *item = [value componentsSeparatedByString:@"="];
		if (item.count == 2) {
			[dict setValue:[Common decodeURIComponent:[item objectAtIndex:1]] forKey:[Common decodeURIComponent:[item objectAtIndex:0]]];
		}
	}
	return dict;
}

+ (NSDictionary *)decodeURL:(NSString *)url
{
	// url参数转化为字典
	NSDictionary *queryDict = [Common parseUrlQueryString:[[NSURL URLWithString:url] query]];
    NSString *path = url;
    
	__block BOOL found = NO;
	if (!found) {
		// 解析帖子链接 http://www.huaxi100.com/thread-(\\d+)-(\\d+)-\\d+\\.html
		NSRegularExpression *tidexp = [NSRegularExpression regularExpressionWithPattern:@"thread-(\\d+)-(\\d+)" options:NSRegularExpressionCaseInsensitive error:nil];
		NSArray *matches = [tidexp matchesInString:path options:NSMatchingReportCompletion range:NSMakeRange(0, path.length)];
		for (NSTextCheckingResult *match in matches) {
			if (match.numberOfRanges == 3) {
				NSString *tid = [path substringWithRange:[match rangeAtIndex:1]];
				NSInteger page = [[path substringWithRange:[match rangeAtIndex:2]] integerValue];
				
                if ([tid integerValue] > 0) {
                    return @{@"type":@"post",@"tid":tid,@"page":[NSNumber numberWithInteger:page]};
                }
			}
		}
	}
	if (!found) {
		// mod=viewthread&tid=1901480
		//NSLog(@"%@", queryDict);
		if (queryDict && [queryDict respondsToSelector:@selector(objectForKey:)]) {
			if ([@"viewthread" isEqualToString:[queryDict objectForKey:@"mod"]]) {
				NSString *tid = [queryDict objectForKey:@"tid"];
				NSInteger page = [[queryDict objectForKey:@"page"] integerValue];
				if (page <= 0) {
					page = 1;
				}
				if ([tid integerValue] > 0) {
					return @{@"type":@"post",@"tid":tid,@"page":[NSNumber numberWithInteger:page]};
				}
			}
		}
	}
	if (!found) {
		// 查找是否是跳转楼层的链接
		if (queryDict && [queryDict respondsToSelector:@selector(objectForKey:)]) {
			if ([@"findpost" isEqualToString:[queryDict objectForKey:@"goto"]]
				&& [@"redirect" isEqualToString:[queryDict objectForKey:@"mod"]]) {
				NSInteger ptid = [[queryDict objectForKey:@"ptid"] integerValue];
				NSInteger pid = [[queryDict objectForKey:@"pid"] integerValue];
				if (ptid > 0 && pid > 0) {
                    return @{@"type":@"post",@"tid":[NSString stringWithFormat:@"%d",ptid],@"pid":[NSNumber numberWithInteger:pid]};
				}
			}
		}
	}
	if (!found) {
		// 解析头像链接
        __block NSInteger uid = 0;
		NSRegularExpression *uidexp = [NSRegularExpression regularExpressionWithPattern:@"home\\.php\\?mod=space&uid=(\\d+)|space-uid-(\\d+)" options:NSRegularExpressionCaseInsensitive error:nil];
		[uidexp enumerateMatchesInString:path options:0 range:NSMakeRange(0, path.length) usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
			NSString *uidstr = [path substringWithRange:result.range];
			
			if ([uidstr hasPrefix:@"home.php?mod=space&uid="]) {
				uid = [[uidstr substringWithRange:NSMakeRange(23, uidstr.length-23)] integerValue];
			} else if ([uidstr hasPrefix:@"space-uid-"]) {
				uid = [[uidstr substringWithRange:NSMakeRange(10, uidstr.length-10)] integerValue];
			}
			//		NSInteger uid = [[path substringWithRange:[result rangeAtIndex:1]] integerValue];
			
			found = YES;
			*stop = YES;
		}];
        if (uid > 0) {
            return @{@"type": @"user",@"uid":[NSString stringWithFormat:@"%d",uid]};
        }
	}
    if (!found) {
        if (queryDict && [queryDict respondsToSelector:@selector(objectForKey:)]) {
			if ([@"redirect" isEqualToString:[queryDict objectForKey:@"mod"]]) {
				NSString *tid = [queryDict objectForKey:@"ptid"];
				//NSString *pid = [queryDict objectForKey:@"pid"];

				if ([tid integerValue] > 0) {
					return @{@"type":@"post",@"tid":tid};
				}
			}
		}
    }
	return nil;
}

+ (void)uploadAvator:(UIImageView *)imageView uid:(id)uid type:(NSString *)type
{
    NSURL *url = [NSURL URLWithString:[Common formatAvatarWithUID:uid type:type]];
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
    request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    [imageView setImageWithURLRequest:request
                                placeholderImage:nil
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
//                                             dispatch_async(dispatch_get_main_queue(), ^{
//                                                 imageView.image = image;
//                                             });
                                         } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
//                                             NSLog(@"failed loading image: %@", error);
                                         }];
}

+ (void)writeToFile:(NSString *)key data:(NSArray *)data{
    NSString *path = [NSString stringWithFormat:@"%@/Documents/%@.plist",NSHomeDirectory(),key];
    if ([data writeToFile:path atomically:YES]) {
//        NSLog(@"写入数据成功：--%@",path);
    }else{
//        NSLog(@"写入数据失败：————%@",path);
    }
    
    
}
+(NSArray *)getDataFromFile:(NSString *)key{
    NSString *path = [NSString stringWithFormat:@"%@/Documents/%@.plist",NSHomeDirectory(),key];
    
    NSArray *arr = [NSArray arrayWithContentsOfFile:path];
    
    return arr;
    
    
}
+ (BOOL)isDown:(NSString *)key{
    NSString *path = [NSString stringWithFormat:@"%@/Documents/%@.plist",NSHomeDirectory(),key];
    
    NSFileManager *fm = [NSFileManager defaultManager];
    if ([fm fileExistsAtPath:path]) {
        NSDictionary *attrDict = [fm fileAttributesAtPath:path traverseLink:YES];
        NSDate *date = [attrDict fileCreationDate];
//        NSLog(@"date--%@",date);
        NSDate *now = [NSDate date];
        
        NSTimeInterval time=[now timeIntervalSinceDate:date];
        
        if (time > 24 * 60 * 60 * 7) {//如果大于两天
            return YES;
        }else{
            
            return NO;
        }
    }else{
        return YES;
    }
}

@end
