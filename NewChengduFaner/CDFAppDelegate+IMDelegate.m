//
//  CDFAppDelegate+IMDelegate.m
//  yanyu
//
//  Created by caiyee on 15/5/6.
//  Copyright (c) 2015年 dahe.cn. All rights reserved.
//

#import "CDFAppDelegate+IMDelegate.h"

@interface CDFAppDelegate ()

@end

@implementation CDFAppDelegate (IMDelegate)
static const CGFloat kDefaultPlaySoundInterval = 5.0;

- (void)handleMessageChange:(NSNotification *)note
{
    NSInteger all = [[[note userInfo] objectForKey:@"allcount"] integerValue];
    NSString *badgeValue = all > 0  ? [NSString stringWithFormat:@"%d", all] : nil;
    //NSLog(@"message badgeValue : %@",badgeValue);
    [[[(UITabBarController *)(self.window.rootViewController) viewControllers][3] tabBarItem] setBadgeValue:badgeValue];
}
-(void)registerNotifications
{
    [self unregisterNotifications];
    [[EaseMob sharedInstance].chatManager addDelegate:self delegateQueue:nil];
}

-(void)unregisterNotifications
{
    [[EaseMob sharedInstance].chatManager removeDelegate:self];
}

- (void)didUnreadMessagesCountChanged {
    int indexICareAbout = 3;
    NSArray *conversations = [[[EaseMob sharedInstance] chatManager] conversations];
    NSInteger unreadCount = 0;
    for (EMConversation *conversation in conversations) {
        unreadCount += conversation.unreadMessagesCount;
    }
    //    unreadCount += [CDFMe shareInstance].newpm;
    unreadCount = unreadCount + [CDFMe shareInstance].newLikeCount + [CDFMe shareInstance].newTingZhongCount + [CDFMe shareInstance].tiezeCount ;
    NSString *badgeValue = [NSString stringWithFormat:@"%ld", (long)unreadCount];
    if (unreadCount > 0) {
        [[[[(UITabBarController *)(self.window.rootViewController) viewControllers] objectAtIndex: indexICareAbout] tabBarItem] setBadgeValue:badgeValue];
    } else {
        [[[[(UITabBarController *)(self.window.rootViewController) viewControllers] objectAtIndex: indexICareAbout] tabBarItem] setBadgeValue:nil];
    }
    UIApplication *application = [UIApplication sharedApplication];
    [application setApplicationIconBadgeNumber:unreadCount];
}

- (void)didReceiveMessage:(EMMessage *)message {
    
#if !TARGET_IPHONE_SIMULATOR
    
    BOOL isAppActivity = [[UIApplication sharedApplication] applicationState] == UIApplicationStateActive;
    if (!isAppActivity) {
        [self showNotificationWithMessage:message];
    }else {
        
        [self playSoundAndVibration];
    }
#endif
    
}

- (void)showNotificationWithMessage:(EMMessage *)message
{
    EMPushNotificationOptions *options = [[EaseMob sharedInstance].chatManager pushNotificationOptions];
    //发送本地推送
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.fireDate = [NSDate date]; //触发通知的时间
    
    if (options.displayStyle == ePushNotificationDisplayStyle_messageSummary) {
        id<IEMMessageBody> messageBody = [message.messageBodies firstObject];
        NSString *messageStr = nil;
        switch (messageBody.messageBodyType) {
            case eMessageBodyType_Text:
            {
                messageStr = ((EMTextMessageBody *)messageBody).text;
            }
                break;
            case eMessageBodyType_Image:
            {
                messageStr = NSLocalizedString(@"message.image", @"Image");
            }
                break;
            case eMessageBodyType_Location:
            {
                messageStr = NSLocalizedString(@"message.location", @"Location");
            }
                break;
            case eMessageBodyType_Voice:
            {
                messageStr = NSLocalizedString(@"message.voice", @"Voice");
            }
                break;
            case eMessageBodyType_Video:{
                messageStr = NSLocalizedString(@"message.vidio", @"Vidio");
            }
                break;
            default:
                break;
        }
        
        NSString *title = message.from;
        NSString *name;
        NSString *oneUser = message.ext[@"from_nickname"];
        NSString *twoUser = message.ext[@"to_nickname"];
        if([[CDFMe shareInstance].userName isEqualToString:oneUser]) {
            name = twoUser;
        } else {
            name = oneUser;
        }
        if (message.isGroup) {
            NSArray *groupArray = [[EaseMob sharedInstance].chatManager groupList];
            for (EMGroup *group in groupArray) {
                if ([group.groupId isEqualToString:message.conversationChatter]) {
                    title = [NSString stringWithFormat:@"%@(%@)", message.groupSenderName, group.groupSubject];
                    break;
                }
            }
        }
        
        notification.alertBody = [NSString stringWithFormat:@"%@:%@", name, messageStr];
    }
    else{
        notification.alertBody = NSLocalizedString(@"receiveMessage", @"you have a new message");
    }
    
#warning 去掉注释会显示[本地]开头, 方便在开发中区分是否为本地推送
    //notification.alertBody = [[NSString alloc] initWithFormat:@"[本地]%@", notification.alertBody];
    
    notification.alertAction = NSLocalizedString(@"open", @"Open");
    notification.timeZone = [NSTimeZone defaultTimeZone];
    notification.soundName = UILocalNotificationDefaultSoundName;
    //发送通知
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    //    UIApplication *application = [UIApplication sharedApplication];
    //    application.applicationIconBadgeNumber += 1;
}

- (void)playSoundAndVibration{
    
//    if (!self.lastPlaySoundDate) {
//        self.lastPlaySoundDate= [NSDate date];
//    }
    NSTimeInterval timeInterval = [[NSDate date]
                                   timeIntervalSinceDate:self.lastPlaySoundDate];
    if (timeInterval < kDefaultPlaySoundInterval) {
        //如果距离上次响铃和震动时间太短, 则跳过响铃
        NSLog(@"skip ringing & vibration %@, %@", [NSDate date], self.lastPlaySoundDate);
        return;
    }
    
    //保存最后一次响铃时间
    self.lastPlaySoundDate = [NSDate date];
    
    // 收到消息时，播放音频
    if ([CDFMe shareInstance].isIMSound) {
        [[EaseMob sharedInstance].deviceManager asyncPlayNewMessageSound];
    }
    // 收到消息时，震动
    if ([CDFMe shareInstance].isIMShake) {
        [[EaseMob sharedInstance].deviceManager asyncPlayVibration];
    }
    
}


@end
