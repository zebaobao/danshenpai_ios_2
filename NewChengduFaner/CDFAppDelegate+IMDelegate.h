//
//  CDFAppDelegate+IMDelegate.h
//  yanyu
//
//  Created by caiyee on 15/5/6.
//  Copyright (c) 2015年 dahe.cn. All rights reserved.
//

#import "CDFAppDelegate.h"

@interface CDFAppDelegate (IMDelegate)
-(void)registerNotifications;
@end
