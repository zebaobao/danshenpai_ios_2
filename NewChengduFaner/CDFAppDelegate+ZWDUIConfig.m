//
//  CDFAppDelegate+ZWDUIConfig.m
//  yanyu
//
//  Created by Case on 15/4/8.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "CDFAppDelegate+ZWDUIConfig.h"
#import "CDFIndexViewController.h"
#import "ChatListViewController.h"
#import "CDFMeViewController.h"
#import "JuJCViewController.h"

#define TITLEEDG -4



@implementation CDFAppDelegate (ZWDUIConfig)



- (void)UIConfig{
    /*
    UITabBarController *tabbar = (UITabBarController *)self.window.rootViewController;
    tabbar.delegate = self;
    
    [[MMExampleDrawerVisualStateManager sharedManager] setLeftDrawerAnimationType:MMDrawerAnimationTypeSwingingDoor];
    _drawerController = [[MMDrawerController alloc] initWithNibName:nil bundle:nil];
    _drawerController.centerViewController = self.window.rootViewController;
    [_drawerController
     setDrawerVisualStateBlock:^(MMDrawerController *drawerController, MMDrawerSide drawerSide, CGFloat percentVisible) {
         MMDrawerControllerDrawerVisualStateBlock block;
         block = [[MMExampleDrawerVisualStateManager sharedManager]
                  drawerVisualStateBlockForDrawerSide:drawerSide];
         if(block){
             block(drawerController, drawerSide, percentVisible);
         }
     }];
    [_drawerController setMaximumLeftDrawerWidth:240.0];
    
#warning rootViewController
    self.window.rootViewController = _drawerController;
     */
     UIFont* font = [UIFont fontWithName:@"Arial-ItalicMT" size:13.0];
    NSDictionary *attributesDictHigh = @{UITextAttributeTextColor: [Common colorWithHex:@"#FF3300" alpha:1],
                                         NSFontAttributeName:font};
    NSDictionary *attributesDictNormal = @{UITextAttributeTextColor: [UIColor colorWithWhite:102.0f/255.0f alpha:1],
                                           NSFontAttributeName:font};
    
    //_replyButton.imageEdgeInsets = UIEdgeInsetsMake(0, -7, 0, 0);
    //_replyButton.titleEdgeInsets = UIEdgeInsetsMake(0, 7, 0, 0);
    
    
    self.window.rootViewController = nil;
    CDFTabBarController *tabBar = [[CDFTabBarController alloc]init];
    //首页
    CDFNavigationController *indexNavi = [[CDFNavigationController alloc]initWithRootViewController:[IndexViewController new]];
    UITabBarItem *indexItem = [[UITabBarItem alloc]initWithTitle:@"首页" image:[[UIImage imageNamed:@"首页未选"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@"首页"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [indexItem setTitleTextAttributes:attributesDictHigh forState:UIControlStateHighlighted];
    [indexItem setTitleTextAttributes:attributesDictNormal forState:UIControlStateNormal];
    indexNavi.tabBarItem = indexItem;
    [indexItem setTitlePositionAdjustment:UIOffsetMake(0, TITLEEDG)];
    [indexItem setImageInsets:UIEdgeInsetsMake(-3, 0, 3, 0)];
    //社区
    CDFNavigationController *commNavi = [[CDFNavigationController alloc]initWithRootViewController:[communityViewController new]];
    UITabBarItem *commItem = [[UITabBarItem alloc]initWithTitle:@"圈子" image:[[UIImage imageNamed:@"社区未选"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@"社区"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [commItem setTitleTextAttributes:attributesDictHigh forState:UIControlStateHighlighted];
    [commItem setTitleTextAttributes:attributesDictNormal forState:UIControlStateNormal];
    commNavi.tabBarItem = commItem;
    [commItem setTitlePositionAdjustment:UIOffsetMake(0, TITLEEDG)];
    [commItem setImageInsets:UIEdgeInsetsMake(-3, 0, 3, 0)];
    //聚精彩
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
//    CDFIndexViewController *JuJCView = [storyboard instantiateViewControllerWithIdentifier:@"CDFIndexViewController"];
    CDFNavigationController *JuJCNavi = [[CDFNavigationController alloc]initWithRootViewController:[[JuJCViewController alloc]init]];
    UITabBarItem *JuJCItem = [[UITabBarItem alloc]initWithTitle:@"聚精彩" image:[[UIImage imageNamed:@"聚精彩"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@"聚精彩选中"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    JuJCItem.tag = 199;
    [JuJCItem setTitleTextAttributes:attributesDictHigh forState:UIControlStateHighlighted];
    [JuJCItem setTitleTextAttributes:attributesDictNormal forState:UIControlStateNormal];
    JuJCNavi.tabBarItem = JuJCItem;
    [JuJCItem setTitlePositionAdjustment:UIOffsetMake(0, TITLEEDG)];
    [JuJCItem setImageInsets:UIEdgeInsetsMake(-10, 0, 10, 0)];
    //消息ChatListViewController
    ChatListViewController *ChatListViewController = [storyboard instantiateViewControllerWithIdentifier:@"ChatListViewController"];
    CDFNavigationController *ChatListNavi = [[CDFNavigationController alloc]initWithRootViewController:ChatListViewController];
    UITabBarItem *ChatListItem = [[UITabBarItem alloc]initWithTitle:@"消息" image:[[UIImage imageNamed:@"信息"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@"消息选中"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [ChatListItem setTitleTextAttributes:attributesDictHigh forState:UIControlStateHighlighted];
    [ChatListItem setTitleTextAttributes:attributesDictNormal forState:UIControlStateNormal];
    ChatListNavi.tabBarItem = ChatListItem;
    [ChatListItem setTitlePositionAdjustment:UIOffsetMake(0, TITLEEDG)];
    [ChatListItem setImageInsets:UIEdgeInsetsMake(-3, 0, 3, 0)];
    //我的userViewController
    CDFMeViewController *MeViewController = [storyboard instantiateViewControllerWithIdentifier:@"userViewController"];
    CDFNavigationController *MeNavi = [[CDFNavigationController alloc]initWithRootViewController:MeViewController];
    UITabBarItem *MeItem = [[UITabBarItem alloc]initWithTitle:@"我的" image:[[UIImage imageNamed:@"我的"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@"我的选中"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [MeItem setTitleTextAttributes:attributesDictHigh forState:UIControlStateHighlighted];
    [MeItem setTitleTextAttributes:attributesDictNormal forState:UIControlStateNormal];
    MeNavi.tabBarItem = MeItem;
    [MeItem setTitlePositionAdjustment:UIOffsetMake(0, TITLEEDG)];
    [MeItem setImageInsets:UIEdgeInsetsMake(-3, 0, 3, 0)];
    tabBar.viewControllers = @[indexNavi,commNavi,JuJCNavi,ChatListNavi,MeNavi];
    self.window.rootViewController = tabBar;
    
    
}

@end
