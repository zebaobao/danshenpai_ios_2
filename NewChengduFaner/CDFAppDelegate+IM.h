//
//  CDFAppDelegate+IM.h
//  yanyu
//
//  Created by caiyee on 15/5/4.
//  Copyright (c) 2015年 dahe.cn. All rights reserved.
//

#import "CDFAppDelegate.h"

@interface CDFAppDelegate (IM)
- (void)IMapplication:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
- (void)IMapplicationWillTerminate:(UIApplication *)application;
- (void)IMapplicationDidBecomeActive:(UIApplication *)application;
-(void)IMapplicationWillEnterForeground:(UIApplication *)application;
- (void)IMapplicationDidEnterBackground:(UIApplication *)application;
- (void)IMapplication:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error;
- (void)IMapplication:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken;
@end
