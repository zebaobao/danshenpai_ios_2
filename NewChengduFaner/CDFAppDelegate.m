//
//  CDFAppDelegate.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-10-24.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFAppDelegate.h"
#import "DCIntrospect.h"
//#import "APService.h"
#import "UMSocial.h"
#import "UMSocialQQHandler.h"
#import "CDFAppDelegate+IMDelegate.h"
#import "UMSocialSinaHandler.h"
#import "ChatViewController.h"
#import "ChatListViewController.h"
#import "CDFMessageViewController.h"
#import "SVProgressHUD.h"
#import "CDFAppDelegate+ZWDUIConfig.h"
#import "CDFAppDelegate+IM.h"

@interface CDFAppDelegate()
@property (strong, nonatomic) UIImageView *startImageView;

@end

@implementation CDFAppDelegate
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= _IPHONE80_

//注册UserNotification成功的回调
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    //用户已经允许接收以下类型的推送
    // UIUserNotificationType allowedTypes = [notificationSettings types];
}

//按钮点击事件回调
- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void (^)())completionHandler {
    if([identifier isEqualToString:@"ACCEPT_IDENTIFIER"]){
    }
    completionHandler();
}

#endif
- (void)registerPushForIOS8 {
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= _IPHONE80_
    
    //Types
    UIUserNotificationType types = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    
    //Actions
    UIMutableUserNotificationAction *acceptAction = [[UIMutableUserNotificationAction alloc] init];
    
    acceptAction.identifier = @"ACCEPT_IDENTIFIER";
    acceptAction.title = @"Accept";
    
    acceptAction.activationMode = UIUserNotificationActivationModeForeground;
    acceptAction.destructive = NO;
    acceptAction.authenticationRequired = NO;
    
    //Categories
    UIMutableUserNotificationCategory *inviteCategory = [[UIMutableUserNotificationCategory alloc] init];
    
    inviteCategory.identifier = @"INVITE_CATEGORY";
    
    [inviteCategory setActions:@[acceptAction] forContext:UIUserNotificationActionContextDefault];
    
    [inviteCategory setActions:@[acceptAction] forContext:UIUserNotificationActionContextMinimal];
    
    NSSet *categories = [NSSet setWithObjects:inviteCategory, nil];
    
    UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:categories];
    
    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
#endif
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 102) {
        if (buttonIndex == 0) {
            [self notificationAction];
        }
    }
}

- (void)registerPush{
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
}
#pragma mark - 信鸽注册方法,切换用户----
- (void)XGRegister{
    //注册XGPush服务，注册后才能收到推送
    //iOS8注册push方法
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= _IPHONE80_
    
    //    [self registerPushForIOS8]; //具体实现参考Demo
    
#else
    //iOS8之前注册push方法
    //    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
#endif
    //    [self registerPush];
    
    ///////////
//    NSLog(@"%@",[Common unarchiveForKey:kOnlineInfo][@"data"][@"member_uid"]);
    [XGPush setAccount:[Common unarchiveForKey:kOnlineInfo][@"data"][@"member_uid"]];
    
    void (^successCallback)(void) = ^(void){
        //如果变成需要注册状态
        if(![XGPush isUnRegisterStatus])
        {
            //iOS8注册push方法
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= _IPHONE80_
            
            float sysVer = [[[UIDevice currentDevice] systemVersion] floatValue];
            if(sysVer < 8){
                [self registerPush];
            }
            else{
                [self registerPushForIOS8];
            }
#else
            //iOS8之前注册push方法
            //注册Push服务，注册后才能收到推送
            [self registerPush];
#endif
        }
    };
    
    //推送反馈回调版本示例
    void (^successBlock)(void) = ^(void){
        //成功之后的处理
//        NSLog(@"[XGPush]handleLaunching's successBlock");
    };
    
    void (^errorBlock)(void) = ^(void){
        //失败之后的处理
//        NSLog(@"[XGPush]handleLaunching's errorBlock");
    };
    
    [XGPush initForReregister:successCallback];
    
}
- (void)xgUnregister{
    [XGPush unRegisterDevice];
}
#pragma mark - 信鸽注册方法,切换用户----

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [CDFMe shareInstance];
    [self IMapplication:application didFinishLaunchingWithOptions:launchOptions];
    
    [XGPush startApp:2200095666 appKey:@"I47G54PFT3XE"];
    //    [XGPush startApp:2200095869 appKey:@"IU28U6UPT24F"];
    
#pragma mark - 注册登录成功通知
    //注册登录成功通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(XGRegister) name:kLOGIN_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(xgUnregister) name:kLOGOUT_NOTIFICATION object:nil];
    
    [self XGRegister];
    
#if TARGET_IPHONE_SIMULATOR
    [[DCIntrospect sharedIntrospector] start];
    
    //iOS8 注册APNS
    if ([application respondsToSelector:@selector(registerForRemoteNotifications)]) {
        [application registerForRemoteNotifications];
        UIUserNotificationType notificationTypes = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:notificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
    }else{
        UIRemoteNotificationType notificationTypes = UIRemoteNotificationTypeBadge |
        UIRemoteNotificationTypeSound |
        UIRemoteNotificationTypeAlert;
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:notificationTypes];
    }
#endif
    
    //        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handlePostResult:) name:kPOST_SUCCESS object:nil];
    //        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handlePostResult:) name:kPOST_FAIL object:nil];
#pragma mark - 转至uiconfig
    [self UIConfig];

    //    ChatViewController *chatVC = [[ChatViewController alloc] initWithChatter:@"bot" isGroup:NO];
    //    self.window.rootViewController = chatVC;
    
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    
    //JPush
    //    [defaultCenter addObserver:self selector:@selector(networkDidSetup:) name:kJPFNetworkDidSetupNotification object:nil];
    //    [defaultCenter addObserver:self selector:@selector(networkDidClose:) name:kJPFNetworkDidCloseNotification object:nil];
    //    [defaultCenter addObserver:self selector:@selector(networkDidRegister:) name:kJPFNetworkDidRegisterNotification object:nil];
    //    [defaultCenter addObserver:self selector:@selector(networkDidLogin:) name:kJPFNetworkDidLoginNotification object:nil];
    //    [defaultCenter addObserver:self selector:@selector(networkDidReceiveMessage:) name:kJPFNetworkDidReceiveMessageNotification object:nil];
    
    
    
    //角标清0
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    //清除所有通知(包含本地通知)
    //[[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    
    ///////////
    
    
    // 推送设置
    //    [APService registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
    //                                                   UIRemoteNotificationTypeSound |
    //                                                   UIRemoteNotificationTypeAlert)
    //                                       categories:nil];
    //
    //    [APService setupWithOption:launchOptions];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    // 分享设置
    [UMSocialData setAppKey:kUmeng_Appkey];
    //打开新浪微博的SSO开关
    //    [UMSocialConfig setSupportSinaSSO:YES];
    [UMSocialSinaHandler openSSOWithRedirectURL:@"http://sns.whalecloud.com/sina2/callback"];
    [UMSocialQQHandler setQQWithAppId:kQQ_Appkey appKey:kUmeng_Appkey url:nil];
    [UMSocialData defaultData].extConfig.wxMessageType = UMSocialWXMessageTypeWeb;
    // 统计设置
    [MobClick startWithAppkey:kUmeng_Appkey];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    NSArray* scheduledNotifications = [NSArray arrayWithArray:application.scheduledLocalNotifications];
    application.scheduledLocalNotifications = scheduledNotifications;
    
    //IM推送
//    EMPushNotificationOptions *options = [[EaseMob sharedInstance].chatManager pushNotificationOptions];
//    options.displayStyle = ePushNotificationDisplayStyle_messageSummary;
//    [[EaseMob sharedInstance].chatManager asyncUpdatePushOptions:options completion:^(EMPushNotificationOptions *options, EMError *error) {
//        if (!error) {
////            NSLog(@"设置成功");
//        }
//    } onQueue:nil];
    [XGPush handleLaunching:launchOptions];
    

#warning 杀死状态下推送测试
    NSDictionary* remoteNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    extras = remoteNotification;

    if (remoteNotification) {

        if ([remoteNotification[@"type"] integerValue] == 2) {
            //type = 2, 新听众
            [self notificationActionIntoNotePageWithType:2];
        } else if ([remoteNotification[@"type"] integerValue] == 3) {
            //type = 3, 喜欢
            [self notificationActionIntoNotePageWithType:3];
        } else if ([remoteNotification[@"type"] integerValue]== 1) {
            //type = 1 : 回帖
            [self notificationActionIntoNotePageWithType:1];
        } else if ([remoteNotification[@"type"] integerValue] == 4) {
#warning 管理员注意!!! 管理员推送帖子消息的时候, type设置为4!!!
            //type = 4 : 管理员推送
            [self notificationAction];
        } else {
            //IM
            [self notificationActionIntoNotePageWithType:0];
        }
    }
    [self registerNotifications];

    return YES;

}

#pragma mark -

- (void)networkDidSetup:(NSNotification *)notification {
//    NSLog(@"已连接");
}

- (void)networkDidClose:(NSNotification *)notification {
//    NSLog(@"未连接。。。");
}

- (void)networkDidRegister:(NSNotification *)notification {
//    NSLog(@"已注册");
}

- (void)networkDidLogin:(NSNotification *)notification {
//    NSLog(@"已登录");
}

static NSDictionary *extras;

- (void)notificationActionIntoNotePageWithType:(NSInteger)type {
    UINavigationController *nav;
//    if (self.window.rootViewController.modalViewController) {
////        NSLog(@"modalViewController");
//        nav = (UINavigationController *)self.window.rootViewController.modalViewController;
//    } else if([self.window.rootViewController isKindOfClass:[MMDrawerController class]]) {
////        NSLog(@"rootViewController");
//        UITabBarController *tabbar = (UITabBarController *)_drawerController.centerViewController;
//        if ([tabbar.selectedViewController isKindOfClass:[UINavigationController class]]) {
//            nav = (UINavigationController *)tabbar.selectedViewController;
//        } else {
////            NSLog(@"%@",tabbar.selectedViewController);
//        }
//    } else {
////        NSLog(@"%@",self.window.rootViewController);
//    }
//    nav = (UINavigationController *)self.window.rootViewController.parentViewController;
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
//    UITabBarController *tabbar = nav.tabBarController;
   UITabBarController *tabbar = (UITabBarController *)self.window.rootViewController;
    [tabbar setSelectedIndex:3];
    UINavigationController *thirdVC = (UINavigationController *)([tabbar viewControllers][3]);
    
    CDFMessageViewController *vc = [s instantiateViewControllerWithIdentifier:@"CDFMessageViewController"];
    if (type == 2) {
        //新听众
        vc.type = newListen;
        vc.navigationItem.title = @"我的听众";
        [thirdVC pushViewController:vc animated:NO];
    } else if (type == 3) {
        //喜欢
        vc.type = like;
        vc.navigationItem.title = @"赞";
        [thirdVC pushViewController:vc animated:NO];
    } else if (type == 1) {
        //帖子回复
        vc.type = reply;
        vc.navigationItem.title = @"帖子回复";
        [thirdVC pushViewController:vc animated:NO];
    } else if (type == 0) {
        //IM
//        UITabBarController *tabbar1 = (UITabBarController *)_drawerController.centerViewController;
        [tabbar setSelectedIndex:3];
    }
}

- (void)notificationAction
{
    id tid = extras[@"tid"];
    id pid = extras[@"pid"];
    id type = extras[@"type"];
    
    UITabBarController *tab = (UITabBarController *)self.window.rootViewController;
    UINavigationController *navi = [tab selectedViewController];
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CDFDisplayViewController *dis = [s instantiateViewControllerWithIdentifier:@"DisplayViewContoller"];
    dis.tid = tid;
    [navi pushViewController:dis animated:YES];
    
    
    return;
    
    UINavigationController *nav;
//    if (self.window.rootViewController.presentedViewController) {
////        NSLog(@"modalViewController");
//        nav = (UINavigationController *)self.window.rootViewController.presentedViewController;
//    } else if([self.window.rootViewController isKindOfClass:[MMDrawerController class]]) {
////        NSLog(@"rootViewController");
//        UITabBarController *tabbar = (UITabBarController *)_drawerController.centerViewController;
//        if ([tabbar.selectedViewController isKindOfClass:[UINavigationController class]]) {
//            nav = (UINavigationController *)tabbar.selectedViewController;
//        } else {
////            NSLog(@"%@",tabbar.selectedViewController);
//        }
//    } else {
////        NSLog(@"%@",self.window.rootViewController);
//    }
    nav = (UINavigationController *)self.window.rootViewController.presentingViewController;
    
//    NSLog(@"nav = %@ ; tid = %@ ; pid = %@", nav, tid, pid);
    if (nav && [tid integerValue] > 0) {
        UIViewController *displayViewController = [nav.storyboard instantiateViewControllerWithIdentifier:@"DisplayViewContoller"];
        [displayViewController setValue:tid forKey:@"tid"];
        if ([pid integerValue] > 0) {
            [displayViewController setValue:pid forKey:@"pid"];
//            NSLog(@"piddd:%@", pid);
        }
        [nav pushViewController:displayViewController animated:YES];
    }
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
//<<<<<<< HEAD
//    NSLog(@"userInfo:%@", userInfo);
//=======
//    NSLog(@"userInfo:%@", userInfo);
    //    //IM推送
    //    EMPushNotificationOptions *options = [[EaseMob sharedInstance].chatManager pushNotificationOptions];
    //    options.displayStyle = ePushNotificationDisplayStyle_messageSummary;
    //    [[EaseMob sharedInstance].chatManager asyncUpdatePushOptions:options completion:^(EMPushNotificationOptions *options, EMError *error) {
    //        if (!error) {
    //            NSLog(@"设置成功");
    //        }
    //    } onQueue:nil];
//>>>>>>> f30344495724dc9ff497527d304299a5a0ad6b1b
    
//    NSLog(@"didReceive Remote Notification : %@ %d", userInfo, [UIApplication sharedApplication].applicationState);
    // Required
    
    [XGPush handleReceiveNotification:userInfo];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    extras = userInfo;
    
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateInactive) {
        
//        NSLog(@"%@", userInfo);
        
        //后台状态下
        if ([userInfo[@"type"] integerValue] == 2) {
            //type = 2, 新听众
            [self notificationActionIntoNotePageWithType:2];
        } else if ([userInfo[@"type"] integerValue] == 3) {
            //type = 3, 喜欢
            [self notificationActionIntoNotePageWithType:3];
        } else if ([userInfo[@"type"] integerValue] == 1) {
            //type = 1 : 回帖
            [self notificationActionIntoNotePageWithType:1];
        } else if ([userInfo[@"type"] integerValue] == 4) {
#warning 管理员注意!!! 管理员推送帖子消息的时候, type设置为4!!!
            //type = 4 : 管理员推送
            [self notificationAction];
        }
    } else {
        //前台状态下
        if ([userInfo[@"type"] integerValue] == 2) {
            //type = 2, 新听众
            if ([CDFMe shareInstance].isIMSound) {
                [[EaseMob sharedInstance].deviceManager asyncPlayNewMessageSound];
            }
            [CDFMe shareInstance].newTingZhongCount += 1;
            UITabBarController *tabbar = (UITabBarController *)self.window.rootViewController;
            NSArray *conversations = [[[EaseMob sharedInstance] chatManager] conversations];
            NSInteger unreadCount = 0;
            for (EMConversation *conversation in conversations) {
                unreadCount += conversation.unreadMessagesCount;
            }
            [[[tabbar viewControllers][3] tabBarItem] setBadgeValue:[NSString stringWithFormat:@"%d", [CDFMe shareInstance].tiezeCount + unreadCount + [CDFMe shareInstance].newLikeCount + [CDFMe shareInstance].newTingZhongCount]];
            [[NSNotificationCenter defaultCenter] postNotificationName:kNewLessonUser object:nil];
            
        } else if ([userInfo[@"type"] integerValue] == 3) {
            //type = 3, 喜欢
            if ([CDFMe shareInstance].isIMSound) {
                [[EaseMob sharedInstance].deviceManager asyncPlayNewMessageSound];
            }
            [CDFMe shareInstance].newLikeCount += 1;
            UITabBarController *tabbar = (UITabBarController *)self.window.rootViewController;
            NSArray *conversations = [[[EaseMob sharedInstance] chatManager] conversations];
            NSInteger unreadCount = 0;
            for (EMConversation *conversation in conversations) {
                unreadCount += conversation.unreadMessagesCount;
            }
            [[[tabbar viewControllers][3] tabBarItem] setBadgeValue:[NSString stringWithFormat:@"%d", [CDFMe shareInstance].tiezeCount + unreadCount + [CDFMe shareInstance].newLikeCount + [CDFMe shareInstance].newTingZhongCount]];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kNewLike object:nil];
            
        } else if ([userInfo[@"type"] integerValue] == 4) {
            //type == 4, 为后台管理员推帖子
//            NSLog(@"userInfo:%@", userInfo);
            //这种情况为后台人员推帖子, 没有pid字段
            [self showPushInAppWithDic:userInfo];
        } else if ([userInfo[@"type"] integerValue] == 1) {
            //前台状态下收到推送, 显示小红点+1
            // 收到消息时，播放音频 借用的环信的播放声音
            if ([CDFMe shareInstance].isIMSound) {
                [[EaseMob sharedInstance].deviceManager asyncPlayNewMessageSound];
            }
            [CDFMe shareInstance].tiezeCount += 1;
            UITabBarController *tabbar = (UITabBarController *)self.window.rootViewController;
            NSArray *conversations = [[[EaseMob sharedInstance] chatManager] conversations];
            NSInteger unreadCount = 0;
            for (EMConversation *conversation in conversations) {
                unreadCount += conversation.unreadMessagesCount;
            }
            [[[tabbar viewControllers][3] tabBarItem] setBadgeValue:[NSString stringWithFormat:@"%d", [CDFMe shareInstance].tiezeCount + unreadCount + [CDFMe shareInstance].newLikeCount + [CDFMe shareInstance].newTingZhongCount]];
            [[NSNotificationCenter defaultCenter] postNotificationName:kTieziCount_Change object:nil];
        }
    }
}

- (void)showPushInAppWithDic:(NSDictionary *)dic
{
    //index为1时,代表t为空
    if (!dic[@"tid"]) {
        UIAlertView *alertView1 = [[UIAlertView alloc] initWithTitle:@"消息提醒" message:dic[@"aps"][@"alert"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView1 show];
    } else {
        //index为2, 代表t不为空
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"消息提醒" message:dic[@"aps"][@"alert"] delegate:self cancelButtonTitle:@"打开" otherButtonTitles:@"关闭", nil];
        alertView.tag = 102;
        [alertView show];
    }
    //    [self notificationAction];
//<<<<<<< HEAD
//=======
//    NSLog(@"-- hutest -- %s, %d", __func__, __LINE__);
    /*
     [BWStatusBarOverlay showWithMessage:extras[@"content"] animated:NO];
     [BWStatusBarOverlay setActionBlock:^{
     [BWStatusBarOverlay dismissAnimated:YES];
     [self notificationAction];
     }];
     */
    
//>>>>>>> f30344495724dc9ff497527d304299a5a0ad6b1b
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}
- (void)showPushInApp
{
    [BWStatusBarOverlay showWithMessage:extras[@"content"] animated:NO];
    
    [BWStatusBarOverlay setActionBlock:^{
        [BWStatusBarOverlay dismissAnimated:YES];
        [self notificationAction];
    }];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}
- (void)networkDidReceiveMessage:(NSNotification *)notification {
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    NSDictionary * userInfo = [notification userInfo];
    NSString *title = [userInfo valueForKey:@"title"];
    NSString *content = [userInfo valueForKey:@"content"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    extras = @{@"content": content, @"t" : userInfo[@"extras"][@"t"]};
    if (self.window.rootViewController.modalViewController && ![self.window.rootViewController.modalViewController isKindOfClass:[UINavigationController class]]) {
        return;
    }
}
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    //注册设备
//    NSString * deviceTokenStr = [XGPush registerDevice: deviceToken];
    [self IMapplication:application didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
    
    void (^successBlock)(void) = ^(void){
        //成功之后的处理
//        NSLog(@"[XGPush]register successBlock ,deviceToken: %@",deviceTokenStr);
    };
    
    void (^errorBlock)(void) = ^(void){
        //失败之后的处理
//        NSLog(@"[XGPush]register errorBlock");
    };
    //注册设备
//    [[XGSetting getInstance] setChannel:@"appstore"];
//    [[XGSetting getInstance] setGameServer:@"巨神峰"];
    [XGPush registerDevice:deviceToken successCallback:successBlock errorCallback:errorBlock];
    //如果不需要回调
    //[XGPush registerDevice:deviceToken];
    
    //打印获取的deviceToken的字符串
//    NSLog(@"deviceTokenStr is %@",deviceTokenStr);
}

- (void)application:(UIApplication *)app didReceiveLocalNotification:(UILocalNotification *)notification {
//    [[EaseMob sharedInstance] application:app didReceiveLocalNotification:notification];

    [self notificationActionIntoNotePageWithType:0];
    //IM推送
    EMPushNotificationOptions *options = [[EaseMob sharedInstance].chatManager pushNotificationOptions];
    options.displayStyle = ePushNotificationDisplayStyle_messageSummary;
    [[EaseMob sharedInstance].chatManager asyncUpdatePushOptions:options completion:^(EMPushNotificationOptions *options, EMError *error) {
        if (!error) {
            NSLog(@"设置成功");
        }
    } onQueue:nil];

}
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"error:%@", error);
    [self IMapplication:application didFailToRegisterForRemoteNotificationsWithError:error];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    [[EaseMob sharedInstance] applicationWillResignActive:application];
//    NSLog(@"-- hutest -- %s, %d", __func__, __LINE__);
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    
    [self IMapplicationDidEnterBackground:application];
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    NSArray* scheduledNotifications = [NSArray arrayWithArray:application.scheduledLocalNotifications];
    application.scheduledLocalNotifications = scheduledNotifications;
}
- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [self IMapplicationWillEnterForeground:application];
    
       //程序即将进入前台时检测与服务器的连接
    [self isLinking];
}
- (void)isLinking{
    //心跳检测
    if (![CDFMe shareInstance].isLogin) {
        return;
    }
    
    NSString *url = @"api/mobile/?version=3&module=heartbeat";
    [[HXHttpClient shareInstance]grabURL:url success:^(id responseObject) {
//        NSLog(@"%@",responseObject);
        if ([responseObject[@"Variables"][@"member_uid"] isEqualToString:@"0"]) {
//            NSLog(@"服务器连接失败");
            [SVProgressHUD showErrorWithStatus:@"您已掉线，请重新登录！"];
            [Common logout];
            
        }else{
            //正常连接
//            NSLog(@"服务器连接正常--%@",responseObject[@"Variables"][@"member_uid"]);
        }
    } fail:^(NSError *error) {
//        NSLog(@"心跳检测失败");
        [self isLinking];//
    }];
    
}
- (void)applicationDidBecomeActive:(UIApplication *)application
{
//<<<<<<< HEAD
    [self IMapplicationDidBecomeActive:application];
//=======
    //环信
//    [[EaseMob sharedInstance] applicationDidBecomeActive:application];
//    if ([CDFMe shareInstance].isLogin && ![[EaseMob sharedInstance].chatManager isLoggedIn]) {
//        NSString *mima = [CDFAppDelegate md5:[CDFMe shareInstance].uid];
////        NSLog(@"mima:%@", mima);
//        NSString *NewMima = [mima substringWithRange:NSMakeRange(0, 10)];
////        NSLog(@"NewMima:%@",NewMima);
//        [[EaseMob sharedInstance].chatManager asyncLoginWithUsername:[NSString stringWithFormat:@"dahe_%@",[CDFMe shareInstance].uid] password:NewMima completion:^(NSDictionary *loginInfo, EMError *error) {
//            if (!error && loginInfo) {
////                NSLog(@"登陆成功");
//            }else {
////                NSLog(@"登陆失败:%@", error);
////                NSLog(@"error.errorCode:%u", error.errorCode);
//            }
//        } onQueue:nil];
//    }
    
//    NSLog(@"-- hutest -- %s, %d", __func__, __LINE__);
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
//>>>>>>> f30344495724dc9ff497527d304299a5a0ad6b1b
    [UMSocialSnsService  applicationDidBecomeActive];
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    return [UMSocialSnsService handleOpenURL:url];
}
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [UMSocialSnsService handleOpenURL:url];
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
//    NSLog(@"-- hutest -- %s, %d", __func__, __LINE__);
    
    if ([Common systemVersion] >= 7.0) {
        if (tabBarController.selectedIndex == 1) {
            [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
        } else {
            //[UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
        }
    }
}

-(void)applicationWillTerminate:(UIApplication *)application {
    [self IMapplicationWillTerminate:application];
}

@end
