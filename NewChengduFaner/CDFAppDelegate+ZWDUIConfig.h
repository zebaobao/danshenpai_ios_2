//
//  CDFAppDelegate+ZWDUIConfig.h
//  yanyu
//
//  Created by Case on 15/4/8.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "CDFAppDelegate.h"
#import "MMDrawerController.h"
#import "MMExampleDrawerVisualStateManager.h"
#import "IndexViewController.h"
#import "communityViewController.h"
#import "CDFTabBarController.h"
#import "CDFNavigationController.h"

@interface CDFAppDelegate (ZWDUIConfig)<UITabBarControllerDelegate>
- (void)UIConfig;

@end
