//
//  CDFAppDelegate.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-10-24.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMDrawerController.h"
#import "MMExampleDrawerVisualStateManager.h"
#import "CDFDisplayViewController.h"

@interface CDFAppDelegate : UIResponder <UIApplicationDelegate, UIAlertViewDelegate, UITabBarControllerDelegate,IChatManagerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) MMDrawerController * drawerController;

@property (strong, nonatomic) NSDate *lastPlaySoundDate;

@end
