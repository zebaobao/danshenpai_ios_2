
//
//  main.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-10-24.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CDFAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CDFAppDelegate class]));
    }
}
