//
//  UITableView+AnimateReloadData.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-5.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "UITableView+AnimateReloadData.h"

@implementation UITableView (AnimateReloadData)

- (void)reloadData:(BOOL)animated animateFrom:(NSString *)drection
{
    [self reloadData];
    if (animated) {
        
        CATransition *animation = [CATransition animation];
        [animation setType:kCATransitionPush];
        [animation setSubtype:drection];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [animation setFillMode:kCAFillModeBoth];
        [animation setDuration:.3];
        [[self layer] addAnimation:animation forKey:@"UITableViewReloadDataAnimationKey"];
        
    }
}

@end
