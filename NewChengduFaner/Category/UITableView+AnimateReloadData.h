//
//  UITableView+AnimateReloadData.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-5.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (AnimateReloadData)

- (void)reloadData:(BOOL)animated animateFrom:(NSString *)drection;

@end
