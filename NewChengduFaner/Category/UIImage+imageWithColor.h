//
//  UIImage+imageWithColor.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-13.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (imageWithColor)
+ (UIImage *)imageWithColor:(UIColor *)color;
@end
