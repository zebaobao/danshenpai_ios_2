//
//  CDFPostManager.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-28.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CDFDraft.h"

@interface CDFPostManager : NSObject

@property (nonatomic, assign, readonly) BOOL sending;               //是否处于上传状态
@property (nonatomic, strong) NSMutableArray *queue;                //帖子列表
@property (nonatomic, assign, readonly) unsigned int uploadCursor;  //上传图片队列的指针

+ (CDFPostManager*)defaultManager;

- (void)appendDraft:(CDFDraft *)draft;

- (void)prepareSend;
@end
