//
//  HXHttpClient.m
//  YiBin
//
//  Created by Huaxi100 com on 12-12-18.
//  Copyright (c) 2012年 Huaxi100.com. All rights reserved.
//

#import "HXHttpClient.h"
#import "AFNetworking.h"

@implementation HXHttpClient {
    AFHTTPClient *httpClient;
}

static HXHttpClient *client = nil;

- (id)init {
    self = [super init];
    if (self) {
        httpClient = [[AFHTTPClient alloc]initWithBaseURL:[NSURL URLWithString:kBaseURL]];
        [httpClient setParameterEncoding:AFFormURLParameterEncoding];
        [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    }
    return self;
}

+ (HXHttpClient *)shareInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        client = [HXHttpClient new];
    });
    return client;
}

-(void)grabURL:(NSString *)url success:(void (^)(id))success fail:(void (^)(NSError *))fail {
    //设置cookie
    [Common appendDefaultCookiesForRequest];
    [httpClient getPath:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSLog(@"%@",operation.response.allHeaderFields);
        [Common saveDefaultCookies];
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        fail(error);
    }];
}

- (void)postURL:(NSString *)url postData:(NSDictionary *)data success:(void (^)(id))success fail:(void (^)(NSError *))fail {
//    NSLog(@"post data = %@",data);
    [Common appendDefaultCookiesForRequest];
    [httpClient postPath:url parameters:data success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [Common saveDefaultCookies];
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        fail(error);
    }];
}

- (void)grabBaseURL:(NSString *)baseUrl path:(NSString *)path success:(void (^)(id))success fail:(void (^)(NSError *))fail {
    AFHTTPClient *cl = [[AFHTTPClient alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    [cl registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [cl getPath:path parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [Common saveDefaultCookies];
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        fail(error);
    }];
}

- (void)deletePath:(NSString *)url postData:(NSDictionary *)data success:(void (^)(id))success fail:(void (^)(NSError *))fail {
//    NSLog(@"delete data = %@",data);
    [httpClient deletePath:url parameters:data success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [Common saveDefaultCookies];
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        fail(error);
    }];
}

- (void)putPath:(NSString *)url postData:(NSDictionary *)data success:(void (^)(id))success fail:(void (^)(NSError *))fail {
//    NSLog(@"put data = %@",data);
    [httpClient putPath:url parameters:data success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [Common saveDefaultCookies];
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        fail(error);
    }];
}

- (void)downloadRequest:(NSURLRequest*)request toPath:(NSString*)path success:(void (^)(id))success fail:(void (^)(NSError *))fail {
    AFHTTPRequestOperation *opt = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    opt.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
    [opt setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [Common saveDefaultCookies];
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        fail(error);
    }];
    [opt start];
}

- (void)setDefaultValue:(NSString *)value forHeader:(NSString *)header
{
    [httpClient setDefaultHeader:header value:value];
}

- (void)multipartFormRequestWithMethod:(NSString *)method
                                  path:(NSString *)path
                            parameters:(NSDictionary *)parameters
                                  data:(NSData *)data
                               success:(void (^)(id))success
                                  fail:(void (^)(NSError *))fail
                              progress:(void (^)(NSUInteger, long long, long long))progress
                              fileName:(NSString *)fileName
{
    [Common appendDefaultCookiesForRequest];
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:method path:path parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:data name:@"Filedata" fileName:fileName mimeType:@"image/jpeg"];
    }];
    request.timeoutInterval = 5000;
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
    [op setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        if (progress) {
            progress(bytesWritten, totalBytesWritten, totalBytesExpectedToWrite);
        }
    }];
    [op start];
}

@end
