//
//  CDFDraft.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-29.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFDraft.h"
#import "CDFLocationManager.h"

@interface CDFDraft()

/**
 *  通过key获取草稿的信息
 *
 *  @param key NSString
 *
 *  @return NSDictionary
 */
+ (NSDictionary *)draftWithKey:(NSString *)key;

@end

@implementation CDFDraft {
    NSDictionary *_dataSource;
}
@synthesize dataSource = _dataSource;

- (id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self) {
        [self setDataSource:dictionary];
    }
    return self;
}

- (id)initWithKey:(NSString *)key
{
    return [self initWithDictionary:[CDFDraft draftWithKey:key]];
}

- (id)draftT
{
    if (!_draftT) {
        _draftT = @"0";
    }
    return _draftT;
}

- (id)fid
{
    if (!_fid) {
        _fid = @"0";
    }
    return  _fid;
}

- (NSString *)subject
{
    if (!_subject) {
        _subject = @"";
    }
    return _subject;
}

- (void)setDataSource:(NSDictionary *)dataSource
{
    _dataSource = dataSource;
    for (NSString *key in [dataSource allKeys]) {
        if ([self respondsToSelector:NSSelectorFromString(key)]) {
            [self setValue:dataSource[key] forKey:key];
        }
    }
    NSMutableArray *attach = [NSMutableArray new];
    for (NSDictionary *dic in self.datasource) {
        if ([dic[@"type"] isEqualToString:@"Image"]) {
            [attach addObject:dic[@"content"]];
        }
    }
    self.attachements = [NSArray arrayWithArray:attach];
}

- (void)setDatasource:(NSArray *)datasource
{
    _datasource = datasource;
    NSMutableArray *attach = [NSMutableArray new];
    for (NSDictionary *dic in self.datasource) {
        if ([dic[@"type"] isEqualToString:@"Image"]) {
            [attach addObject:dic[@"content"]];
        }
    }
    self.attachements = [NSArray arrayWithArray:attach];
}

- (void)setSelectedBoard:(NSDictionary *)selectedBoard
{
    _selectedBoard = selectedBoard;
    self.fid = selectedBoard[@"fid"];
    self.draftT = selectedBoard[@"typeid"];
}

- (NSDictionary *)formatData
{
//    NSLog(@"type=%@,,fid=%@,,subject=%@,,message=%@,,timeline=%@,,key=%@,,data=%@,,att=%@,,content=%@,,statue=%d,,sele=%@",self.draftT,self.fid,self.subject,self.message,self.timeline,self.key,self.datasource,self.attachements,self.contents,self.status,self.selectedBoard);
    NSDictionary *data = @{
                           @"typeid"               :   self.draftT,
                           @"fid"                  :   self.fid,
                           @"subject"              :   self.subject,
                           @"message"              :   self.message,
                           @"timeline"             :   self.timeline,
                           @"key"                  :   self.key,
                           @"datasource"           :   self.datasource,
                           @"attachements"         :   self.attachements,
                           @"contents"             :   self.contents,
                           @"status"               :   [NSNumber numberWithInt:self.status],
                           @"selectedBoard"         :   self.selectedBoard
                           };
    return data;

}

- (void)save
{
    if (!self.key) {
        return;
    }
    
    BOOL found = NO;
    NSMutableArray *draftList = [NSMutableArray arrayWithArray:[Common unarchiveForKey:kDraftList]];
    for (int i = 0; i < draftList.count; i++) {
        if ([draftList[i][@"key"] isEqualToString:self.key]) {
            found = YES;
            [draftList replaceObjectAtIndex:i withObject:[self formatData]];
        }
    }
    if (!found) {
        [draftList addObject:[self formatData]];
    }
    
    [Common archive:draftList withKey:kDraftList];
    [[NSNotificationCenter defaultCenter] postNotificationName:kDRAFTS_CHANGED object:nil userInfo:[self formatData]];
}

- (void)destroy
{
    NSMutableArray *draftList = [NSMutableArray arrayWithArray:[Common unarchiveForKey:kDraftList]];
    for (int i = 0; i < draftList.count; i++) {
        if ([draftList[i][@"key"] isEqualToString:self.key]) {
            [draftList removeObjectAtIndex:i];
        }
    }
    [Common archive:draftList withKey:kDraftList];
    [[NSNotificationCenter defaultCenter] postNotificationName:kDRAFTS_CHANGED object:nil userInfo:[self formatData]];
}

- (void)send
{
    [[CDFPostManager defaultManager] appendDraft:self];
    self.status = DraftStatusSending;
}
 
+ (NSDictionary *)draftWithKey:(NSString *)key
{
    NSMutableArray *draftList = [NSMutableArray arrayWithArray:[Common unarchiveForKey:kDraftList]];
    for (int i = 0; i < draftList.count; i++) {
        if ([draftList[i][@"key"] isEqualToString:key]) {
            return draftList[i];
        }
    }
    return nil;
}

- (NSDictionary *)prepareToSend
{
    CDFLocationManager *locationManager = [CDFLocationManager sharedInstance];
    NSString *locationStr = locationManager.locationURL;
    
    NSMutableDictionary *sendData = [NSMutableDictionary new];
    NSDictionary *selected = self.selectedBoard;
//    NSLog(@"5555%@", locationStr);

    if (locationManager.isAllowLocation && locationStr != nil) {
        [sendData setObject:locationStr forKey:@"location"];
//        NSLog(@"5555%@", locationStr);
    }
    [sendData setObject:(selected ? selected[@"typeid"] : self.draftT) forKey:@"typeid"];
    [sendData setObject:(selected ? selected[@"fid"] : self.fid) forKey:@"fid"];
    [sendData setObject:[CDFMe shareInstance].formHash forKey:@"formhash"];
    NSMutableString *msg = [NSMutableString new];
    for (NSDictionary *dic in self.datasource) {
        //NSLog(@"%@",dic);
        if ([dic[@"type"] isEqualToString:@"Text"]) {
            [msg appendString:dic[@"content"]];
        } else {
            CDFAttachment *attach = dic[@"content"];
            [msg appendFormat:@"[attachimg]%@[/attachimg]",attach.attachmentID];
            [sendData setObject:@"" forKey:[NSString stringWithFormat:@"attachnew[%@][description]", attach.attachmentID]];
        }
    }

    [sendData setObject:msg forKey:@"message"];
    
    [sendData setObject:self.subject forKey:@"subject"];
    self.message = msg;
    
//    NSLog(@"%@",self.message);
    
    return sendData;
}

- (void)setAid:(NSString *)aid ForAttachmentAtIndex:(NSInteger)index
{
    if (index >= self.attachements.count) {
        return;
    }
    NSMutableArray *ds = [NSMutableArray arrayWithArray:self.datasource];
    for (NSDictionary *dic in ds) {
        if ([dic[@"content"] isEqual:self.attachements[index]]) {
            [(CDFAttachment *)dic[@"content"] setAttachmentID:aid];
            [(CDFAttachment *)dic[@"content"] setIsUploaded:YES];
        }
    }
    [(CDFAttachment *)self.attachements[index] setAttachmentID:aid];
    [(CDFAttachment *)self.attachements[index] setIsUploaded:YES];
    [self save];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@",[self formatData]];
}

+ (NSArray *)allDrafts
{
    return [Common unarchiveForKey:kDraftList];
}

@end
