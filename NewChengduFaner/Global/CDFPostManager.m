//
//  CDFPostManager.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-28.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFPostManager.h"

@implementation CDFPostManager

static CDFPostManager *manager = nil;

- (id)init {
    self = [super init];
    if (self) {
        _queue = [NSMutableArray new];
    }
    return self;
}

+ (CDFPostManager *)defaultManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [CDFPostManager new];
    });
    return manager;
}

- (void)appendDraft:(CDFDraft *)draft
{
    if (self.queue.count > 1) {
        [SVProgressHUD showErrorWithStatus:@"已经有帖子正在发送了，歇息一会吧..."];
        return;
    }
    [self.queue addObject:draft];
    if (!self.sending) {
        [self prepareSend];
    }
}

- (void)prepareSend
{
    if (self.sending || self.queue.count <= 0) {
        return;
    }

    CDFDraft *draft = (CDFDraft *)self.queue[0];
    if (draft.attachements.count <= 0) {
        [self send];
    } else {
        [self uploadNextAttachment];
    }
}

- (void)uploadNextAttachment
{
    
    if (self.queue.count <= 0) {
        return;
    }
    CDFDraft *draft = (CDFDraft *)self.queue[0];
    
    if (_uploadCursor >= draft.attachements.count) {
        [self send];
    } else {
        CDFAttachment *attach = draft.attachements[_uploadCursor];
        
        if (attach.isUploaded) {
            _uploadCursor++;
            [self uploadNextAttachment];
        }
        
        [BWStatusBarOverlay showLoadingWithMessage:[NSString stringWithFormat:@"准备上传第 %d 张图片...",_uploadCursor + 1] animated:YES];
        NSDictionary *para = @{@"hash": [CDFMe shareInstance].uploadHash,@"uid": [CDFMe shareInstance].uid, @"formhash": [CDFMe shareInstance].formHash};
//        NSLog(@"%@",para);
        
        NSString *url = [NSString stringWithFormat:@"api/mobile/index.php?version=%@&module=forumupload&type=image&simple=1",kApiVersion];
        [[HXHttpClient shareInstance] multipartFormRequestWithMethod:@"POST" path:url parameters:para data:[attach getHDImageData] success:^(id responseObject){
            
            NSString *result = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            NSArray *resultArray = [result componentsSeparatedByString:@"|"];
            if (resultArray.count == 5) {
                if ([resultArray[1] integerValue] == 0) {
                    //上传成功
                    [draft setAid:resultArray[2] ForAttachmentAtIndex:_uploadCursor];
                }
            }
            _uploadCursor ++;
            
            //上传下一个附件
            [self uploadNextAttachment];
        } fail:^(NSError *error) {
//            NSLog(@"%@",error);
            [BWStatusBarOverlay setProgress:0 animated:NO];
            [BWStatusBarOverlay showErrorWithMessage:[error localizedDescription] duration:2 animated:YES];
            if (self.queue.count > 0) {
                [self.queue removeObjectAtIndex:0];
            }
            
            [self performSelector:@selector(uploadNextAttachment) withObject:nil afterDelay:2];
        } progress:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
            if (totalBytesExpectedToWrite <= 0) {
                return;
            }
            [BWStatusBarOverlay showLoadingWithMessage:[NSString stringWithFormat:@"正在上传第 %d 张图片(%@/%@)",_uploadCursor + 1, [Common formatBytes:totalBytesWritten], [Common formatBytes:totalBytesExpectedToWrite]] animated:NO];
            CGFloat percent = (totalBytesWritten * 1.0f) / (totalBytesExpectedToWrite * 1.0f);
            [BWStatusBarOverlay setProgress:percent animated:NO];
        } fileName:attach.fileName];
    }
}

- (void)send
{
    _uploadCursor = 0;//重置上传指针
    
    CDFDraft *draft = (CDFDraft *)self.queue[0];
    NSDictionary *dic = [draft prepareToSend];
    NSString *url ;
    url = [NSString stringWithFormat:@"api/mobile/index.php?version=%@&module=newthread&submodule=checkpost&topicsubmit=yes",kApiVersion];
//    NSLog(@"dayuezaiyuji:%@; url:%@",kBaseURL,url);
    [BWStatusBarOverlay showWithMessage:[NSString stringWithFormat:@"%@ 发送中...",draft.subject] loading:YES animated:YES];
//    NSLog(@"%@",dic);
    [[HXHttpClient shareInstance] postURL:url postData:dic success:^(id responseObject) {
        _sending = NO;
        if ((NSNull *)responseObject[@"Variables"][@"tid"] == [NSNull null] || (NSNull *)responseObject[@"Variables"][@"typeid"] == [NSNull null] || [responseObject[@"Message"][@"messageval"] isEqualToString:@"post_newbie_span"]) {
            
            //发帖失败
            [[NSNotificationCenter defaultCenter] postNotificationName:kPOST_FAIL object:draft userInfo:responseObject];
            [BWStatusBarOverlay showErrorWithMessage:responseObject[@"Message"][@"messagestr"] duration:2 animated:YES];
            if (self.queue.count > 0) {
                [self.queue removeObjectAtIndex:0];
            }
        } else {
            if ([responseObject[@"Variables"][@"tid"] integerValue] != 0 || [responseObject[@"Variables"][@"pid"] integerValue] != 0) {
                [[NSNotificationCenter defaultCenter] postNotificationName:kPOST_SUCCESS object:draft];
                draft.status = DraftStatusSended;
                [draft destroy];
                if (self.queue.count > 0) {
                    [self.queue removeObjectAtIndex:0];
                }
                [BWStatusBarOverlay showSuccessWithMessage:[NSString stringWithFormat:@"%@ 发送成功",draft.subject] duration:2 animated:YES];
            } else if ([responseObject[@"Message"][@"messageval"] isEqualToString:@"replyperm_login_nopermission"]) {

                //发帖失败
                [[NSNotificationCenter defaultCenter] postNotificationName:kPOST_FAIL object:draft userInfo:responseObject];
                [BWStatusBarOverlay showErrorWithMessage:@"登录过期，请重新登录!" duration:2 animated:YES];
                if (self.queue.count > 0) {
                    [self.queue removeObjectAtIndex:0];
                }
                [Common logout];
            }
        }
        
    } fail:^(NSError *error) {

        if (self.queue.count > 0) {
            [self.queue removeObjectAtIndex:0];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:kPOST_FAIL object:draft];
        [BWStatusBarOverlay showErrorWithMessage:[error localizedDescription] duration:2 animated:YES];
    }];
}

@end
