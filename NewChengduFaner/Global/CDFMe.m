//
//  CDFMe.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-29.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFMe.h"
#import "CDFLocationManager.h"

@implementation CDFMe

static CDFMe *me = nil;

- (id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

+ (CDFMe *)shareInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        me = [CDFMe new];
        
        /*
         NSDictionary *setupInfoDict = @{@"uid":[CDFMe shareInstance].uid,ISLOCATION:[NSString stringWithFormat:@"%d", YES],ISSOUND:[NSString stringWithFormat:@"%d", YES],ISSHAKE:[NSString stringWithFormat:@"%d", YES]};
         NSLog(@"%@",setupInfoDict);
         
         BOOL ret = [[CDFMe shareInstance] setupInfo:setupInfoDict];
         if (ret) {
         NSLog(@"保存本地成功");
         }else{
         NSLog(@"保存失败");
         }
         */
        [me tongbuSetupInfo];
        [[NSNotificationCenter defaultCenter] addObserver:me selector:@selector(tongbuSetupInfo) name:kLOGIN_NOTIFICATION object:nil];
        
        
    });
    return me;
}
- (void)tongbuSetupInfo{
    if ([self isCunZai]) {
        
        NSDictionary *dict = [self getSetupInfo];
//        NSLog(@"%@",dict);
        self.isIMShake = [dict[ISSHAKE] boolValue];
        self.isIMSound = [dict[ISSOUND] boolValue];
        [CDFLocationManager sharedInstance].isAllowLocation = [dict[ISLOCATION] boolValue];
        
    }else{
        NSDictionary *setupInfoDict = @{@"uid":self.uid,ISLOCATION:[NSString stringWithFormat:@"%d", 1],ISSOUND:[NSString stringWithFormat:@"%d", 1],ISSHAKE:[NSString stringWithFormat:@"%d", 1]};
//        NSLog(@"%@",setupInfoDict);
        [CDFLocationManager sharedInstance].isAllowLocation = YES;
        BOOL ret = [self setupInfo:setupInfoDict];
        if (ret) {
//            NSLog(@"保存本地成功");
        }else{
//            NSLog(@"保存失败");
        }
        
    }
//    NSLog(@"shake:%d--sound:%d--location:%d",self.isIMShake,self.isIMSound,[CDFLocationManager sharedInstance].isAllowLocation);
}
- (NSDictionary *)loginInfo
{
    return [Common unarchiveForKey:kOnlineInfo];
}

- (BOOL)isLogin
{
    return [self.loginInfo isKindOfClass:[NSDictionary class]];
}

- (id)uid
{
    if (!self.isLogin) {
        return @"";
    }
    return self.loginInfo[@"data"][@"member_uid"];
}

- (NSString *)userName
{
    if (!self.isLogin) {
        return @"";
    }
    return self.loginInfo[@"data"][@"member_username"];
}

- (NSArray *)cookies
{
    if (!self.isLogin) {
        return [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:[NSURL URLWithString:kBaseURL]];
    }
    return self.loginInfo[@"cookie"];
}

- (NSString *)uploadHash
{
    if (!self.isLogin) {
        return @"";
    }
    return self.loginInfo[@"data"][@"hash"];
}

- (NSString *)formHash
{
    if (!self.isLogin) {
        return @"";
    }
    return self.loginInfo[@"data"][@"formhash"];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@",[Common unarchiveForKey:kOnlineInfo]];
}
- (BOOL)isCunZai{
    NSString *pathTmp = [[NSString alloc]initWithFormat:@"%@",NSHomeDirectory()];
    
    NSString *path = [pathTmp stringByAppendingString:[NSString stringWithFormat:@"/Documents/%@.plist",self.uid]];
    NSFileManager *fm = [NSFileManager defaultManager];
    return [fm fileExistsAtPath:path];
}
//用户设置信息保存至本地方法;保存至Documents中，以用户uid.plist命名；
- (BOOL)setupInfo:(NSDictionary *)infoDict{

    NSString *pathTmp = [NSHomeDirectory() stringByAppendingString:@"/Documents/"];
    NSString *path = [pathTmp stringByAppendingString:[NSString stringWithFormat:@"%@.plist",self.uid]];
    
    return [infoDict writeToFile:path atomically:YES];
}

//弹出登陆界面
- (void)wellcomeLogin:(UIViewController *)viewController {
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"welcome" bundle:nil];
    [viewController presentViewController:s.instantiateInitialViewController animated:YES completion:nil];
}


- (NSDictionary *)getSetupInfo{
    
    NSString *pathTmp = [[NSString alloc]initWithFormat:@"%@",NSHomeDirectory()];
    
    NSString *path = [pathTmp stringByAppendingString:[NSString stringWithFormat:@"/Documents/%@.plist",self.uid]];
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:path];
    if (dict == nil) {
        return nil;
    }
    //NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    return dict;
}
@end
