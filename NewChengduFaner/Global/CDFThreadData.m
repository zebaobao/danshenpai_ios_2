//
//  CDFThreadData.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-14.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFThreadData.h"

@implementation CDFThreadData

- (NSString *)description{
	return [NSString stringWithFormat:@"{tid=%d, pid=%d, uid=%d, height:%f, author=%@, dateline=%@, message=%@}", _tid, _pid, _uid, _height, _author, _dateline, _message];
}

- (void)processMessage{
	[self messageAttributedString];
}

+ (NSMutableAttributedString *)process:(NSString *)message{
	NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
							 [NSNumber numberWithFloat:1.2], NSTextSizeMultiplierDocumentOption,
							 [NSNumber numberWithFloat:1.3], DTDefaultLineHeightMultiplier,
							 @"Arial", DTDefaultFontFamily,
							 @"Blue", DTDefaultLinkColor,
							 @"none", DTDefaultLinkDecoration,
							 [[DTCSSStylesheet alloc] initWithStyleBlock:[NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"forum" ofType:@"css"] encoding:NSUTF8StringEncoding error:nil]], DTDefaultStyleSheet,
							 [NSValue valueWithCGSize:CGSizeMake(320, 1000)] ,DTMaxImageSize,
							 nil];
    
	return [[NSMutableAttributedString alloc] initWithHTMLData:[message dataUsingEncoding:NSUTF8StringEncoding] options:options documentAttributes:nil];
}

- (CGFloat)width{
	if (_width <= 0) {
		_width = 270;
	}
	return _width;
}

- (NSAttributedString *)messageAttributedString{
	if (!_messageAttributedString) {
		// 内容
		if (!_message) {
			_message = @"";
		}
		NSMutableString *message = [[NSMutableString alloc] initWithString:_message];
		
		// 替换标签属性
		//NSRegularExpression *exp = [NSRegularExpression regularExpressionWithPattern:@"<(\\w+)\\b[^>]*?>" options:NSRegularExpressionCaseInsensitive error:nil];
		//[exp replaceMatchesInString:message options:0 range:NSMakeRange(0, message.length) withTemplate:@"<$1>"];
		
		// 去掉image
		NSRegularExpression *exp;
		
		exp = [NSRegularExpression regularExpressionWithPattern:@"&nbsp;|<\\/?(table|th|td|font|div|strong)\\b[^>]*?>" options:NSRegularExpressionCaseInsensitive error:nil];
		[exp replaceMatchesInString:message options:0 range:NSMakeRange(0, message.length) withTemplate:@""];
        //
        //		exp = [NSRegularExpression regularExpressionWithPattern:@"(<br[^>]*?>\\s*)+" options:NSRegularExpressionCaseInsensitive error:nil];
        //		[exp replaceMatchesInString:message options:0 range:NSMakeRange(0, message.length) withTemplate:@"<br />"];
        //
        //		exp = [NSRegularExpression regularExpressionWithPattern:@"(<br[^>]*?>\\s*?)+(<img)" options:NSRegularExpressionCaseInsensitive error:nil];
        //		[exp replaceMatchesInString:message options:0 range:NSMakeRange(0, message.length) withTemplate:@"<br />$2"];
        //
        //
        //		exp = [NSRegularExpression regularExpressionWithPattern:@"\\s{2,}" options:NSRegularExpressionCaseInsensitive error:nil];
        //		[exp replaceMatchesInString:message options:0 range:NSMakeRange(0, message.length) withTemplate:@" "];
        //
        //		exp = [NSRegularExpression regularExpressionWithPattern:@"(\\s?<br />\\s?){2,}" options:NSRegularExpressionCaseInsensitive error:nil];
        //		[exp replaceMatchesInString:message options:0 range:NSMakeRange(0, message.length) withTemplate:@"<br />"];
        
		// 替换附件
		NSRegularExpression *attachexp = [NSRegularExpression regularExpressionWithPattern:@"\\[attach\\](\\d+)\\[/attach\\]" options:NSRegularExpressionCaseInsensitive error:nil];
		NSMutableString *newmessage = [message mutableCopy];
        
		if (self.activityInfo) {
			[newmessage insertString:@"<object class=\"activityView\"></object>" atIndex:0];
		}
		if (self.showAttachsAsGallery && self.attachments.count > 0) {
			[newmessage appendFormat:@"<br/><object width=\"%f\" height=\"60\" class=\"attachGallery\"></object>", roundf(self.width)];
		} else {
			__block NSInteger offset = 0;
			
			NSMutableDictionary *unusedDictionary = [NSMutableDictionary dictionaryWithDictionary:_attachments];
			// 未使用的附件
			[attachexp enumerateMatchesInString:message options:0 range:NSMakeRange(0, message.length) usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
				if (result.numberOfRanges == 2) {
					NSString *attachid = [message substringWithRange:[result rangeAtIndex:1]];
					// 从附件列表中查找是否有这个attachid的附件
					NSDictionary *attach = [_attachments objectForKey:attachid];
					if ([attach respondsToSelector:@selector(objectForKey:)] && [attach objectForKey:@"url"]) {
						// 从未使用的附件中移除此附件
						[unusedDictionary removeObjectForKey:attachid];
						NSString *imageurl = [NSString stringWithFormat:@"%@%@", [attach valueForKey:@"url"], [attach valueForKey:@"attachment"]];
						NSString *attachstr = [NSString stringWithFormat:@"<br/><img width=\"120\" height=\"90\" src=\"%@\" />", imageurl];
						CGFloat width = [[attach valueForKey:@"width"] integerValue];
						CGFloat height = [[attach valueForKey:@"height"] integerValue];
						if (width && height) {
							CGSize newSize = [Common fitSize:CGSizeMake(width, height) inSize:CGSizeMake(self.width, 2000)];
							if (width >= self.width * 2) {
								newSize.width = width / 2;
								newSize.height = height / 2;
							}
							attachstr = [NSString stringWithFormat:@"<br/><img src=\"%@\" width=\"%d\" height=\"%d\" />", imageurl, (int)newSize.width, (int)newSize.height];
						}
						// 取图片占用的字符位置
						NSRange range = NSMakeRange(result.range.location + offset, result.range.length);
						
						offset += attachstr.length - result.range.length;
						[newmessage replaceCharactersInRange:range withString:attachstr];
					} else {
						NSRange range = NSMakeRange(result.range.location + offset, result.range.length);
						offset -= result.range.length;
						[newmessage replaceCharactersInRange:range withString:@""];
					}
				}
			}];
			//		NSLog(@"%@", newmessage);
			// 将未使用的附件添加到文件尾部
			for (NSString *attachid in unusedDictionary) {
				NSDictionary *attach = [unusedDictionary objectForKey:attachid];
				if ([attach respondsToSelector:@selector(objectForKey:)] && [attach objectForKey:@"url"]) {
					NSString *imageurl = [NSString stringWithFormat:@"%@%@", [attach valueForKey:@"url"], [attach valueForKey:@"attachment"]];
					// 取图片占用的字符位置
					CGFloat width = [[attach valueForKey:@"width"] integerValue];
					CGFloat height = [[attach valueForKey:@"height"] integerValue];
					if (width && height) {
						CGSize newSize = [Common fitSize:CGSizeMake(width, height) inSize:CGSizeMake(self.width, 2000)];
						[newmessage appendFormat:@"<br/><img src=\"%@\" width=\"%d\" height=\"%d\" />", imageurl, (int)newSize.width, (int)newSize.height];
					} else {
						[newmessage appendFormat:@"<br/><img width=\"120\" height=\"90\" src=\"%@\" />", imageurl];
					}
				}
			}
		}
        
		NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
								 [NSNumber numberWithFloat:1.2], NSTextSizeMultiplierDocumentOption,
								 [NSNumber numberWithFloat:1.4], DTDefaultLineHeightMultiplier,
								 @"Arial", DTDefaultFontFamily,
								 @"Blue", DTDefaultLinkColor,
								 @"none", DTDefaultLinkDecoration,
								 [[DTCSSStylesheet alloc] initWithStyleBlock:[NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"forum" ofType:@"css"] encoding:NSUTF8StringEncoding error:nil]], DTDefaultStyleSheet,
								 [NSValue valueWithCGSize:CGSizeMake(self.width, 1000)] ,DTMaxImageSize,
								 nil];
		_contentMessage = [NSString stringWithString:newmessage];
		if (_subject) {
			[newmessage replaceCharactersInRange:NSMakeRange(0, 0) withString:[NSString stringWithFormat:@"<h1>%@</h1>", _subject]];
		}
		_messageAttributedString = [[NSAttributedString alloc] initWithHTMLData:[newmessage dataUsingEncoding:NSUTF8StringEncoding] options:options documentAttributes:nil];
	}
	return _messageAttributedString;
}

- (NSString *)contentMessage{
	NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithHTMLData:[_contentMessage dataUsingEncoding:NSUTF8StringEncoding] documentAttributes:nil];
	NSMutableString *newStr = [[NSMutableString alloc] initWithString:attrStr.string];
	NSRegularExpression *regexp = [[NSRegularExpression alloc] initWithPattern:@"\\s+" options:NSRegularExpressionCaseInsensitive error:nil];
	[regexp replaceMatchesInString:newStr options:NSMatchingCompleted range:NSMakeRange(0, newStr.length) withTemplate:@" "];
	if (newStr.length > 50) {
		return [NSString stringWithFormat:@"%@...", [newStr substringWithRange:NSMakeRange(0, 50)]];
	} else {
		return newStr;
	}
}

- (CGFloat)heightForWidth{
	CTFramesetterRef frameSetter = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)_messageAttributedString);
	CFRange range;
	CGSize size = CTFramesetterSuggestFrameSizeWithConstraints(frameSetter, CFRangeMake(0, _messageAttributedString.length), NULL, CGSizeMake(self.width, 10000), &range);
	if (frameSetter) {
		CFRelease(frameSetter);
	}
	return roundf(size.height);
}


@end
