//
//  CDFMe.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-29.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CDFMe : NSObject

@property (nonatomic, strong) id uid;
@property (nonatomic, strong) NSString *userName;

@property (nonatomic, strong) NSString *formHash;
@property (nonatomic, strong) NSString *uploadHash;
@property (nonatomic, strong) NSArray *cookies;
@property (nonatomic, assign) BOOL isLogin;
@property (nonatomic, assign) int newpm;
@property (nonatomic, assign) int newTingZhongCount; //新听众(红点)个数
@property (nonatomic, assign) int newLikeCount; //喜欢(红点)个数
@property (nonatomic, assign) int tiezeCount; //帖子(红点)数
@property (nonatomic, assign) int allCount; //tabbat[3]上显示的红点数
@property (nonatomic, assign) BOOL isIMSound;
@property (nonatomic, assign) BOOL isIMShake;
@property (nonatomic, strong) NSDictionary *loginInfo;
@property (nonatomic, assign) BOOL isFirstLogin;

@property (nonatomic, strong) NSArray *board;
- (void)tongbuSetupInfo;
+ (CDFMe*)shareInstance;
//用户设置信息保存至本地方法
- (BOOL)setupInfo:(NSDictionary *)infoDict;
- (NSDictionary *)getSetupInfo;
- (void)wellcomeLogin:(UIViewController *)viewController;
@end
