//
//  CDFAttachment.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-12-3.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CDFAttachment : NSObject<NSCoding>

@property (nonatomic, strong) NSString *fileName;
@property (nonatomic, strong) NSString *thumbnailPath;
@property (nonatomic, strong) NSString *hdImagePath;
@property (nonatomic, strong) NSString *attachmentID;
@property (nonatomic, assign) long long fileSize;
@property (nonatomic, assign) BOOL isUploaded;

- (CDFAttachment *)initWithFileName:(NSString *)fileName thumbnail:(UIImage *)thumbnail hdImage:(UIImage *)hdImage;

- (NSData *)getThumbnailData;
- (NSData *)getHDImageData;
@end
