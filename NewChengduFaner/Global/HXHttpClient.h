//
//  HXHttpClient.h
//  YiBin
//
//  Created by Huaxi100 com on 12-12-18.
//  Copyright (c) 2012年 Huaxi100.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HXHttpClient : NSObject

+ (HXHttpClient*)shareInstance;

- (void)grabURL:(NSString*)url success:(void(^)(id responseObject))success fail:(void(^)(NSError *error))fail;
- (void)postURL:(NSString*)url postData:(NSDictionary*)data  success:(void(^)(id responseObject))success fail:(void(^)(NSError *error))fail;
- (void)grabBaseURL:(NSString*)baseUrl path:(NSString*)path  success:(void(^)(id responseObject))success fail:(void(^)(NSError *error))fail;
- (void)putPath:(NSString*)url postData:(NSDictionary*)data success:(void(^)(id responseObject))success fail:(void(^)(NSError *error))fail;
- (void)deletePath:(NSString*)url postData:(NSDictionary*)data success:(void(^)(id responseObject))success fail:(void(^)(NSError *error))fail;
- (void)downloadRequest:(NSURLRequest*)request toPath:(NSString*)path success:(void (^)(id responseObject))success fail:(void (^)(NSError *error))fail;
- (void)setDefaultValue:(NSString *)value forHeader:(NSString *)header;

/*
 * 上传附件到成都范儿，目前只支持图片格式，其他格式暂时无法上传，若需要上传其他格式文件，请将 mimeType 作为方法参数
 *
 */
- (void)multipartFormRequestWithMethod:(NSString *)method
                                  path:(NSString *)path
                            parameters:(NSDictionary *)parameters
                                 data:(NSData *)data
                               success:(void(^)(id responseObject))success
                                  fail:(void(^)(NSError *error))fail
                              progress:(void(^)(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite)) progress
                              fileName:(NSString *)fileName;
@end
