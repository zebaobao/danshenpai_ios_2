//
//  CDFThreadData.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-14.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CDFThreadData : NSObject

@property (nonatomic, copy) NSDictionary *threadInfo;	// 帖子信息
@property (nonatomic, assign) NSInteger tid;	// thread id
@property (nonatomic, assign) NSInteger pid;	// post id
@property (nonatomic, assign) NSInteger uid;	// 用户id
@property (nonatomic, assign) CGFloat height;	// 缓存高度
@property (nonatomic, strong) UIColor *flagColor;	// 版块颜色
@property (nonatomic, assign) BOOL islike;		// 是否喜欢
@property (nonatomic, assign) NSInteger likeCount; 	// 喜欢的数量
@property (nonatomic, assign) NSInteger repliesCount;	// 回复数量
@property (nonatomic, copy) NSString *author;	// 作者
@property (nonatomic, copy) NSString *dateline; // 时间
@property (nonatomic, assign) NSInteger dbdateline;	// 时间(timestamp)
@property (nonatomic, copy) NSString *subject;	// subject;
@property (nonatomic, copy) NSString *message;	// 内容
@property (nonatomic, assign) NSInteger floor; // 楼层编号
@property (nonatomic, copy) NSString *floorName; // 楼层名字
@property (nonatomic, copy) NSDictionary *activityInfo;	// 活动信息
@property (nonatomic, copy) NSMutableDictionary *attachments; // 附件信息
@property (nonatomic, assign) CGFloat width;	// 宽度
@property (nonatomic, assign) BOOL showAttachsAsGallery;
@property (nonatomic, strong) NSAttributedString *messageAttributedString;	// 内容解析完成后
@property (nonatomic, strong) NSString *contentMessage;	// 内容解析完成后

- (void)processMessage;
+ (NSMutableAttributedString *)process:(NSString *)message;
- (CGFloat)heightForWidth;
@end
