//
//  CDFAttachment.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-12-3.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFAttachment.h"

@implementation CDFAttachment

- (id)initWithCoder:(NSCoder *)aDecoder{
	self = [super init];
	if (self != nil) {
        self.fileName = [aDecoder decodeObjectForKey:@"fileName"];
		self.thumbnailPath = [aDecoder decodeObjectForKey:@"thumbnailPath"];
		self.hdImagePath = [aDecoder decodeObjectForKey:@"hdImagePath"];
		self.isUploaded = [aDecoder decodeBoolForKey:@"isUploaded"];
		self.fileSize = [aDecoder decodeInt64ForKey:@"fileSize"];
		self.attachmentID = [aDecoder decodeObjectForKey:@"attachmentID"];
	}
	return self;
}

- (CDFAttachment *)initWithFileName:(NSString *)fileName thumbnail:(UIImage *)thumbnail hdImage:(UIImage *)hdImage
{
    self = [super init];
    if (self) {
        self.fileName = fileName;
        NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
        self.thumbnailPath = [NSString stringWithFormat:@"thumb_%@",self.fileName];
        self.hdImagePath = [NSString stringWithFormat:@"hd_%@",self.fileName];
        NSData *thumbData = UIImageJPEGRepresentation(thumbnail, .8);
        NSData *hdData = UIImageJPEGRepresentation(hdImage, .8);
        self.fileSize = hdData.length;
        self.isUploaded = NO;
        self.attachmentID = @"";
        [thumbData writeToFile:[path stringByAppendingPathComponent:self.thumbnailPath] atomically:YES];
        [hdData writeToFile:[path stringByAppendingPathComponent:self.hdImagePath] atomically:YES];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.fileName forKey:@"fileName"];
	[aCoder encodeObject:self.thumbnailPath forKey:@"thumbnailPath"];
	[aCoder encodeObject:self.hdImagePath forKey:@"hdImagePath"];
	[aCoder encodeBool:self.isUploaded forKey:@"isUploaded"];
	[aCoder encodeInt64:self.fileSize forKey:@"fileSize"];
	[aCoder encodeObject:self.attachmentID forKey:@"attachmentID"];
}

- (NSData *)getThumbnailData
{
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    return [NSData dataWithContentsOfFile:[path stringByAppendingPathComponent:self.thumbnailPath]];
}

- (NSData *)getHDImageData
{
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    return [NSData dataWithContentsOfFile:[path stringByAppendingPathComponent:self.hdImagePath]];
}

@end
