//
//  CDFDraft.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-29.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum{
    DraftStatusNew,
    DraftStatusSending,
    DraftStatusSended
}DraftStatus;

@interface CDFDraft : NSObject
@property (nonatomic, strong) NSString *key;

@property (nonatomic, strong) id draftT;

@property (nonatomic, strong) id fid;
@property (nonatomic, strong) NSString *subject;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSDate *timeline;
@property (nonatomic, strong) NSArray *datasource;
@property (nonatomic, strong) NSArray *uploaded;
@property (nonatomic, strong) NSArray *attachements;
@property (nonatomic, strong) NSArray *contents;
@property (nonatomic, assign) DraftStatus status;
@property (nonatomic, strong) NSDictionary *selectedBoard;
@property (nonatomic, strong) NSString *locationInfo;

@property (nonatomic, strong) NSDictionary *dataSource;

- (id)initWithDictionary:(NSDictionary*)dictionary;
- (id)initWithKey:(NSString *)key;

/**
 *  保存草稿
 */
- (void)save;

/**
 *  删除草稿
 */
- (void)destroy;

/**
 *  发送草稿至后台
 *
 */
- (void)send;

- (void)setAid:(NSString *)aid ForAttachmentAtIndex:(NSInteger)index;

- (NSDictionary *)prepareToSend;

/**
 *  获取所有的草稿
 *
 *  @return NSArray
 */
+ (NSArray *)allDrafts;

@end
