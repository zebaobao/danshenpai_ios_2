//
//  CDFImageViewController.m
//  yanyu
//
//  Created by dev@huaxi100.com on 14/11/13.
//  Copyright (c) 2014年 www.huaxi100.com. All rights reserved.
//

#import "CDFImageViewController.h"

@interface CDFImageViewController ()

@property (nonatomic, strong) UIButton *saveButton;
@end

@implementation CDFImageViewController

- (UIScrollView *)scrollView
{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
        _scrollView.backgroundColor = [UIColor blackColor];
        _scrollView.alpha = 1;
        _scrollView.scrollEnabled = YES;
        _scrollView.pagingEnabled = YES;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss:)];
        [_scrollView addGestureRecognizer:tap];
        _scrollView.delegate = self;
    }
    return _scrollView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view addSubview:self.scrollView];
    self.view.backgroundColor = [UIColor clearColor];

    
    for (int i = 0; i < _picArray.count; i++) {
    
        UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.view.bounds) * i, 0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds))];
        iv.tag = 100 + i;
        UIActivityIndicatorView *ind = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        ind.center = self.view.center;
        ind.hidesWhenStopped = YES;
        [iv addSubview:ind];
        [ind startAnimating];
        iv.userInteractionEnabled = YES;
        
        NSString *attachmentURL = [NSString stringWithFormat:@"%@%@",_picArray[i][@"url"],_picArray[i][@"attachment"]];
        
        [iv setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:attachmentURL]] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
            
            [ind stopAnimating];
            [ind removeFromSuperview];
            
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
            
        }];
        
        iv.contentMode = UIViewContentModeScaleAspectFit;
        [self.scrollView addSubview:iv];
        
        _saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage *image = [UIImage imageNamed:@"btn_saveimage@2x"];
        _saveButton.frame = CGRectMake(((CGRectGetWidth(self.view.frame) - image.size.width / 1.5) / 2) + CGRectGetWidth(self.view.bounds) * i, CGRectGetHeight(self.scrollView.frame) - image.size.height / 2 - 10, image.size.width / 1.5, image.size.height / 1.5);
        _saveButton.tag = 200 + i;
        _saveButton.backgroundColor = [UIColor clearColor];
        [_saveButton addTarget:self action:@selector(saveImage:) forControlEvents:UIControlEventTouchUpInside];
        [_saveButton setImage:image forState:UIControlStateNormal];
        [_scrollView addSubview:self.saveButton];
    }
    self.scrollView.contentSize = CGSizeMake(CGRectGetWidth(self.view.bounds) * _picArray.count, 0);
    self.scrollView.contentOffset = CGPointMake(CGRectGetWidth(self.view.bounds) * _index, 0);
}

- (void)dismiss:(UIGestureRecognizer *)gest
{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)saveImage:(UIButton *)sender
{
    UIImageView *hdImageView = (UIImageView *)[self.view viewWithTag:sender.tag - 100];
    UIImageWriteToSavedPhotosAlbum(hdImageView.image, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
}

// 指定回调方法
- (void)image: (UIImage *) image didFinishSavingWithError: (NSError *) error contextInfo: (void *) contextInfo
{
    
    NSString *msg = nil ;
    if(error != NULL){
        msg = @"保存图片失败" ;
        [SVProgressHUD showErrorWithStatus:msg];
    }else{
        msg = @"保存图片成功" ;
        [SVProgressHUD showSuccessWithStatus:msg];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
