//
//  CDFTabBarController.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-10-28.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFTabBarController.h"

@interface CDFTabBarController ()

@end

@implementation CDFTabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.tabBar.backgroundImage = [UIImage imageNamed:@"TabBarBacgroundView"];
        //[self.tabBarController.tabBar setBackgroundImage:[TDUtils createImageWithColor:[UIColor clearColor]]];
    //[self.tabBarController.tabBar setShadowImage:[TDUtils createImageWithColor:[UIColor clearColor]]];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 6)
    {
        [[UITabBar appearance] setShadowImage:[[UIImage alloc] init]];
    }
}
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    NSArray *arr = tabBar.items;
    for (UITabBarItem *item in arr) {
        item.enabled = YES;
    }
    //item.enabled = NO;
    if (item.tag == 199) {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(KDeviceSizeWidth/5 * 2, KDeviceSizeHeight - 49, KDeviceSizeWidth/5, 49)];
        view.backgroundColor = [UIColor clearColor];
        view.tag = 2999;
        [[[UIApplication sharedApplication].delegate window] addSubview:view];
    }else{
        UIView *view = [[[UIApplication sharedApplication].delegate window] viewWithTag:2999];
        [view removeFromSuperview];
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (void)setHidden:(BOOL)hidden
{
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    float fHeight = screenRect.size.height;
    if(UIDeviceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        fHeight = screenRect.size.width;
    }
    
    if(!hidden) fHeight -= self.tabBar.frame.size.height;
    
    [UIView animateWithDuration:0.25 animations:^{
        for(UIView *view in self.view.subviews){
            if([view isKindOfClass:[UITabBar class]]){
                [view setFrame:CGRectMake(view.frame.origin.x, fHeight, view.frame.size.width, view.frame.size.height)];
            }else{
                if(hidden) [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, fHeight)];
            }
        }
    }completion:^(BOOL finished){
        if(!hidden){
            
            [UIView animateWithDuration:0.25 animations:^{
                
                for(UIView *view in self.view.subviews)
                {
                    if(![view isKindOfClass:[UITabBar class]])
                        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, fHeight)];
                }
                
            }];
        }
    }];
    
}

@end
