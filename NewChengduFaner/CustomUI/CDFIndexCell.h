//
//  CDFIndexCell.h
//  帖子列表cell
//
//  Created by dev@huaxi100.com on 13-10-25.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDFLikeAndViews.h"
#import "UMSocial.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import "UMSocialWechatHandler.h"
#import "UMSocialQQHandler.h"
#import "CDFIndexViewController.h"


#import "ZWDShareButton.h"

@interface CDFIndexCell : UITableViewCell<UMSocialUIDelegate,CDFLikeAndViewsDelegate,ZWDShareButtonDelegate>
@property (strong ,nonatomic) NSDictionary *dataSource;

@property (strong, nonatomic) IBOutlet CDFAvatarImageView *authorFaceView;
@property (strong, nonatomic) IBOutlet UILabel *authorLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeAndBoardLabel;
@property (strong, nonatomic) IBOutlet UILabel *summaryLabel;
@property (strong, nonatomic) IBOutlet UIView *attachmentsView;
@property (strong, nonatomic) IBOutlet UIButton *likeButton;
@property (strong, nonatomic) IBOutlet UIButton *replyButton;
@property (strong, nonatomic) IBOutlet UIButton *shareButton;
@property (strong, nonatomic) NSArray *attachmentsData;
@property (strong, nonatomic) IBOutlet UIButton *authorButton;
@property (strong, nonatomic) IBOutlet CDFLikeAndViews *likeAndViewsControl;

@property (nonatomic , assign) BOOL isFav;

@property (strong, nonatomic) UINavigationController *nav;
@property (weak , nonatomic) CDFIndexViewController *viewControllerl;
@property (strong ,nonatomic) NSIndexPath *indexPath;
@property (assign ,nonatomic) NSInteger frameCount;//纪录图片左边是否调整
- (void)imageViewPressed:(UIGestureRecognizer *)gesture;
- (void)layoutMySubviews;//调整内容坐标，如果需要的话调用
@end
