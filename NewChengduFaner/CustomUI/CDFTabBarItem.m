//
//  CDFTabBarItem.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-10-28.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFTabBarItem.h"

@implementation CDFTabBarItem

- (id)init
{
    self = [super init];
    if (self) {
        //NSLog(@"init %d",self.tag);
    }
    return self;
}

- (instancetype)initWithTitle:(NSString *)title image:(UIImage *)image selectedImage:(UIImage *)selectedImage
{
    self = [super initWithTitle:title image:image selectedImage:selectedImage];
    if (self) {
        
    }
    return self;
}

- (void)awakeFromNib
{
//    [self setup];
    NSArray *imgs = @[@"nav1",@"nav2",@"nav3",@"nav4",@"nav6"];
    if ([Common systemVersion] >= 7.0) {
//        id weakself = self;
//        weakself = [self initWithTitle:self.title image:[UIImage imageNamed:imgs[self.tag]] selectedImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_selected",imgs[self.tag]]]];
        [self setTitleTextAttributes:@{UITextAttributeTextColor: [Common colorWithHex:@"#FF3300" alpha:1]} forState:UIControlStateHighlighted];
        
        UIImage *image = [UIImage imageNamed:imgs[self.tag]];
        UIImage *selectedImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_selected",imgs[self.tag]]];
        selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        [self setFinishedSelectedImage:selectedImage withFinishedUnselectedImage:image];
        
    } else {
        [self setup];
    }
}

- (void)setup
{
    NSArray *imgs = @[@"nav1",@"nav2",@"nav3",@"nav4",@"nav6"];
    
    if ([Common systemVersion] < 7.0) {
        
//        [self setFinishedSelectedImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_selected",imgs[self.tag]]] withFinishedUnselectedImage:[UIImage imageNamed:imgs[self.tag]]];
        UIImage *image = [UIImage imageNamed:imgs[self.tag]];
        UIImage *selectedImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_selected",imgs[self.tag]]];
        
        if ([Common systemVersion] >= 7.0) {
            selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        }
        [self setFinishedSelectedImage:selectedImage withFinishedUnselectedImage:image];
    }
    
    [self setTitleTextAttributes:@{UITextAttributeTextColor: [UIColor colorWithWhite:102.0f/255.0f alpha:1]} forState:UIControlStateNormal];

    [self setTitleTextAttributes:@{UITextAttributeTextColor: [Common colorWithHex:@"#FF3300" alpha:1]} forState:UIControlStateHighlighted];
}

@end
