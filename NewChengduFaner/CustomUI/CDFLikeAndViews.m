//
//  CDFLikeAndViews.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 14-1-7.
//  Copyright (c) 2014年 www.huaxi100.com. All rights reserved.
//

#import "CDFLikeAndViews.h"

#define REPLYCOLOR [UIColor colorWithRed:155.f/255.f green:155.f/255.f blue:155.f/255.f alpha:1.0f]

@interface CDFLikeAndViews()



@end

@implementation CDFLikeAndViews

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (UIButton *)likeButton
{
    if (!_likeButton) {
        
        _likeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _likeButton.titleLabel.font = [UIFont systemFontOfSize:12];
        //[_likeButton setTitleColor:[UIColor colorWithWhite:153.0f/255.0f alpha:1] forState:UIControlStateNormal];
        [_likeButton setTitleColor:REPLYCOLOR forState:UIControlStateNormal];
        
        [_likeButton setTitleColor:[UIColor redColor] forState:UIControlStateDisabled];
        [_likeButton setTitleColor:[UIColor redColor] forState:UIControlStateHighlighted];
        [_likeButton setImage:[UIImage imageNamed:@"JuJCLike"] forState:UIControlStateNormal];
        //_likeButton.backgroundColor = [UIColor grayColor];//hu-test
        [_likeButton setImage:[UIImage imageNamed:@"JuJCLikeHight"] forState:UIControlStateHighlighted];
        [_likeButton setImage:[UIImage imageNamed:@"JuJCLikeHight"] forState:UIControlStateDisabled];
        _likeButton.imageEdgeInsets = UIEdgeInsetsMake(-2, -6, 0, 0);
        _likeButton.titleEdgeInsets = UIEdgeInsetsMake(0, 6, 0, 0);
        [_likeButton addTarget:self action:@selector(like:) forControlEvents:UIControlEventTouchUpInside];
        
//        _likeButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        [_likeButton setBackgroundImage:[UIImage imageNamed:@"heart_normal"] forState:UIControlStateNormal];
//        _likeButton.backgroundColor = [UIColor redColor];
        
        
    }
    return _likeButton;
}

- (UIButton *)replyButton
{
    if (!_replyButton) {
        
        _replyButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _replyButton.titleLabel.font = [UIFont systemFontOfSize:12];
        //[_replyButton setTitleColor:[UIColor colorWithWhite:153.0f/255.0f alpha:1] forState:UIControlStateNormal];
        [_replyButton setTitleColor:REPLYCOLOR forState:UIControlStateNormal];
        //[_replyButton setTitleColor:[UIColor colorWithWhite:153.0f/255.0f alpha:1] forState:UIControlStateDisabled];
        [_replyButton setTitleColor:REPLYCOLOR forState:UIControlStateDisabled];
        //[_replyButton setImage:[UIImage imageNamed:@"reply"] forState:UIControlStateNormal];
        //_replyButton.backgroundColor = [UIColor yellowColor];//hu-test
        //[_replyButton setImage:[UIImage imageNamed:@"reply"] forState:UIControlStateDisabled];
        //_replyButton.imageEdgeInsets = UIEdgeInsetsMake(0, -7, 0, 0);
        //_replyButton.titleEdgeInsets = UIEdgeInsetsMake(0, 7, 0, 0);
        _replyButton.enabled = YES;
        [_replyButton addTarget:self action:@selector(replyButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _replyButton;
}
#pragma mark - 点击回复
- (void)replyButtonClick{
    if ([self.delegate respondsToSelector:@selector(CDFLikeAndViewsReplyClick)]) {
        [self.delegate CDFLikeAndViewsReplyClick];
    }
}
- (UIView *)splitView
{
    if (!_splitView) {
        _splitView = [[UIView alloc] initWithFrame:CGRectMake(10, 3, .5, 10)];
        _splitView.backgroundColor = [UIColor colorWithWhite:204.0f/255.0f alpha:1];
    }
    return _splitView;
}

- (void)awakeFromNib
{
    [self setup];
}

- (void)setup
{
    self.views = 0;
    self.likes = 0;
    self.liked = NO;
    [self addSubview:self.likeButton];
#pragma mark - 中间分割线
    //[self addSubview:self.splitView];
    [self addSubview:self.replyButton];
    [self layoutSubviews];
}

- (void)setEnabled:(BOOL)enabled
{
    [super setEnabled:enabled];
    self.likeButton.enabled = enabled;
    
}

- (void)setLiked:(BOOL)liked
{
    _liked = liked;
    self.likeButton.enabled = !_liked;
}

- (void)setLikes:(NSInteger)likes
{
    _likes = likes;
    
    
    
    [self.likeButton setTitle:[NSString stringWithFormat:@"%d",_likes] forState:UIControlStateNormal];
    [self layoutSubviews];
}

- (void)setViews:(NSInteger )views
{
    _views = views;
    [self.replyButton setTitle:[NSString stringWithFormat:@"回复:%d",_views] forState:UIControlStateNormal];
    [self layoutSubviews];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    NSString *s = [NSString stringWithFormat:@"回复:%d", self.views];
    CGSize size = [s sizeWithFont:self.replyButton.titleLabel.font];
    CGFloat width = size.width + 30;
//    CGFloat height = 14;
    CGFloat height = 44;
    height = self.frame.size.height;
//    if (width > 66) {
//        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@%dR",LIKEANDREPLYFRAMECHANGE,self.indexPath.row] object:self userInfo:@{@"width":[NSNumber numberWithFloat:width]}];
//    }
    self.replyButton.frame = CGRectMake(CGRectGetWidth(self.frame) - 74, 0, width, height);
    
    
    // 37/14
    CGRect rect = self.splitView.frame;
    rect.origin.x = CGRectGetMinX(self.replyButton.frame) - 1;
    self.splitView.frame = rect;
    s = [NSString stringWithFormat:@"%d", self.likes];
    size = [s sizeWithFont:self.likeButton.titleLabel.font];
    width = size.width + 30;
    CGFloat ret = -2;
    if (width > 38) {
        ret = 2;
    }
    self.likeButton.frame = CGRectMake(CGRectGetMinX(self.splitView.frame) - 33 - ret, 0, width, height);
//    if (width > 38) {
//        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@%dL",LIKEANDREPLYFRAMECHANGE,self.indexPath.row] object:self userInfo:@{@"width":[NSNumber numberWithFloat:width]}];
//    }

}

- (void)like:(id)sender
{
    self.likeButton.enabled = NO;
    [MobClick event:@"Like" label:@"喜欢控件"];
    NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=like&tid=%@", kApiVersion, self.tid];
    [[HXHttpClient shareInstance] grabURL:url success:^(id responseObject) {
//        NSLog(@"%@", responseObject);
        [[NSNotificationCenter defaultCenter] postNotificationName:kMESSAGE_BADGE object:nil userInfo:responseObject[@"Variables"][@"newmessage2"]];
        if ([responseObject[@"Message"][@"messageval"] isEqualToString:@"recommend_succed"]) {
            //[SVProgressHUD showSuccessWithStatus:responseObject[@"Message"][@"messagestr"]];
            [SVProgressHUD showSuccessWithStatus:@"点赞成功"];
            self.likeButton.enabled = NO;

            self.likes += 1;
            [self sendActionsForControlEvents:UIControlEventEditingChanged];
        } else {
            self.likeButton.enabled = YES;

            [SVProgressHUD showErrorWithStatus:responseObject[@"Message"][@"messagestr"]];
        }
    } fail:^(NSError *error) {
        self.likeButton.enabled = YES;

//        NSLog(@"%@", error);
        [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
    }];
}
- (void)likeBtnClick{
    [self like:self.likeButton];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
