//
//  CDFAvatarImageView.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-12-6.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDFAvatarImageView : UIControl

@property (strong, nonatomic) NSString *uid;
@property (strong, nonatomic) UINavigationController *navigationController;
@property (strong, nonatomic, readonly) UIImageView *imageView;

- (void)setImageWithURL:(NSURL *)url;

@end
