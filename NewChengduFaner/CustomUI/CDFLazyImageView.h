//
//  CDFLazyImageView.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-20.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

@interface CDFLazyImageView : DTLazyImageView<UIScrollViewDelegate>
@property (strong, nonatomic) NSString *hqURL;
@property (strong, nonatomic) NSMutableArray *urlArray;
@property (strong, nonatomic) UINavigationController *nav;
@end
