//
//  CDFAvatarImageView.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-12-6.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFAvatarImageView.h"

@implementation CDFAvatarImageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];
}

- (void)setup
{
    if (!_imageView) {
        _imageView = [[UIImageView alloc] initWithFrame:self.bounds];
    }
    [self addSubview:_imageView];
    UIButton *btn = [[UIButton alloc] initWithFrame:self.bounds];
    [btn addTarget:self action:@selector(handleClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btn];
}

- (void)setImageWithURL:(NSURL *)url {
    //[_imageView setImageWithURL:url];
   [_imageView setImageWithURL:url placeholderImage:[UIImage imageNamed:@"河宝娃"]];
}

- (void)handleClick:(id)sender {
    [self sendActionsForControlEvents:UIControlEventTouchUpInside];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
