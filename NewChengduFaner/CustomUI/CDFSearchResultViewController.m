//
//  CDFSearchResultViewController.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 14-1-23.
//  Copyright (c) 2014年 www.huaxi100.com. All rights reserved.
//

#import "CDFSearchResultViewController.h"
#import "CDFLikeAndViews.h"
#import "CDFIndexCell.h"
#import "IndexViewCell.h"

@interface CDFSearchResultViewController ()

@property (nonatomic, strong) NSDictionary *postData;
@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, strong) NSCache *heightCache;
@end

@implementation CDFSearchResultViewController {
    NSUInteger  _currentPage;
    BOOL        _isloading;
    BOOL        _hasMore;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (NSCache *)heightCache
{
    if (!_heightCache) {
        _heightCache = [NSCache new];
    }
    return _heightCache;
}

- (NSMutableArray *)dataSource
{
    if (!_dataSource) {
        _dataSource = [NSMutableArray new];
    }
    return _dataSource;
}

- (void)prepareForSearch
{
    [SVProgressHUD showWithStatus:@"请稍后..." maskType:SVProgressHUDMaskTypeGradient];
    NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=search&srchtxt=%@&formhash=%@", kApiVersion, [self.keyword stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], [CDFMe shareInstance].formHash];
    [[HXHttpClient shareInstance] grabURL:url success:^(id responseObject) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kMESSAGE_BADGE object:nil userInfo:responseObject[@"Variables"][@"newmessage2"]];
//        NSLog(@"%@",[Common dictionaryToJSON:responseObject]);
        self.postData = @{
                          @"searchid": responseObject[@"Variables"][@"searchid"],
                          @"orderby" : responseObject[@"Variables"][@"orderby"],
                          @"ascdesc" : responseObject[@"Variables"][@"ascdesc"],
                          @"kw" : [responseObject[@"Variables"][@"kw"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                          };
        [self doSearch];
    } fail:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
//        NSLog(@"%@",error);
    }];
}

- (void)doSearch
{
    if (!self.postData || _isloading) {
        return;
    }
    _isloading = YES;
    NSString *surl = [NSString stringWithFormat:@"api/mobile/?version=%@&module=searchresult&page=%d",kApiVersion,_currentPage];
    [[HXHttpClient shareInstance] postURL:surl postData:_postData success:^(id responseObject) {
//        NSLog(@"%@",responseObject);
        [[NSNotificationCenter defaultCenter] postNotificationName:kMESSAGE_BADGE object:nil userInfo:responseObject[@"Variables"][@"newmessage2"]];
        [SVProgressHUD dismiss];
        _isloading = NO;
//        NSLog(@"%@",[Common dictionaryToJSON:responseObject]);
        if (responseObject[@"Message"]) {
            [SVProgressHUD showErrorWithStatus:responseObject[@"Message"][@"messagestr"]];
        } else {
            NSArray *result = [NSArray arrayWithArray:responseObject[@"Variables"][@"postlist"]];
            _hasMore = self.dataSource.count < [responseObject[@"Variables"][@"count"] integerValue];
            if (_currentPage <= 1) {
                [self.dataSource removeAllObjects];
            }
            for (int i = 0; i < result.count; i++) {
                [self.dataSource addObject:[NSMutableDictionary dictionaryWithDictionary:result[i]]];
            }
            if (self.dataSource.count <= 0) {
                [SVProgressHUD showErrorWithStatus:@"没有搜到相关内容，请换一个关键词试试..."];
                [self.navigationController popViewControllerAnimated:YES];
            } else {
                [self.tableView reloadData];
            }
        }
    } fail:^(NSError *error) {
        _isloading = NO;
//        NSLog(@"%@", error);
        [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _currentPage = 1;
    [self prepareForSearch];
    
    if ([Common systemVersion] < 7.0) {
        UIView *bg = [[UIView alloc] initWithFrame:self.tableView.bounds];
        bg.backgroundColor = bgColor;
        self.tableView.backgroundView = bg;
    } else {
        self.view.backgroundColor = bgColor;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"搜索列表"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"搜索列表"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 *  计算indexpath的高度
 *
 *  @param indexPath cell的位置
 *
 *  @return CGFloat 高度
 */
- (CGFloat)offsetHeightAtIndexPath:(NSIndexPath *)indexPath
{
//    CGFloat height = 0.0;
//    NSArray *array = [self formatAttachmentList:self.dataSource[indexPath.section][@"attachments"]];
//    
//    height += (array.count <= 0 ? 65 : 0);
//    height += (60 - [self heightForSummaryAtIndexPath:indexPath]);
//    
//    return height;
    
    #pragma mark - ZWD修改
    CGFloat height = 0.0;
    NSArray *array = [self formatAttachmentList:self.dataSource[indexPath.section][@"attachments"]];
    if (array.count < 3) {
        height += (array.count <= 0 ? 96.33 : (-104 - 50));
        
        height += (60 - [self heightForSummaryAtIndexPath:indexPath]);
        
        return height;
    }
    
    height += (array.count <= 0 ? 96.333 : 0);
    
    height += (60 - [self heightForSummaryAtIndexPath:indexPath]);
    
    return height;
}

/**
 *  计算摘要的高度
 *
 *  @param indexPath 摘要所属的indexpath
 *
 *  @return CGFloat 高度
 */
- (CGFloat)heightForSummaryAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *summaryStr = [NSString stringWithFormat:@"%@%@",self.dataSource[indexPath.section][@"subject"],(self.dataSource[indexPath.section][@"message"] ? self.dataSource[indexPath.section][@"message"] : @"")];
    CGSize summarySize = [[summaryStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(280, ([Common systemVersion] >= 7.0 ? 73 : 60))];
    
    return summarySize.height;
}

/**
 *  将数据源中的附件对象转化为数组
 *
 *  @param object 要转化的对象
 *
 *  @return 数组
 */
- (NSArray *)formatAttachmentList:(id)object
{
    NSMutableArray *array = [NSMutableArray new];
    for (id obj in object) {
        [array addObject:object[obj]];
    }
    return array;
}

- (void)avatarClick:(CDFAvatarImageView *)avatarView
{
    //NSLog(@"%@",avatarView.uid);
    id userViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"userViewController"];
    if ([userViewController respondsToSelector:@selector(uid)]) {
        [userViewController setValue:avatarView.uid forKey:@"uid"];
    }
    [self.navigationController pushViewController:userViewController animated:YES];
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSString *key = [NSString stringWithFormat:@"%d-%d",indexPath.section,indexPath.row];
//    CGFloat height = [[self.heightCache objectForKey:key] floatValue];
//    if (height <= 0) {
//        height = 180 - [self offsetHeightAtIndexPath:indexPath];
//        [self.heightCache setObject:[NSNumber numberWithFloat:height] forKey:key];
//    }
//    return height;
    
#pragma mark - ZWD修改
    NSArray *imageArray =[self formatAttachmentList:self.dataSource[indexPath.section][@"attachments"]];
    if (imageArray.count < 3) {
        NSString *key = [NSString stringWithFormat:@"%d-%d",indexPath.section,indexPath.row];
        CGFloat height = [[self.heightCache objectForKey:key] floatValue];
        if (height <= 0) {
            
            height = 246 - [self offsetHeightAtIndexPath:indexPath];
            [self.heightCache setObject:[NSNumber numberWithFloat:height] forKey:key];
            
        }
        return height;
        
    }
    
    NSString *key = [NSString stringWithFormat:@"%d-%d",indexPath.section,indexPath.row];
    CGFloat height = [[self.heightCache objectForKey:key] floatValue];
    if (height <= 0) {
        
        height = 246 - [self offsetHeightAtIndexPath:indexPath];
        [self.heightCache setObject:[NSNumber numberWithFloat:height] forKey:key];
        
    }
    return height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.bounds), [self tableView:tableView heightForFooterInSection:section])];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
#pragma mark - ZWD修改
    
    static NSString *CellIdentifier = @"Cell";
    CDFIndexCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    NSString *authorFaceURL = [Common formatAvatarWithUID:self.dataSource[indexPath.section][@"authorid"] type:@"small"];;
    [cell.authorFaceView setImageWithURL:[NSURL URLWithString:authorFaceURL]];
    [cell.authorFaceView addTarget:self action:@selector(avatarClick:) forControlEvents:UIControlEventTouchUpInside];
    cell.authorFaceView.uid = self.dataSource[indexPath.section][@"authorid"];
    
    cell.authorLabel.text = [NSString stringWithFormat:@"%@",self.dataSource[indexPath.section][@"author"]];
    
    NSString *summaryStr = [NSString stringWithFormat:@"%@%@",self.dataSource[indexPath.section][@"subject"],(self.dataSource[indexPath.section][@"message"] ? self.dataSource[indexPath.section][@"message"] : @"")];
    cell.summaryLabel.text = [summaryStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    CGFloat summaryLabelHeight = [self heightForSummaryAtIndexPath:indexPath];
    CGRect summaryFrame = cell.summaryLabel.frame;
    summaryFrame.size.height = summaryLabelHeight;
    cell.summaryLabel.frame = summaryFrame;
    
    cell.attachmentsData = [self formatAttachmentList:self.dataSource[indexPath.section][@"attachments"]];
    
    NSString *date = self.dataSource[indexPath.section][@"dateline"];
    
    cell.timeAndBoardLabel.text = [NSString stringWithFormat:@"%@ 发表于 %@", date, self.dataSource[indexPath.section][@"forumname"]];
    //NSLog(@"%@", [Common dictionaryToJSON:self.dataSource[indexPath.section]]);
    cell.likeAndViewsControl.likes = [self.dataSource[indexPath.section][@"recommend_add"] integerValue];
    cell.likeAndViewsControl.tid = self.dataSource[indexPath.section][@"tid"];
    cell.likeAndViewsControl.views = [self.dataSource[indexPath.section][@"replies"] integerValue];
    cell.likeAndViewsControl.liked = [self.dataSource[indexPath.section][@"like"] boolValue];
    cell.likeAndViewsControl.indexPath = indexPath;
    [cell.likeAndViewsControl addTarget:self action:@selector(likeAction:) forControlEvents:UIControlEventEditingChanged];
    
    
    cell.dataSource = self.dataSource[indexPath.section];
    cell.viewControllerl = self;
    cell.indexPath = indexPath;
//    NSLog(@"%@",self.dataSource[indexPath.section]);
//    NSLog(@"%d",indexPath.row);
    [cell layoutMySubviews];
    return cell;
    
}

- (void)likeAction:(CDFLikeAndViews *)control
{
    [self.dataSource[control.indexPath.section] setObject:[NSNumber numberWithBool:YES] forKey:@"like"];
    NSInteger likes = [self.dataSource[control.indexPath.section][@"recommend_add"] integerValue];
    [self.dataSource[control.indexPath.section] setObject:[NSNumber numberWithInteger:likes+1] forKey:@"recommend_add"];
}

#pragma mark UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([sender isKindOfClass:[UITableViewCell class]]) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        if ([segue.destinationViewController respondsToSelector:@selector(setTid:)]) {
            [segue.destinationViewController setValue:self.dataSource[indexPath.section][@"tid"] forKey:@"tid"];
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    //下拉加载更多
    if (scrollView.contentSize.height - scrollView.contentOffset.y < CGRectGetHeight(scrollView.frame) + 50) {
        if (_hasMore && !_isloading) {
            _currentPage++;
            [self doSearch];
        }
    }
}

@end
