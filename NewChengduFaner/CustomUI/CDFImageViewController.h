//
//  CDFImageViewController.h
//  yanyu
//
//  Created by dev@huaxi100.com on 14/11/13.
//  Copyright (c) 2014年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDFImageViewController : UIViewController<UIScrollViewDelegate>
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, retain) NSMutableArray *picArray;
@property (nonatomic, assign) NSInteger index;
@end
