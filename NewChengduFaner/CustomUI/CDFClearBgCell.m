//
//  CDFClearBgCell.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-12.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFClearBgCell.h"

@implementation CDFClearBgCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];
}

- (void)setup
{
    UIView *bg = [[UIView alloc] initWithFrame:self.bounds];
    bg.backgroundColor = [UIColor clearColor];
    self.backgroundView = bg;
    self.backgroundColor = [UIColor clearColor];
    
//    UIView *selBg = [[UIView alloc] initWithFrame:self.bounds];
//    selBg.backgroundColor = [UIColor colorWithWhite:0 alpha:.1];
//    self.selectedBackgroundView = selBg;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    UIView *bg = self.backgroundView;
    [UIView animateWithDuration:(animated ? .25 : 0) animations:^{
        bg.backgroundColor = (selected ? [UIColor colorWithWhite:0 alpha:.1] : [UIColor clearColor]);
    }];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [self setSelected:highlighted animated:animated];
}

- (void)setHighlighted:(BOOL)highlighted
{
    [self setSelected:highlighted];
}

- (void)layoutSubviews
{
    CGRect frame = self.frame;
    frame.size.width = CGRectGetWidth([UIScreen mainScreen].bounds) - 20;
    frame.origin.x = 10;
    self.frame = frame;
    
    [super layoutSubviews];
}

- (void)setIsFirst:(BOOL)isFirst
{
    _isFirst = isFirst;
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    UIColor *lineColor = [UIColor colorWithWhite:180.0f/255.0f alpha:1];
    UIColor *backColor = [UIColor whiteColor];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    [backColor setFill];
    CGContextFillRect(context, rect);
    
    CGContextSetLineWidth(context, .5);
    [lineColor setStroke];
    CGContextStrokeRect(context, rect);
    
    if (!self.isFirst) {
        //上边线,覆盖上面重复边框
        draw1PxStroke(context, CGPointMake(CGRectGetMinX(rect) + .5, CGRectGetMinY(rect)), CGPointMake(CGRectGetMaxX(rect) - .5, CGRectGetMinY(rect)),backColor.CGColor);
    }
   
}

@end
