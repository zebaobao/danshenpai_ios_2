//
//  CDFSpeech.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 14-1-24.
//  Copyright (c) 2014年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RecognizerFactory.h"
#import "iflyMSC/IFlySpeechRecognizer.h"
#import "iflyMSC/IFlyDataUploader.h"

@interface CDFSpeech : UIControl

@property (strong, nonatomic) NSMutableString *message;     //所有结果
@property (strong, nonatomic) NSMutableString *curMessage;  //当前结果

- (void)showInView:(UIView *)view;
- (void)dismiss:(id)sender;

@end
