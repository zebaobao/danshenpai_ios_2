//
//  CDFSearchResultViewController.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 14-1-23.
//  Copyright (c) 2014年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDFSearchResultViewController : UITableViewController

@property (nonatomic, strong) NSString *keyword;
@property (nonatomic,assign) searchType type;

@end
