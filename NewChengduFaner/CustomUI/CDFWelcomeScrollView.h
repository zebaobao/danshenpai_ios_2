//
//  CDFWelcomeScrollView.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 14-1-27.
//  Copyright (c) 2014年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDFWelcomeScrollView : UIViewController<UIScrollViewDelegate>
@property (nonatomic, strong) UIScrollView *scrollView;
@end
