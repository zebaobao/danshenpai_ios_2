//
//  CDFDisplayCell.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-15.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDFLikeAndViews.h"

@protocol CDFDisplaycellDelegate <NSObject>

- (void)CDFDisplaycellButtonClick;

@end


@interface CDFReplyCellButton : UIButton
@property (nonatomic, strong) id cell;
@end

@interface CDFDisplayCell : UITableViewCell

@property (weak,nonatomic)id<CDFDisplaycellDelegate>delegate;
@property (strong, nonatomic) IBOutlet DTAttributedTextView *contentCoreTextView;
@property (weak, nonatomic) IBOutlet UIView *cellBackground;
@property (strong, nonatomic) IBOutlet CDFAvatarImageView *avatarImageView;
@property (strong, nonatomic) IBOutlet UILabel *authorLabel;
@property (strong, nonatomic) IBOutlet UILabel *timelineLabel;
@property (strong, nonatomic) IBOutlet CDFReplyCellButton *replyButton;
@property (strong, nonatomic) IBOutlet CDFLikeAndViews *likeAndViewsControl;

@property (assign ,nonatomic) BOOL isActivity;

@property (assign,nonatomic) BOOL isEnd;

- (void)showButton;

@end
