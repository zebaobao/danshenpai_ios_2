//
//  CDFMyDynCellContentView.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-11.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFMyDynCellContentView.h"

@implementation CDFMyDynCellContentView {
    CGFloat shadowInset;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];
}

- (void)setup
{
    self.isMine = YES;
    shadowInset = 2;
}

- (void)setIsMine:(BOOL)isMine
{
    _isMine = isMine;
    CGRect rect = self.frame;
    rect.size.width = CGRectGetWidth([UIScreen mainScreen].bounds) * (_isMine ? .89 : .7);
    
    self.frame = rect;
    
    [self setNeedsDisplay];
}

- (void)setIsTouched:(BOOL)isTouched
{
    _isTouched = isTouched;
    [self setNeedsDisplay];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    UIColor *normalFillColor = [UIColor whiteColor];
    UIColor *touchFillColor = [UIColor colorWithWhite:242.0f/255.0f alpha:1];
    
    [(self.isTouched ? touchFillColor : normalFillColor) setFill];
    
    CGMutablePathRef path = CGPathCreateMutable();
    
    CGPathMoveToPoint(path, NULL, CGRectGetMinX(rect) + 5 + shadowInset, CGRectGetMinY(rect) + 30 + shadowInset);
    CGPathAddLineToPoint(path, NULL, CGRectGetMinX(rect) + shadowInset, CGRectGetMinY(rect) + 25 + shadowInset);
    CGPathAddLineToPoint(path, NULL, CGRectGetMinX(rect) + 5 + shadowInset, CGRectGetMinY(rect) + 20 + shadowInset);
    CGPathAddLineToPoint(path, NULL, CGRectGetMinX(rect) + 5 + shadowInset, CGRectGetMinY(rect) + shadowInset);
    CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(rect) - shadowInset, CGRectGetMinY(rect) + shadowInset);
    CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(rect) - shadowInset, CGRectGetMaxY(rect) - shadowInset);
    CGPathAddLineToPoint(path, NULL, CGRectGetMinX(rect) + 5 + shadowInset, CGRectGetMaxY(rect) - shadowInset);
    CGPathAddLineToPoint(path, NULL, CGRectGetMinX(rect) + 5 + shadowInset, CGRectGetMinY(rect) + 30 + shadowInset);
    
    CGContextAddPath(context, path);
    
    CGContextSetShadowWithColor(context, CGSizeMake(0, 0), shadowInset, [UIColor colorWithWhite:0 alpha:.15].CGColor);
    CGContextFillPath(context);
    
    CGPathRelease(path);
    
    
}

@end
