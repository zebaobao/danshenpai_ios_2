//
//  CDFClearBgCell.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-12.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDFClearBgCell : UITableViewCell
@property (assign, nonatomic) BOOL isFirst;
@end
