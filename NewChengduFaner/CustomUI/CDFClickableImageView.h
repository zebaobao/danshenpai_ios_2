//
//  CDFClickableImageView.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-10-30.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDFClickableImageView : UIImageView <UIScrollViewDelegate>

@property (strong, nonatomic) NSString *hqURL;
@property (strong, nonatomic) UIImage *hdImage;

@end
