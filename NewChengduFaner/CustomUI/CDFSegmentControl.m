//
//  CDFSegmentControl.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-12.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFSegmentControl.h"

@implementation CDFSegmentControl

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setItems:(NSArray *)items
{
    _items = items;
    [self.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [obj removeFromSuperview];
    }];
    for (int i = 0; i < items.count; i++) {
        UIButton *btn = [self createItem:i];
        [self addSubview:btn];
        btn.enabled = !(i == self.activeIndex);
        [btn addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *badge = [[UILabel alloc] initWithFrame:CGRectZero];
        badge.layer.cornerRadius = 8;
        badge.textColor = [UIColor whiteColor];
        badge.font = [UIFont systemFontOfSize:10];
        badge.textAlignment = UITextAlignmentCenter;
        badge.tag = 998;
        badge.backgroundColor = [Common colorWithHex:@"#FF3510" alpha:1];
        [btn addSubview:badge];
    }
    [self setNeedsDisplay];
}

- (void)setActiveIndex:(NSInteger)activeIndex
{
    _activeIndex = activeIndex;
    [self.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[UIButton class]]) {
            [obj setEnabled:!([obj tag] == activeIndex)];
        }
        
    }];
}

- (void)setBadgeValue:(NSString *)badgeValue atIndex:(NSInteger)index
{
    if (index > self.items.count) {
        return;
    }
    
    [self.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        if ([obj tag] == index) {
            UILabel *lb = (UILabel *)[obj viewWithTag:998];
            if (!badgeValue) {
                lb.text = @"";
                lb.hidden = YES;
            } else {
                CGSize size = [badgeValue sizeWithFont:lb.font];
                size.width += 10;
                lb.frame = CGRectMake(CGRectGetWidth([obj bounds]) - size.width - 5, 5, size.width, 15);
                lb.text = badgeValue;
                lb.hidden = NO;
            }
        }
    }];
}

- (void)buttonClicked:(UIButton *)btn
{
    self.activeIndex = btn.tag;
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}

- (UIButton *)createItem:(NSInteger)index
{
    if (self.items.count <= 0) {
        return nil;
    }
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    CGFloat width = CGRectGetWidth(self.frame) / self.items.count;
    
    button.frame = CGRectMake(index * (width + .5), 0, width - .5, CGRectGetHeight(self.frame) - 1);
    [button setTitle:self.items[index] forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateDisabled];
    [button setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateHighlighted];
    [button setBackgroundImage:[UIImage imageWithColor:[UIColor clearColor]] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor colorWithWhite:102.0f/255.0f alpha:1] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor colorWithRed:255/255.0f green:62/255.0f blue:0/255.0f alpha:1] forState:UIControlStateDisabled];
    [button setTitleColor:[UIColor colorWithRed:0 green:147/255.0 blue:208/255.0 alpha:1] forState:UIControlStateHighlighted];
    button.tag = index;
    
    return button;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    UIColor *lineColor = [UIColor colorWithWhite:224.0f/255.0f alpha:1];
    UIColor *backColor = [UIColor colorWithWhite:244.0f/255.0f alpha:1];
    
    CGFloat eachItemWidth = CGRectGetWidth(rect) / self.items.count;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    [backColor setFill];
    CGContextFillRect(context, rect);
    draw1PxStroke(context, CGPointMake(CGRectGetMinX(rect), CGRectGetMaxY(rect) - 1), CGPointMake(CGRectGetMaxX(rect), CGRectGetMaxY(rect) - 1), lineColor.CGColor);
    
    for (int i = 0; i < self.items.count; i++) {
        if (i > 0) {
            draw1PxStroke(context, CGPointMake(CGRectGetMinX(rect) + eachItemWidth * i, CGRectGetMinY(rect)), CGPointMake(CGRectGetMinX(rect) + eachItemWidth * i, CGRectGetMaxY(rect)), lineColor.CGColor);
        }
    }
}


@end
