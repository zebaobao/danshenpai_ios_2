//
//  CDFBoardView.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-12-4.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDFBoardView : UIControl
@property (strong, nonatomic, readonly) NSMutableArray *dataSource;
@property (strong, nonatomic) NSDictionary *selectedItem;

- (void)reloadData;

@end
