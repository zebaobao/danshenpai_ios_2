//
//  CDFDisplayCell.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-15.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFDisplayCell.h"

@implementation CDFReplyCellButton

@end

@implementation CDFDisplayCell {
    CGFloat _cellPadding;
    
    
    UIButton *_button;
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];
}

- (void)setup
{
    self.contentCoreTextView.shouldDrawImages = YES;
    self.contentCoreTextView.shouldDrawLinks = YES;
    self.contentCoreTextView.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [Common colorWithHex:@"#e2e2e2" alpha:1.0];
    
    UIView *bg = [[UIView alloc] initWithFrame:self.bounds];
    bg.backgroundColor = [UIColor clearColor];
    self.backgroundView = bg;
    
    self.replyButton.cell = self;
    _cellPadding = 7.0f;
    
    
    _button = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.contentView addSubview:_button];
    [_button addTarget:self action:@selector(buttonClick) forControlEvents:UIControlEventTouchUpInside];
    
}
- (void)buttonClick{
    
    if ([self.delegate respondsToSelector:@selector(CDFDisplaycellButtonClick)]) {
        [self.delegate performSelector:@selector(CDFDisplaycellButtonClick)];
        
    }
}
//改变cell frame
//- (void)layoutSubviews
//{
//    CGRect frame = self.frame;
//    frame.size.width = CGRectGetWidth([UIScreen mainScreen].bounds) - 18;
//    frame.origin.x = 8;
//    self.frame = frame;
//    
//    [super layoutSubviews];
//}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)showButton{
    
    if (!self.isEnd) {
        _button.frame = CGRectMake(20, self.contentView.bounds.size.height - 50, self.contentView.bounds.size.width - 40, 35);
        [_button setImage:[UIImage imageNamed:@"点击报名"] forState:UIControlStateNormal];
    }else{
        _button.frame = CGRectMake(10, self.contentView.bounds.size.height - 30, self.contentView.bounds.size.width - 20, 35);
        [_button setImage:[UIImage imageNamed:@"已经报过名"] forState:UIControlStateNormal];
    }
    
}
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    UIColor *strokeColor = [UIColor colorWithWhite:200.0f/255.0f alpha:.5];
    UIColor *fillColor = [UIColor whiteColor];
    
    CGContextSaveGState(context);
    [fillColor setFill];
    
    //设置阴影
//    CGContextSetShadowWithColor(context, CGSizeMake(0, 0), 2, [UIColor colorWithWhite:0 alpha:.15].CGColor);
    
    CGContextFillRect(context, rect);

    //描边
//    [strokeColor setStroke];
//    CGContextSetLineWidth(context, 1);
//    CGContextStrokeRect(context, rect);
//    CGContextRestoreGState(context);
    
    //上面的分割线
    //draw1PxStroke(context, CGPointMake(_cellPadding, CGRectGetMinY(rect) + 45), CGPointMake(CGRectGetWidth(rect) - _cellPadding, CGRectGetMinY(rect) + 45), strokeColor.CGColor);
    
    //下面的分割线
    draw1PxStroke(context, CGPointMake(_cellPadding, CGRectGetMaxY(rect)-1), CGPointMake(CGRectGetWidth(rect) - _cellPadding, CGRectGetMaxY(rect)-1), strokeColor.CGColor);
    
    //按钮分割线
//    draw1PxStroke(context, CGPointMake((int)(CGRectGetWidth(rect) / 3) + .5, CGRectGetMaxY(rect) - 25), CGPointMake((int)(CGRectGetWidth(rect) / 3) + .5, CGRectGetMaxY(rect) - 5), strokeColor.CGColor);
//    draw1PxStroke(context, CGPointMake((int)(CGRectGetWidth(rect) * 2 / 3) + .5, CGRectGetMaxY(rect) - 25), CGPointMake((int)(CGRectGetWidth(rect) * 2 / 3) + .5, CGRectGetMaxY(rect) - 5), strokeColor.CGColor);
}

@end
