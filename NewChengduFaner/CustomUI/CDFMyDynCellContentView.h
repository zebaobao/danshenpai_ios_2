//
//  CDFMyDynCellContentView.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-11.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDFMyDynCellContentView : UIView

@property (assign, nonatomic) BOOL isMine;
@property (assign, nonatomic) BOOL isTouched;
@end
