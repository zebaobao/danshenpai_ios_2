//
//  CDFMyDynCell.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-11.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CDFMyDynCellContentView.h"

@interface CDFMyDynCell : UITableViewCell

@property (assign, nonatomic) BOOL isMine;
@property (strong, nonatomic) IBOutlet CDFMyDynCellContentView *myContentView;
@property (assign, nonatomic) BOOL isFirst;
@property (assign, nonatomic) BOOL isTouched;
@property (strong, nonatomic) IBOutlet UIImageView *thumbnailImageView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeAndGroupLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet UILabel *authorLabel;
@property (strong, nonatomic) IBOutlet UIImageView *avatarImageView;

@end
