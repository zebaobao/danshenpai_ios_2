//
//  CDFPostingViewCell.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-12-11.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDFPostingViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UITextView *contentTextArea;
@property (strong, nonatomic) IBOutlet UIImageView *attachmentImageView;

@end
