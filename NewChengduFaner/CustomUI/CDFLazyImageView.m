//
//  CDFLazyImageView.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-20.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFLazyImageView.h"
#import "CDFImageViewController.h"
@interface CDFLazyImageView()
@property (nonatomic, strong) UIImageView *hdImageView;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIView *maskView;
@property (nonatomic, strong) __block UIButton *saveButton;
@property (nonatomic, assign) BOOL smallType;
@end

@implementation CDFLazyImageView {
    CGRect fromRect;
    BOOL needDismiss;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _urlArray = [[NSMutableArray alloc] init];
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];
}

- (void)setup
{
    self.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    
    [self addGestureRecognizer:tap];
}

- (void)handleTap:(UIGestureRecognizer *)gest
{
    [self showHDImage];
//    CDFImageViewController *imageView = [[CDFImageViewController alloc] init];
//    imageView.picArray = [[NSMutableArray alloc] initWithArray:_urlArray];
//    imageView.index = 1;
//    [_nav presentViewController:imageView animated:YES completion:^{
//        
//    }];
}

- (void)dismissMask:(UIGestureRecognizer *)gesture
{
    needDismiss = YES;
    if (self.scrollView.zoomScale == 1.0f) {
        [self scrollViewDidEndZooming:self.scrollView withView:self.hdImageView atScale:1.0f];
    } else {
        [self.scrollView setZoomScale:1.0f animated:YES];
    }
}

- (void)autoZoom:(UIGestureRecognizer *)gesture
{
    if (self.scrollView.zoomScale != 1.0f) {
        [self.scrollView setZoomScale:1.0f animated:YES];
    } else {
        [self.scrollView setZoomScale:(self.smallType ? self.scrollView.minimumZoomScale : self.scrollView.maximumZoomScale) animated:YES];
    }
}

- (UIScrollView *)scrollView
{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectZero];
        _scrollView.backgroundColor = [UIColor colorWithWhite:0 alpha:.1];
        _scrollView.delegate = self;
    }
    
    return _scrollView;
}

- (UIView *)maskView
{
    if (!_maskView) {
        UIWindow *window = [UIApplication sharedApplication].keyWindow;
        _maskView = [[UIView alloc] initWithFrame:window.bounds];
        _maskView.userInteractionEnabled = YES;
        
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(dismissMask:)];
        singleTap.numberOfTapsRequired = 1;
        singleTap.numberOfTouchesRequired = 1;
        [_maskView addGestureRecognizer: singleTap];
        
        UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(autoZoom:)];
        doubleTap.numberOfTapsRequired = 2;
        doubleTap.numberOfTouchesRequired = 1;
        [_maskView addGestureRecognizer: doubleTap];
        
        [singleTap requireGestureRecognizerToFail: doubleTap];
    }
    return _maskView;
}

- (UIImageView *)hdImageView
{
    if (!_hdImageView) {
        _hdImageView = [[UIImageView alloc] initWithFrame:self.scrollView.bounds];
        _hdImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _hdImageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _hdImageView;
}

- (UIButton *)saveButton
{
    if (!_saveButton) {
        _saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage *image = [UIImage imageNamed:@"btn_saveimage@2x"];
        _saveButton.frame = CGRectMake(CGRectGetWidth(self.scrollView.frame) / 2 - image.size.width / 4, CGRectGetHeight(self.scrollView.frame) - image.size.height / 2 - 10, image.size.width / 2, image.size.height / 2);
        [_saveButton addTarget:self action:@selector(saveImage:) forControlEvents:UIControlEventTouchUpInside];
        [_saveButton setImage:image forState:UIControlStateNormal];
    }
    
    return _saveButton;
}

- (void)saveImage:(id)sender
{
    UIImageWriteToSavedPhotosAlbum(self.hdImageView.image, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
}

// 指定回调方法
- (void)image: (UIImage *) image didFinishSavingWithError: (NSError *) error contextInfo: (void *) contextInfo
{
    NSString *msg = nil ;
    if(error != NULL){
        msg = @"保存图片失败" ;
        [SVProgressHUD showErrorWithStatus:msg];
    }else{
        msg = @"保存图片成功" ;
        [SVProgressHUD showSuccessWithStatus:msg];
    }
}

/**
 *  显示高清图片
 */
- (void)showHDImage
{
    needDismiss = NO;
    
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    fromRect = [self convertRect:self.bounds toView:window];
    self.scrollView.frame = fromRect;
    [window addSubview:self.maskView];
    
    [self.maskView addSubview:self.scrollView];
    [self.scrollView addSubview:self.hdImageView];
    
    
    [UIView animateWithDuration:.25 animations:^{
        self.maskView.backgroundColor = [UIColor colorWithWhite:0 alpha:.8];
        self.scrollView.frame = self.maskView.frame;
        self.hdImageView.alpha = 1;
    }];
    
    self.saveButton.enabled = NO;
    
    UIActivityIndicatorView *ind = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    ind.hidesWhenStopped = YES;
    ind.center = window.center;
    [self.scrollView addSubview:ind];
    [ind startAnimating];
    __weak CDFLazyImageView *weakSelf = self;
    
    [self.hdImageView setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.hqURL]] placeholderImage:self.image success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        
        [ind stopAnimating];
        [ind removeFromSuperview];
        weakSelf.alpha = 0;
        weakSelf.saveButton.enabled = YES;
        if (image.size.width > CGRectGetWidth(weakSelf.scrollView.frame)) {
            weakSelf.smallType = NO;
            weakSelf.scrollView.maximumZoomScale = (image.size.width / CGRectGetWidth(weakSelf.scrollView.frame)) * 2;
            weakSelf.scrollView.minimumZoomScale = .5;
        } else {
            weakSelf.smallType = YES;
            weakSelf.scrollView.minimumZoomScale = (image.size.width / CGRectGetWidth(weakSelf.scrollView.frame)) * .5;
            weakSelf.scrollView.maximumZoomScale = 2;
        }
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        //加载图片失败
    }];
    [self.maskView addSubview:self.saveButton];
    
}

#pragma mark UIScrollViewDelegate

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale
{
    if (scale == 1.0f && needDismiss) {
        UIImageView *myself = self;
        [UIView animateWithDuration:.25 animations:^{
            self.maskView.backgroundColor = [UIColor clearColor];
            self.scrollView.frame = fromRect;
            self.hdImageView.alpha = 0;
            myself.alpha = 1;
        } completion:^(BOOL finished) {
            [self.maskView  removeFromSuperview];
        }];
    }
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.hdImageView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    
    CGFloat xcenter = scrollView.center.x , ycenter = scrollView.center.y;
    
    //目前contentsize的width是否大于原scrollview的contentsize，如果大于，设置imageview中心x点为contentsize的一半，以固定imageview在该contentsize中心。如果不大于说明图像的宽还没有超出屏幕范围，可继续让中心x点为屏幕中点，此种情况确保图像在屏幕中心。
    
    xcenter = scrollView.contentSize.width > scrollView.frame.size.width ? scrollView.contentSize.width/2 : xcenter;
    
    //同上，此处修改y值
    
    ycenter = scrollView.contentSize.height > scrollView.frame.size.height ? scrollView.contentSize.height/2 : ycenter;
    
    [self.hdImageView setCenter:CGPointMake(xcenter, ycenter)];
    
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
