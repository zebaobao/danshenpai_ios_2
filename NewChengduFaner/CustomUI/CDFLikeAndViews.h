//
//  CDFLikeAndViews.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 14-1-7.
//  Copyright (c) 2014年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CDFLikeAndViewsDelegate <NSObject>

- (void)CDFLikeAndViewsReplyClick;

@end

@interface CDFLikeAndViews : UIControl

@property (assign, nonatomic) NSInteger likes;
@property (assign, nonatomic) NSInteger views;
@property (assign, nonatomic) BOOL liked;
@property (strong, nonatomic) id tid;
@property (strong, nonatomic) NSIndexPath *indexPath;

@property (nonatomic, strong) UIView *splitView;
@property (nonatomic, strong) UIButton *likeButton;
@property (nonatomic, strong) UIButton *replyButton;
@property (nonatomic, weak) id<CDFLikeAndViewsDelegate> delegate;

- (void)likeBtnClick;
@end
