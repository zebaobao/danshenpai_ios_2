//
//  CDFColorButton.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-26.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFColorButton.h"

@interface CDFColorButton()
@property (nonatomic, strong) UIColor *normalColor;
@property (nonatomic, strong) UIColor *highlightColor;
@end

@implementation CDFColorButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];
}

- (void)setup
{
    self.normalColor = self.backgroundColor;
    CGFloat red, green, blue, alpha;
    [self.normalColor getRed:&red green:&green blue:&blue alpha:&alpha];
    
    self.highlightColor = [UIColor colorWithRed:red * .7 green:green * .7 blue:blue * .7 alpha:alpha];
    [self setBackgroundImage:[UIImage imageWithColor:self.highlightColor] forState:UIControlStateHighlighted];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
