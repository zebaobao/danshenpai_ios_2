//
//  CDFNavigationBar.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-10-24.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFNavigationBar.h"

@implementation CDFNavigationBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];
}

/**
 *  初始化该类
 */
- (void)setup
{
//    NSDictionary *titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
//                                         [UIColor whiteColor],UITextAttributeTextColor,
//                                         [NSValue valueWithUIOffset:UIOffsetMake(0, 0)],UITextAttributeTextShadowOffset,
//                                         nil];
    
    NSDictionary *titleTextAttributes = @{
                                          UITextAttributeTextColor: TITLECOLOR,
                                          UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0, 0)],
                                          UITextAttributeFont: [UIFont systemFontOfSize:20]
                                          };
    [self setTitleTextAttributes:titleTextAttributes];
    //成都范儿的主题色
//    [self setBarTintColor:[UIColor colorWithRed:36.0/255.0f green:187.0/255.0f blue:250.0/255.0f alpha:1]];
    //蓝色背景
//    [self setBarTintColor:[UIColor colorWithRed:1.0f green:48.0/255.0f blue:0.0f alpha:1]];
    //橙色背景
    [self setBarTintColor:BARTINCOLOR];

    if ([Common systemVersion] >= 7.0) {
        self.tintColor = TITLECOLOR;
    }
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.


@end
