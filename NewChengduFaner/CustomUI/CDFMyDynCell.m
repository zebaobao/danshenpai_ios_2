//
//  CDFMyDynCell.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-11.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFMyDynCell.h"

@implementation CDFMyDynCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];
}

- (void)setup
{
    UIView *bgv = [[UIView alloc] initWithFrame:self.frame];
    bgv.backgroundColor = [UIColor clearColor];
    self.backgroundView = bgv;
    self.backgroundColor = [UIColor clearColor];
    self.avatarImageView.layer.cornerRadius = 2;
    self.avatarImageView.layer.masksToBounds = YES;
    self.avatarImageView.layer.borderWidth = 0.5;
    self.avatarImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.backgroundColor = [UIColor clearColor];
}

- (void)setIsTouched:(BOOL)isTouched
{
    _isTouched = isTouched;
    self.myContentView.isTouched = _isTouched;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setIsMine:(BOOL)isMine {
    _isMine = isMine;
    self.myContentView.isMine = _isMine;
}

- (void)setIsFirst:(BOOL)isFirst
{
    _isFirst = isFirst;
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    UIColor *lineColor = [UIColor colorWithWhite:0 alpha:.05];
    
    CGFloat offsetY = 29.0f;
    CGFloat offsetX = 15;
    
    //画虚线
    CGContextSaveGState(context);
    CGContextSetLineWidth(context, 1.5);
    CGContextSetLineDash(context, 0, (CGFloat[]){5, 2}, 2);
    [lineColor setStroke];
    if (_isFirst) {
        CGContextMoveToPoint(context, CGRectGetMinX(rect) + offsetX, CGRectGetMinY(rect));
        CGContextAddLineToPoint(context, CGRectGetMinX(rect) + offsetX, CGRectGetMinY(rect) + offsetY);
    }
    CGContextStrokePath(context);
    CGContextRestoreGState(context);
    
    //画实线
    CGContextSaveGState(context);
    
    CGContextSetLineWidth(context, 2.5f);
    [lineColor setStroke];
    CGContextMoveToPoint(context, CGRectGetMinX(rect) + offsetX, (_isFirst ? CGRectGetMinY(rect) + offsetY : CGRectGetMinY(rect)));
    CGContextAddLineToPoint(context, CGRectGetMinX(rect) + offsetX, CGRectGetMaxY(rect));
    CGContextStrokePath(context);
    CGContextRestoreGState(context);
    
    //画圆
    CGContextSaveGState(context);
    CGContextAddEllipseInRect(context, CGRectMake(CGRectGetMinX(rect) + offsetX - 3, CGRectGetMinY(rect) + offsetY, 6, 6));
    [[UIColor colorWithWhite:153.0f/255.0f alpha:1] setFill];
    CGContextFillPath(context);
    CGContextRestoreGState(context);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    self.isTouched = YES;
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesCancelled:touches withEvent:event];
    self.isTouched = NO;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    self.isTouched = NO;
}

@end
