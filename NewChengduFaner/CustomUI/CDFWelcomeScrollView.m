//
//  CDFWelcomeScrollView.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 14-1-27.
//  Copyright (c) 2014年 www.huaxi100.com. All rights reserved.
//

#import "CDFWelcomeScrollView.h"

@implementation CDFWelcomeScrollView {
    NSInteger page;
}

- (UIScrollView *)scrollView
{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
        _scrollView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_wel"]];
        _scrollView.scrollEnabled = YES;
        _scrollView.pagingEnabled = YES;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss:)];
        [_scrollView addGestureRecognizer:tap];
        _scrollView.delegate = self;
    }
    return _scrollView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view addSubview:self.scrollView];
    self.view.backgroundColor = [UIColor whiteColor];
    
    if ([UIScreen mainScreen].bounds.size.height > 520) {
        
        NSArray *imgs = @[@"50.jpg",@"51.jpg",@"52.jpg",@"53.jpg"];
        for (int i = 0; i < imgs.count; i++) {
            UIImageView *iv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgs[i]]];
            iv.frame = CGRectMake(CGRectGetWidth(self.view.bounds) * i, 0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds));
            iv.contentMode = UIViewContentModeScaleAspectFit;
            [self.scrollView addSubview:iv];
        }
        self.scrollView.contentSize = CGSizeMake(CGRectGetWidth(self.view.bounds) * imgs.count, 0);
    }else{
        
        NSArray *imgs = @[@"40",@"41",@"42"];
//        NSArray *imgs = @[@"50.jpg",@"51.jpg",@"52.jpg",@"53.jpg"];
        for (int i = 0; i < imgs.count; i++) {
            UIImageView *iv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgs[i]]];
            iv.frame = CGRectMake(CGRectGetWidth(self.view.bounds) * i, 0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds));
            iv.contentMode = UIViewContentModeScaleAspectFit;
            [self.scrollView addSubview:iv];
        }
        self.scrollView.contentSize = CGSizeMake(CGRectGetWidth(self.view.bounds) * imgs.count, 0);
    }

}
static int i = 0;
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    if (scrollView.contentOffset.x >= scrollView.contentSize.width-CGRectGetWidth(scrollView.bounds) && i > 0) {
        [self dismiss:nil];
    }
    if (self.scrollView.contentOffset.x >= self.scrollView.contentSize.width - CGRectGetWidth(self.scrollView.bounds)) {
        i++;
    }
}
- (void)dismiss:(UIGestureRecognizer *)gest
{
    if (self.scrollView.contentOffset.x >= self.scrollView.contentSize.width - CGRectGetWidth(self.scrollView.bounds)) {
        [self dismissModalViewControllerAnimated:YES];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
