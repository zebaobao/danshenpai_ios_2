//
//  CDFBoardView.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-12-4.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFBoardView.h"

@interface CDFBoardView()
@property (strong, nonatomic) NSMutableArray *buttons;
@property (strong, nonatomic) UIScrollView *scrollView;
@end

@implementation CDFBoardView {
    NSMutableArray *dataSource;
    NSInteger numberPerPage;
    NSInteger numberPerRow;
    NSInteger numberPerCell;
}

@synthesize dataSource;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];

    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];
}

- (void)setup
{
    numberPerPage = 9;
    numberPerRow = 3;
    numberPerCell = 3;
    
    NSInteger pages = self.dataSource.count / numberPerPage;
    self.scrollView.contentSize = CGSizeMake((pages + 1) * CGRectGetWidth(self.bounds), 0);
    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    
    for (int i = 0; i < self.dataSource.count; i++) {
        UIButton *btn = [self buttonAtIndex:i];
        [self.buttons addObject:btn];
        [self.scrollView addSubview:btn];
    }
}

- (UIScrollView *)scrollView
{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
        _scrollView.backgroundColor = [UIColor clearColor];
        [self addSubview:_scrollView];
    }
    return _scrollView;
}

- (NSDictionary *)selectedItem
{
    if (!_selectedItem) {
        for (id obj in self.dataSource) {
            if ([obj[@"default"] boolValue]) {
                _selectedItem = obj;
            }
        }
    }
    return _selectedItem;
}

- (NSMutableArray *)buttons
{
    if (!_buttons) {
        _buttons = [NSMutableArray new];
    }
    return _buttons;
}

- (NSMutableArray *)dataSource
{
    if (!dataSource) {
        dataSource = [NSMutableArray array];
        [dataSource addObjectsFromArray:[CDFMe shareInstance].board];
    }
    
    return dataSource;
}

- (UIButton *)buttonAtIndex:(NSInteger)index
{
    NSInteger page      = index / numberPerPage;
    NSInteger pindex    = index - page * numberPerPage;
    CGFloat width       = CGRectGetWidth(self.bounds) / numberPerRow;
    CGFloat height      = CGRectGetHeight(self.bounds) / numberPerCell;
    CGFloat x, y;
    x = (pindex % numberPerRow) * width + page * CGRectGetWidth(self.bounds);
    y = (int)(pindex / numberPerCell) * height;
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(x - .5, y, width + .5, height);
    [btn setTitle:self.dataSource[index][@"name"] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor colorWithWhite:51.0f / 255.0f alpha:1] forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithRed:255/255.0 green:62/255.0 blue:0/255.0 alpha:1.0]] forState:UIControlStateDisabled];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateDisabled];
    btn.enabled = ![self.dataSource[index] isEqual: self.selectedItem];
    btn.tag = index;
    [btn addTarget:self action:@selector(selectItemChange:) forControlEvents:UIControlEventTouchUpInside];
    
    return btn;
}

- (void)selectItemChange:(UIButton *)sender
{
    self.selectedItem = self.dataSource[sender.tag];
    for (UIButton *btn in self.buttons) {
        
        btn.enabled = ![self.dataSource[btn.tag] isEqual:self.selectedItem];
    }
    
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    [[UIColor colorWithWhite:240.0f / 255.0f alpha:1] setFill];
    CGContextFillRect(context, rect);
    
    for (int i = 0; i < numberPerCell; i ++) {
        draw1PxStroke(context, CGPointMake(CGRectGetMinX(rect), i * CGRectGetHeight(rect) / numberPerCell), CGPointMake(CGRectGetMaxX(rect), i * CGRectGetHeight(rect) / numberPerCell), [UIColor colorWithWhite:166.0f / 255.0f alpha:1].CGColor);
    }
    for (int i = 0; i < numberPerRow; i++) {
        draw1PxStroke(context, CGPointMake(CGRectGetWidth(rect) / numberPerRow * i, CGRectGetMinY(rect)), CGPointMake(CGRectGetWidth(rect) / numberPerRow * i, CGRectGetMaxY(rect)), [UIColor colorWithWhite:166.0f / 255.0f alpha:1].CGColor);
    }
}

- (void)reloadData
{
    [self.buttons removeAllObjects];
    [self.dataSource removeAllObjects];
    dataSource = nil;
    [self.scrollView.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [obj removeFromSuperview];
    }];
    [self setup];
}

@end
