//
//  CDFIndexCell.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-10-25.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFIndexCell.h"
#import "CDFTabBarController.h"
#import "CDFDisplayViewController.h"
#import "CDFImageViewController.h"
#import "CDFReplyViewController.h"
#define SHARECOLOR [UIColor colorWithRed:155.f/255.f green:155.f/255.f blue:155.f/255.f alpha:1.0f]

#define IMAGEHEIGHT 250


@implementation CDFIndexCell {
    NSInteger _cellPadding;
    //UIButton *shareButton;
    UIButton *SCButton;
    UIButton * favButton;
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setup];
        self.frameCount = 0;
    }
    return self;
}

/**
 *  数据源的设置方法
 *
 *  @param attachmentsData 数组
 */
- (void)setAttachmentsData:(NSArray *)attachmentsData
{
    
    _attachmentsData = attachmentsData;
    [self reloadAttachmentView];
}
- (void)layoutMySubviews{
    //调整indexCell中点赞和回复控件的frame
    [self bringSubviewToFront:self.likeAndViewsControl];
    
    CGRect frame = self.likeAndViewsControl.frame;
    frame.origin.y = self.bounds.size.height - 35;
    frame.size.height = 35;
    frame.size.width = 100;
    frame.origin.x = 220;
    self.likeAndViewsControl.frame = frame;
    
    frame = self.likeAndViewsControl.splitView.frame;
    frame.origin.y = 12;
    self.likeAndViewsControl.splitView.frame = frame;
    /*
    frame = self.likeAndViewsControl.likeButton.frame;
    frame.origin.y = 5;
    self.likeAndViewsControl.likeButton.frame = frame;
    
    frame = self.likeAndViewsControl.replyButton.frame;
    frame.origin.y = 5;
    self.likeAndViewsControl.replyButton.frame = frame;
     */
    


}
/**
 *  刷新附件view
 */
- (void)reloadAttachmentView
{
    
#pragma mark - 右上角关注button
    if (!self.isFav) {
        [self addSubview:favButton];
    }
    
#pragma mark - 改变关注TA按钮
    if (![self.dataSource[@"isfollow"] boolValue]) {
//        [favButton setTitle:@"关注TA" forState:UIControlStateNormal];
//        [favButton setBackgroundColor:RGBACOLOR(251, 142, 142, 1.0)];
//        [favButton setImage:[UIImage imageNamed:@"GuanZhuButtonHight"] forState:UIControlStateNormal];
        [favButton setBackgroundImage:[UIImage imageNamed:@"GuanZhuButtonHight"] forState:UIControlStateNormal];
        favButton.userInteractionEnabled = YES;
    }else{
//        [favButton setTitle:@"已关注" forState:UIControlStateNormal];
//        [favButton setBackgroundColor:TITLEGRAY];
//        [favButton setImage:[UIImage imageNamed:@"GuanZhuButton"] forState:UIControlStateNormal];
        [favButton setBackgroundImage:[UIImage imageNamed:@"GuanZhuButton"] forState:UIControlStateNormal];

        favButton.userInteractionEnabled = NO;
    }
#pragma mark - 收藏按钮改变
    if ([self.dataSource[@"isfav"] boolValue]) {
        [SCButton setImage:[UIImage imageNamed:@"JuJCFavHeight"] forState:UIControlStateNormal];
        [SCButton setTitleColor:RGBACOLOR(255, 0, 0, 1) forState:UIControlStateNormal];
        SCButton.userInteractionEnabled = NO;
    }else{
        [SCButton setImage:[UIImage imageNamed:@"JuJCFav"] forState:UIControlStateNormal];
        [SCButton setTitleColor:SHARECOLOR forState:UIControlStateNormal];
        SCButton.userInteractionEnabled = YES;
    }
    
//    [self bringSubviewToFront:self.likeAndViewsControl];
//    [self.likeAndViewsControl becomeFirstResponder];
    //NSLog(@"%d",self.frameCount);
    if (self.frameCount > 0) {
        CGRect frame1 = self.attachmentsView.frame;
        frame1.origin.y += (IMAGEHEIGHT - 96)*self.frameCount;
        frame1.size.height = 96;
        frame1.size.width = 306;
        self.attachmentsView.frame = frame1;
        self.frameCount = 0;
    }
    
    
    //清空容器
    [_attachmentsView.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [obj removeFromSuperview];
    }];
    
    
    
    if ([self.attachmentsData count] < 3 && [self.attachmentsData count] > 0) {
        for (int i = 0; i < MIN(_attachmentsData.count, 1); i++) {
            
            NSString *attachmentURL = [NSString stringWithFormat:@"%@%@",self.attachmentsData[i][@"url"],self.attachmentsData[i][@"attachment"]];
//            NSLog(@"1290:%@", attachmentURL);
            //        CDFClickableImageView *attachment = [[CDFClickableImageView alloc] initWithFrame:CGRectMake(i * (50 + 8.5), 0, 50, 50)];
            //        attachment.backgroundColor = [UIColor colorWithWhite:.92 alpha:1];
            //        [self.attachmentsView addSubview:attachment];
            //        attachment.hqURL = attachmentURL;
            //        [attachment setImageWithURL:[NSURL URLWithString:[Common getThumbImage:attachmentURL size:CGSizeMake(50, 50) withRetinaDisplayDetect:YES]]];
//            NSLog(@"%@",self.attachmentsData);
            //以下为根据图片的实际尺寸计算出高为250时的宽
            NSDictionary *dict = self.attachmentsData[i];
            CGFloat heigth = [dict[@"height"] floatValue];
            CGFloat width = [dict[@"width"] floatValue];
            CGFloat imageWidth = width * IMAGEHEIGHT /heigth;
            CGFloat imageHeigth = 0.0f;
//            NSLog(@"%f",imageWidth);
            if (imageWidth > [UIScreen mainScreen].bounds.size.width-12) {
                imageWidth =[UIScreen mainScreen].bounds.size.width-12;
                imageHeigth = heigth * imageWidth / width;
            }else{
                imageHeigth = IMAGEHEIGHT;
            }
            //NSLog(@"%f--%f  shiji -- %f---%f",imageWidth,imageHeigth,width,heigth);
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(i * ((self.attachmentsView.bounds.size.width - 2*8.5)/3 + 8.5), 0, imageWidth, IMAGEHEIGHT)];
            imageView.tag = 100 + i;
            imageView.userInteractionEnabled = YES;
            imageView.contentMode = UIViewContentModeScaleAspectFit;
            
            //        [imageView setImageWithURL:[NSURL URLWithString:[Common getThumbImage:attachmentURL size:CGSizeMake(50, 50) withRetinaDisplayDetect:YES]]];
            
            //[imageView setImageWithURL:[NSURL URLWithString:[Common getThumbImage:attachmentURL size:CGSizeMake(imageWidth, IMAGEHEIGHT) withRetinaDisplayDetect:YES]] placeholderImage:[UIImage imageNamed:@"bg_imageloader"]];
           // [imageView setImageWithURL:[NSURL URLWithString:attachmentURL] placeholderImage:[UIImage imageNamed:@"bg_imageloader"]];
            __block UIImageView *blockImageView = imageView;
            [imageView setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:attachmentURL]] placeholderImage:[UIImage imageNamed:@"bg_imageloader"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                    //imageView.image = [self imageCompressForSize:image targetSize:CGSizeMake(imageWidth, imageHeigth)];

                    blockImageView.image = image;
                
            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                
            }];
            //NSLog(@"%@",NSStringFromCGRect(imageView.frame));
            ///
            CGRect frame1 = self.attachmentsView.frame;
            frame1.origin.y -= IMAGEHEIGHT - 96;
            frame1.size.width = imageWidth;
            frame1.size.height = IMAGEHEIGHT;
            self.attachmentsView.frame = frame1;
            self.frameCount += 1;
            ///
            [self.attachmentsView addSubview:imageView];
            
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(imageViewPressed:)];
            [imageView addGestureRecognizer: singleTap];
        }

        
    }else{
        for (int i = 0; i < MIN(_attachmentsData.count, 3); i++) {
            
            NSString *attachmentURL = [NSString stringWithFormat:@"%@%@",self.attachmentsData[i][@"url"],self.attachmentsData[i][@"attachment"]];
//            NSLog(@"1290:%@", attachmentURL);
            //        CDFClickableImageView *attachment = [[CDFClickableImageView alloc] initWithFrame:CGRectMake(i * (50 + 8.5), 0, 50, 50)];
            //        attachment.backgroundColor = [UIColor colorWithWhite:.92 alpha:1];
            //        [self.attachmentsView addSubview:attachment];
            //        attachment.hqURL = attachmentURL;
            //        [attachment setImageWithURL:[NSURL URLWithString:[Common getThumbImage:attachmentURL size:CGSizeMake(50, 50) withRetinaDisplayDetect:YES]]];
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(i * ((self.attachmentsView.bounds.size.width - 2*8.5)/3 + 8.5), 0, (self.attachmentsView.bounds.size.width - 2*8.5)/3, (self.attachmentsView.bounds.size.width - 2*8.5)/3)];
            imageView.tag = 100 + i;
            imageView.userInteractionEnabled = YES;
            //        [imageView setImageWithURL:[NSURL URLWithString:[Common getThumbImage:attachmentURL size:CGSizeMake(50, 50) withRetinaDisplayDetect:YES]]];
            [imageView setImageWithURL:[NSURL URLWithString:[Common getThumbImage:attachmentURL size:CGSizeMake((self.attachmentsView.bounds.size.width - 2*8.5)/3, (self.attachmentsView.bounds.size.width - 2*8.5)/3) withRetinaDisplayDetect:YES]] placeholderImage:[UIImage imageNamed:@"bg_imageloader"]];
            
            [self.attachmentsView addSubview:imageView];
            
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(imageViewPressed:)];
            [imageView addGestureRecognizer: singleTap];
        }

    }
    
   
    
//    if (self.attachmentsData.count > 4) {
//        
//        UIButton *totalAttachmentButton = [self createTotalAttachmentButton:self.attachmentsData.count];
//        totalAttachmentButton.frame = CGRectMake(4 * (50 + 8.5), 0, 50, 50);
//        [self.attachmentsView addSubview:totalAttachmentButton];
//    }
}

- (void)imageViewPressed:(UIGestureRecognizer *)gesture{

    CDFImageViewController *imageView = [[CDFImageViewController alloc] init];
    imageView.picArray = [[NSMutableArray alloc] initWithArray:_attachmentsData];
    imageView.index = gesture.view.tag - 100;
    [_nav presentViewController:imageView animated:YES completion:^{
        
    }];
}

- (UIButton *)createTotalAttachmentButton:(NSInteger)total
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor = [UIColor colorWithWhite:.8 alpha:1];
    button.titleLabel.numberOfLines = 0;
    button.titleLabel.font = [UIFont systemFontOfSize:10];
    button.titleLabel.textAlignment = UITextAlignmentCenter;
    [button setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    button.userInteractionEnabled = NO;
    [button setTitle:[NSString stringWithFormat:@"总共 %d 张图片",total] forState:UIControlStateNormal];
    return button;
}

- (void)awakeFromNib
{
    [self setup];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(frameChangeR:) name:[NSString stringWithFormat:@"%@%dR",LIKEANDREPLYFRAMECHANGE,self.indexPath.row] object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(frameChangeL:) name:[NSString stringWithFormat:@"%@%dL",LIKEANDREPLYFRAMECHANGE,self.indexPath.row] object:nil];
}
-(void)dealloc{
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:[NSString stringWithFormat:@"%@%dR",LIKEANDREPLYFRAMECHANGE,self.indexPath.row] object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:[NSString stringWithFormat:@"%@%dL",LIKEANDREPLYFRAMECHANGE,self.indexPath.row] object:nil];
}
- (void)frameChangeL:(NSNotification *)noti{
    //37
    CGFloat width = [noti.userInfo[@"width"] floatValue];
//    NSLog(@"%f",width);//37
    self.shareButton.frame = CGRectMake(self.likeAndViewsControl.frame.origin.x - 50 - (width - 37), self.bounds.size.height - 35, 50, 35);
    SCButton.frame = CGRectMake(self.bounds.size.width - self.likeAndViewsControl.bounds.size.width - 50*2 - (width - 37), self.bounds.size.height - 35, 50, 35);
    
}
- (void)frameChangeR:(NSNotification *)noti{
    CGFloat width = [noti.userInfo[@"width"] floatValue];
//    NSLog(@"%f",width);//65
    //    _shareButton.frame = CGRectMake(self.bounds.size.width - self.likeAndViewsControl.bounds.size.width, self.bounds.size.height - 35, 50, 35);
    //    SCButton.frame = CGRectMake(self.bounds.size.width - self.likeAndViewsControl.bounds.size.width - 50-2, self.bounds.size.height - 35, 50, 35);

    self.shareButton.frame = CGRectMake(self.likeAndViewsControl.frame.origin.x - 50 - (width - 65), self.bounds.size.height - 35, 50, 35);
    SCButton.frame = CGRectMake(self.bounds.size.width - self.likeAndViewsControl.bounds.size.width - 50*2 - (width - 65), self.bounds.size.height - 35, 50, 35);
    
}
- (void)setup
{
    //self.backgroundColor = [UIColor clearColor];
    
//    UIView *bg = [[UIView alloc] initWithFrame:self.bounds];
//    bg.backgroundColor = [UIColor whiteColor];
//    self.backgroundView = bg;

    _cellPadding = 7;
    self.autoresizesSubviews = YES;
    
    //cell右上角关注按钮
    favButton = [UIButton buttonWithType:UIButtonTypeCustom];
    favButton.frame = CGRectMake(KDeviceSizeWidth - 7 - 70, 7, 70, 31);
//    [favButton setTitle:@"关注TA" forState:UIControlStateNormal];
//    [favButton setTintColor:[UIColor whiteColor]];
//    [favButton setImage:[UIImage imageNamed:@"GuanZhuButtonHight"] forState:UIControlStateNormal];
    [favButton setBackgroundImage:[UIImage imageNamed:@"GuanZhuButtonHight"] forState:UIControlStateNormal];
    //[favButton setBackgroundColor:RGBACOLOR(251, 142, 142, 1.0)];
//    favButton.layer.masksToBounds = YES;
//    favButton.layer.cornerRadius = 5;
    [favButton addTarget:self action:@selector(favButtonClick:) forControlEvents:UIControlEventTouchUpInside];
//#pragma mark - 右上角关注button
//    if (self.isFav) {
//        [self addSubview:favButton];
//    }
    
    
    
    //分享
    _shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _shareButton.frame = CGRectMake(self.bounds.size.width - self.likeAndViewsControl.bounds.size.width - 10, self.bounds.size.height - 35, 50, 35);
    [_shareButton setTitle:@"分享" forState:UIControlStateNormal];
    _shareButton.titleLabel.font = [UIFont systemFontOfSize:12];
    [_shareButton setTitleColor:SHARECOLOR forState:UIControlStateNormal];
    //[shareButton setBackgroundColor:[UIColor grayColor]];
    [_shareButton addTarget:self action:@selector(shareButtonClick) forControlEvents:UIControlEventTouchUpInside];
    /*
     _likeButton.imageEdgeInsets = UIEdgeInsetsMake(-2, -6, 0, 0);
     _likeButton.titleEdgeInsets = UIEdgeInsetsMake(0, 6, 0, 0);
     */
    [_shareButton setImage:[UIImage imageNamed:@"JuJCShare"] forState:UIControlStateNormal];
    [_shareButton setImage:[UIImage imageNamed:@"JuJCShareHight"] forState:UIControlStateHighlighted];
    [_shareButton setImage:[UIImage imageNamed:@"JuJCShareHight"] forState:UIControlStateDisabled];
    _shareButton.imageEdgeInsets = UIEdgeInsetsMake(-2, -4, 0, 0);
    _shareButton.titleEdgeInsets = UIEdgeInsetsMake(0, 4, 0, 0);
    [self addSubview:_shareButton];
    _shareButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    //收藏
    SCButton = [UIButton buttonWithType:UIButtonTypeCustom];
    SCButton.frame = CGRectMake(self.bounds.size.width - self.likeAndViewsControl.bounds.size.width - 50-15, self.bounds.size.height - 35, 50, 35);
    
    [SCButton setTitle:@"收藏" forState:UIControlStateNormal];
    SCButton.titleLabel.font = [UIFont systemFontOfSize:12];
    [SCButton setTitleColor:SHARECOLOR forState:UIControlStateNormal];
    [SCButton addTarget:self action:@selector(SCButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [SCButton setImage:[UIImage imageNamed:@"JuJCFav"] forState:UIControlStateNormal];
    [SCButton setImage:[UIImage imageNamed:@"JuJCFavHeight"] forState:UIControlStateHighlighted];
    [SCButton setImage:[UIImage imageNamed:@"JuJCFavHeight"] forState:UIControlStateDisabled];
    SCButton.imageEdgeInsets = UIEdgeInsetsMake(-2, -4, 0, 0);
    SCButton.titleEdgeInsets = UIEdgeInsetsMake(0, 4, 0, 0);
    [self addSubview:SCButton];
    SCButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;

    
    self.likeAndViewsControl.delegate = self;
    
}
/*
 {
 author = uvzxqvul6hs;
 authorid = 14633162;
 boardcolor = ffa300;
 boardname = "\U5feb\U65c5";
 dateline = "\U524d\U5929&nbsp;15:35";
 dbdateline = 1428737738;
 digest = 0;
 fid = 53;
 lastpost = 1428889778;
 lastposter = uvzxqvul6hs;
 like = 1;
 message = "\U6211\U660e\U5929\U8981\U5230\U516c\U53f8\U53bb\U770b\U770b\U516c\U53f8\U7684\U60f3\U6cd5\Uff0c\U201c\U751f\U6d3b\U4e2d\U6211\U4ece\U6ca1\U8bd5\U8fc7\U5973\U8ffd\U7537\U2019\Uff0c\U5374\U8ba9\U89c2\U4f17\U500d\U611f\U8212\U670d\U3002\U5b89\U59ae\U65af\U987f\U4e0e\U585e\U6d1b\U514b\U65af\U53bb\U5e74\U590f\U5929\U8ba2\U5a5a\Uff0c\U7528\U884c\U52a8\U5ef6\U4f38\U7231\U7684\U529b\U91cf\Uff0c\U4e0e\U5979\U5403\U5b8c\U6e29\U99a8\U6d6a\U6f2b\U7684\U70db\U5149\U665a\U9910\U3002\U4ee4\U4ed6\U73b0\U5728\U5df2\U80fd\U8bf4\U7b80\U5355\U7684\U4e2d\U6587\Uff0c\U7ee7\U201c\U5c0f\U5f3a50\U4e07 ...";
 readperm = 0;
 "recommend_add" = 1;
 replies = 0;
 subject = "\U6211\U660e\U5929\U8981\U5230\U516c\U53f8\U53bb\U770b\U770b\U516c\U53f8\U7684\U60f3\U6cd5";
 "thread_name" = "\U63a8\U8350\U7248\U533a";
 tid = 7785978;
 views = 0;
 }
 */
- (void)replSuccess:(NSNotification *)noti{
    //回复成功
   //[[_listDataSource[indexPath.section] objectForKey:@"replies"] integerValue];
//    NSLog(@"%@",noti.userInfo);
    if ([noti.userInfo[@"status"] isEqualToString:@"success"]) {
        [self.viewControllerl.listDataSource[self.indexPath.section] setValue:[NSString stringWithFormat:@"%d",[[self.viewControllerl.listDataSource[self.indexPath.section] objectForKey:@"replies"] integerValue]+1] forKey:@"replies"];
        [self.viewControllerl.tableView reloadRowsAtIndexPaths:@[self.indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:self.dataSource[@"tid"] object:nil];
}
#pragma mark - 点击回复
- (void)CDFLikeAndViewsReplyClick{
    //CDFReplyViewController
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(replSuccess:) name:self.dataSource[@"tid"] object:nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CDFReplyViewController *DisplayView = [storyboard instantiateViewControllerWithIdentifier:@"CDFReplyViewController"];
    DisplayView.tid = self.dataSource[@"tid"];
    DisplayView.fid = self.dataSource[@"fid"];
    DisplayView.uid = self.dataSource[@"authorid"];
    DisplayView.author = self.dataSource[@"author"];
    DisplayView.pid = @"nil";
    DisplayView.isPush = YES;
    [self.viewControllerl.navigationController pushViewController:DisplayView animated:YES];
    
}
#pragma mark - 收藏按钮点击
- (void)SCButtonClick{
    
    SCButton.userInteractionEnabled = NO;
    
    NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=favthread&id=%@&formhash=%@", kApiVersion, self.dataSource[@"tid"],[CDFMe shareInstance].formHash];
    //http://svnbang.dahe.cn/api/mobile/?version=3&module=favthread&tid=7783838
    [[HXHttpClient shareInstance]grabURL:url success:^(id responseObject) {
        if ([responseObject[@"Message"][@"messageval"] isEqualToString:@"favorite_do_success"]) {
            [SVProgressHUD showSuccessWithStatus:@"收藏成功" duration:1];
            [SCButton setImage:[UIImage imageNamed:@"JuJCFavHeight"] forState:UIControlStateNormal];
            [SCButton setTitleColor:RGBACOLOR(255, 0, 0, 1) forState:UIControlStateNormal];
        }else{
            [SVProgressHUD showSuccessWithStatus:responseObject[@"Message"][@"messagestr"] duration:2];

        }
//        NSLog(@"%@",responseObject);
    } fail:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:@"请检查网络连接" duration:1];
    }];

}
#pragma mark - 分享按钮点击
- (void)shareButtonClick{
    CDFTabBarController *tab = (CDFTabBarController *)[[UIApplication sharedApplication].delegate window].rootViewController;
    [tab setHidden:YES];
    
//    [self share:nil];
    [self newShare];
}
/*
 {
 author = uvzxqvul6hs;
 authorid = 14633162;
 boardcolor = ffa300;
 boardname = "\U5feb\U65c5";
 dateline = "\U524d\U5929&nbsp;15:35";
 dbdateline = 1428737738;
 digest = 0;
 fid = 53;
 lastpost = 1428889778;
 lastposter = uvzxqvul6hs;
 like = 1;
 message = "\U6211\U660e\U5929\U8981\U5230\U516c\U53f8\U53bb\U770b\U770b\U516c\U53f8\U7684\U60f3\U6cd5\Uff0c\U201c\U751f\U6d3b\U4e2d\U6211\U4ece\U6ca1\U8bd5\U8fc7\U5973\U8ffd\U7537\U2019\Uff0c\U5374\U8ba9\U89c2\U4f17\U500d\U611f\U8212\U670d\U3002\U5b89\U59ae\U65af\U987f\U4e0e\U585e\U6d1b\U514b\U65af\U53bb\U5e74\U590f\U5929\U8ba2\U5a5a\Uff0c\U7528\U884c\U52a8\U5ef6\U4f38\U7231\U7684\U529b\U91cf\Uff0c\U4e0e\U5979\U5403\U5b8c\U6e29\U99a8\U6d6a\U6f2b\U7684\U70db\U5149\U665a\U9910\U3002\U4ee4\U4ed6\U73b0\U5728\U5df2\U80fd\U8bf4\U7b80\U5355\U7684\U4e2d\U6587\Uff0c\U7ee7\U201c\U5c0f\U5f3a50\U4e07 ...";
 readperm = 0;
 "recommend_add" = 1;
 replies = 0;
 subject = "\U6211\U660e\U5929\U8981\U5230\U516c\U53f8\U53bb\U770b\U770b\U516c\U53f8\U7684\U60f3\U6cd5";
 "thread_name" = "\U63a8\U8350\U7248\U533a";
 tid = 7785978;
 views = 0;
 }
 */
- (IBAction)share:(id)sender {
 
    //NSString *shareText =
    //[self hideFooterView];
    //NSLog(@"%@", [self.threadInfo[@"summary"] class]);
    [MobClick event:@"Share"];
   // self.shareImageView =  nil;
    NSString *shareURL = [NSString stringWithFormat:@"http://bang.dahe.cn/forum.php?mod=viewthread&tid=%@",self.dataSource[@"tid"]];

    [UMSocialData defaultData].extConfig.title = self.dataSource[@"subject"];
    
    //设置手机QQ的AppId，url传nil，将使用友盟的网址
    [UMSocialQQHandler setQQWithAppId:kQQ_Appkey appKey:kUmeng_Appkey url:shareURL];
    //设置微信AppId，url地址传nil，将默认使用友盟的网址
    [UMSocialWechatHandler setWXAppId:kWeichat_Appkey appSecret:kUmeng_Appkey url:shareURL];
        //            if ([self.dataSource[0][0][@"imagelist"] count] > 0) {
        NSString *second; //分享信息的第二个%@
        if (self.dataSource[@"summary"]) {
            second = self.dataSource[@"summary"];
        } else {
            second = @"";
        }
        
    
        [UMSocialSnsService presentSnsIconSheetView:self.viewControllerl
                                             appKey:kUmeng_Appkey
                                          shareText:[NSString stringWithFormat:@"%@%@ %@",self.dataSource[@"subject"],second, shareURL]
                                         shareImage:[UIImage imageNamed:@"appicon120yuanjiao"]
                                    shareToSnsNames:[NSArray arrayWithObjects:UMShareToSina,UMShareToTencent,UMShareToQQ,UMShareToWechatSession,UMShareToWechatTimeline,UMShareToQzone,nil]
                                           delegate:self];
    
}
- (void)newShare{
//    UMSocialSnsPlatform *sinaPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToSina];
//    sinaPlatform.bigImageName = @"icon";
//    sinaPlatform.displayName = @"微博";
//    sinaPlatform.snsClickHandler = ^(UIViewController *presentingController, UMSocialControllerService * socialControllerService, BOOL isPresentInController){
//        NSLog(@"点击新浪微博的响应");
//    };
    UIView *bacView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KDeviceSizeWidth, KDeviceSizeHeight)];
    bacView.backgroundColor = [UIColor blackColor];
    bacView.alpha = 0.4;
    [[[UIApplication sharedApplication].delegate window]addSubview:bacView];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(bacClick:)];
    bacView.tag = 2000;
    [bacView addGestureRecognizer:tap];
    
    
    UIView *view = [[UIView  alloc]initWithFrame:CGRectMake(0, KDeviceSizeHeight, KDeviceSizeWidth, 350)];
    view.backgroundColor = [UIColor blackColor];
    view.userInteractionEnabled = YES;
    view.tag = 2001;
    view.alpha = 1;
    [[[UIApplication sharedApplication].delegate window] addSubview:view];
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = view.frame;
        frame.origin.y = KDeviceSizeHeight - 350;
        view.frame  = frame;
    } completion:^(BOOL finished) {
        
    }];
    
    NSArray *arr = @[@"微信",@"微信朋友圈",@"新浪微博",@"腾讯微博",@"QQ空间",@"QQ"];
    NSArray *imageArr = @[@"icon_share_wx",@"icon_share_pyq",@"icon_share_sina_wb",@"icon_share_tencent_wb",@"icon_share_qzone",@"icon_share_qq"];
    for (int i = 0; i < arr.count; i++) {
        ZWDShareButton *button = [[ZWDShareButton alloc]initWithFrame:CGRectMake(5 + (KDeviceSizeWidth - 5*4)/3 * (i%3), 5 + ((KDeviceSizeWidth - 5*4)/3 +20)*(i/3), (KDeviceSizeWidth - 5*4)/3, (KDeviceSizeWidth - 5*4)/3 + 20) name:arr[i] imageName:imageArr[i]];
        [view addSubview:button];
        button.delegate = self;
    }
    
    UIButton *cancleButton = [UIButton buttonWithType:UIButtonTypeSystem];
    cancleButton.frame = CGRectMake(10, view.bounds.size.height - 50, view.bounds.size.width - 20, 40);
    [cancleButton setTitle:@"取消" forState:UIControlStateNormal];
    [cancleButton addTarget:self action:@selector(cancleButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:cancleButton];
    [cancleButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    cancleButton.layer.masksToBounds = YES;
    cancleButton.layer.cornerRadius = 5;
    [cancleButton setBackgroundColor:TITLEGRAY];
    
}
- (void)cancleButtonClick{
    UIView *view = (UIView *)[[[UIApplication sharedApplication].delegate window] viewWithTag:2001];
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = view.frame;
        frame.origin.y = KDeviceSizeHeight;
        view.frame = frame;
    } completion:^(BOOL finished) {
        if (finished) {
            [view removeFromSuperview];
            CDFTabBarController *tab = (CDFTabBarController *)[[UIApplication sharedApplication].delegate window].rootViewController;
            [tab setHidden:NO];
        }
    }];
    
    UIView *view2 = (UIView *)[[[UIApplication sharedApplication].delegate window] viewWithTag:2000];
    [view2 removeFromSuperview];
}
- (void)bacClick:(UIGestureRecognizer *)tap{
    
    [tap.view removeFromSuperview];
    UIView *view = (UIView *)[[[UIApplication sharedApplication].delegate window] viewWithTag:2001];
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = view.frame;
        frame.origin.y = KDeviceSizeHeight;
        view.frame = frame;
    } completion:^(BOOL finished) {
        if (finished) {
            [view removeFromSuperview];
            CDFTabBarController *tab = (CDFTabBarController *)[[UIApplication sharedApplication].delegate window].rootViewController;
            [tab setHidden:NO];
        }
    }];
}
- (void)ZWDShareButtonClick:(NSString *)ButtonName{
    NSArray *arr = @[@"微信",@"微信朋友圈",@"新浪微博",@"腾讯微博",@"QQ空间",@"QQ"];
    NSArray *UMName = @[UMShareToWechatSession,UMShareToWechatTimeline,UMShareToSina,UMShareToTencent,UMShareToQzone,UMShareToQQ];
    
    
    
    
    NSString *shareURL = [NSString stringWithFormat:@"http://bang.dahe.cn/forum.php?mod=viewthread&tid=%@",self.dataSource[@"tid"]];
    [UMSocialWechatHandler setWXAppId:kWeichat_Appkey appSecret:kUmeng_Appkey url:shareURL];
    [UMSocialQQHandler setQQWithAppId:kQQ_Appkey appKey:kUmeng_Appkey url:shareURL];
    [UMSocialData defaultData].extConfig.title = self.dataSource[@"subject"];
    
    
    
    NSString *second; //分享信息的第二个%@
    if (self.dataSource[@"summary"]) {
        second = self.dataSource[@"summary"];
    } else {
        second = @"";
    }
    NSString *name = UMName[[arr indexOfObject:ButtonName]];
    NSString *shareText =[NSString stringWithFormat:@"%@%@ %@",self.dataSource[@"subject"],second, shareURL];
    UIImage *shareImage =[UIImage imageNamed:@"appicon120yuanjiao"];
    //设置分享内容，和回调对象
    [[UMSocialControllerService defaultControllerService] setShareText:shareText shareImage:shareImage socialUIDelegate:self];
    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:name];
    
    
    snsPlatform.snsClickHandler(self.viewControllerl,[UMSocialControllerService defaultControllerService],YES);
    
    
    
}

/*
- (void)buttonClick:(UIButton *)button{
    
    NSString *shareURL = [NSString stringWithFormat:@"http://bang.dahe.cn/forum.php?mod=viewthread&tid=%@",self.dataSource[@"tid"]];
    [UMSocialWechatHandler setWXAppId:kWeichat_Appkey appSecret:kUmeng_Appkey url:shareURL];
    [UMSocialQQHandler setQQWithAppId:kQQ_Appkey appKey:kUmeng_Appkey url:shareURL];
    [UMSocialData defaultData].extConfig.title = self.dataSource[@"subject"];



    NSString *second; //分享信息的第二个%@
    if (self.dataSource[@"summary"]) {
        second = self.dataSource[@"summary"];
    } else {
        second = @"";
    }
    NSString *name;
    if (button.tag == 0) {
        //新浪
        name =UMShareToSina;
        
    }else if (button.tag == 1){
        //腾讯微博
        name =UMShareToTencent;
    }else if (button.tag == 2){
        //QQ
        name =UMShareToQQ;
    }
    NSString *shareText =[NSString stringWithFormat:@"%@%@ %@",self.dataSource[@"subject"],second, shareURL];
    UIImage *shareImage =[UIImage imageNamed:@"appicon120yuanjiao"];
    
    //设置分享内容，和回调对象
    [[UMSocialControllerService defaultControllerService] setShareText:shareText shareImage:shareImage socialUIDelegate:self];
    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:name];
    snsPlatform.snsClickHandler(self.viewControllerl,[UMSocialControllerService defaultControllerService],YES);
    
}
 */
- (void)didSelectSocialPlatform:(NSString *)platformName withSocialData:(UMSocialData *)socialData{
    UIView *view = (UIView *)[[[UIApplication sharedApplication].delegate window] viewWithTag:2001];
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = view.frame;
        frame.origin.y = KDeviceSizeHeight;
        view.frame = frame;
    } completion:^(BOOL finished) {
        if (finished) {
            [view removeFromSuperview];
            CDFTabBarController *tab = (CDFTabBarController *)[[UIApplication sharedApplication].delegate window].rootViewController;
            [tab setHidden:NO];
        }
    }];
    
    UIView *view2 = (UIView *)[[[UIApplication sharedApplication].delegate window] viewWithTag:2000];
    [view2 removeFromSuperview];

    
    CDFTabBarController *tab = (CDFTabBarController *)[[UIApplication sharedApplication].delegate window].rootViewController;
    [tab setHidden:NO];
}
- (void)didCloseUIViewController:(UMSViewControllerType)fromViewControllerType
{
//    NSLog(@"fenxiangchenggong ");
    CDFTabBarController *tab = (CDFTabBarController *)[[UIApplication sharedApplication].delegate window].rootViewController;
    [tab setHidden:NO];
}

#pragma mark - 点击关注TA
- (void)favButtonClick:(UIButton *)button{
//    [button setTitle:@"已关注" forState:UIControlStateNormal];
//    [button setBackgroundColor:TITLEGRAY];
    button.userInteractionEnabled = NO;
    
    NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=addfollow&hash=%@&fuid=%@", kApiVersion, [CDFMe shareInstance].formHash, self.dataSource[@"authorid"]];
//    NSLog(@"%@",url);
    [[HXHttpClient shareInstance] grabURL:url success:^(id responseObject) {
//        NSLog(@"%@",responseObject);
        if ([responseObject[@"Message"][@"messageval"] isEqualToString:@"follow_add_succeed"]) {
            [SVProgressHUD showSuccessWithStatus:responseObject[@"Message"][@"messagestr"]];
//            [button setImage:[UIImage imageNamed:@"GuanZhuButton"] forState:UIControlStateNormal];
            [button setBackgroundImage:[UIImage imageNamed:@"GuanZhuButton"] forState:UIControlStateNormal];
        } else {
            [SVProgressHUD showErrorWithStatus:responseObject[@"Message"][@"messagestr"]];
            button.userInteractionEnabled = YES;
        }
    } fail:^(NSError *error) {
        button.userInteractionEnabled = YES;
//        NSLog(@"%@",error);
        [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
    }];
    
}
//改变cell的frame

//- (void)layoutSubviews
//{
//    CGRect frame = self.frame;
//    frame.size.width = CGRectGetWidth([UIScreen mainScreen].bounds) - 18;
//    frame.origin.x = 8;
//    self.frame = frame;
//    
//    [super layoutSubviews];
//}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    UIView *bg = self.backgroundView;
    [UIView animateWithDuration:(animated ? .25 : 0) animations:^{
        bg.backgroundColor = (selected ? [UIColor colorWithWhite:0 alpha:.1] : [UIColor clearColor]);
    }];
}

- (void)setHighlighted:(BOOL)highlighted
{
    [self setSelected:highlighted];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [self setSelected:highlighted animated:animated];
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    UIColor *fillColor = [UIColor whiteColor];
    
    CGContextSaveGState(context);
    [fillColor setFill];
    
    //设置阴影
//    CGContextSetShadowWithColor(context, CGSizeMake(0, 0), 2, [UIColor colorWithWhite:0 alpha:.15].CGColor);
//    
    CGContextFillRect(context, rect);

    //描边
//    [strokeColor setStroke];
//    CGContextSetLineWidth(context, 1);
//    CGContextStrokeRect(context, rect);
    CGContextRestoreGState(context);
    
    //上面的分割线
//    draw1PxStroke(context, CGPointMake(_cellPadding, CGRectGetMinY(rect) + 45), CGPointMake(CGRectGetWidth(rect) - _cellPadding, CGRectGetMinY(rect) + 45), strokeColor.CGColor);

    //下面的分割线
//    draw1PxStroke(context, CGPointMake(_cellPadding, CGRectGetMaxY(rect) - 30), CGPointMake(CGRectGetWidth(rect) - _cellPadding, CGRectGetMaxY(rect) - 30), strokeColor.CGColor);
    
    //按钮分割线
//    draw1PxStroke(context, CGPointMake((int)(CGRectGetWidth(rect) / 3) + .5, CGRectGetMaxY(rect) - 25), CGPointMake((int)(CGRectGetWidth(rect) / 3) + .5, CGRectGetMaxY(rect) - 5), strokeColor.CGColor);
//    draw1PxStroke(context, CGPointMake((int)(CGRectGetWidth(rect) * 2 / 3) + .5, CGRectGetMaxY(rect) - 25), CGPointMake((int)(CGRectGetWidth(rect) * 2 / 3) + .5, CGRectGetMaxY(rect) - 5), strokeColor.CGColor);
}
@end
