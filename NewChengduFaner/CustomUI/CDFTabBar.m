//
//  CDFTabBar.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-10-28.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFTabBar.h"
#import "AMBlurView.h"

@implementation CDFTabBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    
        [self setup];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];
}

- (void)setup
{
    if ([Common systemVersion] < 7.0) {
        [self setSelectionIndicatorImage:[UIImage imageNamed:@"selectionIndicatorImage"]];
    } else {
        CGRect blurRect = self.bounds;
        blurRect.origin.y += .5;
        blurRect.size.height -= .5;
        AMBlurView *blurView = [[AMBlurView alloc] initWithFrame:blurRect];
        [self addSubview:blurView];
        [self sendSubviewToBack:blurView];
    }
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    if ([Common systemVersion] < 7.0) {
        [[UIColor whiteColor] setFill];
    } else {
        [[UIColor colorWithWhite:1 alpha:0] setFill];
    }
    CGContextFillRect(context, rect);
    
    draw1PxStroke(context, CGPointMake(CGRectGetMinX(rect), CGRectGetMinY(rect)), CGPointMake(CGRectGetMaxX(rect), CGRectGetMinY(rect)), [UIColor colorWithWhite:229.0f/255.0f alpha:.9].CGColor);
     
}


@end
