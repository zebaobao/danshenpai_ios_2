//
//  CDFSpeech.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 14-1-24.
//  Copyright (c) 2014年 www.huaxi100.com. All rights reserved.
//

#import "CDFSpeech.h"

@interface CDFSpeech()<IFlySpeechRecognizerDelegate,IFlyDataUploaderDelegate>
@property (strong, nonatomic) IFlySpeechRecognizer *iFlySpeechRecognizer;
@property (strong, nonatomic) IFlyDataUploader *uploader;
@property (strong, nonatomic) UIView *speechView;
@property (strong, nonatomic) UIButton *cancelButton;
@property (strong, nonatomic) UIButton *endButton;
@property (strong, nonatomic) UIImageView *volView;
@end

@implementation CDFSpeech {
    CGRect  _volFrame;
    BOOL    _isCancel;
}

- (id)init
{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];
}

- (UIImageView *)volView
{
    if (!_volView) {
        _volView = [UIImageView new];
        _volView.backgroundColor = [Common colorWithHex:@"#00CC00" alpha:1];
        UIImage *mask = [UIImage imageNamed:@"voice_big"];//pic.png就是不规则的图片
        CALayer* maskLayer = [CALayer layer];
        maskLayer.frame = CGRectMake(0, 0, mask.size.width, mask.size.height);
        maskLayer.contents = (id)[mask CGImage];
        [_volView.layer setMask:maskLayer];
    }
    return _volView;
}

- (NSMutableString *)message
{
    if (!_message) {
        _message = [NSMutableString new];
    }
    return _message;
}



- (UIButton *)cancelButton
{
    if (!_cancelButton) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(CGRectGetMidX(_speechView.bounds), CGRectGetMaxY(_speechView.bounds) - 44, CGRectGetWidth(_speechView.bounds) / 2, 44);
        [btn setTitle:@"取消" forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(dismiss:) forControlEvents:UIControlEventTouchUpInside];
        [btn setTitleColor:[UIColor colorWithWhite:102.0f/255.0f alpha:1] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:16];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [btn setBackgroundImage:[UIImage imageWithColor:[UIColor lightGrayColor]] forState:UIControlStateHighlighted];
        _cancelButton = btn;
    }
    return _cancelButton;
}

- (UIButton *)endButton
{
    if (!_endButton) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(0, CGRectGetMaxY(_speechView.bounds) - 44, CGRectGetWidth(_speechView.bounds) / 2, 44);
        [btn setTitle:@"说好了" forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(done:) forControlEvents:UIControlEventTouchUpInside];

        btn.titleLabel.font = [UIFont systemFontOfSize:16];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn setBackgroundImage:[UIImage imageWithColor:[Common colorWithHex:@"#00CC00" alpha:1]] forState:UIControlStateNormal];
        _endButton = btn;
    }
    return _endButton;
}

- (UIView *)speechView
{
    if (!_speechView) {
        _speechView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 180, 210)];
        _speechView.backgroundColor = [UIColor whiteColor];
        _speechView.layer.cornerRadius = 10;
        _speechView.layer.masksToBounds = YES;
        
        UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_speechView.frame), 50)];
        lb.text = @"来，说两句～";
        lb.textAlignment = UITextAlignmentCenter;
        lb.backgroundColor = [UIColor clearColor];
        lb.font = [UIFont systemFontOfSize:16];
        lb.textColor = [UIColor colorWithWhite:153.0f/255.0f alpha:1];
        [_speechView addSubview:lb];
        
        UIImage *img = [UIImage imageNamed:@"voice_big"];
        UIImageView *iv = [[UIImageView alloc] initWithImage:img];
        iv.frame = CGRectMake((CGRectGetMaxX(_speechView.bounds) - img.size.width) / 2, CGRectGetMaxY(lb.frame), img.size.width, img.size.height);
        iv.contentMode = UIViewContentModeCenter;
        [_speechView addSubview:iv];
        
        self.volView.frame = iv.frame;
        [_speechView addSubview:self.volView];
        self.volView.alpha = 0;
        _volFrame = self.volView.frame;
        
        [self volumeChange:0];
        
        [_speechView addSubview:self.endButton];
        [_speechView addSubview:self.cancelButton];
        
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_speechView.bounds) - 44.5, CGRectGetWidth(_speechView.bounds), .5)];
        line.backgroundColor = [UIColor colorWithWhite:0 alpha:.1];
        [_speechView addSubview:line];
        line = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMidX(_speechView.bounds), CGRectGetMaxY(_speechView.bounds) - 44.5, .5, 44)];
        line.backgroundColor = [UIColor colorWithWhite:0 alpha:.1];
        [_speechView addSubview:line];
    }
    return _speechView;
}

- (void)setup
{
    self.backgroundColor = [UIColor colorWithWhite:0 alpha:.5];
    [self addSubview:self.speechView];
}

- (IFlyDataUploader *)uploader
{
    if (!_uploader) {
        _uploader = [[IFlyDataUploader alloc] initWithDelegate:nil pwd:nil params:nil delegate:self];
    }
    return _uploader;
}

- (IFlySpeechRecognizer *)iFlySpeechRecognizer
{
    if (!_iFlySpeechRecognizer) {
        _iFlySpeechRecognizer = [RecognizerFactory CreateRecognizer:self Domain:@"sms"];
    }
    return  _iFlySpeechRecognizer;
}

- (NSMutableString *)curMessage
{
    if (!_curMessage) {
        _curMessage = [NSMutableString new];
    }
    return _curMessage;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#pragma mark - 私有方法

- (void)volumeChange:(int)volume
{
    CGFloat percent = (volume * 1.0) / 100;
    self.volView.alpha = percent + (percent * 1.5);
}

- (void)done:(id)sender
{
    [self.iFlySpeechRecognizer stopListening];
}

#pragma mark - 公开方法
/**
 *  显示控件
 *
 *  @param view 父窗口
 */
- (void)showInView:(UIView *)view
{
    self.frame = view.bounds;
    self.speechView.center = self.center;
    [view addSubview:self];
    [UIView animateWithDuration:.25 animations:^{
        self.alpha = 1;
    } completion:^(BOOL finished) {
        [self.iFlySpeechRecognizer startListening];
    }];
}

/**
 *  关闭控件
 *
 *  @param sender
 */
- (void)dismiss:(id)sender
{
    _isCancel = YES;
    [self.iFlySpeechRecognizer stopListening];
}

- (void)remove
{
    [UIView animateWithDuration:.25 animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - IFlySpeechRecognizerDelegate
/**
 * @fn      onVolumeChanged
 * @brief   音量变化回调
 *
 * @param   volume      -[in] 录音的音量，音量范围1~100
 * @see
 */
- (void) onVolumeChanged: (int)volume
{
//    NSString * vol = [NSString stringWithFormat:@"音量：%d",volume];
//    [_popUpView setText: vol];
//    [self.view addSubview:_popUpView];
    [self volumeChange:volume];
}

/**
 * @fn      onBeginOfSpeech
 * @brief   开始识别回调
 *
 * @see
 */
- (void) onBeginOfSpeech
{
//    [_popUpView setText: @"正在录音"];
//    [self.view addSubview:_popUpView];
//    NSLog(@"onBeginOfSpeech");
}

/**
 * @fn      onEndOfSpeech
 * @brief   停止录音回调
 *
 * @see
 */
- (void) onEndOfSpeech
{
//    [_popUpView setText: @"停止录音"];
//    [self.view addSubview:_popUpView];
//    NSLog(@"onEndOfSpeech");
}

/**
 * @fn      onError
 * @brief   识别结束回调
 *
 * @param   errorCode   -[out] 错误类，具体用法见IFlySpeechError
 */
- (void) onError:(IFlySpeechError *) error
{
//    NSLog(@"onError : %@",error);
    if (_isCancel) {
        [self remove];
        return;
    }
    else if (error.errorCode ==0 ) {
        if (self.curMessage.length == 0) {
//            NSLog(@"无匹配结果");
            [SVProgressHUD showErrorWithStatus:@"你好像没有说话..."];
        }
        else
        {
            [self.message appendString:self.curMessage];
            [self sendActionsForControlEvents:UIControlEventValueChanged];
        }
        [self remove];
    }
    else
    {
        NSString *text = [NSString stringWithFormat:@"发生错误：%d %@",error.errorCode,error.errorDesc];
        [SVProgressHUD showErrorWithStatus:text];
//        NSLog(@"%@",text);
        [self remove];
    }
}

/**
 * @fn      onResults
 * @brief   识别结果回调
 *
 * @param   result      -[out] 识别结果，NSArray的第一个元素为NSDictionary，NSDictionary的key为识别结果，value为置信度
 * @see
 */
- (void) onResults:(NSArray *) results
{

    NSMutableString *result = [[NSMutableString alloc] init];
    NSDictionary *dic = [results objectAtIndex:0];
    for (NSString *key in dic) {
        //id value = [dic objectForKey:key];
        [result appendFormat:@"%@",key];
    }
    [self.curMessage appendString:result];
    
}

/**
 * @fn      onCancal
 * @brief   取消识别回调
 *
 * @see
 */

- (void) onCancel
{
//    [_popUpView setText: @"正在取消"];
//    [self.view addSubview:_popUpView];
//    NSLog(@"onCancel");
}

#pragma mark - IFlyDataUploaderDelegate
- (void) onEnd:(IFlyDataUploader*) uploader grammerID:(NSString *)grammerID error:(IFlySpeechError *)error
{
    if (error) {
//        NSLog(@"onEnd %@ grammerID %@ error %d", uploader, grammerID, [error errorCode]);
    }
    
//    NSLog(@"%d",[error errorCode]);
//    
//    if (![error errorCode]) {
//        [_popUpView setText: @"上传成功"];
//        [self.view addSubview:_popUpView];
//        _isUploaded = YES;
//    }
//    else {
//        [_popUpView setText: @"上传失败"];
//        [self.view addSubview:_popUpView];
//        _isUploaded = NO;
//        
//    }
//    [self setGrammerId:grammerID];
//    _uploadBtn.enabled = YES;
}


@end
