//
//  CDFAppDelegate+IM.m
//  yanyu
//
//  Created by caiyee on 15/5/4.
//  Copyright (c) 2015年 dahe.cn. All rights reserved.
//

#import "CDFAppDelegate+IM.h"

@implementation CDFAppDelegate (IM)

- (void)IMapplication:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    /**
     初始化环信SDK
     */
    
    [[EaseMob sharedInstance] registerSDKWithAppKey:@"dahewang#bang" apnsCertName:@"Yanyu"];
    [[EaseMob sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    
    //IM推送
    EMPushNotificationOptions *options = [[EaseMob sharedInstance].chatManager pushNotificationOptions];
    options.displayStyle = ePushNotificationDisplayStyle_messageSummary;
    [[EaseMob sharedInstance].chatManager asyncUpdatePushOptions:options completion:^(EMPushNotificationOptions *options, EMError *error) {
        if (!error) {
            NSLog(@"设置成功");
        }
    } onQueue:nil];


}

- (void)IMapplicationWillTerminate:(UIApplication *)application {
    //环信
    [[EaseMob sharedInstance] applicationWillTerminate:application];
    
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)IMapplicationDidBecomeActive:(UIApplication *)application {
    
    //环信
    [[EaseMob sharedInstance] applicationDidBecomeActive:application];
    if ([CDFMe shareInstance].isLogin && ![[EaseMob sharedInstance].chatManager isLoggedIn]) {
        NSString *mima = [CDFAppDelegate md5:[CDFMe shareInstance].uid];
        NSLog(@"mima:%@", mima);
        NSString *NewMima = [mima substringWithRange:NSMakeRange(0, 10)];
        NSLog(@"NewMima:%@",NewMima);
        [[EaseMob sharedInstance].chatManager asyncLoginWithUsername:[NSString stringWithFormat:@"dahe_%@",[CDFMe shareInstance].uid] password:NewMima completion:^(NSDictionary *loginInfo, EMError *error) {
            if (!error && loginInfo) {
                NSLog(@"登陆成功");
            }else {
                NSLog(@"登陆失败:%@", error);
                NSLog(@"error.errorCode:%u", error.errorCode);
            }
        } onQueue:nil];
    }
}
+ (NSString *) md5:(NSString *)str {
    const char *cStr = [str UTF8String];
    unsigned char result[16];
    CC_MD5(cStr, strlen(cStr), result );
    return [NSString stringWithFormat:@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}
-(void)IMapplicationWillEnterForeground:(UIApplication *)application {
    [[EaseMob sharedInstance] applicationWillEnterForeground:application];
}

- (void)IMapplicationDidEnterBackground:(UIApplication *)application {
    //环信初始化
    [[EaseMob sharedInstance] applicationDidEnterBackground:application];
}
- (void)IMapplication:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"error:%@", error);
    [[EaseMob sharedInstance] application:application didFailToRegisterForRemoteNotificationsWithError:error];
}
- (void)IMapplication:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    //打印获取的deviceToken的字符串
    [[EaseMob sharedInstance] application:application didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}

@end
