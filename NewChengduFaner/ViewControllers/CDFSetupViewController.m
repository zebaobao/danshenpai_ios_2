//
//  CDFSetupViewController.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-12.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFSetupViewController.h"
#import "CDFClearBgCell.h"
#import "CDFDraft.h"
#import "CDFLocationManager.h"
#import "NewMimaViewController.h"



@interface CDFSetupViewController ()<UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet UIView *postListFlagView;
@property (strong, nonatomic) IBOutlet UILabel *checkVersionIconView;
@property (assign, nonatomic) BOOL isLogin;
@property (strong, nonatomic) IBOutlet UILabel *authorLabel;
@property (strong, nonatomic) IBOutlet UILabel *postCountLabel;
@property (strong, nonatomic) IBOutlet UILabel *sinaBindLabel;
@property (strong, nonatomic) IBOutlet UILabel *tencentBindLabel;
@property (strong, nonatomic) IBOutlet UISwitch *saveFlowSwitch;
@property (strong, nonatomic) IBOutlet UILabel *versionLabel;
@property (strong, nonatomic) CDFWelcomeScrollView *welcomeScrollView;
@property (copy, nonatomic) NSString *url;
@end

@implementation CDFSetupViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (CDFWelcomeScrollView *)welcomeScrollView
{
    if (!_welcomeScrollView) {
        _welcomeScrollView = [CDFWelcomeScrollView new];
    }
    return _welcomeScrollView;
}

- (IBAction)logoutAction:(id)sender {
    [MobClick event:@"Logout"];
    [Common logout];
    self.isLogin = NO;
    [[EaseMob sharedInstance].chatManager asyncLogoffWithCompletion:^(NSDictionary *info, EMError *error) {
        if (error) {
//            NSLog(@"error:%@", error);
        } else {
//            NSLog(@"dic:%@", info);
        }
    } onQueue:nil];
    
}
//点击允许定位切换按钮
- (IBAction)handleLocationAccess:(UISwitch *)sender {
    
    UISwitch *swi = (UISwitch *)sender;
    
    NSDictionary *setupInfoDict = @{@"uid":[CDFMe shareInstance].uid,ISLOCATION:[NSString stringWithFormat:@"%d",sender.isOn],ISSOUND:[NSString stringWithFormat:@"%d",self.voiceSwitch.isOn],ISSHAKE:[NSString stringWithFormat:@"%d",self.shakeSwitch.isOn]};
//    NSLog(@"%@",setupInfoDict);
    BOOL ret = [[CDFMe shareInstance] setupInfo:setupInfoDict];
    if (ret) {
//        NSLog(@"保存本地成功");
    }else{
//        NSLog(@"保存失败");
    }
    [[CDFMe shareInstance] tongbuSetupInfo];
    
    
    BOOL isSwith = [swi isOn];
    if (isSwith) {
        CDFLocationManager *locationManager = [CDFLocationManager sharedInstance];
        locationManager.isAllowLocation = YES;
//        NSLog(@"swith = ");
    } else {
        CDFLocationManager *locationManager = [CDFLocationManager sharedInstance];
        locationManager.isAllowLocation = NO;
//        NSLog(@"fei");
    }
}
- (IBAction)handleShake:(UISwitch *)sender {
    UISwitch *swi = (UISwitch *)sender;
    NSDictionary *setupInfoDict = @{@"uid":[CDFMe shareInstance].uid,ISLOCATION:[NSString stringWithFormat:@"%d",self.locationSwitch.isOn],ISSOUND:[NSString stringWithFormat:@"%d",self.voiceSwitch.isOn],ISSHAKE:[NSString stringWithFormat:@"%d",sender.isOn]};
//    NSLog(@"%@",setupInfoDict);
    BOOL ret = [[CDFMe shareInstance] setupInfo:setupInfoDict];
    if (ret) {
//        NSLog(@"保存本地成功");
    }else{
//        NSLog(@"保存失败");
    }
    [[CDFMe shareInstance] tongbuSetupInfo];
    
    
    BOOL isSwith = [swi isOn];
    if (isSwith) {
        [CDFMe shareInstance].isIMShake = YES;
    } else {
        [CDFMe shareInstance].isIMShake = NO;
    }
}
- (IBAction)handleSound:(UISwitch *)sender {
    UISwitch *swi = (UISwitch *)sender;
    NSDictionary *setupInfoDict = @{@"uid":[CDFMe shareInstance].uid,ISLOCATION:[NSString stringWithFormat:@"%d",self.locationSwitch.isOn],ISSOUND:[NSString stringWithFormat:@"%d",sender.isOn],ISSHAKE:[NSString stringWithFormat:@"%d",self.shakeSwitch.isOn]};
//    NSLog(@"%@",setupInfoDict);
    BOOL ret = [[CDFMe shareInstance] setupInfo:setupInfoDict];
    if (ret) {
//        NSLog(@"保存本地成功");
    }else{
//        NSLog(@"保存失败");
    }
    [[CDFMe shareInstance] tongbuSetupInfo];
    
    BOOL isSwith = [swi isOn];
    if (isSwith) {
        [CDFMe shareInstance].isIMSound = YES;
    } else {
        [CDFMe shareInstance].isIMSound = NO;
    }
    
}

- (void)handleLogNotification:(NSNotification *)notification
{
    if ([notification.name isEqualToString: kLOGOUT_NOTIFICATION]) {
        //退出登录
        self.isLogin = NO;
        self.authorLabel.text = @"未登录";
        [[[self.tabBarController viewControllers][3] tabBarItem]setBadgeValue:nil];//退出账号的时候,角标清0
        [self.tableView reloadData];
    }
    
    if ([notification.name isEqualToString:kLOGIN_NOTIFICATION]) {
        //登录
        self.isLogin = YES;
        [self loadData];
        [self.tableView reloadData];
    }
}

- (void)handleDraftsChanged:(NSNotification *)notification
{
    [self loadData];
}

- (void)addNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLogNotification:) name:kLOGIN_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLogNotification:) name:kLOGOUT_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDraftsChanged:) name:kDRAFTS_CHANGED object:nil];
}

- (void)removeNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLOGIN_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLOGOUT_NOTIFICATION object:nil];
}

- (void)loadData
{
    self.isLogin = [[Common unarchiveForKey:kOnlineInfo] isKindOfClass:[NSDictionary class]];
    //账号
    NSString *author = [Common unarchiveForKey:kOnlineInfo][@"data"][@"member_username"];
    if (!author) {
        author = @"未登录";
    }
    self.authorLabel.text = author;
    
    //发送队列
    NSInteger postCount = [CDFDraft allDrafts].count;
    if (postCount <= 0) {
        self.postListFlagView.hidden = YES;
    } else {
        self.postListFlagView.hidden = NO;
    }
    self.postCountLabel.text = [NSString stringWithFormat:@"%d",postCount];
}

#pragma mark UIViewController Super Methods
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"设置"];
    BOOL isSinaOauth = [UMSocialAccountManager isOauthWithPlatform:UMShareToSina];
    BOOL isTCoauth = [UMSocialAccountManager isOauthWithPlatform:UMShareToTencent];
    self.sinaBindLabel.text = isSinaOauth ? @"已绑定" : @"未绑定";
    self.tencentBindLabel.text = isTCoauth ? @"已绑定" : @"未绑定";
    [self addNotification];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"设置"];
}

- (void)dealloc
{
    [self removeNotification];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([Common systemVersion] < 7.0) {
        UIView *bg = [[UIView alloc] initWithFrame:self.tableView.bounds];
        bg.backgroundColor = bgColor;
        self.tableView.backgroundView = bg;
    } else {
        UIEdgeInsets insets = UIEdgeInsetsMake(-25, 0, 0, 0);
        self.tableView.contentInset = insets;
        self.view.backgroundColor = bgColor;
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        
    }
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 30)];
    label.text = @"设置";
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = TITLEGRAY;
    self.navigationItem.titleView = label;
    
    
    //去掉底部多余的cell
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectZero];
    CGRect footerRect = self.view.bounds;
    footerRect.size.height = 10;
    footerView.frame = footerRect;
    footerView.backgroundColor = [UIColor clearColor];
    self.tableView.tableFooterView = footerView;
    
    self.postListFlagView.layer.cornerRadius = 6;
    self.checkVersionIconView.layer.cornerRadius = 6;
    
    [self loadData];
    [self switchConfig];
}
//设置界面初始化时，从本地读区当前用户设置信息
- (void)switchConfig{
    NSDictionary *dict = [[CDFMe shareInstance] getSetupInfo];
    [self.shakeSwitch setOn:[dict[ISSHAKE] boolValue] animated:NO];
    [self.voiceSwitch setOn:[dict[ISSOUND] boolValue] animated:NO];
    [CDFLocationManager sharedInstance].isAllowLocation = [dict[ISLOCATION] boolValue];
    [self.locationSwitch setOn:[[CDFLocationManager sharedInstance] isAllowLocation] animated:NO];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return (self.isLogin ? [super numberOfSectionsInTableView:tableView] : [super numberOfSectionsInTableView:tableView] - 1);
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 10)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CDFClearBgCell *cell = (CDFClearBgCell*)[super tableView:tableView cellForRowAtIndexPath:indexPath];
    
    cell.isFirst = (indexPath.row == 0);
    if (indexPath.section == 2 && indexPath.row == 6+1) {
        NSDictionary* infoDict =[[NSBundle mainBundle] infoDictionary];
        NSString* versionNum =[infoDict objectForKey:@"CFBundleVersion"];
        self.versionInfo.text = [NSString stringWithFormat:@"%@", versionNum ];
        cell.userInteractionEnabled = NO;
    }
    return cell;
}

#pragma mark UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 2 && indexPath.row == 2+2) {
        //意见反馈
        [MobClick event:@"Feedback"];
        [UMFeedback showFeedback:self withAppkey:kUmeng_Appkey];
        
    } else if (indexPath.section == 2 && indexPath.row == 4+1) {
        //喜欢我就打个分吧~
        [MobClick event:@"Rate"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/yan-yu/id933071401?l=zh&ls=1&mt=8"]];
    } else if (indexPath.section == 2 && indexPath.row == 5+1) {
        //关于眼遇
        [MobClick event:@"AboutUs"];
        [self presentModalViewController:self.welcomeScrollView animated:YES];
        
    } else if (indexPath.section == 2 && indexPath.row == 6+1) {
        
        //
        //        NSLog(@"%@", text);
        //        //版本信息
        //        UIAlertView *infoAlerView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"版本信息(%@)", versionNum] message:[NSString stringWithFormat:@"当前版本号为%@, 请关注新版本的更新, 体验更精彩的眼遇", versionNum] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        //
        //        infoAlerView.tag = 200;
        //        [infoAlerView show];
        
        
        
        
        //    } else if (indexPath.section == 2 && indexPath.row == 1) {
        //
        //        if (![UMSocialAccountManager isOauthWithPlatform:UMShareToSina]) {
        //
        //            [MobClick event:@"Login" label:@"微博登录"];
        //            UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToSina];
        //            snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response) {
        //            });
        //
        //        }else{
        //
        //            UIAlertView *sinaAlertView = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"您要解除新浪微博绑定吗?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        //            sinaAlertView.tag = 100;
        //            [sinaAlertView show];
        //        }
        //    }else if (indexPath.section == 2 && indexPath.row == 2) {
        //
        //        if (![UMSocialAccountManager isOauthWithPlatform:UMShareToTencent]) {
        //
        //            [MobClick event:@"Login" label:@"QQ登录"];
        //            UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToTencent];
        //            snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response) {
        //            });
        //        }else{
        //            UIAlertView *tencentAlertView = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"您要解除腾讯微博绑定吗?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        //            tencentAlertView.tag = 101;
        //            [tencentAlertView show];
        //        }
    } else if (indexPath.row == 2 && indexPath.section == 1) {
        if (![CDFMe shareInstance].isLogin) {
            [SVProgressHUD showWithStatus:@"请登录后再进行此操作!"];
        } else {
            NewMimaViewController *mimaVC = [[NewMimaViewController alloc] init];
            [self.navigationController pushViewController:mimaVC animated:YES];
        }
    }
}

//- (void)appUpdate:(NSDictionary *)appInfo{
//
//    NSLog(@"%@",appInfo);
//    if ([[appInfo objectForKey:@"update"] isEqualToString:@"YES"]) {
//
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"已经是最新版本" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
//        alertView.tag = 103;
//        [alertView show];
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            [alertView dismissWithClickedButtonIndex:0 animated:YES];
//        });
//    }else{
//
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"新版本%@",[appInfo objectForKey:@"current_version"]] message:[appInfo objectForKey:@"update_log"] delegate:self cancelButtonTitle:@"忽略" otherButtonTitles:@"更新", nil];
//        alertView.tag = 104;
//        _url = [appInfo objectForKey:@"path"];
//        [alertView show];
//    }
//}

#pragma mark <UIAlertViewDelegate>
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 100 && buttonIndex == 1) {
        
        [[UMSocialDataService defaultDataService] requestUnOauthWithType:UMShareToSina  completion:^(UMSocialResponseEntity *response){
            self.sinaBindLabel.text = @"未绑定";
        }];
        
    }else if (alertView.tag == 101 && buttonIndex == 1){
        
        [[UMSocialDataService defaultDataService] requestUnOauthWithType:UMShareToTencent  completion:^(UMSocialResponseEntity *response){
            NSLog(@"%@",response);
            self.tencentBindLabel.text = @"未绑定";
        }];
    }
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController respondsToSelector:@selector(userInfo)]) {
        [segue.destinationViewController setValue:self.userInfo forKey:@"userInfo"];
    }
}
@end






