//
//  CDFReplyViewController.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-12-11.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFReplyViewController.h"
//#import "CTAssetsPickerController.h"
#import "ZYQAssetPickerController.h"
#import "CDFSpeech.h"
#import "CDFLocationManager.h"


@interface CDFReplyViewController ()<UITextFieldDelegate, UITextViewDelegate, UITableViewDataSource, UITableViewDelegate, /*CTAssetsPickerControllerDelegate*/ZYQAssetPickerControllerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIAlertViewDelegate>{
    BOOL isPostNoti;
}
@property (strong, nonatomic) IBOutlet UITableView *contentTableView;
@property (strong, nonatomic) IBOutlet UIView *keyboardToolbar;
@property (strong, nonatomic) IBOutlet UIButton *camButton;
@property (strong, nonatomic) IBOutlet UIButton *albumButton;
@property (strong, nonatomic) IBOutlet UIButton *hideKeyboardButton;
@property (strong, nonatomic) IBOutlet UIButton *emojiButton;
@property (strong, nonatomic) IBOutlet UIButton *boardButton;
@property (strong, nonatomic) IBOutlet UIView *footerView;

@property (strong, nonatomic) NSMutableArray *attachments;
@property (strong, nonatomic) IBOutlet UIButton *voiceButton;

- (IBAction)buttonClicked:(id)sender;
@end

@implementation CDFReplyViewController {
    id _firstResponder;
    CGRect _contentTableViewRect;
    NSUInteger _uploadCursor;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

/**
 *  发帖cell数据源
 *
 *  @return 可变数组
 */
- (NSMutableArray *)inputDataSource
{
    if (!_inputDataSource) {
        _inputDataSource = [NSMutableArray array];
        
        //如果数据源为空，说明是新建发帖
        NSMutableDictionary *initData = [NSMutableDictionary dictionaryWithDictionary:[self createEmptyTextInputCell]];
        [initData setObject:[NSNumber numberWithBool:NO] forKey:@"editable"];
        [initData setObject:[NSNumber numberWithFloat:300.0f] forKey:@"height"];
        [_inputDataSource addObject:initData];
    }
    
    return _inputDataSource;
}

- (void)setAuthor:(NSString *)author
{
    _author = author;
    self.navigationItem.title = [NSString stringWithFormat:@"回复%@",_author];
}

- (NSMutableArray *)attachments
{
    if (!_attachments) {
        _attachments = [NSMutableArray new];
    }
    return _attachments;
}

/**
 *  初始化一个文本输入框
 *
 *  @return NSDictionary
 */
- (NSDictionary *)createEmptyTextInputCell
{
    NSMutableDictionary *initData = [NSMutableDictionary dictionary];
    [initData setObject:[NSNumber numberWithFloat:44] forKey:@"height"];
    
    //类型为 Text 和 Image
    [initData setObject:@"Text" forKey:@"type"];
    [initData setObject:@"" forKey:@"content"];
    [initData setObject:[NSNumber numberWithBool:YES] forKey:@"editable"];
    
    return initData;
}

- (void)dealTableViewFooter
{
    self.contentTableView.tableFooterView = nil;
    CGFloat footerHeight = CGRectGetHeight(self.contentTableView.frame) - self.contentTableView.contentSize.height;
    if (footerHeight > 0) {
        CGRect rect = self.footerView.frame;
        rect.size.height = footerHeight;
        self.footerView.frame = rect;
        self.contentTableView.tableFooterView = self.footerView;
    }
}

/**
 *  定位输入光标
 *
 *  @param indexPath NSIndexPath Object
 */
- (void)locationFirstResponderFromIndexPath:(NSIndexPath *)indexPath
{
    if ([self.inputDataSource[indexPath.row][@"type"] isEqualToString:@"Image"]) {
        
        if (indexPath.row < self.inputDataSource.count - 1) {
            //如果点击的不是最后一个
            id control = self.inputDataSource[indexPath.row + 1][@"control"];
            if ([control isKindOfClass:[UITextView class]]) {
                //如果下一行是textview
                [control becomeFirstResponder];
            } else {
                [self createNewControlForInputAtIndexPath:indexPath];
            }
        } else {
            [self createNewControlForInputAtIndexPath:indexPath];
        }
    } else {
        id control = self.inputDataSource[indexPath.row][@"control"];
        if ([control isKindOfClass:[UITextView class]]) {
            //如果下一行是textview
            [control becomeFirstResponder];
        }
    }
}

/** *  @创建一个空的文本cell并focus
 *
 *  @param indexPath NSIndexPath
 */
- (void)createNewControlForInputAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *initData = [NSMutableDictionary dictionaryWithDictionary:[self createEmptyTextInputCell]];
    [self.inputDataSource insertObject:initData atIndex:indexPath.row + 1];
    [self.contentTableView reloadData];
    
    [self.contentTableView setContentOffset:CGPointMake(0, self.contentTableView.contentSize.height - CGRectGetHeight(self.contentTableView.bounds)) animated:NO];
    NSIndexPath *ip = [NSIndexPath indexPathForRow:indexPath.row + 1 inSection:indexPath.section];
    [self performSelector:@selector(locationFirstResponderFromIndexPath:) withObject:ip afterDelay:.15];
}

- (IBAction)focus:(id)sender {
    [self locationFirstResponderFromIndexPath:[NSIndexPath indexPathForRow:(self.inputDataSource.count - 1) inSection:0]];
}

- (IBAction)send:(id)sender {
    if (![CDFMe shareInstance].isLogin) {
        UIStoryboard *user = [UIStoryboard storyboardWithName:@"welcome" bundle:nil];
        [self presentModalViewController:user.instantiateInitialViewController animated:YES];
        return;
    }
    if (self.inputDataSource.count <= 1 && [self.inputDataSource[0][@"content"] length] <= 0) {
        [SVProgressHUD showErrorWithStatus:@"多少说两句吧！"];
        return;
    }
    [self prepareSend];
    //已经点击一次发送后先关掉按钮的用户交互防治一贴多发
//    [sender setUserInteractionEnabled:NO];
    UIBarButtonItem *itm = (UIBarButtonItem *)sender;
    itm.enabled = NO;
    [sender setTag:999];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendOK) name:kPOST_SUCCESS object:nil];
}
//发帖成功后打开发送按钮的用户交互
- (void)sendOK{
    
    [(UIBarButtonItem *)[self.view viewWithTag:999] setEnabled:YES];
}


- (IBAction)buttonClicked:(id)sender
{
    
    if ([sender isEqual:_hideKeyboardButton]) {
        //收起键盘
        [_firstResponder resignFirstResponder];
        [self.contentTableView reloadData];
        [self dealTableViewFooter];
    } else if ([sender isEqual:_albumButton]) {
        //从相册多选照片
        /*
        [_firstResponder resignFirstResponder];
        CTAssetsPickerController *picker = [[CTAssetsPickerController alloc] init];
        picker.maximumNumberOfSelection = 9;
        picker.assetsFilter = [ALAssetsFilter allPhotos];
        picker.delegate = self;
        
        [self presentViewController:picker animated:YES completion:NULL];
         */
        if ([Common systemVersion] >= 6.0) {
            ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] init];
            picker.maximumNumberOfSelection = 10;
            picker.assetsFilter = [ALAssetsFilter allPhotos];
            picker.showEmptyGroups=NO;
            picker.delegate=self;
            picker.selectionFilter = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
                if ([[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypeVideo]) {
                    NSTimeInterval duration = [[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyDuration] doubleValue];
                    return duration >= 5;
                } else {
                    return YES;
                }
            }];
            
            [self presentViewController:picker animated:YES completion:NULL];
        } else {
            //[SVProgressHUD showErrorWithStatus:@"Comming soon!"];
            UIImagePickerController *camPick = [[UIImagePickerController alloc] init];
            camPick.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
            camPick.delegate = self;
            //[self presentModalViewController:camPick animated:YES];
            [self presentViewController:camPick animated:YES completion:^{
                // scroll to the end - hack
                UIView *imagePickerView = camPick.view;
                
                UIView *view = [imagePickerView hitTest:CGPointMake(5,5) withEvent:nil];
                while (![view isKindOfClass:[UIScrollView class]] && view != nil) {
                    // note: in iOS 5, the hit test view is already the scroll view. I don't want to rely on that though, who knows
                    // what Apple might do with the ImagePickerController view structure. Searching backwards from the hit view
                    // should always work though.
                    //NSLog(@"passing %@", view);
                    view = [view superview];
                }
                
                if ([view isKindOfClass:[UIScrollView class]]) {
                    //NSLog(@"got a scroller!");
                    UIScrollView *scrollView = (UIScrollView *) view;
                    // check what it is scrolled to - this is the location of the initial display - very important as the image picker
                    // actually slides under the navigation bar, but if there's only a few images we don't want this to happen.
                    // The initial location is determined by status bar height and nav bar height - just get it from the picker
                    CGPoint contentOffset = scrollView.contentOffset;
                    CGFloat y = MAX(contentOffset.y, [scrollView contentSize].height-scrollView.frame.size.height);
                    CGPoint bottomOffset = CGPointMake(0, y);
                    [scrollView setContentOffset:bottomOffset animated:NO];
                }
            }];
        }
    } else if ([sender isEqual:_camButton]) {
        //摄像头拍照
#if !TARGET_IPHONE_SIMULATOR
        UIImagePickerController *camPick = [[UIImagePickerController alloc] init];
        camPick.sourceType = UIImagePickerControllerSourceTypeCamera;
        camPick.delegate = self;
        [self presentModalViewController:camPick animated:YES];
#endif
    } else if ([sender isEqual:_emojiButton]) {
        //表情
    } else if ([sender isEqual:_boardButton]) {
        //板块选择
    } else if ([sender isEqual:_voiceButton]) {
        //语音
        [_firstResponder resignFirstResponder];
        CDFSpeech *speech = [CDFSpeech new];
        [speech addTarget:self action:@selector(speechChanged:) forControlEvents:UIControlEventValueChanged];
        [speech showInView:self.navigationController.view];
    }
}

- (void)speechChanged:(CDFSpeech *)speech
{
    
    NSString *msg = [NSString stringWithFormat:@"%@%@",[_firstResponder text],speech.message];
    [_firstResponder setText:msg];
//    NSLog(@"speech message : %@",msg);
    [speech dismiss:nil];
    [_firstResponder becomeFirstResponder];
}

- (void)scrollToInput:(NSInteger)tag
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:tag inSection:0];
    [self.contentTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)handlePostResult:(NSNotification *)notification
{
    if ([notification.name isEqualToString:kPOST_SUCCESS]) {
        //发帖成功
        
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//	NSLog(@"%@ %@ %@",self.quote, self.tid, self.displayViewController);
    self.contentTableView.editing = YES;
    self.contentTableView.allowsSelectionDuringEditing = YES;
    
    _contentTableViewRect = self.contentTableView.frame;
    if ([Common systemVersion] < 7.0) {
        self.navigationItem.leftBarButtonItem.tintColor = self.navigationController.navigationBar.tintColor;
        self.navigationItem.rightBarButtonItem.tintColor = self.navigationController.navigationBar.tintColor;
    }
    
    //判断是否为第一个试图控制器，如果是，则加一个取消按钮到左边导航上，如果否则不加
    if (self == [self.navigationController.viewControllers objectAtIndex:0]) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelPosting:)];
    }
    if ([Common systemVersion] < 7.0) {
        
        self.navigationItem.rightBarButtonItem.tintColor = self.navigationController.navigationBar.tintColor;
        self.navigationItem.backBarButtonItem.tintColor = self.navigationController.navigationBar.tintColor;
    } else {
        
        self.navigationController.navigationBar.tintColor = [UIColor blackColor];
        self.navigationItem.rightBarButtonItem.tintColor = self.navigationController.navigationBar.tintColor;
    }
}


- (void)viewWillAppear:(BOOL)animated
{
    
    isPostNoti = NO;
    CDFLocationManager *locationManager = [CDFLocationManager sharedInstance];
    [locationManager locationURL];
    
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"回复"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handlePostResult:) name:kPOST_SUCCESS object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handlePostResult:) name:kPOST_FAIL object:nil];
    //[(CDFTabBarController*)self.tabBarController setHidden:YES];
    [self addKeyBoardNotification];
    if ([Common systemVersion] >= 7.0 && [self.navigationController isKindOfClass:NSClassFromString(@"CDFNavigationBar")]) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
        [self setNeedsStatusBarAppearanceUpdate];
        
    }
}
- (void)viewDidAppear:(BOOL)animated {
    CDFLocationManager *locationManager = [CDFLocationManager sharedInstance];
    [locationManager locationURL];
    [super viewDidAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"回复"];
    [_firstResponder resignFirstResponder];
    if (!isPostNoti) {
        [[NSNotificationCenter defaultCenter] postNotificationName:self.tid object:nil userInfo:@{@"status":@"fail"}];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 *  @取消发帖
 *
 *  @param sender 取消发帖的sender
 */
- (IBAction)cancelPosting:(id)sender
{
//    NSLog(@"%@", self.navigationController.class);
    if ([Common systemVersion] >= 7.0 && [self.navigationController isKindOfClass:NSClassFromString(@"CDFNavigationBar")]) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque animated:YES];
        isPostNoti = YES;
        [self setNeedsStatusBarAppearanceUpdate];
    }
    if (self.isPush) {
        if (!isPostNoti) {
            [[NSNotificationCenter defaultCenter] postNotificationName:self.tid object:nil userInfo:@{@"status":@"fail"}];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [self dismissModalViewControllerAnimated:YES];

    }
}

#pragma mark UIKeyboardController Listener

//监听键盘隐藏和显示事件
- (void)addKeyBoardNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowOrHide:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowOrHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)keyboardWillShowOrHide:(NSNotification *)notification {
    
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    BOOL isShow = [[notification name] isEqualToString:UIKeyboardWillShowNotification] ? YES : NO;
    
    UIEdgeInsets insets = UIEdgeInsetsMake(0, 0, 0, 0);
    
    CGRect animateRect;
    
    if (isShow) {
        self.contentTableView.tableFooterView = nil;
        
        
        animateRect = CGRectMake(0, CGRectGetHeight(self.view.bounds) - CGRectGetHeight(_keyboardToolbar.bounds) - keyboardSize.height, CGRectGetWidth(_keyboardToolbar.bounds), CGRectGetHeight(_keyboardToolbar.bounds));
        insets = UIEdgeInsetsMake(0, 0, keyboardSize.height + CGRectGetHeight(_keyboardToolbar.bounds), 0);
    }else{
        [self dealTableViewFooter];
        animateRect = CGRectMake(0, CGRectGetHeight(self.view.bounds)+CGRectGetHeight(_keyboardToolbar.bounds), CGRectGetWidth(self.view.bounds), CGRectGetHeight(_keyboardToolbar.bounds));
    }
    
    _keyboardToolbar.hidden = !isShow;
    
    [UIView animateWithDuration:.25 animations:^{
        _keyboardToolbar.frame = animateRect;
    }];
    
    self.contentTableView.contentInset = insets;
    
}

//注销监听事件
- (void)removeKeyBoardNotification {
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark UIImagePickControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissModalViewControllerAnimated:YES];
    
    @autoreleasepool {
        if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
            [SVProgressHUD showWithStatus:@"请稍后..." maskType:SVProgressHUDMaskTypeGradient];
            ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
            UIImage *image = [Common fixRotation:[info objectForKey:UIImagePickerControllerOriginalImage]];
            [library writeImageToSavedPhotosAlbum:image.CGImage orientation:(ALAssetOrientation)image.imageOrientation completionBlock:^(NSURL *assetURL, NSError *error )
             {
                 [library assetForURL:assetURL
                          resultBlock:^(ALAsset *asset )
                  {
                      [self insertImagesToCell:[NSArray arrayWithObject:asset]];
                      [SVProgressHUD dismiss];
                  }
                  
                         failureBlock:^(NSError *error )
                  {
                      [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
//                      NSLog(@"Error loading asset");
                  }];
             }];
        } else {
            ALAssetsLibrary *lb = [ALAssetsLibrary new];
            [lb assetForURL:[info objectForKey:UIImagePickerControllerReferenceURL] resultBlock:^(ALAsset *asset) {
                [self insertImagesToCell:[NSArray arrayWithObject:asset]];
            } failureBlock:^(NSError *error) {
                [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
            }];
        }
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissModalViewControllerAnimated:YES];
    [_firstResponder becomeFirstResponder];
}

#pragma mark - ZYQAssetPickerController Delegate
-(void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets
{
    [_firstResponder resignFirstResponder];
    [self insertImagesToCell:[NSArray arrayWithArray:assets]];
}

-(void)assetPickerControllerDidCancel:(ZYQAssetPickerController *)picker
{
    [_firstResponder becomeFirstResponder];
}

/*
#pragma mark CTAssetsPickerControllerDelegate
- (void)assetsPickerController:(CTAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets
{
    [_firstResponder resignFirstResponder];
    [self insertImagesToCell:[NSArray arrayWithArray:assets]];
}

- (void)assetsPickerControllerDidCancel:(CTAssetsPickerController *)picker
{
    [_firstResponder becomeFirstResponder];
}
 */

#pragma mark UITextViewDelegate
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    _keyboardToolbar.hidden = NO;
    _firstResponder = textView;
    [self updateTextViewCellHeight:textView];
    [self scrollToInput:textView.tag];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
}

- (void)textViewDidChange:(UITextView *)textView
{
    [self updateTextViewCellHeight:textView];
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
//    NSLog(@"%@",text);
//    
//    NSLog(@"%@",@{@"123":text});
    
    if ([[[UITextInputMode currentInputMode]primaryLanguage] isEqualToString:@"emoji"]) {
        return NO;
    }
    return YES;
    
    if ([self isContainsEmoji:text]) {
        return NO;
    }
    return YES;
    
}

#pragma mark UITableViewDatasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.inputDataSource[indexPath.row][@"height"] floatValue];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.inputDataSource.count;
}

- (CDFPostingViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIndentifer = [NSString stringWithFormat:@"%@",self.inputDataSource[indexPath.row][@"type"]];
    CDFPostingViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifer];
    
    if ([cellIndentifer isEqualToString:@"Text"]) {
        cell.contentTextArea.delegate = self;
        cell.contentTextArea.tag = indexPath.row;
        cell.contentTextArea.text = [NSString stringWithFormat:@"%@",self.inputDataSource[indexPath.row][@"content"]];
        [self.inputDataSource[indexPath.row] setObject:cell.contentTextArea forKey:@"control"];
        if (!_firstResponder) {
            [cell.contentTextArea becomeFirstResponder];
        }
    } else if ([cellIndentifer isEqualToString:@"Image"]) {
        
        CDFAttachment *attach = self.inputDataSource[indexPath.row][@"content"];
        cell.attachmentImageView.image = [UIImage imageWithData:[attach getThumbnailData]];
        [self.inputDataSource[indexPath.row] setObject:cell.attachmentImageView forKey:@"control"];
    }
    
    return cell;
}

#pragma mark UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"%@",self.inputDataSource[indexPath.row]);
    [self locationFirstResponderFromIndexPath:indexPath];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView
           editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.inputDataSource[indexPath.row][@"editable"] boolValue] ? UITableViewCellEditingStyleDelete : UITableViewCellEditingStyleNone;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"%d",[self.inputDataSource[indexPath.row][@"editable"] boolValue]);
    return [self.inputDataSource[indexPath.row][@"editable"] boolValue];
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self.inputDataSource[indexPath.row][@"editable"] boolValue];
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)
sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    NSUInteger fromRow = [sourceIndexPath row];
    NSUInteger toRow = [destinationIndexPath row];
    
    //    从数组中读取需要移动行的数据
    id object = [self.inputDataSource objectAtIndex:fromRow];
    //    在数组中移动需要移动的行的数据
    [self.inputDataSource removeObjectAtIndex:fromRow];
    //    把需要移动的单元格数据在数组中，移动到想要移动的数据前面
    [self.inputDataSource insertObject:object atIndex:toRow];
    [self.contentTableView reloadData:YES animateFrom:kCATransitionFade];
    //NSLog(@"move %d to %d",fromRow, toRow);
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"删除本段";
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //删除一行
        [self.inputDataSource removeObjectAtIndex:indexPath.row];
//        [tableView beginUpdates];
//        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationBottom];
//        [tableView endUpdates];
        [tableView reloadData];
        [self dealTableViewFooter];
    }
}

/**
 *  更新textView高度
 *
 *  @param textView
 */
- (void)updateTextViewCellHeight:(UITextView *)textView
{
    [self.inputDataSource[textView.tag] setObject:[NSNumber numberWithFloat:textView.contentSize.height+10] forKey:@"height"];
    //更新内容
    [self.inputDataSource[textView.tag] setObject:textView.text forKey:@"content"];
    
    //定位光标到textview的底部
    if(textView.text.length > 0 ) {
        NSRange bottom = NSMakeRange(textView.text.length -1, 1);
        [textView scrollRangeToVisible:bottom];
    }
}

/**
 *  插入图片数组
 *
 *  @param images NSArray Object
 */
- (void)insertImagesToCell:(NSArray *)assets
{
    [self appendPhotoesFromArray:assets];
    [self.contentTableView reloadData];
    [self dealTableViewFooter];
}

/**
 *  向帖子内容里面添加图片
 *
 *  @param assets 图片资源列表
 *
 *  @return 需要增加的indexPaths数组
 */
- (NSArray *)appendPhotoesFromArray:(NSArray *)assets
{
    NSMutableArray *indexPaths = [NSMutableArray array];
    
    int inputCount = self.inputDataSource.count;
    
    for (ALAsset *asset in assets) {
        
        @autoreleasepool {
            UIImage *thumbnail = [UIImage imageWithCGImage:[asset thumbnail]];
            
            //修正图片方向
            UIImageOrientation orientation = UIImageOrientationUp;
            NSNumber* orientationValue = [asset valueForProperty:@"ALAssetPropertyOrientation"];;
            if (orientationValue != nil) {
                orientation = [orientationValue intValue];
            }
            
            UIImage *hdImage = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullResolutionImage] scale:1 orientation:orientation];

            CGSize hdSize = hdImage.size;
            CGSize hdnewSize = CGSizeZero;
            if (hdSize.width > 799.0f && hdSize.width > 0) {
                //处理图片的尺寸，以便节省流量
                hdnewSize.width = 799.0f;
                hdnewSize.height = 20000.0f;
                hdImage = [Common resizeImage:hdImage fitSize:hdnewSize];
            }
            NSString *fileName = [asset defaultRepresentation].filename;
            
            CDFAttachment *attach = [[CDFAttachment alloc] initWithFileName:fileName thumbnail:thumbnail hdImage:hdImage];
            
            NSDictionary *dic = @{@"type": @"Image",@"content":attach,@"height":@"100",@"editable":[NSNumber numberWithBool:YES]};
            NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:dic];
            [self.inputDataSource addObject:dictionary];
        }
    }
    
    for (int i = inputCount; i < assets.count + inputCount; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        [indexPaths addObject:indexPath];
    }
    
    return indexPaths;
}

#pragma mark - 回复相关方法
- (void)confirmAttachments
{
    [self.attachments removeAllObjects];
    for (int i = 0; i < self.inputDataSource.count; i++) {
        if ([self.inputDataSource[i][@"type"] isEqualToString:@"Image"]) {
            [self.attachments addObject:self.inputDataSource[i][@"content"]];
        }
    }
}

- (void)prepareSend
{
    [self confirmAttachments];
    if (self.attachments.count <= 0) {
        [self send];
    } else {
        [self uploadNextAttachment];
    }
}

- (void)setAid:(NSString *)aid ForAttachmentAtIndex:(NSInteger)index
{
    if (index >= self.attachments.count) {
        return;
    }
    for (NSDictionary *dic in self.inputDataSource) {
        if ([dic[@"content"] isEqual:self.attachments[index]]) {
            [(CDFAttachment *)dic[@"content"] setAttachmentID:aid];
            [(CDFAttachment *)dic[@"content"] setIsUploaded:YES];
        }
    }
    [(CDFAttachment *)self.attachments[index] setAttachmentID:aid];
    [(CDFAttachment *)self.attachments[index] setIsUploaded:YES];
}

- (void)uploadNextAttachment
{
    if (_uploadCursor >= self.attachments.count) {
        [self send];
    } else {
        CDFAttachment *attach = self.attachments[_uploadCursor];
        
        if (attach.isUploaded) {
            _uploadCursor++;
            [self uploadNextAttachment];
        }
        
        [SVProgressHUD showWithStatus:[NSString stringWithFormat:@"准备上传第 %d 张图片...",_uploadCursor + 1] maskType:SVProgressHUDMaskTypeGradient];
        NSDictionary *para = @{@"hash": [CDFMe shareInstance].uploadHash,@"uid": [CDFMe shareInstance].uid, @"formhash": [CDFMe shareInstance].formHash};
        
        NSString *url = [NSString stringWithFormat:@"api/mobile/index.php?version=%@&module=forumupload&type=image&simple=1",kApiVersion];

        [[HXHttpClient shareInstance] multipartFormRequestWithMethod:@"POST" path:url parameters:para data:[attach getHDImageData] success:^(id responseObject){

            NSString *result = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            NSArray *resultArray = [result componentsSeparatedByString:@"|"];
            if (resultArray.count == 5) {
                if ([resultArray[1] integerValue] == 0) {
                    //上传成功
                    [self setAid:resultArray[2] ForAttachmentAtIndex:_uploadCursor];
                }
            }
            _uploadCursor ++;
            
            //上传下一个附件
            [self uploadNextAttachment];
        } fail:^(NSError *error) {
//            NSLog(@"%@",error);
            [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
            
            [self performSelector:@selector(uploadNextAttachment) withObject:nil afterDelay:2];
        } progress:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
            if (totalBytesExpectedToWrite <= 0) {
                return;
            }
            //[BWStatusBarOverlay showLoadingWithMessage:[NSString stringWithFormat:@"正在上传第 %d 个附件(%@/%@)",_uploadCursor + 1, [Common formatBytes:totalBytesWritten], [Common formatBytes:totalBytesExpectedToWrite]] animated:NO];
            CGFloat percent = (totalBytesWritten * 1.0f) / (totalBytesExpectedToWrite * 1.0f);
              
            [SVProgressHUD showProgress:percent status:[NSString stringWithFormat:@"\n正在上传第 %d 张图片",_uploadCursor + 1] maskType:SVProgressHUDMaskTypeGradient];
        } fileName:attach.fileName];
    }
}

- (NSDictionary *)prepareToSend
{
    
    CDFLocationManager *locationManager = [CDFLocationManager sharedInstance];
    NSString *locationStr = [locationManager locationURL];
//    NSLog(@"9900%@", locationStr);
    
    NSMutableDictionary *sendData = [NSMutableDictionary new];
    [sendData setObject:self.fid forKey:@"fid"];
    [sendData setObject:self.tid forKey:@"tid"];
    [sendData setObject:self.pid forKey:@"pid"];
    [sendData setObject:@1 forKey:@"mobiletype"];
    if (locationStr != nil && locationManager.isAllowLocation) {
        [sendData setObject:locationStr forKey:@"location"];
//        NSLog(@"676%@", locationStr);
    } else {
//        NSLog(@"767");
    }
    [sendData setObject:[CDFMe shareInstance].formHash forKey:@"formhash"];
    if (self.quote) {
        [sendData setObject:self.quote forKey:@"noticetrimstr"];
    }
    
    NSMutableString *msg = [NSMutableString new];
    for (NSDictionary *dic in self.inputDataSource) {
        //NSLog(@"%@",dic);
        if ([dic[@"type"] isEqualToString:@"Text"]) {
            [msg appendString:dic[@"content"]];
        } else {
            CDFAttachment *attach = dic[@"content"];
            [msg appendFormat:@"[attachimg]%@[/attachimg]",attach.attachmentID];
            [sendData setObject:@"" forKey:[NSString stringWithFormat:@"attachnew[%@][description]", attach.attachmentID]];
        }
    }
    if (msg.length <= 0) {
        return nil;
    }
    [sendData setObject:msg forKey:@"message"];
    
    return sendData;
}

- (void)send
{
    _uploadCursor = 0;//重置上传指针
    
    NSDictionary *dic = [self prepareToSend];
    if (!dic) {
        [SVProgressHUD showErrorWithStatus:@"内容为空！"];
        return;
    }
//    NSLog(@"self.uid:%@", self.uid);
    [dic setValue:self.uid forKey:@"repuid"];
    if ([self.postType isEqualToString:@"1"]) {
        [dic setValue:self.pid forKey:@"reppid"];
    }

    NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=sendreply&submodule=checkpost&replysubmit=yes&handlekey=fastpost",kApiVersion];
    [SVProgressHUD showWithStatus:@"回复中，请稍后..." maskType:SVProgressHUDMaskTypeGradient];
    [[HXHttpClient shareInstance] postURL:url postData:dic success:^(id responseObject) {
        if ([responseObject[@"Message"][@"messageval"] isEqualToString:@"post_reply_succeed"]) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kMESSAGE_BADGE object:nil userInfo:responseObject[@"Variables"][@"newmessage2"]];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReturnAndRefreshData" object:responseObject[@"Variables"][@"floor"]];
            [[NSNotificationCenter defaultCenter] postNotificationName:self.tid object:nil userInfo:@{@"status":@"success"}];
            isPostNoti = YES;
            
            //[SVProgressHUD showSuccessWithStatus:responseObject[@"Message"][@"messagestr"]];
            [SVProgressHUD showSuccessWithStatus:@"回复成功"];
            self.displayViewController.pid = responseObject[@"Variables"][@"pid"];
            [self cancelPosting:nil];
            
        } else if ([responseObject[@"Message"][@"messageval"] isEqualToString:@"replyperm_login_nopermission"]||[responseObject[@"Message"][@"messageval"] isEqualToString:@"replyperm_login_nopermission//1"]) {
            
            [SVProgressHUD showErrorWithStatus:@"登录过期，请重新登录!"];
            [Common logout];
        }else if ([responseObject[@"Message"][@"messageval"] isEqualToString:@"post_reply_mod_succeed"] || [responseObject[@"Message"][@"messageval"] isEqualToString:@"post_newbie_span"]){
        
            [SVProgressHUD showSuccessWithStatus:responseObject[@"Message"][@"messagestr"]];
            [[NSNotificationCenter defaultCenter] postNotificationName:self.tid object:nil userInfo:@{@"status":@"fail"}];
            isPostNoti = YES;
            [self cancelPosting:nil];
        }
        
    } fail:^(NSError *error) {
//        NSLog(@"%@",error);
        [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
        
    }];
}
- (BOOL)isContainsEmoji:(NSString *)string {
    
    __block BOOL isEomji = NO;
    [string enumerateSubstringsInRange:NSMakeRange(0, [string length]) options:NSStringEnumerationByComposedCharacterSequences usingBlock:
     ^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
         const unichar hs = [substring characterAtIndex:0];
         if (0xd800 <= hs && hs <= 0xdbff) {
             if (substring.length > 1) {
                 const unichar ls = [substring characterAtIndex:1];
                 const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                 if (0x1d000 <= uc && uc <= 0x1f77f) {
                     isEomji = YES;
                 }
             }
         } else if (substring.length > 1) {
             const unichar ls = [substring characterAtIndex:1];
             if (ls == 0x20e3) {
                 isEomji = YES;
             }
         } else {
             if (0x2100 <= hs && hs <= 0x27ff && hs != 0x263b) {
                 isEomji = YES;
             } else if (0x2B05 <= hs && hs <= 0x2b07) {
                 isEomji = YES;
             } else if (0x2934 <= hs && hs <= 0x2935) {
                 isEomji = YES;
             } else if (0x3297 <= hs && hs <= 0x3299) {
                 isEomji = YES;
             } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50|| hs == 0x231a ) {
                 isEomji = YES;
             }
         }
     }];
    return isEomji;
}
@end
