//
//  LoginWebViewController.m
//  yanyu
//
//  Created by dev@huaxi100.com on 14-10-31.
//  Copyright (c) 2014年 www.huaxi100.com. All rights reserved.
//

#import "LoginWebViewController.h"
#import "UMSocial.h"
#import "Common.h"
#define DOMAIN @"http://bang.dahe.cn/"
@interface LoginWebViewController ()<UIWebViewDelegate>{
    
    UIActivityIndicatorView *_activityIndicatorView;
    AFHTTPClient *httpClient;
}
@property (nonatomic, readonly) NSInteger uid;
@property (nonatomic, readonly) NSString *username;
@property (nonatomic, readonly) NSString *formhash;
@property (nonatomic, readonly) NSString *cookiepre;
//@property (nonatomic, readonly) NSString *hash;
@end

@implementation LoginWebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.bounds.size.height)];
    _webView.delegate = self;
    _webView.backgroundColor = [UIColor whiteColor];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://bang.dahe.cn/connect.php?mod=login&op=init&referer=Mobile_iOS&statfrom=login&oauth_style=mobile"]];
    [self.view addSubview:_webView];
    [_webView loadRequest:request];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
#pragma mark <UIWebViewDelegate>

- (void)webViewDidStartLoad:(UIWebView *)webView{
    
//    NSLog(@"webViewDidStartLoad");
    UIView *backView = [[UIView alloc] initWithFrame:self.view.bounds];
    backView.tag = 108;
    backView.backgroundColor = [UIColor blackColor];
    backView.alpha = 0.5;
    [self.view addSubview:backView];
    
    _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 150, 150)];
    _activityIndicatorView.center = backView.center;
    [_activityIndicatorView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [backView addSubview:_activityIndicatorView];
    [_activityIndicatorView startAnimating];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
//    NSLog(@"webViewDidFinishLoad");
    [_activityIndicatorView stopAnimating];
    UIView *backView = (UIView *)[self.view viewWithTag:108];
    [backView removeFromSuperview];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
//    NSLog(@"webViewDidFailLoad%@",error);
    [_activityIndicatorView stopAnimating];
    UIView *backView = (UIView *)[self.view viewWithTag:108];
    [backView removeFromSuperview];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{

//    NSLog(@"%@",request.URL.absoluteString);
    NSString *url = request.URL.absoluteString;
    if ([url hasPrefix:@"http://wap.dahe.cn"] ||
        [url hasPrefix:[NSString stringWithFormat:@"%@%@",DOMAIN,@"forum.php"]] ||
        [url hasPrefix:[NSString stringWithFormat:@"%@%@",DOMAIN,@"index.php"]] ||
        [url hasPrefix:[NSString stringWithFormat:@"%@%@",DOMAIN,@"portal.php"]] ||
        [url hasPrefix:@"discuz://connect_register_bind_success"] ||
        [url hasPrefix:@"discuz://location_login_succeed"] ||
        [url hasPrefix:@"discuz://register_succeed"]) {
        [[HXHttpClient shareInstance] grabURL:[NSString stringWithFormat:@"api/mobile/?version=%@&module=login&loginsubmit=yes&loginfield=auto&submodule=checkpost&lssubmit=yes",kApiVersion] success:^(id responseObject){
            
//            NSLog(@"%@",responseObject);
//            NSLog(@"登录成功");
            [Common login:responseObject];
            [self.navigationController popViewControllerAnimated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"isLogin" object:nil] ;
        } fail:^(NSError *error) {
//            NSLog(@"登录失败%@",error);
        }];
        return NO;
    }else if ([url rangeOfString:@"/Mobile_iOS"].location != NSNotFound || [url hasPrefix:@"discuz://login_succeed"]){

        [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@api/mobile/index.php?module=connect&mobilemessage=1",DOMAIN]]]];
        return NO;
    }else if ([url hasPrefix:@"discuz://"]){
        
        return NO;
    }else{
    
        return YES;
    }
    
//    if (range.location != NSNotFound) {
//        
//        [self.navigationController popViewControllerAnimated:YES];
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"isLogin" object:nil] ;
//         return NO;
//    }else {
//
//        
//    }
	return YES;
}


@end
