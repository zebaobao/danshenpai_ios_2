//
//  CDFPostingViewController.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-10-31.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDFTabBarController.h"
#import "CDFDraft.h"
#import "CDFPostingViewCell.h"
#import "CDFSpeech.h"
#import <MapKit/MapKit.h>
#import "CDFLocationManager.h"

@interface CDFPostingViewController : UIViewController

@property (strong, nonatomic) NSMutableArray *inputDataSource;
@property (strong, nonatomic) NSString *key;
@property (strong, nonatomic) CDFDraft *draft;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sendButton;

@property (assign ,nonatomic) BOOL isShowViewController;

@end
