//
//  CDFPlazaViewController.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 14-1-22.
//  Copyright (c) 2014年 www.huaxi100.com. All rights reserved.
//

#import "CDFPlazaViewController.h"
#import "CDFWebViewController.h"

@interface CDFPlazaViewController ()<UISearchBarDelegate>
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;

@property (strong, nonatomic) NSArray *adDataSource;
@property (strong, nonatomic) NSArray *kwDataSource;
@property (strong, nonatomic) NSArray *picDatSource;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *keywordsButtons;
@property (strong, nonatomic) id refreshHeaderView;
@end

@implementation CDFPlazaViewController {
    BOOL _isLoading;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (UIButton *)getButtonByTag:(int)tag
{
    __block UIButton *btn = nil;
    [self.keywordsButtons enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj tag] == tag) {
            btn = (UIButton *)obj;
        }
    }];
    return btn;
}

/**
 *  实现refreshHeaderView的get方法
 *
 *  @return refreshHeaderView
 */
- (id)refreshHeaderView
{
    if (!_refreshHeaderView) {
        if ([Common systemVersion] < 6.0) {
            
        } else {
            UIRefreshControl *rc = [[UIRefreshControl alloc] init];
            rc.attributedTitle = [[NSAttributedString alloc] initWithString:@"用力，不要停..."];
            [rc addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
            _refreshHeaderView = rc;
        }
    }
    return _refreshHeaderView;
}

/**
 *  刷新控件的逻辑方法
 *
 *  @param refresh 刷新控件
 */
- (void)refreshView:(UIRefreshControl *)refresh
{
    //刷新的逻辑代码
    if (!_isLoading) {
        refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"我正在很用力的加载..."];
        [self loadData];
    } else {
        return;
    }
}

- (void)setAdDataSource:(NSArray *)adDataSource
{
    _adDataSource = adDataSource;
    [self reloadAdView];
}
- (void)reloadAdView
{
    [self.scrollView.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [obj removeFromSuperview];
    }];
    for (int i = 0; i < self.adDataSource.count; i++) {
        CGRect rect = self.scrollView.bounds;
        rect.origin.x = i * CGRectGetWidth(self.scrollView.bounds);
        
        UIButton *btn = [[UIButton alloc] initWithFrame:rect];
        btn.tag = i;
        UIImageView *iv = [[UIImageView alloc] initWithFrame:btn.bounds];
        //        [iv setImageWithURL:[NSURL URLWithString:self.adDataSource[i][@"pic"]]];
        [iv setImageWithURL:[NSURL URLWithString:self.adDataSource[i][@"pic"]] placeholderImage:[UIImage imageNamed:@"bg_imageloader"]];
        [btn addSubview:iv];
        
        UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.scrollView.bounds) - 30, CGRectGetWidth(self.scrollView.bounds), 30)];
        lb.backgroundColor = [UIColor colorWithWhite:0 alpha:.5];
        lb.textColor = [UIColor whiteColor];
        lb.font = [UIFont systemFontOfSize:15];
        lb.text = [NSString stringWithFormat:@" %@",self.adDataSource[i][@"title"]];
        [btn addSubview:lb];
        [btn addTarget:self action:@selector(adClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:btn];
    }
    self.pageControl.numberOfPages = self.adDataSource.count;
    self.scrollView.contentSize = CGSizeMake(self.adDataSource.count * CGRectGetWidth(self.scrollView.bounds), 0);
}
- (void)adClicked:(id)sender
{
    [MobClick event:@"Plaza" label:@"点击大图"];
    if (self.adDataSource.count > 0) {
        NSString *url = self.adDataSource[[sender tag]][@"url"];
//        NSLog(@"%@", self.adDataSource[[sender tag]]);
        NSDictionary *dic = [Common decodeURL:self.adDataSource[[sender tag]][@"url"]];
//        NSLog(@"url:%@", url);
        
        NSString *newURL = [url substringWithRange:NSMakeRange(0, 4)];
        
        if (![newURL isEqualToString:@"http"]) {
            [self performSegueWithIdentifier:@"pushToDetail" sender:dic];
        } else {
            //获取uid
            CDFMe *me = [CDFMe shareInstance];
            id uid = me.uid;
//            NSLog(@"uid = %@", [NSString stringWithFormat:@"%@", uid]);
            CDFWebViewController *webView = [[CDFWebViewController alloc] init];
            webView.url = [uid isEqualToString:@""] ? self.adDataSource[[sender tag]][@"url"] : [NSString stringWithFormat:@"%@&uid=%@", self.adDataSource[[sender tag]][@"url"], uid];
//            NSLog(@"webView.url = %@", webView.url);
            webView.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:webView animated:YES];
        }
    }
}

- (void)setKwDataSource:(NSArray *)kwDataSource
{
    _kwDataSource = kwDataSource;
    [self reloadKwView];
}
- (void)reloadKwView
{
    [self.keywordsButtons enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [obj setEnabled:NO];
        [obj setTitle:@"" forState:UIControlStateNormal];
    }];
    for (int i = 0; i < MIN(4, self.kwDataSource.count); i++) {
        UIButton *btn = [self getButtonByTag:i];
        [btn addTarget:self action:@selector(kwClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btn setTitle:[NSString stringWithFormat:@"＃%@＃",self.kwDataSource[i][@"title"]] forState:UIControlStateNormal];
        btn.enabled = YES;
    }
}
- (void)kwClicked:(id)sender
{
    [MobClick event:@"Plaza" label:@"点击关键词"];
    [self prepareForSearch:self.kwDataSource[[sender tag]][@"title"] dic:nil index:2];
}

- (void)setPicDatSource:(NSArray *)picDatSource
{
    _picDatSource = picDatSource;
    [self.tableView reloadData];
}

- (void)loadData
{
    if (_isLoading) {
        return;
    }
    _isLoading = YES;
    NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=plaza",kApiVersion];
    [SVProgressHUD showWithStatus:@"请稍后..."];
    [[HXHttpClient shareInstance] grabURL:url success:^(id responseObject) {
        _isLoading = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:kMESSAGE_BADGE object:nil userInfo:responseObject[@"Variables"][@"newmessage2"]];
        if (responseObject[@"Message"]) {
            [SVProgressHUD showErrorWithStatus:responseObject[@"Message"][@"messagestr"]];
            [self refreshView:_refreshHeaderView endRefreshWithMessage:responseObject[@"Message"][@"messagestr"]];
        } else {
            [self refreshView:_refreshHeaderView endRefreshWithMessage:@""];
            [SVProgressHUD dismiss];
            self.adDataSource = [NSArray arrayWithArray:responseObject[@"Variables"][@"plaza_ad"]];
            
            self.kwDataSource = [NSArray arrayWithArray:responseObject[@"Variables"][@"plaza_keywords"]];
            self.picDatSource = [NSArray arrayWithArray:responseObject[@"Variables"][@"plaza_keywords_pic"]];
        }
    } fail:^(NSError *error) {
        [self refreshView:_refreshHeaderView endRefreshWithMessage:@"刷新成功"];
        _isLoading = NO;
        [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
//        NSLog(@"%@",error);
    }];
}

/**
 *  刷新完成以后调用
 *
 *  @param refresh 刷新控件
 *  @param message 显示的消息，如果没有特殊消息显示则会显示默认消息
 */
- (void)refreshView:(UIRefreshControl *)refresh endRefreshWithMessage:(NSString *)message
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM.dd, hh:mm a"];
    NSString *lastUpdated = [NSString stringWithFormat:@"%@ %@", [formatter stringFromDate:[NSDate date]], (message.length > 0 ? message : @"刷新成功")];
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:lastUpdated];
    [refresh endRefreshing];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([Common systemVersion] >= 6.0) {
        self.refreshControl = self.refreshHeaderView;
    }
    self.searchBar.backgroundColor = [UIColor clearColor];
    [self.searchBar.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        //NSLog(@"%@",[obj class]);
        if ([obj isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
            [obj removeFromSuperview];
        }
    }];
    self.navigationItem.titleView = self.searchBar;
    [self loadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"广场"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"广场"];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    [MobClick event:@"Plaza" label:@"搜索"];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelSearch:)];
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [MobClick event:@"Plaza" label:@"有效搜索"];
    [self prepareForSearch:searchBar.text dic:nil index:2];
    [self cancelSearch:nil];
}

- (void)cancelSearch:(id)sender
{
    if (!sender) {
        [MobClick event:@"Plaza" label:@"取消搜索"];
    }
    [self.searchBar resignFirstResponder];
    self.navigationItem.rightBarButtonItem = nil;
}

- (IBAction)changePage:(UIPageControl *)sender {
    //NSLog(@"%d",sender.currentPage);
    [self.scrollView setContentOffset:CGPointMake(sender.currentPage * CGRectGetWidth(self.scrollView.bounds), 0) animated:YES];
}
#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if ([scrollView isEqual: self.scrollView]) {
        int page = scrollView.contentOffset.x / CGRectGetWidth(scrollView.bounds);
        //NSLog(@"%d",page);
        self.pageControl.currentPage = page;
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.picDatSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *indentify = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indentify];
    
    UIImageView *iv = (UIImageView *)[cell.contentView viewWithTag:1];
    //    [iv setImageWithURL:[NSURL URLWithString:self.picDatSource[indexPath.row][@"pic"]]];
    [iv setImageWithURL:[NSURL URLWithString:self.picDatSource[indexPath.row][@"pic"]] placeholderImage:[UIImage imageNamed:@"bg_imageloader"]];
    UILabel *lb = (UILabel *)[cell.contentView viewWithTag:2];
    lb.text = self.picDatSource[indexPath.row][@"title"];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 91;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [MobClick event:@"Plaza" label:@"点击图文列表"];
    
    NSString *str = self.picDatSource[indexPath.row][@"url"];
    NSString *newStr = [str substringWithRange:NSMakeRange(0, 4)];
    
    if (![newStr isEqualToString:@"http"]) {
        //url不是以http开头, 打开帖子
        [self prepareForSearch:nil dic:self.picDatSource[indexPath.row] index:1];
    } else {
        //url以http开头, 打开WebView
        //获取uid
        CDFMe *me = [CDFMe shareInstance];
        id uid = me.uid;
//        NSLog(@"uid = %@", [NSString stringWithFormat:@"%@", uid]);
        CDFWebViewController *webView = [[CDFWebViewController alloc] init];
        webView.url = [uid isEqualToString:@""] ? str : [NSString stringWithFormat:@"%@&uid=%@", str, uid];
//        NSLog(@"webView.url = %@", webView.url);
        webView.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:webView animated:YES];
    }
}

#pragma mark - 搜索相关
- (void)prepareForSearch:(NSString *)keyword dic:(NSDictionary *)dic  index:(NSInteger)index {
    if (index == 1) {
        //1代表直接进入详情页面
        [self performSegueWithIdentifier:@"pushToDetail" sender:dic];
    } else if (index == 2) {
        //2代表进入搜索页面
        [self performSegueWithIdentifier:@"pushToSearchResult" sender:keyword];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([sender isKindOfClass:[NSString class]]) {
        if ([segue.destinationViewController respondsToSelector:@selector(keyword)]) {
            [segue.destinationViewController setValue:sender forKey:@"keyword"];
        }
    } else if([sender isKindOfClass:[NSDictionary class]]) {
        if ([segue.destinationViewController respondsToSelector:@selector(tid)]) {
            //            [segue.destinationViewController setValue:sender[@"tid"] forKey:@"tid"];
            id tt = sender[@"id"] > sender[@"tid"] ? sender[@"id"] : sender[@"tid"];
            
            [segue.destinationViewController setValue:tt forKey:@"tid"];
        }
    }
}

@end
