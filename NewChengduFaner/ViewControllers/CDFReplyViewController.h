//
//  CDFReplyViewController.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-12-11.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDFPostingViewCell.h"
#import "CDFDisplayViewController.h"

@interface CDFReplyViewController : UIViewController
@property (strong, nonatomic) NSMutableArray *inputDataSource;
@property (strong, nonatomic) NSString *uid;//被回帖用户uid
@property (strong, nonatomic) id tid;
@property (strong, nonatomic) id fid;
@property (strong, nonatomic) id pid;
@property (strong, nonatomic) NSString *postType;//type为1,回回复
@property (strong, nonatomic) NSString *author;
@property (strong, nonatomic) NSString *quote;
@property (strong, nonatomic) CDFDisplayViewController *displayViewController;
@property (assign , nonatomic) BOOL isPush;//判断是否是push

@end
