//
//  CDFMeViewController.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-11.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFMeViewController.h"
#import "ChatViewController.h"
#import "CDFMe.h"
#import "ZWDMyFavTieViewController.h"


@implementation CDFMeTableHeaderView

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    CGRect fillRect = rect;
    fillRect.size.height -= 3;
    CGContextSetShadowWithColor(context, CGSizeMake(0, 1), 3, [UIColor colorWithWhite:0 alpha:.15].CGColor);
    [[UIColor whiteColor] setFill];
    CGContextFillRect(context, fillRect);
    CGContextRestoreGState(context);
}

@end

@implementation CDFMeTableFooterView

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    UIColor *lineColor = [UIColor colorWithWhite:0 alpha:.05];
    
    CGFloat offsetY = 19.0f;
    CGFloat offsetX = 15;
    
    //画虚线
    CGContextSaveGState(context);
    CGContextSetLineWidth(context, 1.5);

    CGContextStrokePath(context);
    CGContextRestoreGState(context);
    
    //画实线
    CGContextSaveGState(context);
    
    CGContextSetLineWidth(context, 2.5f);
    [lineColor setStroke];
    CGContextMoveToPoint(context, CGRectGetMinX(rect) + offsetX, CGRectGetMinY(rect));
    CGContextAddLineToPoint(context, CGRectGetMinX(rect) + offsetX, CGRectGetMinY(rect) + offsetY);
    CGContextStrokePath(context);
    CGContextRestoreGState(context);
    
    //画圆
    CGContextSaveGState(context);
    CGContextAddEllipseInRect(context, CGRectMake(CGRectGetMinX(rect) + offsetX - 3, CGRectGetMinY(rect) + offsetY, 6, 6));
    CGContextSetLineWidth(context, 2.5);
    [lineColor setStroke];
    CGContextStrokePath(context);
    CGContextRestoreGState(context);
}

@end

@implementation CDFRoundButton

- (void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    CGPathRef path = createRoundedRectForRect(rect, 4);
    [[UIColor colorWithWhite:228.0f/255.0f alpha:1] setStroke];

    CGContextAddPath(context, path);
    CGContextSetLineWidth(context, 1);
    CGContextStrokePath(context);
    CGPathRelease(path);
    CGContextRestoreGState(context);
    
    path = createRoundedRectForRect(CGRectInset(rect, .5, .5), 2);
    UIColor *startColor = self.highlighted ? [UIColor colorWithWhite:231.0f/255.0f alpha:1] : [UIColor whiteColor];
    UIColor *endColor = self.highlighted ? [UIColor whiteColor] : [UIColor colorWithWhite:240.0f/255.0f alpha:1];
    
    drawLinearGradientForPath(context, path, startColor.CGColor, endColor.CGColor, CGPointMake(CGRectGetMinX(rect), CGRectGetMinY(rect)), CGPointMake(CGRectGetMinX(rect), CGRectGetMaxY(rect)));
    CGPathRelease(path);
    
    draw1PxStroke(context, CGPointMake(CGRectGetMinX(rect) + 2, CGRectGetMinY(rect) + 1), CGPointMake(CGRectGetMaxX(rect) - 2, CGRectGetMinY(rect) + 1), [UIColor whiteColor].CGColor);
}

@end

@interface CDFMeViewController ()<UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *followImage;
@property (weak, nonatomic) IBOutlet UIImageView *chatImage;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *rightBarItem;
@property (weak, nonatomic) IBOutlet UILabel *yanYuMoneyLabel;
@property (weak, nonatomic) IBOutlet UIButton *collectionOfTieZi;
@property (strong, nonatomic) IBOutlet UILabel *authorNameLabel;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (strong, nonatomic) IBOutlet UILabel *signLabel;
@property (strong, nonatomic) NSMutableArray *dataSource;
@property (strong, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIView *backImage;
@property (assign, nonatomic) BOOL isAuthor;
@property (strong, nonatomic, readonly) id authorID;
@property (strong, nonatomic) IBOutlet UILabel *registerTimeLabel;
@property (strong, nonatomic) IBOutlet CDFRoundButton *followButton;
@property (weak, nonatomic) IBOutlet UIView *chatLabel;
@property (weak, nonatomic) IBOutlet UIView *followLabel;
@property (strong, nonatomic) IBOutlet CDFRoundButton *messageButton;
@property (strong, nonatomic) IBOutlet UIButton *detailButton;
@property (strong, nonatomic) IBOutlet UIButton *numberOfFollowerButton;
@property (strong, nonatomic) IBOutlet UIButton *numberOfFriendButton;
@property (strong, nonatomic) IBOutlet CDFRoundButton *signButton;
@property (strong, nonatomic) IBOutlet UIImageView *sexIconImageView;

@property (strong, nonatomic) IBOutlet CDFMeTableHeaderView *headerView;
@property (nonatomic, strong) id refreshHeaderView;
@property (nonatomic, strong) NSCache *heightCache;
@property (nonatomic, strong) NSMutableDictionary *userInfo;
@property (strong, nonatomic) IBOutlet UILabel *joinLabel;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *setupButton;
@property (strong, nonatomic) UIView *longLine;
@property (strong, nonatomic) UIView *firstShotLine;
@property (strong, nonatomic) UIView *secondShotLine;
@property (strong, nonatomic) UIView *thirdShotLine;
@end

@implementation CDFMeViewController {
    NSInteger   _currentPage;
    BOOL        _isLoading;
    BOOL        _hasMore;
    NSInteger   _isFollow;
    BOOL        _isFirstWarn;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];
}

- (void)setup
{
    _currentPage = 1;
    _isLoading = NO;
    _uid = nil;
}

/**
 *  实现refreshHeaderView的get方法
 *
 *  @return refreshHeaderView
 */
- (id)refreshHeaderView
{
    if (!_refreshHeaderView) {
        if ([Common systemVersion] < 6.0) {
            
        } else {
            UIRefreshControl *rc = [[UIRefreshControl alloc] init];
            rc.attributedTitle = [[NSAttributedString alloc] initWithString:@"用力，不要停..."];
            [rc addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
            _refreshHeaderView = rc;
        }
    }
    return _refreshHeaderView;
}

- (NSMutableDictionary *)userInfo {
    if (!_userInfo) {
        _userInfo = [NSMutableDictionary dictionary];
    }
    return _userInfo;
}

- (NSMutableArray *)dataSource
{
    if (!_dataSource) {
        _dataSource = [NSMutableArray new];
    }
    return _dataSource;
}

- (NSCache *)heightCache
{
    if (!_heightCache) {
        _heightCache = [NSCache new];
    }
    return _heightCache;
}

/**
 *  刷新控件的逻辑方法
 *
 *  @param refresh 刷新控件
 */
- (void)refreshView:(UIRefreshControl *)refresh
{
    //刷新的逻辑代码
    if (!_isLoading) {
        refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"我正在很用力的加载..."];
        _currentPage = 1;
        [self loadData];
    } else {
        return;
    }
}

/**
 *  刷新完成以后调用
 *
 *  @param refresh 刷新控件
 *  @param message 显示的消息，如果没有特殊消息显示则会显示默认消息
 */
- (void)refreshView:(UIRefreshControl *)refresh endRefreshWithMessage:(NSString *)message
{
    [self performSelector:@selector(resetLoadFlag) withObject:nil afterDelay:3];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM.dd, hh:mm a"];
    NSString *lastUpdated = [NSString stringWithFormat:@"%@ %@", [formatter stringFromDate:[NSDate date]], (message.length > 0 ? message : @"刷新成功")];
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:lastUpdated];
    [refresh endRefreshing];
}

- (void)resetLoadFlag
{
    _isLoading = NO;
}

//- (IBAction)buttonAction:(id)sender {
//
//    if([sender isEqual:self.signButton]) {
//        
//        [MobClick event:@"Sign"];
//        [self performSegueWithIdentifier:@"pushToSig" sender:sender];
//    }
//}
- (void)loadData
{
//    NSLog(@"%d",[CDFMe shareInstance].isLogin);
    if (![CDFMe shareInstance].isLogin) {
        [self refreshView:_refreshHeaderView endRefreshWithMessage:@"请登录！"];
        return;
    }

    if (_isLoading) {
        return;
    }
    [SVProgressHUD showWithStatus:@"请稍后..."];
    _isLoading = YES;
    NSString *userParam = [NSString stringWithFormat:@"&uid=%@",(_isAuthor ? _authorID : self.uid)];
    
    NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=home%@&page=%d",kApiVersion ,userParam, _currentPage];
    
//    NSLog(@"%@%@",kBaseURL,url);

    
    [[HXHttpClient shareInstance] grabURL:url success:^(id responseObject) {
//        NSLog(@"%@",[Common dictionaryToJSON:responseObject]);
        _isLoading = NO;
//        NSLog(@"%@", responseObject);
        //收藏数
        NSString *collection = responseObject[@"Variables"][@"favcount"];
        //眼遇币
        NSString *yanYuCoin = responseObject[@"Variables"][@"yanyucoin"];
        if (responseObject[@"Message"]) {
            //[SVProgressHUD showErrorWithStatus:responseObject[@"Message"][@"messagestr"]];
            [SVProgressHUD showErrorWithStatus:@"请重新登录！"];
            [self refreshView:_refreshHeaderView endRefreshWithMessage:@"刷新失败"];
        } else {
            [self.userInfo addEntriesFromDictionary:[NSDictionary dictionaryWithDictionary:responseObject[@"Variables"][@"userinfo"]]];
            if (_isAuthor) {
                [self.detailButton setTitle:@"修改资料" forState:UIControlStateNormal];
                [UIView animateWithDuration:.25 animations:^{
                    self.signButton.alpha = 1;
                    self.backImage.alpha = 1;
                }];
                self.detailButton.hidden = YES;
                self.followButton.alpha = 0;
                self.messageButton.alpha = 0;
                self.yanYuMoneyLabel.hidden = NO;
                self.followLabel.alpha = 0;
                self.chatLabel.alpha = 0;
                self.backImage.alpha = 0;
                self.backgroundView.frame = CGRectMake(0, 85, [UIScreen mainScreen].bounds.size.width, 37);
                self.numberOfFollowerButton.frame = CGRectMake(0, 85, 106, 37);
                self.numberOfFriendButton.frame = CGRectMake(106, 85, 108, 37);
                self.detailButton.frame = CGRectMake(214, 85, 106, 37);
                
                self.longLine.frame = CGRectMake(0, 85, CGRectGetWidth(self.headerView.bounds), .5);
                self.firstShotLine.frame = CGRectMake(KDeviceSizeWidth / 4.0, 10, 2, 15);
                self.secondShotLine.frame = CGRectMake(KDeviceSizeWidth / 2.0, 10, 2, 15);
                self.thirdShotLine.frame = CGRectMake(KDeviceSizeWidth / 4.0 * 3.0,10, 2, 15);
//                CGRect rect = [self.tableView cellForRowAtIndexPath:0].frame;
//                NSIndexPath *path = [NSIndexPath indexPathForRow:0 inSection:0];
//                [self.tableView cellForRowAtIndexPath:path].frame = CGRectMake(rect.origin.x - 30, rect.origin.y, rect.size.width, rect.size.height);
                
                CGFloat widthOfBtn = KDeviceSizeWidth / 4.0;
                self.numberOfFriendButton.frame = CGRectMake(0, self.numberOfFriendButton.frame.origin.y, widthOfBtn, self.numberOfFriendButton.frame.size.height);
                self.numberOfFollowerButton.frame = CGRectMake(widthOfBtn, self.numberOfFollowerButton.frame.origin.y, widthOfBtn, self.numberOfFriendButton.frame.size.height);
                self.collectionOfTieZi.frame = CGRectMake(widthOfBtn * 2.0, self.numberOfFriendButton.frame.origin.y, widthOfBtn, self.numberOfFriendButton.frame.size.height);
                self.yanYuMoneyLabel.frame = CGRectMake(widthOfBtn * 3.0, self.numberOfFriendButton.frame.origin.y, widthOfBtn, self.numberOfFriendButton.frame.size.height);
                
                UIView *v = self.tableView.tableHeaderView;
                v.frame =CGRectMake(self.headerView.frame.origin.x, self.headerView.frame.origin.y, self.headerView.frame.size.width, 125);
                self.tableView.tableHeaderView = nil;
                self.tableView.tableHeaderView = v;
                
                self.navigationItem.title = @"我";
                self.navigationItem.rightBarButtonItem = self.setupButton;
            } else {
                UIView *v = self.tableView.tableHeaderView;
                v.frame =CGRectMake(self.headerView.frame.origin.x, self.headerView.frame.origin.y, self.headerView.frame.size.width, 160);
                self.tableView.tableHeaderView = nil;
                self.tableView.tableHeaderView = v;
                
                //self.tableView.tableHeaderView.frame = CGRectMake(self.headerView.frame.origin.x, self.headerView.frame.origin.y, self.headerView.frame.size.width, 60);
                //[self.tableView reloadData];
                self.firstShotLine.frame = CGRectMake(KDeviceSizeWidth / 3.0, 10, 2, 15);
                self.secondShotLine.frame = CGRectMake(KDeviceSizeWidth / 3.0 * 2.0, 10, 2, 15);
                self.thirdShotLine.frame = CGRectMake( KDeviceSizeWidth * -1000,10, 2, 15);
                
                self.title = self.userInfo[@"username"];
                [self.detailButton setTitle:@"详细资料" forState:UIControlStateNormal];
                _isFollow = [responseObject[@"Variables"][@"userinfo"][@"is_follow"] integerValue];
                UIImage *followButtonImage;
                switch (_isFollow) {
                    case 1: {
                        followButtonImage = [UIImage imageNamed:@"followed"];
                        break;
                    }
                    case 2: {
                        followButtonImage = [UIImage imageNamed:@"followed_each"];
                        break;
                    }
                    default: {
                        followButtonImage = [UIImage imageNamed:@"green_plus"];
                        break;
                    }
                }
                self.detailButton.hidden = YES;
                self.yanYuMoneyLabel.frame = self.detailButton.frame;
                self.followImage.image = followButtonImage;
//                [self.followButton setImage:followButtonImage forState:UIControlStateNormal];
                [UIView animateWithDuration:.25 animations:^{
                    self.followButton.alpha = 1;
                    self.messageButton.alpha = 1;
                }];
                self.signButton.alpha = 0;
                self.backImage.alpha = 0;
                self.collectionOfTieZi.hidden = YES;
            }
            
            [self bindHeaderInfo];
            self.numberOfFollowerButton.titleLabel.numberOfLines = 0;
            self.numberOfFriendButton.titleLabel.numberOfLines = 0;
            self.collectionOfTieZi.titleLabel.numberOfLines = 0;
            
            self.numberOfFriendButton.titleLabel.textAlignment = NSTextAlignmentCenter;
            self.numberOfFollowerButton.titleLabel.textAlignment = NSTextAlignmentCenter;
            self.collectionOfTieZi.titleLabel.textAlignment = NSTextAlignmentCenter;

            [self.numberOfFriendButton setTitle:[NSString stringWithFormat:@"%@\n听众", responseObject[@"Variables"][@"userinfo"][@"follower"]] forState:UIControlStateNormal];
            [self.numberOfFollowerButton setTitle:[NSString stringWithFormat:@"%@\n收听", responseObject[@"Variables"][@"userinfo"][@"following"]] forState:UIControlStateNormal];
            [self.collectionOfTieZi setTitle:[NSString stringWithFormat:@"%@\n收藏", collection] forState:UIControlStateNormal];
            [self.yanYuMoneyLabel setText:[NSString stringWithFormat:@"%@\n眼遇币", yanYuCoin]];
            self.yanYuMoneyLabel.numberOfLines = 0;
            self.yanYuMoneyLabel.textAlignment = NSTextAlignmentCenter;
            self.registerTimeLabel.text = [Common formatDate:[responseObject[@"Variables"][@"regdate"] longLongValue]];
            if (_currentPage <= 1) {
                [self.dataSource removeAllObjects];
                [self.heightCache removeAllObjects];
            }
            _hasMore = self.dataSource.count < [responseObject[@"Variables"][@"count"] integerValue];
            if ((NSNull *)responseObject[@"Variables"][@"mylist"] != [NSNull null]) {
                [self.dataSource addObjectsFromArray:[NSArray arrayWithArray:responseObject[@"Variables"][@"mylist"]]];
                [self.tableView reloadData];
                
            }
            
            [SVProgressHUD dismiss];
            [self refreshView:_refreshHeaderView endRefreshWithMessage:@"刷新成功"];
        }
        
    } fail:^(NSError *error) {
        _isLoading = NO;
        [self refreshView:_refreshHeaderView endRefreshWithMessage:@"刷新失败"];
        [SVProgressHUD dismiss];
//        [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
    }];
    
    [self.backgroundView addSubview:self.firstShotLine];
    [self.backgroundView addSubview:self.secondShotLine];
    [self.backgroundView addSubview:self.thirdShotLine];

}

- (void)bindHeaderInfo
{
    [self showAvatar];
    self.authorNameLabel.text = [NSString stringWithFormat:@"%@",self.userInfo[@"username"]];
    self.joinLabel.text = [NSString stringWithFormat:@"%@加入眼遇。",self.userInfo[@"username"]];
    self.signLabel.text = ([_userInfo[@"sightml"] length] <= 0 ? nonSign : [NSString stringWithFormat:@"%@",self.userInfo[@"sightml"]]);
    self.sexIconImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"sex%@",self.userInfo[@"gender"]]];
    CGSize size = [self.authorNameLabel.text sizeWithFont:self.authorNameLabel.font];
    CGRect rect = self.sexIconImageView.frame;
    rect.origin.x = CGRectGetMinX(self.authorNameLabel.frame) + size.width + 5;
//    self.sexIconImageView.frame = rect;
    CGFloat width = [[UIScreen mainScreen] bounds].size.width / 2.0 + size.width / 2.0 + 5;
    CGRect rect1 = CGRectMake(width, self.sexIconImageView.frame.origin.y, self.sexIconImageView.frame.size.width, self.sexIconImageView.frame.size.height);
    self.sexIconImageView.frame = rect1;
}

- (void)handleLogNotification:(NSNotification *)notification
{
    if ([notification.name isEqualToString: kLOGOUT_NOTIFICATION]) {
        //退出登录
        _currentPage = 1;
        _hasMore = NO;
        [self.dataSource removeAllObjects];
        [self.tableView reloadData];
        self.avatarImageView.layer.masksToBounds = YES;
        self.avatarImageView.layer.cornerRadius = 25;
        self.avatarImageView.image = nil;
        self.authorNameLabel.text = @"";
        self.signLabel.text = @"";
        self.registerTimeLabel.text = @"";
        [self.numberOfFollowerButton setTitle:[NSString stringWithFormat:@"0\n听众"] forState:UIControlStateNormal];
        [self.numberOfFriendButton setTitle:[NSString stringWithFormat:@"0\n收听"] forState:UIControlStateNormal];
        [self.collectionOfTieZi setTitle:[NSString stringWithFormat:@"0\n收藏"] forState:UIControlStateNormal];
        self.yanYuMoneyLabel.text = [NSString stringWithFormat:@"0\n眼遇币"];
        self.sexIconImageView.image = nil;
        self.signButton.alpha = 0;
        self.backImage.alpha = 0;
        self.joinLabel.text = @"";
        [self.userInfo removeAllObjects];
    }
    
    if ([notification.name isEqualToString:kLOGIN_NOTIFICATION]) {
        //登录
        _currentPage = 1;
        _hasMore = NO;
        [self prepareForLoad];
        [self loadData];
    }
}

- (void)addNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLogNotification:) name:kLOGIN_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLogNotification:) name:kLOGOUT_NOTIFICATION object:nil];
}

- (void)removeNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLOGIN_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLOGOUT_NOTIFICATION object:nil];
}

- (void)actionSheet:(IBActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=delfollow&fuid=%@",kApiVersion,self.uid];
        [[HXHttpClient shareInstance] grabURL:url success:^(id responseObject) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kMESSAGE_BADGE object:nil userInfo:responseObject[@"Variables"][@"newmessage2"]];

            if ([responseObject[@"Message"][@"messageval"] isEqualToString:@"follow_cancel_succeed"]) {
                [SVProgressHUD showSuccessWithStatus:responseObject[@"Message"][@"messagestr"]];
                _isFollow = 0;
                self.followImage.image = [UIImage imageNamed:@"green_plus"];
//                [self.followButton setImage:[UIImage imageNamed:@"green_plus"] forState:UIControlStateNormal];
            } else {
                [SVProgressHUD showErrorWithStatus:responseObject[@"Message"][@"messagestr"]];
            }
        } fail:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
//            NSLog(@"%@",error);
        }];
    }
}

#pragma mark UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    //下拉加载更多
    if (scrollView.contentSize.height - scrollView.contentOffset.y < CGRectGetHeight(scrollView.frame) + 50) {
        if (_hasMore && !_isLoading) {
            if (self.dataSource.count >0) {
                _currentPage++;
            }
            [self loadData];
        }
    }
}

#pragma mark  -- 懒加载
- (UIView *)longLine {
    if (!_longLine) {
        _longLine = [[UIView alloc] initWithFrame:CGRectMake(0, 120, CGRectGetWidth(self.headerView.bounds), .5)];
    }
    return _longLine;
}
- (UIView *)firstShotLine {
    if (!_firstShotLine) {
        _firstShotLine = [[UIView alloc] initWithFrame:CGRectMake(KDeviceSizeWidth / 4.0, 130, 2, 15)];
        _firstShotLine.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_splt_me"]];
    }
    return _firstShotLine;
}
- (UIView *)secondShotLine {
    if (!_secondShotLine) {
        _secondShotLine = [[UIView alloc] initWithFrame:CGRectMake(KDeviceSizeWidth / 2.0, 130, 2, 15)];
        _secondShotLine.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_splt_me"]];

    }
    return _secondShotLine;
}
- (UIView *)thirdShotLine {
    if (!_thirdShotLine) {
        _thirdShotLine = [[UIView alloc] initWithFrame:CGRectMake(KDeviceSizeWidth / 4.0 * 3.0,130, 2, 15)];
        _thirdShotLine.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_splt_me"]];
    }
    return _thirdShotLine;
}

- (void)layoutLeftNavigationItem {
    UIButton *backBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    [backBtn setImage:[UIImage imageNamed:@"backOrange"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(handleLeftBarBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
}
- (void)handleLeftBarBtn:(UIButton *)btn {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark --
- (void)handleFollowAction:(UITapGestureRecognizer *)tapGesture {
    //点击follow事件
    [MobClick event:@"FollowFromMe"];
    if (_isFollow) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"真的要取消对TA的关注了吗？" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"确定", nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
    } else {
        //添加关注
        NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=addfollow&hash=%@&fuid=%@", kApiVersion, [CDFMe shareInstance].formHash, self.uid];
        
        [[HXHttpClient shareInstance] grabURL:url success:^(id responseObject) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kMESSAGE_BADGE object:nil userInfo:responseObject[@"Variables"][@"newmessage2"]];
//            NSLog(@"%@",responseObject);
            if ([responseObject[@"Message"][@"messageval"] isEqualToString:@"follow_add_succeed"]) {
                [SVProgressHUD showSuccessWithStatus:responseObject[@"Message"][@"messagestr"]];
                _isFollow = 1;
                self.followImage.image = [UIImage imageNamed:@"followed"];
//                [self.followButton setImage:[UIImage imageNamed:@"followed"] forState:UIControlStateNormal];
            } else {
                [SVProgressHUD showErrorWithStatus:responseObject[@"Message"][@"messagestr"]];
            }
        } fail:^(NSError *error) {
//            NSLog(@"%@",error);
            [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
        }];
    }
}
- (void)handleChatAction:(UITapGestureRecognizer *)tapGesture {
    //点击chat事件
    [MobClick event:@"SendPMFromMe"];
    //        [self performSegueWithIdentifier:@"pushToSendPM" sender:sender];
    ChatViewController *chatVC = [[ChatViewController alloc] initWithChatter:[NSString stringWithFormat:@"dahe_%@",self.userInfo[@"uid"]] isGroup:NO userName:self.userInfo[@"username"]];
    
    //username 用户名
    
    CDFMe *me = [CDFMe shareInstance];
//    NSLog(@"myName = %@", me.userName);
    NSString *myName = me.userName; //当前用户名
    NSString *userName = self.userInfo[@"username"]; //打开网友页面用户名
    
//    NSLog(@"%@->%@", myName, userName);
    
    chatVC.hidesBottomBarWhenPushed = YES;
    chatVC.navigationItem.title = self.userInfo[@"username"];
    [self.navigationController pushViewController:chatVC animated:YES];

}

#pragma mark ViewController Super Methods

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
  
    if (![CDFMe shareInstance].isLogin && _isFirstWarn) {
        [[CDFMe shareInstance] wellcomeLogin:self];
        _isFirstWarn = NO;
    }
    
    [MobClick beginLogPageView:@"个人中心"];
    [self addNotification];
//    self.followLabel.backgroundColor = [UIColor lightGrayColor];
    
    UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleFollowAction:)];
    UITapGestureRecognizer* tapGesture1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleChatAction:)];
    
    [self.followLabel addGestureRecognizer:tapGesture];
    [self.chatLabel addGestureRecognizer:tapGesture1];
    
    
    self.followLabel.layer.masksToBounds = YES;
    self.followLabel.layer.cornerRadius = 5;
    self.followLabel.layer.borderWidth = 1;
    self.followLabel.layer.borderColor = [BARTINCOLOR CGColor];
//    self.chatLabel.backgroundColor = [UIColor lightGrayColor];
    self.chatLabel.layer.masksToBounds = YES;
    self.chatLabel.layer.cornerRadius = 5;
    self.chatLabel.layer.borderColor = [BARTINCOLOR CGColor];
    self.chatLabel.layer.borderWidth = 1;
    UIColor *lineColor = [UIColor colorWithWhite:224.0f / 255.0f alpha:1];
    UIImage *splitImage = [UIImage imageNamed:@"bg_splt_me"];
    self.longLine.backgroundColor = lineColor;
    [self.headerView addSubview:self.longLine];
    
    self.followButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.messageButton.layer.borderColor = [[UIColor whiteColor] CGColor];

//    self.firstShotLine = [[UIView alloc] initWithFrame:CGRectMake(105, 120, 2, 15)];
//    self.firstShotLine.backgroundColor = lineColor;
//    self.firstShotLine.backgroundColor = [UIColor greenColor];//hyh测试数据
////    [self.headerView addSubview:self.firstShotLine];
//    self.firstShotLine.frame = CGRectMake(self.firstShotLine.frame.origin.x, 10, self.firstShotLine.frame.size.width, self.firstShotLine.frame.size.height);
//    [self.backgroundView addSubview:self.firstShotLine];
//    
////    self.secondShotLine = [[UIView alloc] initWithFrame:CGRectMake(213, 120, 2, 15)];
//    self.secondShotLine.backgroundColor = lineColor;
//    self.secondShotLine.backgroundColor = [UIColor colorWithPatternImage:splitImage];
//    self.secondShotLine.backgroundColor = [UIColor redColor];//hyh测试数据
////    [self.headerView addSubview:self.secondShotLine];
//    
//    self.thirdShotLine.backgroundColor = lineColor;
//    self.thirdShotLine.backgroundColor = [UIColor colorWithPatternImage:splitImage];
//    self.thirdShotLine.backgroundColor = [UIColor yellowColor];//hyh测试数据
////    [self.headerView addSubview:self.thirdShotLine];
    
    UIColor *hlColor = [UIColor colorWithWhite:235.0f / 255.0f alpha:1];
    [self.numberOfFollowerButton setBackgroundImage:[UIImage imageWithColor:hlColor] forState:UIControlStateHighlighted];
    [self.numberOfFriendButton setBackgroundImage:[UIImage imageWithColor:hlColor] forState:UIControlStateHighlighted];
    [self.detailButton setBackgroundImage:[UIImage imageWithColor:hlColor] forState:UIControlStateHighlighted];
    
    if (![CDFMe shareInstance].isLogin) {
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"登录" style:UIBarButtonItemStyleBordered target:self action:@selector(showLogin:)];
    }
    
}

- (void)showLogin:(id)sender
{
    [MobClick event:@"Login" label:@"从个人中心点击登录"];
    UIStoryboard *user = [UIStoryboard storyboardWithName:@"welcome" bundle:nil];
    [self presentModalViewController:user.instantiateInitialViewController animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"个人中心"];
    [SVProgressHUD dismiss];
}

- (void)dealloc
{
    [self removeNotification];
}

- (void)prepareForLoad
{
    NSDictionary *onlineInfo = [[Common unarchiveForKey:kOnlineInfo] objectForKey:@"data"];
    //NSLog(@"%@",onlineInfo);
    _authorID = onlineInfo[@"member_uid"];
    _isAuthor = (!self.uid || [self.uid isEqual:_authorID]);
    
}

- (void)showAvatar
{
    NSURL *url = [NSURL URLWithString:[Common formatAvatarWithUID:self.userInfo[@"uid"] type:@"middle"]];
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
    request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    self.avatarImageView.layer.masksToBounds = YES;
    self.avatarImageView.layer.cornerRadius = 25;
    [self.avatarImageView setImageWithURLRequest:request
                                placeholderImage:nil
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                             
                                         } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
//                                             NSLog(@"failed loading image: %@", error);
                                         }];
}

- (void)updateUserInfo:(NSNotification *) note
{
    [self.userInfo addEntriesFromDictionary:note.userInfo];
    [self bindHeaderInfo];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    [self layoutLeftNavigationItem];
    _isFirstWarn = YES; //这个BOOL用来标记是否是第一次提醒未登录用户登陆(弹出登陆界面)
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUserInfo:) name:kINFO_CHANGED object:nil];
    
    if ([Common systemVersion] >= 6.0) {
        self.refreshControl = self.refreshHeaderView;
    }
    
    [self prepareForLoad];
    
    if ([Common systemVersion] < 7.0) {
        UIView *bg = [[UIView alloc] initWithFrame:self.tableView.bounds];
        bg.backgroundColor = bgColor;
        self.tableView.backgroundView = bg;
        self.navigationItem.rightBarButtonItem.tintColor = self.navigationController.navigationBar.tintColor;
        self.navigationItem.backBarButtonItem.tintColor = self.navigationController.navigationBar.tintColor;
    } else {
        self.view.backgroundColor = bgColor;
        //self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        self.navigationItem.rightBarButtonItem.tintColor = TITLECOLOR;
        self.navigationItem.backBarButtonItem.tintColor = TITLECOLOR;
    }
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 30)];
    
    NSDictionary *onlineInfo = [[Common unarchiveForKey:kOnlineInfo] objectForKey:@"data"];
    //NSLog(@"%@",onlineInfo);
    _authorID = onlineInfo[@"member_uid"];
    _isAuthor = (!self.uid || [self.uid isEqual:_authorID]);
    if (_isAuthor) {
        label.text = @"我";
    }else{
        label.text = @"TA的主页";
    }
    label.textAlignment = NSTextAlignmentCenter;
//    label.textColor = TITLEGRAY;
    label.font =[UIFont fontWithName:@"Helvetica-Bold" size:20];
    label.textColor = RGBACOLOR(250, 66, 4, 1.0);

    self.navigationItem.titleView = label;

    self.tableView.allowsSelection = YES;
    self.tableView.allowsSelectionDuringEditing = YES;
    [self loadData];
    /*
     UILabel *titleLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
     titleLable.text = @"聚精彩";
     titleLable.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
     titleLable.textAlignment = NSTextAlignmentCenter;
     titleLable.textColor = RGBACOLOR(250, 66, 4, 1.0);
     self.navigationItem.titleView = titleLable;

     */
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSString *)getIndentifierWithIndexPath:(NSIndexPath *)indexPath
{
    NSString *str = [NSString stringWithFormat:@"%@%@",self.dataSource[indexPath.row][@"type"],((NSNull*)self.dataSource[indexPath.row][@"image"] != [NSNull null] && [self.dataSource[indexPath.row][@"image"][@"height"] floatValue] >= 73 ? @"WithImg" : @"WithoutImg")];

    return str;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [self getIndentifierWithIndexPath:indexPath];
    CDFMyDynCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.isFirst = (indexPath.row == 0);
    cell.isTouched = NO;
    
    //NSLog(@"%@",[Common dictionaryToJSON:self.dataSource[indexPath.row]]);
    
    cell.isMine = [self.dataSource[indexPath.row][@"type"] isEqualToString:@"thread"];
    cell.titleLabel.text = [NSString stringWithFormat:@"%@", self.dataSource[indexPath.row][@"subject"]];
    NSString *date = [Common formatDate:[self.dataSource[indexPath.row][@"dateline"] longLongValue]];
    NSString *block = [NSString stringWithFormat:@"%@",self.dataSource[indexPath.row][@"forumname"]];
    cell.timeAndGroupLabel.text = [NSString stringWithFormat:@"%@ 发表于 %@", date, block];
    cell.timeLabel.text = date;
    
    NSString *avatarURL = [Common formatAvatarWithUID:self.dataSource[indexPath.row][@"authorid"] type:@"middle"];
    [cell.avatarImageView setImageWithURL:[NSURL URLWithString:avatarURL]];
    cell.avatarImageView.layer.masksToBounds = YES;
    cell.avatarImageView.layer.cornerRadius = 12;
    cell.avatarImageView.layer.borderWidth = 0;
    cell.authorLabel.text = [NSString stringWithFormat:@"%@",self.dataSource[indexPath.row][@"author"]];
    if ((NSNull *)self.dataSource[indexPath.row][@"image"] != [NSNull null]) {
        CGFloat imgWidth;
        if ([CellIdentifier isEqualToString:@"threadWithImg"]) {
            imgWidth = 259;
        } else {
            imgWidth = 199;
        }
        CGFloat height = [self heightForImageViewAtIndex:indexPath];
        NSString *thumbURL = [Common getThumbImage:[NSString stringWithFormat:@"%@",self.dataSource[indexPath.row][@"image"][@"img_first"]] size:CGSizeMake(imgWidth, height) withRetinaDisplayDetect:YES];
        //NSLog(@"%@",thumbURL);
        [cell.thumbnailImageView setImageWithURL:[NSURL URLWithString:thumbURL]];
        cell.thumbnailImageView.clipsToBounds = YES;
        cell.thumbnailImageView.layer.masksToBounds = YES;
        if (height > 300) {
//            cell.thumbnailImageView.contentMode = UIViewContentModeTop;
            cell.thumbnailImageView.contentMode = UIViewContentModeScaleAspectFit;
        } else {
            cell.thumbnailImageView.contentMode = UIViewContentModeScaleAspectFit;
        }
         //
    }
    
    return cell;
}

- (CGFloat)heightForImageViewAtIndex:(NSIndexPath *)indexPath
{
    NSString *cellIndentifier = [self getIndentifierWithIndexPath:indexPath];
    CGFloat width = 0;//图片宽度
    if ([cellIndentifier isEqualToString:@"threadWithoutImg"]) {
        return 0;
    } else if([cellIndentifier isEqualToString:@"likeWithoutImg"]) {
        return 0;
    } else if ([cellIndentifier isEqualToString:@"threadWithImg"]) {
        width = 259;
    } else {
        width = 199;
    }
    
    CGSize imgSize = CGSizeMake([self.dataSource[indexPath.row][@"image"][@"width"] floatValue], [self.dataSource[indexPath.row][@"image"][@"height"] floatValue]);
    CGFloat imgNewHeight = width * imgSize.height / imgSize.width;
    
    return imgNewHeight;
}

#pragma mark UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *key = [NSString stringWithFormat:@"%d-%d",indexPath.section, indexPath.row];
    CGFloat height = [[self.heightCache objectForKey:key] floatValue];
    if (height <= 0) {
        NSString *cellIndentifier = [self getIndentifierWithIndexPath:indexPath];
        CGFloat needHeight;
        if ([cellIndentifier isEqualToString:@"threadWithoutImg"]) {
            return 55;
        } else if([cellIndentifier isEqualToString:@"likeWithoutImg"]) {
            return 75;
        } else if ([cellIndentifier isEqualToString:@"threadWithImg"]) {
            needHeight = 55;
        } else {
            needHeight = 55;
        }
        height = needHeight + [self heightForImageViewAtIndex:indexPath];
        height = height >= 300 ? 300 : height;
        [self.heightCache setObject:[NSNumber numberWithFloat:height] forKey:key];
    }
    return height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"pushToDetail" sender:self.dataSource[indexPath.row]];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController respondsToSelector:@selector(setTid:)]) {
        [segue.destinationViewController setValue:sender[@"tid"] forKey:@"tid"];
    }
    
    if ([segue.destinationViewController respondsToSelector:@selector(userInfo)]) {
        [segue.destinationViewController setValue:self.userInfo forKey:@"userInfo"];
    }
    
    if ([segue.destinationViewController isKindOfClass:NSClassFromString(@"CDFFollowViewController")]) {
        NSString *belong = (_isAuthor ? @"我" : @"TA");
        NSString *type = (![sender isEqual:self.numberOfFollowerButton] ? @"的听众" : @"收听的");
        [[segue.destinationViewController navigationItem] setTitle:[NSString stringWithFormat:@"%@%@",belong,type]];
        
        [segue.destinationViewController setValue:(self.uid ? self.uid : _authorID) forKey:@"uid"];
        [segue.destinationViewController setValue:([sender isEqual:self.numberOfFollowerButton] ? @"myfollowing" : @"myfollower") forKey:@"filter"];
    }
    /*
    if ([sender isEqual:self.messageButton]) {
        UINavigationController *nav = segue.destinationViewController;
        [nav.viewControllers[0] setValue:self.uid forKey:@"touid"];
        [nav.viewControllers[0] setTitle:[NSString stringWithFormat:@"To：%@",self.authorNameLabel.text]];
    }
     */
}
- (IBAction)handleCollectionBtn:(id)sender {
    
    ZWDMyFavTieViewController *collectionVC = [[ZWDMyFavTieViewController alloc] init];
    [self.navigationController pushViewController:collectionVC animated:YES];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    if (self.tabBarController.selectedIndex != 4) {
        _isFirstWarn = YES;
        return;
    }
//    if (self.tabBarController.selectedIndex == 0) {
//        _isFirstWarn = NO;
//    }
//    _isFirstWarn = NO;
}
- (void)viewDidAppear:(BOOL)animated{
    if ([CDFMe shareInstance].isFirstLogin) {
        //第一次登陆
        
        ZWDFirstLoginViewController *vc = [[ZWDFirstLoginViewController alloc]init];
        [self presentViewController:vc animated:YES completion:nil];
        [CDFMe shareInstance].isFirstLogin = NO;
    }
}
@end
