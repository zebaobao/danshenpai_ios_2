//
//  CDFRegisterViewController.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-26.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFRegisterViewController.h"
#import "CDFLoginViewController.h"
@interface CDFRegisterViewController ()<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *usernameTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UnderLineLabel *dealLabel;
@property (strong, nonatomic) IBOutlet UITextField *userTelephoneNumber;
@property (strong, nonatomic) IBOutlet UITextField *SMSVerificationCode;
@property (strong, nonatomic) IBOutlet UIButton *verificationCodeBtn;
@property (retain, nonatomic) NSMutableDictionary *registrationInformation;
@end

@implementation CDFRegisterViewController{

    int secondsCountDown;
    NSTimer *countDownTimer;
    UILabel *timeLabel;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
    }
    return self;
}
- (IBAction)GetVerificationCode:(id)sender {
    
    if (![self isMobileNumber:_userTelephoneNumber.text]) {
        
        [SVProgressHUD showErrorWithStatus:@"对不起您输入为无效手机号码请重新输入"];
        
    }else{
        
        timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, _verificationCodeBtn.frame.size.width, _verificationCodeBtn.frame.size.height)];
        timeLabel.textAlignment = NSTextAlignmentCenter;
        timeLabel.font = [UIFont systemFontOfSize:12];
        timeLabel.backgroundColor = [UIColor colorWithRed:45/255.0 green:154/255.0 blue:0/255.0 alpha:1.0];
        timeLabel.userInteractionEnabled = YES;
        timeLabel.textColor = [UIColor whiteColor];
        [_verificationCodeBtn addSubview:timeLabel];
        
        __block int timeout = 180; //倒计时时间
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
        dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
        dispatch_source_set_event_handler(_timer, ^{
            if(timeout<=0){ //倒计时结束，关闭
                
                dispatch_source_cancel(_timer);
                dispatch_async(dispatch_get_main_queue(), ^{
                    //设置界面的按钮显示 根据自己需求设置
                    [timeLabel removeFromSuperview];
                });
            }else{
                
                NSString *strTime = [NSString stringWithFormat:@"%d秒后重新获取",timeout];
                dispatch_async(dispatch_get_main_queue(), ^{
                    //设置界面的按钮显示 根据自己需求设置
                    timeLabel.text = strTime;
                });
                timeout--;
            }
        });
        dispatch_resume(_timer);
        NSString *url = [NSString stringWithFormat:@"plugin.php?id=smstong:verifycode&idhash=%@&formhash=%@&seccodeverify=&inajax=yes&infloat=register&inmobile=yes&handlekey=register&from=yymobile&ajaxmenu=1&action=getregverifycode&mobile=%@&0.045216312744171594",_registrationInformation[@"idhash"],_registrationInformation[@"formhash"],_userTelephoneNumber.text];
        [[HXHttpClient shareInstance] grabURL:url success:^(id responseObject) {
            
            if (responseObject) {
                NSString *error = responseObject[@"error"];
                switch ([error integerValue]) {
                    case 0:
                        [SVProgressHUD showSuccessWithStatus:@"发送成功，请注意查收"];
                        break;
                    case 1:
                        [SVProgressHUD showSuccessWithStatus:@"来路不正确"];
                        [timeLabel removeFromSuperview];
                        break;
                    case 2:
                        [SVProgressHUD showSuccessWithStatus:@"输入下图中的字符"];
                        [timeLabel removeFromSuperview];
                        break;
                    case 3:
                        [SVProgressHUD showSuccessWithStatus:@"手机号短信验证注册未开启"];
                        [timeLabel removeFromSuperview];
                        break;
                    case 4:
                        [SVProgressHUD showSuccessWithStatus:@"手机号格式不正确"];
                        [timeLabel removeFromSuperview];
                        break;
                    case 5:
                        [SVProgressHUD showSuccessWithStatus:@"每IP每手机号每180 秒只能获取一次验证码"];
                        [timeLabel removeFromSuperview];
                        break;
                    case 6:
                        [SVProgressHUD showSuccessWithStatus:@"一个手机号只能绑定本站1个帐号"];
                        [timeLabel removeFromSuperview];
                        break;
                    case 7:
                        [SVProgressHUD showSuccessWithStatus:@"验证码短信发送失败"];
                        [timeLabel removeFromSuperview];
                        break;
                    default:
                        [SVProgressHUD showSuccessWithStatus:@"获取验证码失败，请重试"];
                        break;
                }
            }
            
        } fail:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:@"获取验证码失败,请重试"];
            [timeLabel removeFromSuperview];
        }];
    }
}

//验证电话号码是否有效
- (BOOL)isMobileNumber:(NSString *)mobileNum
{
    NSString * MOBILE = @"^1[3|4|5|7|8][0-9]\\d{8}$";
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    if ([regextestmobile evaluateWithObject:mobileNum] == YES)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

-(void)timeFireMethod{
//    secondsCountDown--;
//    NSString *strTime = [NSString stringWithFormat:@"%d秒后重新获取",secondsCountDown];
//    timeLabel.text = strTime;
//    _verificationCodeBtn.selected = NO;
//    if(secondsCountDown==0){
//        
//        [timeLabel removeFromSuperview];
//        [countDownTimer invalidate];
//    }
}

- (IBAction)registerAction:(id)sender {
    
    [MobClick event:@"Register"];
    if ([[self.usernameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] <= 0) {
        [SVProgressHUD showErrorWithStatus:@"请输入用户名！"];
    } else if(self.passwordTextField.text.length <= 0) {
        [SVProgressHUD showErrorWithStatus:@"请输入密码！"];
//    } else if([[self.emailTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] <= 0) {
//        [SVProgressHUD showErrorWithStatus:@"请输入Email！"];
    }else if([[self.userTelephoneNumber.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] <= 0) {
        [SVProgressHUD showErrorWithStatus:@"请输入手机号！"];
    }else if([[self.SMSVerificationCode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] <= 0) {
        [SVProgressHUD showErrorWithStatus:@"请输入验证码！"];
    }else {
        [self.usernameTextField resignFirstResponder];
        [self.passwordTextField resignFirstResponder];
//        [self.emailTextField resignFirstResponder];
        [self.SMSVerificationCode resignFirstResponder];
        [self.userTelephoneNumber resignFirstResponder];
        
        NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=hxregister&mod=registeryy_yanyu&mobiletype=2",kApiVersion];
        [Common logout];
        [SVProgressHUD showWithStatus:@"正在为您注册，请稍后..." maskType:SVProgressHUDMaskTypeGradient];
        [[HXHttpClient shareInstance] grabURL:url success:^(id responseObject) {
            
//            NSLog(@"%@",responseObject);
            NSDictionary *param = @{
                                    responseObject[@"username"] : self.usernameTextField.text,
                                    responseObject[@"password"] : self.passwordTextField.text,
                                    responseObject[@"password2"]: self.passwordTextField.text,
                                    @"mobile" : self.userTelephoneNumber.text,
                                    @"verifycode": self.SMSVerificationCode.text,
                                    @"agreebbrule"              : responseObject[@"agreebbrule"],
                                    @"formhash"                 : responseObject[@"formhash"],
                                    @"regsubmit"                : @"1"
                                    };
            [[HXHttpClient shareInstance] setDefaultValue:kBaseURL forHeader:@"Referer"];
            [Common appendCookies:[Common getDefaultCookies]];
            [[HXHttpClient shareInstance] postURL:[NSString stringWithFormat:@"api/mobile/?version=%@&module=hxregister&mod=registeryy_yanyu&mobiletype=2",kApiVersion] postData:param success:^(id responseObject) {
//                NSLog(@"%@",responseObject);
                if ([responseObject[@"Message"][@"messageval"] isEqualToString:@"register_succeed"]) {
                    //注册成功
//                    [Common login:responseObject];
                    [SVProgressHUD showSuccessWithStatus:@"注册成功，请登录!" duration:2];
                    [self.navigationController popViewControllerAnimated:YES];
                } else {
                    //注册失败
                    [SVProgressHUD showErrorWithStatus:responseObject[@"Message"][@"messagestr"]];
                }
            } fail:^(NSError *error) {
//                NSLog(@"%@",error);
                [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
            }];
        } fail:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
        }];
    }
}

#pragma mark UIViewController Super Methods
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getInfo];
    [MobClick beginLogPageView:@"注册"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"注册"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    if ([Common systemVersion] < 7.0) {
        UIView *bg = [[UIView alloc] initWithFrame:self.tableView.bounds];
        bg.backgroundColor = bgColor;
        self.tableView.backgroundView = bg;
        self.navigationItem.rightBarButtonItem.tintColor = self.navigationController.navigationBar.tintColor;
        self.navigationItem.backBarButtonItem.tintColor = self.navigationController.navigationBar.tintColor;
    } else {
        self.view.backgroundColor = bgColor;
        //self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    }
    self.dealLabel.shouldUnderline = YES;
    _verificationCodeBtn.selected = YES;
}

- (void)getInfo{

    NSString *url = @"/api/mobile/index.php?version=3&module=hxregister&mod=registeryy_yanyu";
    [[HXHttpClient shareInstance] grabURL:url success:^(id responseObject) {
        
        _registrationInformation = [[NSMutableDictionary alloc] initWithDictionary:responseObject];
//        NSLog(@"REs:%@", responseObject);
        
    } fail:^(NSError *error) {
        
        [SVProgressHUD showErrorWithStatus:@"对不起网络请求失败"];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isEqual:self.usernameTextField]) {
        [self.passwordTextField becomeFirstResponder];
    }
    if ([textField isEqual:self.passwordTextField]) {
        [self.emailTextField becomeFirstResponder];
    }
    if ([textField isEqual:self.emailTextField]) {
        [self.emailTextField resignFirstResponder];
    }
    if ([textField isEqual:self.userTelephoneNumber]) {
        [self.userTelephoneNumber resignFirstResponder];
    }
    if ([textField isEqual:self.SMSVerificationCode]) {
        [self.SMSVerificationCode resignFirstResponder];
    }
    
    return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [super numberOfSectionsInTableView:tableView];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [super tableView:tableView numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell;
}


@end
