//
//  CDFChooseFollowerViewController.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-12-27.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDFChooseFollowerViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancleButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *refreshButton;

@end
