//
//  CDFPostingViewController.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-10-31.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFPostingViewController.h"
//#import "CTAssetsPickerController.h"
#import "ZYQAssetPickerController.h"
#import "CDFBoardView.h"

@interface CDFPostingViewController ()<UITextFieldDelegate, UITextViewDelegate, UITableViewDataSource, UITableViewDelegate, /*CTAssetsPickerControllerDelegate*/ZYQAssetPickerControllerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UITextField *titleTextField;
@property (strong, nonatomic) IBOutlet UITableView *contentTableView;
@property (strong, nonatomic) IBOutlet UIView *keyboardToolbar;
@property (strong, nonatomic) IBOutlet UIButton *camButton;
@property (strong, nonatomic) IBOutlet UIButton *albumButton;
@property (strong, nonatomic) IBOutlet UIButton *hideKeyboardButton;
@property (strong, nonatomic) IBOutlet UIButton *emojiButton;
@property (strong, nonatomic) IBOutlet CDFBoardView *boardSelectControl;
@property (strong, nonatomic) IBOutlet UIButton *boardButton;
@property (strong, nonatomic) IBOutlet UIView *footerView;
@property (strong, nonatomic) IBOutlet UIButton *voiceButton;

- (IBAction)buttonClicked:(id)sender;

@end

@implementation CDFPostingViewController {
    id _firstResponder;
    CGRect _contentTableViewRect;
    id typeid;
    id fid;
//    CLLocationManager *locationManager;
}

//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
//    
//    [locationManager stopUpdatingLocation];
//    
//    NSString *strLat = [NSString stringWithFormat:@"%.4f",newLocation.coordinate.latitude];//纬度
//    NSString *strLng = [NSString stringWithFormat:@"%.4f",newLocation.coordinate.longitude];//经度
//    
//    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
//    [geocoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error)
//     {
//         if(error == nil && [placemarks count]>0)
//         {
//             CLPlacemark *placemark = [placemarks objectAtIndex:0];
//             
//             NSLog(@"Country = %@", placemark.country);
//             NSLog(@"Postal Code = %@", placemark.postalCode);
//             NSLog(@"Locality = %@", placemark.locality);
//             NSLog(@"address = %@",placemark.name);
//             NSLog(@"administrativeArea = %@",placemark.administrativeArea);
//             NSLog(@"subAdministrativeArea = %@",placemark.subAdministrativeArea);
//             NSLog(@"locality = %@", placemark.locality);
//             NSLog(@"thoroughfare = %@", placemark.thoroughfare);
//             
//             
//             
//         }
//         else if(error==nil && [placemarks count]==0){
//             NSLog(@"No results were returned.");
//         }
//         else if(error != nil) {
//             NSLog(@"An error occurred = %@", error);
//         }
//     }];
//}


//- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
//    NSLog(@"locError:%@", error);
//    
//}
//-(void)getLocationInfo {
//    locationManager = [[CLLocationManager alloc] init];
//    locationManager.delegate = self;
//    if([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
////        [locationManager requestWhenInUseAuthorization];
//        [locationManager requestAlwaysAuthorization];
//    }
//    [locationManager startUpdatingLocation];
//}
//- (IBAction)handleDingWei:(id)sender {
//    
//    [self getLocationInfo];
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"是否上传当前位置信息?" message:[NSString stringWithFormat:@""] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:@"取消", nil];
//    alertView.tag = 125;
//}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (NSString *)key
{
    if (!_key) {
        int numberLen = 10;
        char data[numberLen];
        for (int x=0;x<numberLen;data[x++] = (char)('A' + (arc4random_uniform(26))));
        NSString *randomStr = [[NSString alloc] initWithBytes:data length:numberLen encoding:NSUTF8StringEncoding];
        _key = randomStr;
    }
    return _key;
}

- (CDFDraft *)draft
{
    if (!_draft) {
        _draft = [[CDFDraft alloc] initWithKey:self.key];
        if (_draft.datasource.count > 0) {
            [self.inputDataSource removeAllObjects];
            [self.inputDataSource addObjectsFromArray:_draft.datasource];
            self.titleTextField.text = [NSString stringWithFormat:@"%@",_draft.subject];
            [self.boardButton setTitle:_draft.selectedBoard[@"name"] forState:UIControlStateNormal];
            self.boardSelectControl.selectedItem = _draft.selectedBoard;
//            NSLog(@"%@",_draft.selectedBoard);
            [self.contentTableView reloadData];
        } else {
            for (NSDictionary *obj in [CDFMe shareInstance].board) {
                if ([obj[@"default"] boolValue]) {
                    _draft.selectedBoard = obj;
//                    NSLog(@"%@",obj);
                }
            }
        }
    }
    return _draft;
}

/**
 *  发帖cell数据源
 *
 *  @return 可变数组
 */
- (NSMutableArray *)inputDataSource
{
    if (!_inputDataSource) {
        _inputDataSource = [NSMutableArray array];
        
        //如果数据源为空，说明是新建发帖
        NSMutableDictionary *initData = [NSMutableDictionary dictionaryWithDictionary:[self createEmptyTextInputCell]];
        [initData setObject:[NSNumber numberWithBool:NO] forKey:@"editable"];
        [initData setObject:[NSNumber numberWithFloat:300.0f] forKey:@"height"];
        [_inputDataSource addObject:initData];
    }
    
    return _inputDataSource;
}

/**
 *  初始化一个文本输入框
 *
 *  @return NSDictionary
 */
- (NSDictionary *)createEmptyTextInputCell
{
    NSMutableDictionary *initData = [NSMutableDictionary dictionary];
    [initData setObject:[NSNumber numberWithFloat:44] forKey:@"height"];
    
    //类型为 Text 和 Image
    [initData setObject:@"Text" forKey:@"type"];
    [initData setObject:@"" forKey:@"content"];
    [initData setObject:[NSNumber numberWithBool:YES] forKey:@"editable"];
    
    return initData;
}

- (void)dealTableViewFooter
{
    self.contentTableView.tableFooterView = nil;
    CGFloat footerHeight = CGRectGetHeight(self.contentTableView.frame) - self.contentTableView.contentSize.height;
    if (footerHeight > 0) {
        CGRect rect = self.footerView.frame;
        rect.size.height = footerHeight;
        self.footerView.frame = rect;
        self.contentTableView.tableFooterView = self.footerView;
    }
}

- (IBAction)focus:(id)sender {
    [self locationFirstResponderFromIndexPath:[NSIndexPath indexPathForRow:(self.inputDataSource.count - 1) inSection:0]];
}

- (IBAction)buttonClicked:(id)sender
{
    
    if ([sender isEqual:_hideKeyboardButton]) {
        //收起键盘
        [_firstResponder resignFirstResponder];
        [self.contentTableView reloadData];
        [self dealTableViewFooter];
        [MobClick event:@"HideKeyboard"];
    } else if ([sender isEqual:_albumButton]) {
        //从相册多选照片
        /*
        [_firstResponder resignFirstResponder];
        CTAssetsPickerController *picker = [[CTAssetsPickerController alloc] init];
        picker.maximumNumberOfSelection = 9;
        picker.assetsFilter = [ALAssetsFilter allPhotos];
        picker.delegate = self;
        
        [self presentViewController:picker animated:YES completion:NULL];
         */
        [MobClick event:@"ShowAlbum"];
        if ([Common systemVersion] >= 6.0) {
            ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] init];
            picker.maximumNumberOfSelection = 10;
            picker.assetsFilter = [ALAssetsFilter allPhotos];
            picker.showEmptyGroups=NO;
            picker.delegate=self;
            picker.selectionFilter = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
                if ([[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypeVideo]) {
                    NSTimeInterval duration = [[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyDuration] doubleValue];
                    return duration >= 5;
                } else {
                    return YES;
                }
            }];
            
            [self presentViewController:picker animated:YES completion:NULL];
        } else {
            //[SVProgressHUD showErrorWithStatus:@"Comming soon!"];
            UIImagePickerController *camPick = [[UIImagePickerController alloc] init];
            camPick.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
            camPick.delegate = self;
            //[self presentModalViewController:camPick animated:YES];
            [self presentViewController:camPick animated:YES completion:^{
                // scroll to the end - hack
                UIView *imagePickerView = camPick.view;
                
                UIView *view = [imagePickerView hitTest:CGPointMake(5,5) withEvent:nil];
                while (![view isKindOfClass:[UIScrollView class]] && view != nil) {
                    // note: in iOS 5, the hit test view is already the scroll view. I don't want to rely on that though, who knows
                    // what Apple might do with the ImagePickerController view structure. Searching backwards from the hit view
                    // should always work though.
                    //NSLog(@"passing %@", view);
                    view = [view superview];
                }
                
                if ([view isKindOfClass:[UIScrollView class]]) {
                    //NSLog(@"got a scroller!");
                    UIScrollView *scrollView = (UIScrollView *) view;
                    // check what it is scrolled to - this is the location of the initial display - very important as the image picker
                    // actually slides under the navigation bar, but if there's only a few images we don't want this to happen.
                    // The initial location is determined by status bar height and nav bar height - just get it from the picker
                    CGPoint contentOffset = scrollView.contentOffset;
                    CGFloat y = MAX(contentOffset.y, [scrollView contentSize].height-scrollView.frame.size.height);
                    CGPoint bottomOffset = CGPointMake(0, y);
                    [scrollView setContentOffset:bottomOffset animated:NO];
                }
            }];
        }
         
    } else if ([sender isEqual:_camButton]) {
        [MobClick event:@"ShowCamera"];
        //摄像头拍照
#if !TARGET_IPHONE_SIMULATOR
        UIImagePickerController *camPick = [[UIImagePickerController alloc] init];
        camPick.sourceType = UIImagePickerControllerSourceTypeCamera;
        camPick.delegate = self;
        [self presentModalViewController:camPick animated:YES];
#endif
    } else if ([sender isEqual:_emojiButton]) {
        //表情
    } else if ([sender isEqual:_boardButton]) {
        //板块选择
        [self.boardSelectControl reloadData];
        [self triggerBoard];
    } else if ([sender isEqual:_voiceButton]) {
        //语音
        [MobClick event:@"Speech"];
        [_firstResponder resignFirstResponder];
        CDFSpeech *speech = [CDFSpeech new];
        [speech addTarget:self action:@selector(speechChanged:) forControlEvents:UIControlEventValueChanged];
        [speech showInView:self.navigationController.view];
    }
}

- (void)speechChanged:(CDFSpeech *)speech
{
    NSString *msg = [NSString stringWithFormat:@"%@%@",[_firstResponder text],speech.message];
    [_firstResponder setText:msg];
//    NSLog(@"speech message : %@",msg);
    [speech dismiss:nil];
    [_firstResponder becomeFirstResponder];
}

- (void)triggerBoard
{
    [MobClick event:@"SelectBoard"];
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [window addSubview:self.boardSelectControl];
    CGRect rect = self.boardSelectControl.frame;
    rect.origin.y = [_firstResponder isFirstResponder] ? CGRectGetHeight(window.bounds) - CGRectGetHeight(rect) : CGRectGetHeight(window.bounds) + CGRectGetHeight(rect);
    
    if (![_firstResponder isFirstResponder]) {
        [_firstResponder becomeFirstResponder];
    } else {
        [_firstResponder resignFirstResponder];
    }
    
    [UIView animateWithDuration:.25 animations:^{
        self.boardSelectControl.frame = rect;
    }];
    
}

/**
 *  @取消发帖
 *
 *  @param sender 取消发帖的sender
 */
- (IBAction)cancelPosting:(id)sender
{
    [MobClick event:@"CancelPost"];
    if (self.inputDataSource.count > 1 || [self.inputDataSource[0][@"content"] length] > 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"您的帖子尚未发送，是否保存到草稿箱？" delegate:self cancelButtonTitle:@"不用了" otherButtonTitles:@"好的", nil];
        alert.tag = 998;
        [alert show];
    } else {
//        self.tabBarController.tabBar.hidden = NO;
//        [self.tabBarController setSelectedIndex:0];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (IBAction)send:(id)sender {
    
    
    [MobClick event:@"Post"];
    if (![CDFMe shareInstance].isLogin) {
        UIStoryboard *user = [UIStoryboard storyboardWithName:@"welcome" bundle:nil];
        [self presentModalViewController:user.instantiateInitialViewController animated:YES];
        return;
    }
    if ([self.titleTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length > 40) {
        [SVProgressHUD showErrorWithStatus:@"标题字数请少于40"];
        return;
    }
    if ([self.titleTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length <= 0) {
        [SVProgressHUD showErrorWithStatus:@"请输入标题！"];
        return;
    }
    if (self.inputDataSource.count <= 1 && [self.inputDataSource[0][@"content"] length] <= 0) {
        [SVProgressHUD showErrorWithStatus:@"多少说两句吧！"];
        return;
    }
#pragma mark -- wei xiuchang shi 
    if (self.isShowViewController && self.inputDataSource.count <= 1 ) {
        [SVProgressHUD showErrorWithStatus:@"秀场板块必须包含图片！"];
        return;
    }
    
    
    
    if (!self.draft.selectedBoard) {
        self.draft.selectedBoard = self.boardSelectControl.selectedItem;
    }
    //检测与服务器之间的连接
    NSString *url = @"api/mobile/?version=3&module=heartbeat";
    [[HXHttpClient shareInstance]grabURL:url success:^(id responseObject) {
//        NSLog(@"%@",responseObject);
        if ([responseObject[@"Variables"][@"member_uid"] isEqualToString:@"0"]) {
//            NSLog(@"服务器连接失败");
            [Common logout];
            UIStoryboard *user = [UIStoryboard storyboardWithName:@"welcome" bundle:nil];
            [self presentModalViewController:user.instantiateInitialViewController animated:YES];
            [SVProgressHUD showErrorWithStatus:@"网络异常，请重新登录！"];
            return;
        }else{
            //正常连接
//            NSLog(@"服务器连接正常--%@",responseObject[@"Variables"][@"member_uid"]);
            //[SVProgressHUD showWithStatus:@"请稍等" maskType:SVProgressHUDMaskTypeBlack];
            self.sendButton.enabled = NO;
//            NSLog(@"%@",self.inputDataSource);
            [self saveDraft];
            [self.draft send];
            [self reset];
            [self performSelector:@selector(cancelPosting:) withObject:nil afterDelay:2];
//            NSLog(@"%@",[CDFDraft allDrafts]);
        }
    } fail:^(NSError *error) {
//        NSLog(@"心跳检测失败");
        [SVProgressHUD showErrorWithStatus:@"请检查网络！"];
        return ;
    }];
    //检测与服务器之间的连接
    
}

/**
 *  向帖子内容里面添加图片
 *
 *  @param assets 图片资源列表
 *
 *  @return 需要增加的indexPaths数组
 */
- (NSArray *)appendPhotoesFromArray:(NSArray *)assets
{
    NSMutableArray *indexPaths = [NSMutableArray array];
    
    int inputCount = self.inputDataSource.count;
    
    for (ALAsset *asset in assets) {
//        NSLog(@"ALAssetPropertyOrientation %@",[asset valueForProperty:ALAssetPropertyOrientation]);
        @autoreleasepool {
            UIImage *thumbnail = [UIImage imageWithCGImage:[asset thumbnail]];
            
            //修正图片方向
            UIImageOrientation orientation = UIImageOrientationUp;
            NSNumber* orientationValue = [asset valueForProperty:@"ALAssetPropertyOrientation"];;
            if (orientationValue != nil) {
                orientation = [orientationValue intValue];
            }
            
            UIImage *hdImage = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullResolutionImage] scale:1 orientation:orientation];

            CGSize hdSize = hdImage.size;
            CGSize hdnewSize = CGSizeZero;
            if (hdSize.width > 799.0f && hdSize.width > 0) {
                //处理图片的尺寸，以便节省流量
                hdnewSize.width = 799.0f;
                hdnewSize.height = 20000.0f;
                hdImage = [Common resizeImage:hdImage fitSize:hdnewSize];
            }

            NSString *fileName = [asset defaultRepresentation].filename;
            
            CDFAttachment *attach = [[CDFAttachment alloc] initWithFileName:fileName thumbnail:thumbnail hdImage:hdImage];
            
            NSDictionary *dic = @{@"type": @"Image",@"content":attach,@"height":@"100",@"editable":[NSNumber numberWithBool:YES]};
            NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:dic];
            [self.inputDataSource addObject:dictionary];
        }
    }
    
    for (int i = inputCount; i < assets.count + inputCount; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        [indexPaths addObject:indexPath];
    }
    
    return indexPaths;
}

/**
 *  更新textView高度
 *
 *  @param textView
 */
- (void)updateTextViewCellHeight:(UITextView *)textView
{
    [self.inputDataSource[textView.tag] setObject:[NSNumber numberWithFloat:textView.contentSize.height+10] forKey:@"height"];
    //更新内容
    [self.inputDataSource[textView.tag] setObject:textView.text forKey:@"content"];
    
    //定位光标到textview的底部
    if(textView.text.length > 0 ) {
        NSRange bottom = NSMakeRange(textView.text.length -1, 1);
        [textView scrollRangeToVisible:bottom];
    }
}

/**
 *  插入图片数组
 *
 *  @param images NSArray Object
 */
- (void)insertImagesToCell:(NSArray *)assets
{
    [self appendPhotoesFromArray:assets];
    [self.contentTableView reloadData];
    [self dealTableViewFooter];
}


/**
 *  定位输入光标
 *
 *  @param indexPath NSIndexPath Object
 */
- (void)locationFirstResponderFromIndexPath:(NSIndexPath *)indexPath
{
    if ([self.inputDataSource[indexPath.row][@"type"] isEqualToString:@"Image"]) {
        
        if (indexPath.row < self.inputDataSource.count - 1) {
            //如果点击的不是最后一个
            id control = self.inputDataSource[indexPath.row + 1][@"control"];
            if ([control isKindOfClass:[UITextView class]]) {
                //如果下一行是textview
                [control becomeFirstResponder];
            } else {
                [self createNewControlForInputAtIndexPath:indexPath];
            }
        } else {
            [self createNewControlForInputAtIndexPath:indexPath];
        }
    } else {
        id control = self.inputDataSource[indexPath.row][@"control"];
        if ([control isKindOfClass:[UITextView class]]) {
            //如果下一行是textview
            [control becomeFirstResponder];
        }
    }
}

/**
 *  @创建一个空的文本cell并focus
 *
 *  @param indexPath NSIndexPath
 */
- (void)createNewControlForInputAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *initData = [NSMutableDictionary dictionaryWithDictionary:[self createEmptyTextInputCell]];
    [self.inputDataSource insertObject:initData atIndex:indexPath.row + 1];
    [self.contentTableView reloadData];
    
    [self.contentTableView setContentOffset:CGPointMake(0, self.contentTableView.contentSize.height - CGRectGetHeight(self.contentTableView.bounds)) animated:NO];
    NSIndexPath *ip = [NSIndexPath indexPathForRow:indexPath.row + 1 inSection:indexPath.section];
    [self performSelector:@selector(locationFirstResponderFromIndexPath:) withObject:ip afterDelay:.15];
}

- (void)scrollToInput:(NSInteger)tag
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:tag inSection:0];
    [self.contentTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)saveDraft
{
    self.draft.fid = fid;
    self.draft.draftT = typeid;
    self.draft.subject = self.titleTextField.text;
    self.draft.timeline = [NSDate date];
    self.draft.key = self.key;
    self.draft.datasource = [NSArray arrayWithArray:self.inputDataSource];
    self.draft.uploaded = [NSArray array];
    self.draft.contents = [NSArray array];
    self.draft.status = DraftStatusNew;
    self.draft.message = @"";
    [self.draft save];
}

- (void)reset
{
    self.key = nil;
    self.draft = nil;
    [self.inputDataSource removeAllObjects];
    self.inputDataSource = nil;
    [self inputDataSource];
    [self.contentTableView reloadData];
    self.titleTextField.text = @"";
    self.sendButton.enabled = YES;
    //[self cancelPosting:nil];
}

- (void)handlePostResult:(NSNotification *)notification
{
    if ([notification.name isEqualToString:kPOST_SUCCESS]) {
        
        [self reset];
        
    }else if ([notification.name isEqualToString:kPOST_FAIL]){
        
        
    }
}

- (void)boardChanged:(CDFBoardView *)boardView
{
    self.draft.fid = boardView.selectedItem[@"fid"];
    self.draft.draftT = boardView.selectedItem[@"typeid"];
    self.draft.selectedBoard = boardView.selectedItem;
    [self.boardButton setTitle:boardView.selectedItem[@"name"] forState:UIControlStateNormal];
    [self triggerBoard];
}

#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 998) {
        if (buttonIndex == 0) {
            [self reset];
            [self dismissViewControllerAnimated:YES completion:nil];
            //self.tabBarController.tabBar.hidden = NO;
            //[self.tabBarController setSelectedIndex:0];
        } else {
            [self saveDraft];
            [self dismissViewControllerAnimated:YES completion:nil];
            //self.tabBarController.tabBar.hidden = NO;
            //[self.tabBarController setSelectedIndex:0];
        }
    }
}

#pragma mark UIViewController Super Methods
- (void)viewDidLoad
{
    [super viewDidLoad];
//    [self getLocationInfo];

    //NSLog(@"%@",self.key);
    self.contentTableView.editing = YES;
    self.contentTableView.allowsSelectionDuringEditing = YES;
    
    _contentTableViewRect = self.contentTableView.frame;
    
    if ([Common systemVersion] < 7.0) {
        self.navigationItem.leftBarButtonItem.tintColor = TITLECOLOR;
        self.navigationItem.rightBarButtonItem.tintColor = TITLECOLOR;
    }else{
        //self.navigationController.navigationBar.tintColor = TITLECOLOR;
        self.navigationItem.leftBarButtonItem.tintColor = TITLECOLOR;
        self.navigationItem.rightBarButtonItem.tintColor = TITLECOLOR;
    }
    
    self.boardSelectControl.selectedItem = self.draft.selectedBoard;
//    NSLog(@"%@",self.draft.selectedBoard);
    [self.boardSelectControl addTarget:self action:@selector(boardChanged:) forControlEvents:UIControlEventValueChanged];
    
    //判断是否为第一个试图控制器，如果是，则加一个取消按钮到左边导航上，如果否则不加
    if (self == [self.navigationController.viewControllers objectAtIndex:0]) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelPosting:)];
    }
    if ([Common systemVersion] < 7.0) {
        self.navigationItem.rightBarButtonItem.tintColor = TITLECOLOR;
        self.navigationItem.backBarButtonItem.tintColor = TITLECOLOR;
    } else {
        //self.navigationController.navigationBar.tintColor = TITLECOLOR;
    }
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 30)];
    label.text = @"";
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = TITLEGRAY;
    self.navigationItem.titleView = label;
    
    
    self.navigationController.navigationBar.translucent = YES;
    

    self.edgesForExtendedLayout = UIRectEdgeAll;
}

- (void)viewWillAppear:(BOOL)animated
{
    CDFLocationManager *locationManager = [CDFLocationManager sharedInstance];
    NSString *rerer = [locationManager locationURL];
    
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"随手拍"];
    self.boardSelectControl.frame = CGRectMake(0, CGRectGetHeight(self.view.bounds) + CGRectGetHeight(self.boardSelectControl.bounds), CGRectGetWidth(self.boardSelectControl.frame), CGRectGetHeight(self.boardSelectControl.frame));
	[self addKeyBoardNotification];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handlePostResult:) name:kPOST_SUCCESS object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handlePostResult:) name:kPOST_FAIL object:nil];
//    [(CDFTabBarController*)self.tabBarController setHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"随手拍"];
    [self.titleTextField resignFirstResponder];
    [self removeKeyBoardNotification];
//    [(CDFTabBarController*)self.tabBarController setHidden:NO];
    [self.boardSelectControl removeFromSuperview];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UIKeyboardController Listener

//监听键盘隐藏和显示事件
- (void)addKeyBoardNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowOrHide:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowOrHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)keyboardWillShowOrHide:(NSNotification *)notification {
    
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    BOOL isShow = [[notification name] isEqualToString:UIKeyboardWillShowNotification] ? YES : NO;

    UIEdgeInsets insets = UIEdgeInsetsMake(0, 0, 0, 0);
    
    CGRect animateRect;
    
    if (isShow) {
        self.contentTableView.tableFooterView = nil;
        [self.boardSelectControl removeFromSuperview];
        insets = UIEdgeInsetsMake(0, 0, keyboardSize.height, 0);
        
        animateRect = CGRectMake(0, CGRectGetHeight(self.view.bounds) - CGRectGetHeight(_keyboardToolbar.bounds) - keyboardSize.height, CGRectGetWidth(_keyboardToolbar.bounds), CGRectGetHeight(_keyboardToolbar.bounds));
        if ([Common systemVersion] < 7.0) {
            animateRect.origin.y += 49;
        }
    }else{
        [self dealTableViewFooter];
        animateRect = CGRectMake(0, CGRectGetHeight(self.view.bounds)+CGRectGetHeight(_keyboardToolbar.bounds), CGRectGetWidth(self.view.bounds), CGRectGetHeight(_keyboardToolbar.bounds));
    }
    
    //_keyboardToolbar.hidden = !isShow;
    
    [UIView animateWithDuration:.25 animations:^{
        _keyboardToolbar.frame = animateRect;
    }];
    
    //_keyboardToolbar.hidden = [_firstResponder isEqual:self.titleTextField];
    
    self.contentTableView.contentInset = insets;
    
}

//注销监听事件
- (void)removeKeyBoardNotification {
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kPOST_SUCCESS object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kPOST_FAIL object:nil];
}

#pragma mark UIImagePickControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissModalViewControllerAnimated:YES];
    
    @autoreleasepool {
        if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
            [SVProgressHUD showWithStatus:@"请稍后..." maskType:SVProgressHUDMaskTypeGradient];
            ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
            UIImage *image = [Common fixRotation:[info objectForKey:UIImagePickerControllerOriginalImage]];
            [library writeImageToSavedPhotosAlbum:image.CGImage orientation:(ALAssetOrientation)image.imageOrientation completionBlock:^(NSURL *assetURL, NSError *error )
             {
                 [library assetForURL:assetURL
                          resultBlock:^(ALAsset *asset )
                  {
                      [self insertImagesToCell:[NSArray arrayWithObject:asset]];
                      [SVProgressHUD dismiss];
                  }
                  
                         failureBlock:^(NSError *error )
                  {
                      [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
//                      NSLog(@"Error loading asset");
                  }];
             }];
        } else {
            ALAssetsLibrary *lb = [ALAssetsLibrary new];
            [lb assetForURL:[info objectForKey:UIImagePickerControllerReferenceURL] resultBlock:^(ALAsset *asset) {
                [self insertImagesToCell:[NSArray arrayWithObject:asset]];
            } failureBlock:^(NSError *error) {
                [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
            }];
        }
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissModalViewControllerAnimated:YES];
    [_firstResponder becomeFirstResponder];
}

#pragma mark - ZYQAssetPickerController Delegate
-(void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets
{
    [_firstResponder resignFirstResponder];
    [self insertImagesToCell:[NSArray arrayWithArray:assets]];
}

-(void)assetPickerControllerDidCancel:(ZYQAssetPickerController *)picker
{
    [_firstResponder becomeFirstResponder];
}

/*
#pragma mark CTAssetsPickerControllerDelegate
- (void)assetsPickerController:(CTAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets
{
    [_firstResponder resignFirstResponder];
    [self insertImagesToCell:[NSArray arrayWithArray:assets]];
}

- (void)assetsPickerControllerDidCancel:(CTAssetsPickerController *)picker
{
    [_firstResponder becomeFirstResponder];
}
 */

#pragma mark UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    //_keyboardToolbar.hidden = YES;
    _firstResponder = textField;
    
    CGRect rect = self.contentTableView.frame;
    rect.size.height += 44;
    self.contentTableView.frame = rect;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect rect = self.contentTableView.frame;
    rect.size.height -= 44;
    self.contentTableView.frame = rect;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //收起键盘
    [textField resignFirstResponder];
    _firstResponder = nil;
    return YES;
}

#pragma mark UITextViewDelegate
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    //_keyboardToolbar.hidden = NO;
    _firstResponder = textView;
    [self updateTextViewCellHeight:textView];
    [self scrollToInput:textView.tag];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
}

- (void)textViewDidChange:(UITextView *)textView
{
    [self updateTextViewCellHeight:textView];
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
//    NSLog(@"%@",text);
//
//    NSLog(@"%@",@{@"123":text});
    
    if ([[[UITextInputMode currentInputMode]primaryLanguage] isEqualToString:@"emoji"]) {
        return NO;
    }
    return YES;
    
    if ([self isContainsEmoji:text]) {
        return NO;
    }
    return YES;
    
}
#pragma mark UITableViewDatasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.inputDataSource[indexPath.row][@"height"] floatValue];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.inputDataSource.count;
}

- (CDFPostingViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIndentifer = [NSString stringWithFormat:@"%@",self.inputDataSource[indexPath.row][@"type"]];
    CDFPostingViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifer];
    
    if ([cellIndentifer isEqualToString:@"Text"]) {
        cell.contentTextArea.delegate = self;
        cell.contentTextArea.tag = indexPath.row;
        cell.contentTextArea.text = [NSString stringWithFormat:@"%@",self.inputDataSource[indexPath.row][@"content"]];
        [self.inputDataSource[indexPath.row] setObject:cell.contentTextArea forKey:@"control"];
        if (!_firstResponder) {
            [cell.contentTextArea becomeFirstResponder];
        }
    } else if ([cellIndentifer isEqualToString:@"Image"]) {
        
        CDFAttachment *attach = self.inputDataSource[indexPath.row][@"content"];
        cell.attachmentImageView.image = [UIImage imageWithData:[attach getThumbnailData]];
        [_inputDataSource[indexPath.row] setObject:cell.attachmentImageView forKey:@"control"];
    }
    return cell;
}

#pragma mark UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"%@",self.inputDataSource[indexPath.row]);
    [self locationFirstResponderFromIndexPath:indexPath];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView
            :(NSIndexPath *)indexPath
{
    return [self.inputDataSource[indexPath.row][@"editable"] boolValue] ? UITableViewCellEditingStyleDelete : UITableViewCellEditingStyleNone;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"%d",[self.inputDataSource[indexPath.row][@"editable"] boolValue]);
    return [self.inputDataSource[indexPath.row][@"editable"] boolValue];
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self.inputDataSource[indexPath.row][@"editable"] boolValue];
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)
sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    NSUInteger fromRow = [sourceIndexPath row];
    NSUInteger toRow = [destinationIndexPath row];
    
    //    从数组中读取需要移动行的数据
    id object = [self.inputDataSource objectAtIndex:fromRow];
    //    在数组中移动需要移动的行的数据
    [self.inputDataSource removeObjectAtIndex:fromRow];
    //    把需要移动的单元格数据在数组中，移动到想要移动的数据前面
    [self.inputDataSource insertObject:object atIndex:toRow];
    [self.contentTableView reloadData:YES animateFrom:kCATransitionFade];
    //NSLog(@"move %d to %d",fromRow, toRow);
}


- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"删除本段";
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //删除一行
        [self.inputDataSource removeObjectAtIndex:indexPath.row];
//        [tableView beginUpdates];
//        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationBottom];
//        [tableView endUpdates];
        [tableView reloadData];
        [self dealTableViewFooter];
    }
}
- (BOOL)isContainsEmoji:(NSString *)string {
    
    __block BOOL isEomji = NO;
    [string enumerateSubstringsInRange:NSMakeRange(0, [string length]) options:NSStringEnumerationByComposedCharacterSequences usingBlock:
     ^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
         const unichar hs = [substring characterAtIndex:0];
         if (0xd800 <= hs && hs <= 0xdbff) {
             if (substring.length > 1) {
                 const unichar ls = [substring characterAtIndex:1];
                 const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                 if (0x1d000 <= uc && uc <= 0x1f77f) {
                     isEomji = YES;
                 }
             }
         } else if (substring.length > 1) {
             const unichar ls = [substring characterAtIndex:1];
             if (ls == 0x20e3) {
                 isEomji = YES;
             }
         } else {
             if (0x2100 <= hs && hs <= 0x27ff && hs != 0x263b) {
                 isEomji = YES;
             } else if (0x2B05 <= hs && hs <= 0x2b07) {
                 isEomji = YES;
             } else if (0x2934 <= hs && hs <= 0x2935) {
                 isEomji = YES;
             } else if (0x3297 <= hs && hs <= 0x3299) {
                 isEomji = YES;
             } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50|| hs == 0x231a ) {
                 isEomji = YES;
             }
         }
     }];
    return isEomji;
}
@end
