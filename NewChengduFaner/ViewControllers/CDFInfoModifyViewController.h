//
//  CDFInfoModifyViewController.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 14-1-2.
//  Copyright (c) 2014年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDFInfoModifyViewController : UITableViewController

@property (nonatomic, strong) NSDictionary *userInfo;

@end
