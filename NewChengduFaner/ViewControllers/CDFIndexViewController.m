//
//  CDFIndexViewController.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-10-24.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFIndexViewController.h"
#import "CDFIndexCell.h"
#import "CDFForumViewController.h"
#import "CDFLikeAndViews.h"
#import "CDFWebViewController.h"
#import "CDFIMViewController.h"
#import "ChatListViewController.h"
#import "CDFIMCell.h"

#import "ZWDNewIndexCell.h"

#import "CDFTabBarController.h"





@implementation OrderbyView {
    CGFloat offsetTop;
    CGFloat buttonHeight;
    UIImageView *checkedImageView;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];
    self.userInteractionEnabled = YES;
}

- (void)setup
{
    offsetTop = 5;
    buttonHeight = 44;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *ob = [defaults objectForKey:@"cdf_orderby"];
    if (!ob) {
        ob = @"dateline";
    }
    UIImage *checkedImage = [UIImage imageNamed:@"filter_checked"];
    checkedImageView = [[UIImageView alloc] initWithImage:checkedImage];
    for (int i = 0; i < self.dataSource.count; i++) {
        
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, buttonHeight * i + offsetTop, CGRectGetWidth(self.frame), buttonHeight)];
        BOOL act = [_dataSource[i][@"data"] isEqualToString:ob];
       // NSLog(@"357: %@", _dataSource);
        btn.enabled = !act;
        if (act) {
            
            checkedImageView.frame = CGRectMake(CGRectGetWidth(self.frame) - 20, CGRectGetMidY(btn.bounds), checkedImage.size.width, checkedImage.size.height);
            CGRect rect = checkedImageView.frame;
            rect.origin.y = CGRectGetMinY(btn.frame) + 17;
            checkedImageView.frame = rect;
        }
        btn.tag = i;
        [btn setTitle:self.dataSource[i][@"title"] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithWhite:153.0f/255.0f alpha:1] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateDisabled];
        btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        btn.titleLabel.font = [UIFont systemFontOfSize:15];
        [btn setImage:[UIImage imageNamed:self.dataSource[i][@"iconNormal"]] forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:self.dataSource[i][@"iconHover"]] forState:UIControlStateHighlighted];
        [btn setImage:[UIImage imageNamed:self.dataSource[i][@"iconHover"]] forState:UIControlStateDisabled];
        btn.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        btn.titleEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
        [btn addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
    }
    [self addSubview:checkedImageView];
}

- (void)valueChanged:(UIButton *)sender
{
    [self.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[UIButton class]]) {
            [obj setEnabled:(sender.tag != [obj tag])];
        }
    }];
    CGRect rect = checkedImageView.frame;
    rect.origin.y = CGRectGetMinY(sender.frame) + 17;
    checkedImageView.frame = rect;
    self.value = self.dataSource[sender.tag][@"data"];
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}

- (NSArray *)dataSource
{
    if (!_dataSource) {
        NSMutableArray *array = [NSMutableArray array];
        
        [array addObject:@{@"title":@"回复时间",@"iconNormal":@"reply_normal",@"iconHover":@"reply_hl",@"checked":@"0",@"data":@"lastpost"}];
        [array addObject:@{@"title":@"发表时间",@"iconNormal":@"time_normal",@"iconHover":@"time_hl",@"checked":@"1",@"data":@"dateline"}];
        [array addObject:@{@"title":@"我的收听",@"iconNormal":@"follow_normal",@"iconHover":@"follow_hl",@"checked":@"0",@"data":@"myfollow"}];
        _dataSource = array;
    }
    return _dataSource;
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, CGRectGetMinX(rect), CGRectGetMinY(rect) + offsetTop);
    CGPathAddLineToPoint(path, NULL, CGRectGetMinX(rect), CGRectGetMaxY(rect));
    CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(rect), CGRectGetMaxY(rect));
    CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(rect), CGRectGetMinY(rect) + offsetTop);
    CGPathAddLineToPoint(path, NULL, CGRectGetMidX(rect) + offsetTop, CGRectGetMinY(rect) + offsetTop);
    CGPathAddLineToPoint(path, NULL, CGRectGetMidX(rect), CGRectGetMinY(rect));
    CGPathAddLineToPoint(path, NULL, CGRectGetMidX(rect) - offsetTop, CGRectGetMinY(rect) + offsetTop);
    CGPathCloseSubpath(path);
    
    CGContextAddPath(context, path);
    [[UIColor colorWithWhite:0 alpha:.9] setFill];
    CGContextFillPath(context);
    
    for (int i = 1; i < self.dataSource.count; i++) {
        draw1PxStroke(context, CGPointMake(CGRectGetMinX(rect), i *(CGRectGetMaxY(rect) - offsetTop) / 3 + offsetTop), CGPointMake(CGRectGetMaxX(rect), i *(CGRectGetMaxY(rect) - offsetTop) / 3 + offsetTop), [UIColor colorWithWhite:102.0f/255.0f alpha:1].CGColor);
    }
}

@end

@interface CDFIndexViewController () <NSURLConnectionDataDelegate>

@property (nonatomic,strong) id refreshHeaderView;
@property (nonatomic,strong) NSArray *advDataSource;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingActInd;
@property (strong, nonatomic) IBOutlet UILabel *loadingLabel;
@property (strong, nonatomic) IBOutlet UIButton *closeAdvButton;
@property (strong, nonatomic) IBOutlet UIImageView *advImageView;
@property (strong, nonatomic) IBOutlet UIView *tableHeaderView;
@property (strong, nonatomic) CDFForumViewController *leftViewController;   //板块控制器
@property (strong, nonatomic) UIButton *titleButton;

@property (strong, nonatomic) NSCache *heightCache;
@property (strong, nonatomic) NSString *orderBy;
@property (strong, nonatomic) OrderbyView *orderByView;
@property (strong, nonatomic) UIView *orderMaskView;
@property (strong, nonatomic) CDFWelcomeScrollView *welcomeScrollView;


- (IBAction)doRemoveAdv:(id)sender;

@end

@implementation CDFIndexViewController {
    NSMutableArray *_listDataSource;
    UIImageView *adimageView;
    NSInteger   _currentPage;
    CGPoint     _scrollPoint;
    BOOL        _isLoading;
    BOOL        _hasMore;
    BOOL        _showAdv;
}

-(void)start {
    
    NSDictionary *adInfo = [Common unarchiveForKey:@"adInfo"];
    if (!adInfo) {
        //第一次进入
        //[self downloadAD];
    } else {
        NSString *adPic = [Common unarchiveForKey:@"adInfo"][@"pic"];
        NSTimeInterval end = [[Common unarchiveForKey:@"adInfo"][@"end"] longLongValue];
        NSString *startImgPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"startImage.png"];
        BOOL shouldShow = YES;
        
        NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
        
        if (now > end && end > 0) {
            shouldShow = NO;
        }
        
        if (shouldShow) {
            UIImageView *iv = [[UIImageView alloc] initWithImage:[UIImage imageWithData:[NSData dataWithContentsOfFile:startImgPath]]];
            iv.contentMode = UIViewContentModeCenter;
            iv.frame = [UIScreen mainScreen].bounds;
            iv.image = [UIImage imageWithContentsOfFile:startImgPath];
            iv.backgroundColor = [UIColor whiteColor];
            iv.userInteractionEnabled = YES;
            UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:Nil];
            [iv addGestureRecognizer:pan];
            adimageView = iv;
            
            [self.tabBarController.view addSubview:adimageView];
            [UIView animateWithDuration:1 delay:2 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                adimageView.transform = CGAffineTransformMakeScale(1.5, 1.5);
                adimageView.alpha = 0;
            } completion:^(BOOL finished) {
                [adimageView removeFromSuperview];
            }];
        }
        //[self downloadAD];
    }
    /*
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *lastVersion = [defaults objectForKey:@"lastVersion"];
    NSString *currentVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    
    NSLog(@"%@ %@",lastVersion, currentVersion);
    if (![lastVersion isEqualToString:currentVersion]) {
        //新安装版本
        [defaults setObject:currentVersion forKey:@"lastVersion"];
        [self presentModalViewController:self.welcomeScrollView animated:NO];
    }
     */
}

- (void)downloadAD
{
    NSMutableDictionary *adInfo = [NSMutableDictionary dictionary];
    NSDictionary *saveAdInfo = [Common unarchiveForKey:@"adInfo"];
    
    [[HXHttpClient shareInstance] grabURL:[NSString stringWithFormat:@"api/mobile/?version=%@&module=indexad", kApiVersion] success:^(id responseObject) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kMESSAGE_BADGE object:nil userInfo:responseObject[@"Variables"][@"newmessage2"]];
        //        NSLog(@"%@", [Common dictionaryToJSON:responseObject]);
        if (![responseObject[@"Variables"][@"forum_recommend"] isKindOfClass:[NSArray class]]) {
            return;
        }
        if ([responseObject[@"Variables"][@"forum_recommend"] count] == 0) {
            return;
        }
        
        NSString *picurl = responseObject[@"Variables"][@"forum_recommend"][0][@"pic"];
        NSArray *tempic = [picurl componentsSeparatedByString:@"/"];
        NSString *picName = [tempic lastObject];
        //        NSLog(@"%@", picName);
        if ([picName isEqualToString:saveAdInfo[@"pic"]]) {
            return;
        }
        [adInfo setObject:picName forKey:@"pic"];
        [adInfo setObject:responseObject[@"Variables"][@"forum_recommend"][0][@"enddate"] forKey:@"end"];
        NSString *downPath =  [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"startImage.png"];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:picurl]];
        
        [[HXHttpClient shareInstance] downloadRequest:request toPath:downPath success:^(id responseObject) {
            
           // NSLog(@"download OK");
            [Common archive:adInfo withKey:@"adInfo"];
            
        } fail:^(NSError *error) {
            //NSLog(@"download error");
        }];
        
        
    } fail:^(NSError *error) {
        //NSLog(@"%@", error);
    }];
}

#pragma mark Init Methods
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];
}

- (void)setup
{
    _currentPage = 1;
    _isLoading = NO;
    _scrollPoint = CGPointZero;
#pragma mark - 设置是否显示上端广告位
    _showAdv = NO;
}

#pragma mark Getter And Setter Methods

- (CDFWelcomeScrollView *)welcomeScrollView
{
    if (!_welcomeScrollView) {
        _welcomeScrollView = [CDFWelcomeScrollView new];
    }
    return _welcomeScrollView;
}
/**
 *  实现listDataSource的get方法
 *
 *  @return NSMutableArray
 */
- (NSMutableArray *)listDataSource
{
    if (!_listDataSource) {
        _listDataSource = [NSMutableArray new];
    }
    return _listDataSource;
}

/**
 *  实现listDataSource的set方法
 *
 *  @param listDataSource 数据源
 */
- (void)setListDataSource:(NSMutableArray *)listDataSource
{
    if (_currentPage <= 1) {
        [self.heightCache removeAllObjects];
        [_listDataSource removeAllObjects];
    }
    //[_listDataSource addObjectsFromArray:listDataSource];
    for (NSDictionary *dic in listDataSource) {
        [_listDataSource addObject:[NSMutableDictionary dictionaryWithDictionary:dic]];
    }
    
    [self.tableView reloadData];
}

- (NSCache *)heightCache
{
    if (!_heightCache) {
        _heightCache = [NSCache new];
    }
    return _heightCache;
}

/**
 *  实现mainData的set方法
 *
 *  @param mainData 服务器返回的数据
 */
- (void)setMainData:(NSDictionary *)mainData
{
    _mainData = mainData;
    self.listDataSource = mainData[@"Variables"][@"forum_threadlist"];
    self.advDataSource = mainData[@"Variables"][@"forum_recommend"];
}

/**
 *  实现refreshHeaderView的get方法
 *
 *  @return refreshHeaderView
 */
- (id)refreshHeaderView
{
    if (!_refreshHeaderView) {
        if ([Common systemVersion] < 6.0) {
            
        } else {
            UIRefreshControl *rc = [[UIRefreshControl alloc] init];
            rc.attributedTitle = [[NSAttributedString alloc] initWithString:@"用力，不要停..."];
            [rc addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
            _refreshHeaderView = rc;
        }
    }
    return _refreshHeaderView;
}

/**
 *  advDataSource set方法，用于刷新广告
 *
 *  @param advDataSource 广告数据源
 */
- (void)setAdvDataSource:(NSArray *)advDataSource
{
    _advDataSource = advDataSource;
    if (_advDataSource.count <= 0) {
        self.tableView.tableHeaderView = nil;
    } else {
        if (!_showAdv) {
            self.tableView.tableHeaderView = nil;
        } else {
            
            //从广告列表里面随机取一条广告进行显示
            int rnd = arc4random() % _advDataSource.count;
            [self.advImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",_advDataSource[rnd][@"pic"]]]];
            [self.advImageView setImageWithURL:[NSURL URLWithString:[Common getThumbImage:[NSString stringWithFormat:@"%@",_advDataSource[rnd][@"pic"]] size:CGSizeMake(_advImageView.frame.size.width * 2 , _advImageView.frame.size.height * 2 ) withRetinaDisplayDetect:YES]] placeholderImage:[UIImage imageNamed:@"bg_imageloader"]];
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(adClicked:)];
            self.advImageView.tag = rnd;
            self.advImageView.userInteractionEnabled = YES;
            [self.advImageView addGestureRecognizer:tap];
            self.advImageView.contentMode = UIViewContentModeScaleAspectFit;
            self.tableView.tableHeaderView = self.tableHeaderView;
        }
    }
}

- (void)adClicked:(UIGestureRecognizer *)gest
{
    [MobClick event:@"ADClick"];
    UIView *view = gest.view;
    NSDictionary *dic = [Common decodeURL:_advDataSource[view.tag][@"url"]];
    //NSLog(@"URL  =  %@", _advDataSource[view.tag][@"url"]);
    NSString *url = _advDataSource[view.tag][@"url"];
   // NSLog(@"dic = %@", dic);
    NSString *newURL = [url substringWithRange:NSMakeRange(0, 4)];
    //NSLog(@"http: %@", newURL);
    
    if (![newURL isEqualToString:@"http"]) {
        [self performSegueWithIdentifier:@"pushToDetail" sender:dic];
    } else {
        //获取uid
        CDFMe *me = [CDFMe shareInstance];
        id uid = me.uid;
        //NSLog(@"uid = %@", [NSString stringWithFormat:@"%@", uid]);
        CDFWebViewController *webView = [[CDFWebViewController alloc] init];
        
        webView.url = [uid isEqualToString:@""] ? _advDataSource[view.tag][@"url"] : [NSString stringWithFormat:@"%@&uid=%@", _advDataSource[view.tag][@"url"], uid];
        //NSLog(@"webView.url = %@", webView.url);
        webView.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:webView animated:YES];
    }
}
/**
 *  刷新控件的逻辑方法
 *
 *  @param refresh 刷新控件
 */
- (void)refreshView:(UIRefreshControl *)refresh
{
    //刷新的逻辑代码
    if (!_isLoading) {
        refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"我正在很用力的加载..."];
        _currentPage = 1;
        [self loadData];
    } else {
        return;
    }
}

- (void)setSelectedForum:(NSDictionary *)selectedForum
{
    _selectedForum = selectedForum;
    _currentPage = 1;
//    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    CDFAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    [delegate.drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:^(BOOL finished) {
        if ([Common systemVersion] >= 7.0) {
            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
        }
    }];
    [SVProgressHUD showWithStatus:@"请稍后..."];
    [self loadData];
}

- (void)resetLoadFlag
{
    _isLoading = NO;
}

- (CDFForumViewController *)leftViewController
{
    if (!_leftViewController) {
        CGFloat leftWidth = 160;
        
        _leftViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ForumViewController"];
        
        CDFAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
        delegate.drawerController.leftDrawerViewController = _leftViewController;
        [delegate.drawerController setMaximumLeftDrawerWidth:leftWidth];
        [delegate.drawerController setGestureCompletionBlock:^(MMDrawerController *drawerController, UIGestureRecognizer *gesture) {
            if ([Common systemVersion] >= 7.0) {
                [[UIApplication sharedApplication] setStatusBarHidden:(drawerController.visibleLeftDrawerWidth == leftWidth) withAnimation:UIStatusBarAnimationSlide];
            }
        }];
    }
    if ([_leftViewController respondsToSelector:@selector(selectedForum)]) {
        [_leftViewController setValue:self.selectedForum forKey:@"selectedForum"];
    }
    return _leftViewController;
}

- (void)showSlideMenu:(id)sender
{
    [MobClick event:@"Menu"];
    CDFAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    [delegate.drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:^(BOOL finished) {
        if ([Common systemVersion] >= 7.0) {
            [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
        }
    }];
}

- (void)avatarClick:(CDFAvatarImageView *)avatarView
{
    //NSLog(@"%@",avatarView.uid);
    [MobClick event:@"AvatarClick"];
    id userViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"userViewController"];
    if ([userViewController respondsToSelector:@selector(uid)]) {
        [userViewController setValue:avatarView.uid forKey:@"uid"];
    }
    [self.navigationController pushViewController:userViewController animated:YES];
}

- (void)boardChangeHandler:(NSNotification *)note
{
    self.selectedForum = note.userInfo;
    if (note.userInfo) {
        self.navigationItem.title = note.userInfo[@"name"];
    } else {
        self.navigationItem.title = @"聚精彩";
    }
    [MobClick event:@"BoardChanged" label:self.navigationItem.title];
    [self.titleButton setTitle:self.navigationItem.title forState:UIControlStateNormal];
}

- (UIButton *)titleButton
{
    if (!_titleButton) {
        _titleButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _titleButton.frame = CGRectMake(0, 0, 150, 44);
        [_titleButton setTitle:self.navigationItem.title forState:UIControlStateNormal];
        _titleButton.titleLabel.font = [UIFont systemFontOfSize:20];
        [_titleButton setTitleColor:TITLEGRAY forState:UIControlStateNormal];
        [_titleButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithWhite:0 alpha:.1]] forState:UIControlStateHighlighted];
        [_titleButton addTarget:self action:@selector(triggerOrderView:) forControlEvents:UIControlEventTouchUpInside];
        [_titleButton setImage:[UIImage imageNamed:@"ddl_arrow"] forState:UIControlStateNormal];
        _titleButton.imageEdgeInsets = UIEdgeInsetsMake(0, 100, 0, 0);
        _titleButton.titleEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0);
    }
    return _titleButton;
}

- (void)triggerOrderView:(id)sender
{
    [self.tabBarController.view addSubview:self.orderMaskView];
}

- (void)setOrderBy:(NSString *)orderBy
{
    if ([_orderBy isEqualToString:orderBy]) {
        [self dismissOrderView:nil];
        return;
    }
    [MobClick event:@"OrderChanged" label:orderBy];
    _orderBy = orderBy;
    NSUserDefaults *defualts = [NSUserDefaults standardUserDefaults];
    [defualts setObject:orderBy forKey:@"cdf_orderby"];
    _currentPage = 1;
    [self.heightCache removeAllObjects];
    //[self loadData];
}

- (OrderbyView *)orderByView
{
    if (!_orderByView) {
        CGFloat bugHeight = 0;
        if ([Common systemVersion] >= 7.0) {
            bugHeight = 20;
        }
        _orderByView = [[OrderbyView alloc] initWithFrame:CGRectMake(0, 39 + bugHeight, CGRectGetWidth(self.view.bounds), 140)];
        _orderByView.backgroundColor = [UIColor clearColor];
        [_orderByView addTarget:self action:@selector(orderByChanged:) forControlEvents:UIControlEventValueChanged];
        _orderByView.tag = 999;
    }
    return _orderByView;
}

- (UIView *)orderMaskView
{
    if (!_orderMaskView) {
        _orderMaskView = [[UIView alloc] initWithFrame:self.tabBarController.view.frame];
        _orderMaskView.backgroundColor = [UIColor colorWithWhite:0 alpha:.1];
        _orderMaskView.userInteractionEnabled = YES;
        if ([Common systemVersion] >= 6.0) {
            UITapGestureRecognizer *gest = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissOrderView:)];
            [_orderMaskView addGestureRecognizer:gest];
        }
        UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dismissOrderView:)];
        [_orderMaskView addGestureRecognizer:pan];
        [_orderMaskView addSubview:self.orderByView];
    }
    return _orderMaskView;
}

- (void)dismissOrderView:(UIGestureRecognizer *)gest
{
    [_orderMaskView removeFromSuperview];
}

- (void)orderByChanged:(OrderbyView *)order
{
    self.orderBy = order.value;
    [self dismissOrderView:nil];
}

- (void)refresh:(id)sender
{
#warning 处理刷新crash
    _currentPage = 1;
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    [SVProgressHUD showWithStatus:@"刷新中..."];
    [MobClick event:@"IndexRefresh"];
    [self loadData];
}

- (void)handlePostSuccess:(NSNotification *)note
{
    [self refresh:note];
    [self.tabBarController setSelectedIndex:0];
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}


#pragma mark UIViewController Supper Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self start];
    
    self.orderBy = [[NSUserDefaults standardUserDefaults] objectForKey:@"cdf_orderby"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(boardChangeHandler:) name:kBOARD_CHANGED object:nil];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handlePostSuccess:) name:kPOST_SUCCESS object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleMessageChange:) name:kMESSAGE_BADGE object:nil];
    
    self.navigationItem.titleView = self.titleButton;
    
    self.closeAdvButton.backgroundColor = [UIColor colorWithWhite:0 alpha:.5];
    self.closeAdvButton.layer.cornerRadius = 10;
    self.tableView.tableHeaderView = nil;
    
    if ([Common systemVersion] < 7.0) {
        UIView *bg = [[UIView alloc] initWithFrame:self.tableView.bounds];
        bg.backgroundColor = bgColor;
        self.tableView.backgroundView = bg;
    } else {
        self.view.backgroundColor = bgColor;
    }
    [self loadData];
    
    if ([Common systemVersion] >= 6.0) {
        self.refreshControl = self.refreshHeaderView;
    }
    
    UIButton *leftBarButtonItem = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBarButtonItem.frame = CGRectMake(0, 0, 30, 30);
    [leftBarButtonItem addTarget:self action:@selector(showSlideMenu:) forControlEvents:UIControlEventTouchUpInside];
    [leftBarButtonItem setImage:[UIImage imageNamed:@"blocklist"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftBarButtonItem];
    
    UIButton *rightBarButtonItem = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBarButtonItem.frame = CGRectMake(0, 0, 30, 30);
    [rightBarButtonItem setImage:[UIImage imageNamed:@"refresh"] forState:UIControlStateNormal];
    [rightBarButtonItem setImage:[UIImage imageNamed:@"refreshHight"] forState:UIControlStateHighlighted];
    [rightBarButtonItem addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarButtonItem];
    if ([Common systemVersion] >= 7.0) {
        leftBarButtonItem.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 0);
        rightBarButtonItem.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -25);
    }
   
}

- (void)viewWillAppear:(BOOL)animated
{
    
    //    [self isNeedUpdateVewsion];
    /*
    NSArray *conversations = [[[EaseMob sharedInstance] chatManager] conversations];
    NSInteger unreadCount = 0;
    for (EMConversation *conversation in conversations) {
        unreadCount += conversation.unreadMessagesCount;
    }
    if ([[[EaseMob sharedInstance] chatManager] isLoggedIn] && unreadCount > 0) {
        [[[self.tabBarController viewControllers][3] tabBarItem] setBadgeValue:[NSString stringWithFormat:@"%d", unreadCount + [CDFMe shareInstance].newpm]];
    }
     */
    
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"聚精彩"];
    //    if (![Common unarchiveForKey:kOnlineInfo]) {
    //        UIStoryboard *user = [UIStoryboard storyboardWithName:@"welcome" bundle:nil];
    //        [self presentModalViewController:user.instantiateInitialViewController animated:NO];
    //    }
    
    
#warning 1.4代码注释
    /*
    if (self.leftViewController) {
        
        CDFAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
        delegate.drawerController.leftDrawerViewController = self.leftViewController;
        [delegate.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
        [delegate.drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    }
     */
}
static bool isShow = NO;
- (void)apper{
    if (![CDFMe shareInstance].isLogin && self.JuJCType == favJuJC && !isShow) {
        
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"温馨提醒" message:@"请登录" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alert show];
        isShow = YES;
    }
}
#pragma mark - alertdelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    //NSLog(@"%d",buttonIndex);
    if (buttonIndex == 1) {
        //确定
        [[CDFMe shareInstance] wellcomeLogin:self];
        
    }else{
        //取消
        
    }
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"聚精彩"];
#warning 1.4代码注释
    /*
    CDFAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    delegate.drawerController.leftDrawerViewController = nil;
    [delegate.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeNone];
    [delegate.drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeNone];
     */
    [self dismissOrderView:Nil];
     
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *imageArray =[self formatAttachmentList:self.listDataSource[indexPath.section][@"attachments"]];
    if (imageArray.count < 3) {
        NSString *key = [NSString stringWithFormat:@"%d-%d",indexPath.section,indexPath.row];
        CGFloat height = [[self.heightCache objectForKey:key] floatValue];
        if (height <= 0) {
            
            height = 246 - [self offsetHeightAtIndexPath:indexPath];
            [self.heightCache setObject:[NSNumber numberWithFloat:height] forKey:key];
            
        }
        return height;
        
    }
    
    NSString *key = [NSString stringWithFormat:@"%d-%d",indexPath.section,indexPath.row];
    CGFloat height = [[self.heightCache objectForKey:key] floatValue];
    if (height <= 0) {
        
            height = 246 - [self offsetHeightAtIndexPath:indexPath];
            [self.heightCache setObject:[NSNumber numberWithFloat:height] forKey:key];
        
    }
    return height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.bounds), [self tableView:tableView heightForFooterInSection:section])];
    view.backgroundColor = [Common colorWithHex:@"#E2E2E2" alpha:1];
//    view.backgroundColor = RGBACOLOR(243, 243, 243, 1);
    return view;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.listDataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *imageArray =[self formatAttachmentList:self.listDataSource[indexPath.section][@"attachments"]];
    
    static NSString *CellIdentifier = @"Cell";
    CDFIndexCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    if (self.JuJCType == favJuJC) {
        cell.isFav = YES;
    }else{
        cell.isFav = NO;
    }
    
    cell.dataSource = self.listDataSource[indexPath.section];

    
    cell.nav = self.navigationController;
    NSString *authorFaceURL = [Common formatAvatarWithUID:_listDataSource[indexPath.section][@"authorid"] type:@"small"];
    
    [cell.authorFaceView setImageWithURL:[NSURL URLWithString:authorFaceURL]];
    [cell.authorFaceView addTarget:self action:@selector(avatarClick:) forControlEvents:UIControlEventTouchUpInside];
    cell.authorFaceView.uid = self.listDataSource[indexPath.section][@"authorid"];
    cell.authorFaceView.layer.masksToBounds = YES;
    cell.authorFaceView.layer.cornerRadius = 15;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.authorLabel.text = [NSString stringWithFormat:@"%@",self.listDataSource[indexPath.section][@"author"]];
    
    NSString *subject = self.listDataSource[indexPath.section][@"subject"];
    NSString *subject2 = [NSString stringWithFormat:@"【%@】", subject];
    
    NSString *newSubject = [subject rangeOfString:@"【"].location != NSNotFound ? subject : subject2 ;
    NSString *summaryStr = [NSString stringWithFormat:@"%@%@",newSubject,(self.listDataSource[indexPath.section][@"message"] ? self.listDataSource[indexPath.section][@"message"] : @"")];
    cell.summaryLabel.text = [summaryStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    CGFloat summaryLabelHeight = [self heightForSummaryAtIndexPath:indexPath];
    CGRect summaryFrame = cell.summaryLabel.frame;
    summaryFrame.size.height = summaryLabelHeight;
    cell.summaryLabel.frame = summaryFrame;
    cell.attachmentsData = [self formatAttachmentList:self.listDataSource[indexPath.section][@"attachments"]];
    NSString *date = [Common formatDate:[self.listDataSource[indexPath.section][@"dbdateline"] longLongValue]];
    
    cell.timeAndBoardLabel.text = [NSString stringWithFormat:@"%@ 发表于 %@", date, self.listDataSource[indexPath.section][@"boardname"]];
    
    cell.likeAndViewsControl.likes = [self.listDataSource[indexPath.section][@"recommend_add"] integerValue];
    cell.likeAndViewsControl.tid = self.listDataSource[indexPath.section][@"tid"];
    
    cell.likeAndViewsControl.views = [[_listDataSource[indexPath.section] objectForKey:@"replies"] integerValue];
    
    cell.likeAndViewsControl.liked = [self.listDataSource[indexPath.section][@"like"] boolValue];
    cell.likeAndViewsControl.indexPath = indexPath;
    [cell.likeAndViewsControl addTarget:self action:@selector(likeAction:) forControlEvents:UIControlEventEditingChanged];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    [cell layoutMySubviews];
    
    cell.viewControllerl = self;
    cell.indexPath = indexPath;
    //NSLog(@"%@",self.listDataSource[indexPath.section]);
    //NSLog(@"%d",indexPath.row);
    return cell;
}

- (void)likeAction:(CDFLikeAndViews *)control
{
    [self.listDataSource[control.indexPath.section] setObject:[NSNumber numberWithBool:YES] forKey:@"like"];
    NSInteger likes = [self.listDataSource[control.indexPath.section][@"recommend_add"] integerValue];
    [self.listDataSource[control.indexPath.section] setObject:[NSNumber numberWithInteger:likes+1] forKey:@"recommend_add"];
}

#pragma mark UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark Private Methods
/**
 *  刷新完成以后调用
 *
 *  @param refresh 刷新控件
 *  @param message 显示的消息，如果没有特殊消息显示则会显示默认消息
 */
- (void)refreshView:(UIRefreshControl *)refresh endRefreshWithMessage:(NSString *)message
{
    [self performSelector:@selector(resetLoadFlag) withObject:nil afterDelay:3];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM.dd, hh:mm a"];
    NSString *lastUpdated = [NSString stringWithFormat:@"%@ %@", [formatter stringFromDate:[NSDate date]], (message.length > 0 ? message : @"刷新成功")];
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:lastUpdated];
    [refresh endRefreshing];
}

/**
 *  计算indexpath的高度
 *
 *  @param indexPath cell的位置
 *
 *  @return CGFloat 高度
 */
- (CGFloat)offsetHeightAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = 0.0;
    NSArray *array = [self formatAttachmentList:self.listDataSource[indexPath.section][@"attachments"]];
    if (array.count < 3) {
        height += (array.count <= 0 ? 96.33 : (-104 - 50));
        
        height += (60 - [self heightForSummaryAtIndexPath:indexPath]);
        
        return height;
    }
    
    height += (array.count <= 0 ? 96.333 : 0);

    height += (60 - [self heightForSummaryAtIndexPath:indexPath]);
    
    return height;
}

/**
 *  计算摘要的高度
 *
 *  @param indexPath 摘要所属的indexpath
 *
 *  @return CGFloat 高度
 */
- (CGFloat)heightForSummaryAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *summaryStr = [NSString stringWithFormat:@"%@%@",self.listDataSource[indexPath.section][@"subject"],(self.listDataSource[indexPath.section][@"message"] ? self.listDataSource[indexPath.section][@"message"] : @"")];
    CGSize summarySize = [[summaryStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] sizeWithFont:[UIFont systemFontOfSize:18] constrainedToSize:CGSizeMake(280, ([Common systemVersion] >= 7.0 ? 140 : 60))];
    
    return summarySize.height;
}

/**
 *  将数据源中的附件对象转化为数组
 *
 *  @param object 要转化的对象
 *
 *  @return 数组
 */
- (NSArray *)formatAttachmentList:(id)object
{
    NSMutableArray *array = [NSMutableArray new];
    for (id obj in object) {
        [array addObject:object[obj]];
    }
    return array;
}


/**
 *  从网络获取数据
 */
- (void)loadData
{
    if (_isLoading) {
        return;
    }
    if (self.JuJCType == favJuJC) {
        _orderBy = @"myfav";
        if (![CDFMe shareInstance].isLogin) {
            //未登录
            //[SVProgressHUD showErrorWithStatus:@"请登录" duration:5];

            [self resetLoadFlag];
            [self refreshView:_refreshHeaderView endRefreshWithMessage:@"刷新失败"];
            [self.loadingActInd stopAnimating];
            self.loadingLabel.text = @"请登录！";

            return;
        }
    }else if (self.JuJCType == bestJuJC){
        _orderBy = @"digest";
    }else if (self.JuJCType == allJuJC){
        _orderBy = @"dateline";
    }
    
    [self.loadingActInd startAnimating];
    self.loadingLabel.text = @"正在加载数据...";
    
    _isLoading = YES;
    self.navigationItem.rightBarButtonItem.customView.userInteractionEnabled = NO;//开始下载的时候把导航栏右侧刷新建的用户交互关掉
    
    
    NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=topicnew&page=%d%@%@",kApiVersion ,_currentPage, self.selectedForum ? [NSString stringWithFormat:@"&fid=%@",_selectedForum[@"fid"]] : @"", _orderBy ? [NSString stringWithFormat:@"&order=%@",_orderBy] : @"&order=dateline"];
    //NSLog(@"%@--%@",kBaseURL,url);
    [[HXHttpClient shareInstance] grabURL:url success:^(id responseObject) {
        //NSLog(@"%@",responseObject);
        self.navigationItem.rightBarButtonItem.customView.userInteractionEnabled = YES;//下载成功后把导航栏右侧的刷新建的用户交互打开
        //NSLog(@"%@",responseObject[@"Variables"][@"forum_threadlist"]);
        if ([responseObject[@"Variables"][@"forum_threadlist"] count] == 0&& self.JuJCType == favJuJC) {
            //空
            //[SVProgressHUD showSuccessWithStatus:@"未关注任何版块！" duration:3];
            [self resetLoadFlag];
            [self refreshView:_refreshHeaderView endRefreshWithMessage:@"刷新成功"];
            [self.loadingActInd stopAnimating];
            self.loadingLabel.text = @"未关注任何版块！";
            return;
        }
        if (!responseObject[@"Variables"]) {
            [self resetLoadFlag];
            [self refreshView:_refreshHeaderView endRefreshWithMessage:@"刷新成功"];
            [self.loadingActInd stopAnimating];
            self.loadingLabel.text = @"没有数据！";
            return;
        }
        //NSLog(@"8989**: %@\n --9898**",responseObject);
        if ([CDFMe shareInstance].isLogin && [[EaseMob sharedInstance].chatManager isLoggedIn]) {
            //        newmypost = 0;
            //        newpm = 0;
            //        newprompt = 0;
            //        newpush = 0;
          
            NSInteger replyCount = [responseObject[@"Variables"][@"notice"][@"newreply"] integerValue];
            NSInteger followCount = [responseObject[@"Variables"][@"notice"][@"newfollow"] integerValue];
            NSInteger likeCount = [responseObject[@"Variables"][@"notice"][@"newlike"] integerValue];

            if (replyCount > 0) {
//                [CDFMe shareInstance].newpm = count;
                [CDFMe shareInstance].tiezeCount = replyCount;
//                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//                CDFIMCell *cell = (CDFIMCell *)[((ChatListViewController *)([[self.tabBarController viewControllers] objectAtIndex:3])).tableView cellForRowAtIndexPath:indexPath];
//                cell.badgeLabel.hidden = NO;
//                cell.badgeLabel.text = [NSString stringWithFormat:@"%d", count + 100];
//                cell.badgeLabel.textAlignment =  NSTextAlignmentCenter;
            }
            if (followCount > 0) {
                [CDFMe shareInstance].newTingZhongCount = followCount;
            }
            if (likeCount > 0) {
                [CDFMe shareInstance].newLikeCount = likeCount;
            }
            NSArray *conversations = [[[EaseMob sharedInstance] chatManager] conversations];
            NSInteger unreadCount = 0;
            for (EMConversation *conversation in conversations) {
                unreadCount += conversation.unreadMessagesCount;
            }
            NSInteger totalCount = 0;
            totalCount = unreadCount + likeCount + replyCount + followCount;
//            if (totalCount > 0) {
//                NSString *badgeValue = [NSString stringWithFormat:@"%d", totalCount];
//               // NSLog(@"unreadCount+count:%@", badgeValue);
//                [self didUnreadMessagesCountChanged];
//            }
        }
        //NSLog(@"ceshi8");
        
        [SVProgressHUD dismiss];
        [self resetLoadFlag];
        //NSLog(@"%@",[Common dictionaryToJSON:responseObject]);
        [[NSNotificationCenter defaultCenter] postNotificationName:kMESSAGE_BADGE object:nil userInfo:responseObject[@"Variables"][@"newmessage2"]];
#pragma mark - zhuwudan 注释
//        if (!self.leftViewController.dataSource) {
//            self.leftViewController.dataSource = responseObject[@"Variables"][@"board"];
//        }
        
        //[CDFMe shareInstance].board = responseObject[@"Variables"][@"board"];
        self.mainData = [NSDictionary dictionaryWithDictionary:responseObject];
        _hasMore = [self.mainData[@"count"] integerValue] <= self.listDataSource.count;
        [self.loadingActInd stopAnimating];
        self.loadingLabel.text = _hasMore ? @"上拉加载更多..." : @"全部加载完毕";
        [self refreshView:_refreshHeaderView endRefreshWithMessage:@"刷新成功"];
    } fail:^(NSError *error) {
        self.navigationItem.rightBarButtonItem.customView.userInteractionEnabled = YES;//下载失败后把导航栏右侧的刷新建的用户交互打开

        [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
        [self resetLoadFlag];
        [self refreshView:_refreshHeaderView endRefreshWithMessage:@"刷新失败"];
    }];
}

#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //下拉加载更多
    if (scrollView.contentSize.height - scrollView.contentOffset.y < CGRectGetHeight(scrollView.frame) + 50) {
        if (_hasMore && !_isLoading) {
            _currentPage++;
            _scrollPoint = CGPointMake(0, scrollView.contentOffset.y);
            [self loadData];
        }
    }
}

#pragma mark Button Click Event
- (IBAction)doRemoveAdv:(id)sender {
    self.tableView.tableHeaderView = nil;
    _showAdv = NO;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([sender isKindOfClass:[UITableViewCell class]]) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        if ([segue.destinationViewController respondsToSelector:@selector(setTid:)]) {
            [segue.destinationViewController setValue:self.listDataSource[indexPath.section][@"tid"] forKey:@"tid"];
        }
    } else if([sender isKindOfClass:[NSDictionary class]]) {
        if ([segue.destinationViewController respondsToSelector:@selector(tid)]) {
            [segue.destinationViewController setValue:sender[@"tid"] forKey:@"tid"];
        }
    }
}
@end
