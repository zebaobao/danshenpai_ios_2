//
//  CDFIndexViewController.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-10-24.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDFAppDelegate.h"
#import "CDFWelcomeScrollView.h"
#import "UMSocial.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import "UMSocialWechatHandler.h"
#import "UMSocialQQHandler.h"

@interface OrderbyView : UIControl
@property (nonatomic, strong) NSArray *dataSource;
@property (nonatomic, strong) NSString *value;
@end

@interface CDFIndexViewController : UITableViewController<UMSocialDataDelegate,UMSocialUIDelegate,UIAlertViewDelegate>

@property (nonatomic,strong) NSDictionary *mainData;
@property (nonatomic,strong) NSDictionary *selectedForum;
@property (nonatomic,strong) NSMutableArray *listDataSource;

@property (nonatomic ,assign) JuJCType JuJCType;
- (void)apper;

@end
