//
//  CDFSignViewController.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 14-1-3.
//  Copyright (c) 2014年 www.huaxi100.com. All rights reserved.
//

#import "CDFSignViewController.h"

@interface CDFSignViewController ()<UITextViewDelegate>
@property (strong, nonatomic) IBOutlet UITextView *signTextView;

@end

@implementation CDFSignViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.signTextView.text = self.userInfo[@"sightml"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"个性签名"];
    [self.signTextView becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"个性签名"];
    [self.signTextView resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)save:(id)sender {
    if (![self.userInfo[@"uid"] isEqual:[CDFMe shareInstance].uid]) {
        return;
    }
    NSString *sig = self.signTextView.text.length <= 0 ? @"" : self.signTextView.text;
    NSDictionary *postData = @{@"sightml":sig};
    NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=userinfo&formhash=%@&op=base&profilesubmit=true",kApiVersion,[CDFMe shareInstance].formHash];
    
    [[HXHttpClient shareInstance] postURL:url postData:postData success:^(id responseObject) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kMESSAGE_BADGE object:nil userInfo:responseObject[@"Variables"][@"newmessage2"]];
        if ([responseObject[@"Message"][@"messageval"] isEqualToString:@"editsuccess"]) {
//            [SVProgressHUD showSuccessWithStatus:responseObject[@"Message"][@"messagestr"]];
            [SVProgressHUD showSuccessWithStatus:@"设置成功"];
            NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:self.userInfo];
            [dic setObject:sig forKey:@"sightml"];
            [[NSNotificationCenter defaultCenter] postNotificationName:kINFO_CHANGED object:nil userInfo:dic];
            
            if ([[self.navigationController.viewControllers lastObject] isKindOfClass:[CDFSignViewController class]]) {
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
    } fail:^(NSError *error) {
//        NSLog(@"%@",error);
        [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
    }];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text compare:@"\n"] == NSOrderedSame) {
        [self save:nil];
        return NO;
    }
    return YES;
}

@end
