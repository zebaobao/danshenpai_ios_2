//
//  CDFDisplayViewController.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-14.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDFMeViewController.h"
#import "HYHActivityDetailFirstCell.h"
#import "ZWDShareButton.h"
#import "DaHeActivityDisplayHeader.h"

@class ActivityTableViewCell;

@interface DisplayFooterView : UIView
@property (strong, nonatomic) IBOutlet UIButton *replyButton;
@end

@interface PageInputView : UIControl
@property (nonatomic, assign) NSInteger maxPage;
@property (nonatomic, assign) NSInteger currentPage;
+ (id)pageWithMaxPage:(NSInteger)max currentPage:(NSInteger)cur inView:(id)view;
@end

@interface CDFDisplayViewController : UIViewController<ZWDShareButtonDelegate>
@property (strong, nonatomic) id tid;
@property (strong, nonatomic) id uid;
@property (strong, nonatomic) id fid;
@property (strong, nonatomic) id pid;
@property (strong,nonatomic) id likeList;
@property (nonatomic, assign) BOOL isActivity;
@property (nonatomic, copy) NSString *activityTitle;
@property (nonatomic, copy) NSString *numberOfperson;
@property (nonatomic, assign) BOOL isActivityClosed;
@property (nonatomic, assign) CGRect rectOfjoinBtn;

@property (strong ,nonatomic) NSMutableArray *urlArr;//保存暗链接

@property (copy,nonatomic) NSString *activityTypeColor;

@property (weak,nonatomic) ActivityTableViewCell *firstCell;
@property (weak,nonatomic) UIView *touView;

- (NSAttributedString *)formatHTMLString:(NSString *)html atIndexPath:(NSIndexPath *)indexPath;
- (void)loadData;
@end
