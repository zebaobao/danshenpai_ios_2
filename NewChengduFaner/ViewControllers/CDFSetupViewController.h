//
//  CDFSetupViewController.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-12.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UMFeedback.h"
#import "CDFWelcomeScrollView.h"
#import "UMSocial.h"

@interface CDFSetupViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UISwitch *shakeSwitch;//聊天震动
@property (weak, nonatomic) IBOutlet UISwitch *voiceSwitch;//聊天声音
@property (weak, nonatomic) IBOutlet UISwitch *locationSwitch;//允许定位
@property (weak, nonatomic) IBOutlet UISwitch *flowSwitch;//省流量模式
@property (weak, nonatomic) IBOutlet UILabel *versionInfo;//版本信息的label
@property (nonatomic, retain) NSMutableDictionary *userInfo;


@end
