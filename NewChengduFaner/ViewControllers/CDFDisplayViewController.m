//
//  CDFDisplayViewController.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-14.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFDisplayViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "DTAttributedTextView.h"
#import "CDFThreadData.h"
#import "CDFDisplayCell.h"
#import "CDFLazyImageView.h"
#import "CDFLikeAndViews.h"
#import "ActivityTableViewCell.h"
#import "UMSocial.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import "UMSocialWechatHandler.h"
#import "UMSocialQQHandler.h"
#import "QViewController.h"
#import "ActivityCommitViewController.h"
#import "HYHActivityDetailFirstCell.h"
#import "ApplyViewController.h"
#import "ZWDWebViewController.h"

#define kSideToLeft     16
#define kHeightOfImage  (KDeviceSizeWidth - 2.0 * kSideToLeft) / 1.8

#define kheightBettowTwoViews   20

#import "ZWDDisplayLikeCell.h"

@implementation DisplayFooterView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:CGRectMake(0, 0, 320, 500)];
    if (self) {
        
        self.backgroundColor = [UIColor redColor];
        UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(3, 0, self.replyButton.bounds.size.width - 6, 44)];
        lable.text = @"回复也是一种鼓励...";
        [self.replyButton addSubview:lable];
    }
    return self;
}

- (void)awakeFromNib
{
    
}

- (void)setup
{
    
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    drawLinearGradient(context, rect, [UIColor colorWithWhite:246.0f/255.0f alpha:1].CGColor, [UIColor colorWithWhite:240.0f/255.0f alpha:1].CGColor, CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect)), CGPointMake(CGRectGetMidX(rect), CGRectGetMaxY(rect)));
    
}
@end

@interface PageInputView() <UITextViewDelegate>
@property (nonatomic, strong) UITextView *inputView;
@property (nonatomic, strong) UIButton *preButton;
@property (nonatomic, strong) UIButton *nextButton;
@property (nonatomic, strong) UIButton *goButton;
@property (nonatomic, strong) UILabel *pageLabel;
@property (nonatomic, strong) UIButton *mask;
@property (nonatomic, strong) UIView *parentView;
@end

@implementation PageInputView

- (UIButton *)preButton
{
    if (!_preButton) {
        _preButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _preButton.frame = CGRectMake(0, 0, 40, CGRectGetHeight(self.frame));
        [_preButton setImage:[UIImage imageNamed:@"btn_pre_hl"] forState:UIControlStateNormal];
        [_preButton setImage:[UIImage imageNamed:@"btn_pre"] forState:UIControlStateHighlighted];
        [_preButton setImage:[UIImage imageNamed:@"btn_pre"] forState:UIControlStateDisabled];
        [_preButton addTarget:self action:@selector(prev:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _preButton;
}

- (void)prev:(id)sender
{
    if (self.currentPage > 1) {
        self.currentPage--;
        [self updateButtons];
    }
}

- (UIButton *)nextButton
{
    if (!_nextButton) {
        _nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _nextButton.frame = CGRectMake(50, 0, 40, CGRectGetHeight(self.frame));
        [_nextButton setImage:[UIImage imageNamed:@"btn_nxt_hl"] forState:UIControlStateNormal];
        [_nextButton setImage:[UIImage imageNamed:@"btn_nxt"] forState:UIControlStateHighlighted];
        [_nextButton setImage:[UIImage imageNamed:@"btn_nxt"] forState:UIControlStateDisabled];
        [_nextButton addTarget:self action:@selector(next:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _nextButton;
}

- (void)next:(id)sender
{
    if (self.currentPage < self.maxPage) {
        self.currentPage++;
        [self updateButtons];
    }
}
- (UIButton *)goButton
{
    if (!_goButton) {
        _goButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _goButton.frame = CGRectMake(CGRectGetWidth(self.frame) - 60, 6, 50, 32);
        [_goButton setTitle:@"确定" forState:UIControlStateNormal];
        [_goButton setTitleColor:[Common colorWithHex:@"#FB3D38" alpha:1.0f] forState:UIControlStateNormal];
        [_goButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        [_goButton addTarget:self action:@selector(go:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _goButton;
}

- (void)go:(id)sender
{
    [self sendActionsForControlEvents:UIControlEventValueChanged];
    [self.mask removeFromSuperview];
}

- (UITextView *)inputView {
    if (!_inputView) {
        _inputView = [[UITextView alloc] initWithFrame:CGRectMake(160, 6, 90, 32)];
        _inputView.keyboardType = UIKeyboardTypeNumberPad;
        _inputView.backgroundColor = [UIColor whiteColor];
        _inputView.textAlignment = UITextAlignmentCenter;
        _inputView.font = [UIFont systemFontOfSize:15];
        _inputView.textColor = [UIColor colorWithWhite:204.0f/255.0f alpha:1.0f];
        _inputView.delegate = self;
    }
    return _inputView;
}

PageInputView *pageInputView;

+ (id)pageWithMaxPage:(NSInteger)max currentPage:(NSInteger)cur inView:(id)view
{
    pageInputView = [[PageInputView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight([view bounds]) - 44, CGRectGetWidth([view bounds]), 44)];
    
    if (pageInputView) {
        
        pageInputView.parentView = view;
        
        pageInputView.mask = [[UIButton alloc] initWithFrame:[view frame]];
        
        pageInputView.mask.backgroundColor = [UIColor colorWithWhite:0 alpha:.1];
        [pageInputView.mask addTarget:pageInputView.mask action:@selector(removeFromSuperview) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:pageInputView.mask];
        pageInputView.backgroundColor = [UIColor colorWithWhite:240.0f/255.0f alpha:.99];
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(pageInputView.frame), .5)];
        line.backgroundColor = [UIColor colorWithWhite:195.0f/255.0f alpha:1];
        [pageInputView addSubview:line];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(100, 0, 50, CGRectGetHeight(pageInputView.bounds))];
        label.text = @"跳转到";
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont systemFontOfSize:16];
        label.textColor = [UIColor colorWithWhite:153.0f/255.0f alpha:1];
        [pageInputView addSubview:label];
        [pageInputView addSubview:pageInputView.inputView];
        
        if (!pageInputView.pageLabel) {
            pageInputView.pageLabel = [UILabel new];
        }
        pageInputView.pageLabel.frame = pageInputView.inputView.frame;
        pageInputView.pageLabel.font = pageInputView.inputView.font;
        pageInputView.pageLabel.textColor = pageInputView.inputView.textColor;
        pageInputView.pageLabel.textAlignment = pageInputView.inputView.textAlignment;
        pageInputView.pageLabel.backgroundColor = pageInputView.inputView.backgroundColor;
        
        pageInputView.maxPage = max;
        pageInputView.currentPage = cur;
        
        [pageInputView addSubview:pageInputView.goButton];
        
        [pageInputView addSubview:pageInputView.preButton];
        [pageInputView addSubview:pageInputView.nextButton];
        
        [pageInputView addSubview:pageInputView.pageLabel];
        
        [pageInputView addKeyBoardNotification];
        [pageInputView.inputView becomeFirstResponder];
        [pageInputView.mask addSubview:pageInputView];
        
        [pageInputView updateButtons];
    }
    return pageInputView;
}

- (void)updateButtons
{
    pageInputView.pageLabel.text = [NSString stringWithFormat:@"%d / %d", self.currentPage, self.maxPage];
    self.preButton.enabled = self.currentPage > 1;
    self.nextButton.enabled = self.currentPage < self.maxPage;
}

#pragma mark UITextView delegate
- (void)textViewDidChange:(UITextView *)textView
{
    NSInteger cp = [textView.text integerValue];
    
    if (cp > self.maxPage) {
        cp = [[textView.text substringWithRange:NSMakeRange(textView.text.length - 1, 1)] integerValue];
    }
    cp = MIN(MAX(1, cp), self.maxPage);
    self.pageLabel.text = [NSString stringWithFormat:@"%d / %d", cp, self.maxPage];
    self.currentPage = cp;
    [self updateButtons];
    textView.text = [NSString stringWithFormat:@"%d", cp];
}

#pragma mark UIKeyboardController Listener

//监听键盘隐藏和显示事件
- (void)addKeyBoardNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowOrHide:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowOrHide:) name:UIKeyboardWillHideNotification object:nil];
}

static NSNotification *kbNote;
-(void)keyboardWillShowOrHide:(NSNotification *)notification {
    
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    kbNote = notification;
    BOOL isShow = [[notification name] isEqualToString:UIKeyboardWillShowNotification] ? YES : NO;
    
    CGRect formRect = self.frame;
    
    if (isShow) {
        formRect.origin.y -= keyboardSize.height - (CGRectGetHeight(self.parentView.frame) - CGRectGetMaxY(formRect));
    } else {
        formRect.origin.y += keyboardSize.height;
    }
    [UIView animateWithDuration:.25 animations:^{
        self.frame = formRect;
    }];
}

//注销监听事件
- (void)removeKeyBoardNotification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}


@end

@interface CDFDisplayViewController ()<UITableViewDataSource, UITableViewDelegate,DTLazyImageViewDelegate,DTAttributedTextContentViewDelegate,UIActionSheetDelegate, UMSocialUIDelegate, HYHActivityDetailFirstCellDelegate, ActivityTableViewCellDelegate, UIAlertViewDelegate,CDFDisplaycellDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *dataSource;
@property (nonatomic, strong) NSMutableSet *mediaPlayers;
@property (nonatomic, strong) NSCache *htmlCache;
@property (nonatomic, strong) NSCache *heightCache;
@property (strong, nonatomic) IBOutlet UIView *pageControl;
@property (strong, nonatomic) IBOutlet UIView *footView;
@property (strong, nonatomic) UIView *tableFooterView;
@property (strong, nonatomic) IBOutlet UIButton *likeButton;
@property (strong, nonatomic) IBOutlet UIButton *shareButton;
@property (strong, nonatomic) NSMutableDictionary *threadInfo;
@property (strong, nonatomic) CDFLikeAndViews *myLikeView;
@property (strong, nonatomic) UIImageView *shareImageView;
@property (copy, nonatomic) NSString *floor;
@property (copy, nonatomic) NSString *title;

#pragma mark - 保存所有下载数据
@property (nonatomic,strong) id mainData;
@property (nonatomic,copy) NSString *ppid;

@end

@implementation CDFDisplayViewController {
    NSInteger       _currentPage;
    BOOL            _isLoading;
    BOOL            _hasMore;
    NSInteger       _totalCells;
    NSInteger       _cellsPerSection;
    NSInteger       _totalPages;
    UILabel         *loadingText;
    BOOL            _liked;
    BOOL            _jumped;
    BOOL            _footerShow;
    NSInteger       _showPage;
    BOOL            _needJump;
    NSMutableDictionary *activityInfo;
    BOOL            _isFirstWarn;
    
    
    BOOL  _isReply;
    
    NSDictionary *_activityDict;//保存活动相关数据
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];
}
- (void)handleJoinLabelAction {
    if ([CDFMe shareInstance].isLogin) {
//        NSLog(@"");
        ActivityCommitViewController *activityCommitVC = [[ActivityCommitViewController alloc]init];
        activityCommitVC.tid = self.tid;
        activityCommitVC.fid = self.fid;
        activityCommitVC.pid = self.ppid;
        activityCommitVC.title = self.activityTitle;
        activityCommitVC.numberOfPerson = self.numberOfperson;
        
        activityCommitVC.specialDict = _mainData[@"special_activity"];
        //        self.dataSource
//        NSLog(@"p:%@, t:%@, u:%@, f:%@", self.pid, self.tid, self.uid, self.fid);
        [self.navigationController pushViewController:activityCommitVC animated:YES];
        
    } else {
        //弹出登陆页面...
        if (_isFirstWarn) {
            [[CDFMe shareInstance] wellcomeLogin:self];
            _isFirstWarn = NO;
        }
    }
}

- (void)setup
{
    _currentPage = 1;
    _isLoading = NO;
    _cellsPerSection = 10;
    _totalCells = 0;
    [self.likeButton setImage:[UIImage imageNamed:@"btn_heart"] forState:UIControlStateNormal];
    [self.likeButton setImage:[UIImage imageNamed:@"btn_heart_hl"] forState:UIControlStateHighlighted];
    [self.likeButton setImage:[UIImage imageNamed:@"btn_heart_hl"] forState:UIControlStateDisabled];
    
}


- (UIView *)tableFooterView
{
    if (!_tableFooterView) {
        
        UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.frame), 20)];
        v.backgroundColor = [UIColor colorWithWhite:240.0f / 255.0f alpha:1];
        UIActivityIndicatorView *indv = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        indv.frame = CGRectMake(10, 10, 0, 0);
        indv.transform = CGAffineTransformMakeScale(0.5, 0.5);
        [indv startAnimating];
        [v addSubview:indv];
        loadingText = [[UILabel alloc] initWithFrame:v.bounds];
        loadingText.backgroundColor = [UIColor clearColor];
        loadingText.font = [UIFont systemFontOfSize:10];
        loadingText.textColor = [UIColor colorWithWhite:185.0f/255.0f alpha:1];
        
        loadingText.textAlignment = UITextAlignmentCenter;
        [v addSubview:loadingText];
        _tableFooterView = v;
    }
    loadingText.text = [NSString stringWithFormat:@"正在加载第 %d 页数据...",_currentPage];
    return _tableFooterView;
}

- (NSMutableArray *)dataSource
{
    if (!_dataSource) {
        _dataSource = [NSMutableArray new];
    }
    
    if (_totalPages + 1 > _dataSource.count) {
        //如果总页数大于目前的页数
        for (int i = _totalPages + 1 - _dataSource.count; i < _totalPages + 1; i++) {
            [_dataSource addObject:[NSMutableArray array]];
        }
    } else if (_totalPages + 1 < _dataSource.count) {
        //如果总页数小于目前的页数
        for (int i = _totalPages + 1; i < _dataSource.count; i++) {
            [_dataSource removeObjectAtIndex:i];
        }
    }
    return _dataSource;
}

- (NSCache *)htmlCache
{
    if (!_htmlCache) {
        _htmlCache = [NSCache new];
    }
    return _htmlCache;
}

- (void)setPid:(id)pid
{
    _pid = pid;
    if (_pid && self.dataSource.count > 0) {
        [self getJumpID];
    }
}

- (NSCache *)heightCache
{
    if (!_heightCache) {
        _heightCache = [NSCache new];
    }
    return _heightCache;
}

- (UIImageView *)shareImageView
{
    if (!_shareImageView) {
        _shareImageView = [UIImageView new];
    }
    return _shareImageView;
}

- (IBAction)reply:(id)sender
{
    [MobClick event:@"Reply" label:@"楼主"];
    [self performSegueWithIdentifier:@"modalToReply" sender:sender];
}

- (IBAction)like:(id)sender
{
    [MobClick event:@"Like" label:@"详情"];
    NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=like&tid=%@", kApiVersion, self.tid];
    [[HXHttpClient shareInstance] grabURL:url success:^(id responseObject) {
        //NSLog(@"%@", responseObject);
        [[NSNotificationCenter defaultCenter] postNotificationName:kMESSAGE_BADGE object:nil userInfo:responseObject[@"Variables"][@"newmessage2"]];
        if ([responseObject[@"Message"][@"messageval"] isEqualToString:@"recommend_succed"]) {
//            [SVProgressHUD showSuccessWithStatus:responseObject[@"Message"][@"messagestr"]];
                        [SVProgressHUD showSuccessWithStatus:@"点赞成功"];
            self.likeButton.enabled = NO;
            self.myLikeView.likes += 1;
            self.myLikeView.enabled = NO;
        } else {
            [SVProgressHUD showErrorWithStatus:responseObject[@"Message"][@"messagestr"]];
        }
    } fail:^(NSError *error) {
//        NSLog(@"%@", error);
        [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
    }];
}

- (void)didCloseUIViewController:(UMSViewControllerType)fromViewControllerType
{
    [self showFooterView];
}
- (void)didSelectSocialPlatform:(NSString *)platformName withSocialData:(UMSocialData *)socialData{
    UIView *view = (UIView *)[[[UIApplication sharedApplication].delegate window] viewWithTag:2001];
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = view.frame;
        frame.origin.y = KDeviceSizeHeight;
        view.frame = frame;
    } completion:^(BOOL finished) {
        if (finished) {
            [view removeFromSuperview];
            [self showFooterView];
        }
    }];
    
    UIView *view2 = (UIView *)[[[UIApplication sharedApplication].delegate window] viewWithTag:2000];
    [view2 removeFromSuperview];
    
    
    [self showFooterView];
}
- (IBAction)share:(id)sender {
    
    //NSString *shareText =
    [self hideFooterView];
    
//    NSLog(@"%@", [self.threadInfo[@"summary"] class]);
    [MobClick event:@"Share"];
    self.shareImageView =  nil;
    NSString *shareURL = [NSString stringWithFormat:@"http://bang.dahe.cn/forum.php?mod=viewthread&tid=%@",self.tid];
    [UMSocialData defaultData].extConfig.title = self.threadInfo[@"subject"];
    
    //设置手机QQ的AppId，url传nil，将使用友盟的网址
    [UMSocialQQHandler setQQWithAppId:kQQ_Appkey appKey:kUmeng_Appkey url:shareURL];
    //设置微信AppId，url地址传nil，将默认使用友盟的网址
    [UMSocialWechatHandler setWXAppId:kWeichat_Appkey appSecret:kUmeng_Appkey url:shareURL];
    
    UIView *bacView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KDeviceSizeWidth, KDeviceSizeHeight)];
    bacView.backgroundColor = [UIColor blackColor];
    bacView.alpha = 0.4;
    [[[UIApplication sharedApplication].delegate window]addSubview:bacView];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(bacClick:)];
    bacView.tag = 2000;
    [bacView addGestureRecognizer:tap];
    
    
    UIView *view = [[UIView  alloc]initWithFrame:CGRectMake(0, KDeviceSizeHeight, KDeviceSizeWidth, 350)];
    view.backgroundColor = [UIColor blackColor];
    view.userInteractionEnabled = YES;
    view.tag = 2001;
    view.alpha = 1;
    [[[UIApplication sharedApplication].delegate window] addSubview:view];
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = view.frame;
        frame.origin.y = KDeviceSizeHeight - 350;
        view.frame  = frame;
    } completion:^(BOOL finished) {
        
    }];
    
    NSArray *arr = @[@"微信",@"微信朋友圈",@"新浪微博",@"腾讯微博",@"QQ空间",@"QQ"];
    NSArray *imageArr = @[@"icon_share_wx",@"icon_share_pyq",@"icon_share_sina_wb",@"icon_share_tencent_wb",@"icon_share_qzone",@"icon_share_qq"];
    for (int i = 0; i < arr.count; i++) {
        ZWDShareButton *button = [[ZWDShareButton alloc]initWithFrame:CGRectMake(5 + (KDeviceSizeWidth - 5*4)/3 * (i%3), 5 + ((KDeviceSizeWidth - 5*4)/3 +20)*(i/3), (KDeviceSizeWidth - 5*4)/3, (KDeviceSizeWidth - 5*4)/3 + 20) name:arr[i] imageName:imageArr[i]];
        [view addSubview:button];
        button.delegate = self;
    }
    
    UIButton *cancleButton = [UIButton buttonWithType:UIButtonTypeSystem];
    cancleButton.frame = CGRectMake(10, view.bounds.size.height - 50, view.bounds.size.width - 20, 40);
    [cancleButton setTitle:@"取消" forState:UIControlStateNormal];
    [cancleButton addTarget:self action:@selector(cancleButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:cancleButton];
    [cancleButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    cancleButton.layer.masksToBounds = YES;
    cancleButton.layer.cornerRadius = 5;
    [cancleButton setBackgroundColor:TITLEGRAY];

    
    /*
    if (self.dataSource.count > 0) {
        
        if ([self.dataSource[0] count] > 0) {
            
            //            if ([self.dataSource[0][0][@"imagelist"] count] > 0) {
            if ([self.dataSource[0][0][@"imagelist"] count] < 0) {
                
                NSString *key = self.dataSource[0][0][@"imagelist"][0];
                NSDictionary *attch = self.dataSource[0][0][@"attachments"][key];
                NSString *imgurl = [NSString stringWithFormat:@"%@%@",attch[@"url"],attch[@"attachment"]];
                
                __block CDFDisplayViewController *Weakself = self;
                [SVProgressHUD showWithStatus:@"准备图片中..." maskType:SVProgressHUDMaskTypeGradient];
                
                NSString *second; //分享信息的第二个%@
                if (Weakself.threadInfo[@"summary"]) {
                    second = Weakself.threadInfo[@"summary"];
                } else {
                    second = @"";
                }
                
                //            shareText:[NSString stringWithFormat:@"%@%@ %@",Weakself.threadInfo[@"subject"],second, shareURL]
                
                [self.shareImageView setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:imgurl]] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                    [UMSocialSnsService presentSnsIconSheetView:Weakself
                                                         appKey:kUmeng_Appkey
                                                      shareText:@"111"
                     
                                                     shareImage:[UIImage imageNamed:@"appicon120.png"]
                                                shareToSnsNames:[NSArray arrayWithObjects:UMShareToSina,UMShareToTencent,UMShareToQQ,UMShareToWechatSession,UMShareToWechatTimeline,UMShareToQzone,nil]
                                                       delegate:Weakself];
                    [SVProgressHUD dismiss];
                } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                    [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
                    
                    NSString *second; //分享信息的第二个%@
                    if (Weakself.threadInfo[@"summary"]) {
                        second = Weakself.threadInfo[@"summary"];
                    } else {
                        second = @"";
                    }
                    
                    
                    
                    //                shareText:[NSString stringWithFormat:@"%@%@ %@",Weakself.threadInfo[@"subject"],second, shareURL]
                    
                    
                    [UMSocialSnsService presentSnsIconSheetView:Weakself
                                                         appKey:kUmeng_Appkey
                                                      shareText:@"222"
                                                     shareImage:[UIImage imageNamed:@"appicon120"]
                                                shareToSnsNames:[NSArray arrayWithObjects:UMShareToSina,UMShareToTencent,UMShareToQQ,UMShareToWechatSession,UMShareToWechatTimeline,UMShareToQzone,nil]
                                                       delegate:Weakself];
                }];
            } else {
                
                NSString *second; //分享信息的第二个%@
                if (self.threadInfo[@"summary"]) {
                    second = self.threadInfo[@"summary"];
                } else {
                    second = @"";
                }
                
                
                [UMSocialSnsService presentSnsIconSheetView:self
                                                     appKey:kUmeng_Appkey
                                                  shareText:[NSString stringWithFormat:@"%@%@ %@",self.threadInfo[@"subject"],second, shareURL]
                                                 shareImage:[UIImage imageNamed:@"appicon120yuanjiao"]
                                            shareToSnsNames:[NSArray arrayWithObjects:UMShareToSina,UMShareToTencent,UMShareToQQ,UMShareToWechatSession,UMShareToWechatTimeline,UMShareToQzone,nil]
                                                   delegate:self];
            }
        }
    }
     */
}
- (void)cancleButtonClick{
    UIView *view = (UIView *)[[[UIApplication sharedApplication].delegate window] viewWithTag:2001];
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = view.frame;
        frame.origin.y = KDeviceSizeHeight;
        view.frame = frame;
    } completion:^(BOOL finished) {
        if (finished) {
            [view removeFromSuperview];
            [self showFooterView];
        }
    }];
    
    UIView *view2 = (UIView *)[[[UIApplication sharedApplication].delegate window] viewWithTag:2000];
    [view2 removeFromSuperview];
}
- (void)bacClick:(UIGestureRecognizer *)tap{
    
    [tap.view removeFromSuperview];
    UIView *view = (UIView *)[[[UIApplication sharedApplication].delegate window] viewWithTag:2001];
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = view.frame;
        frame.origin.y = KDeviceSizeHeight;
        view.frame = frame;
    } completion:^(BOOL finished) {
        if (finished) {
            [view removeFromSuperview];
            [self showFooterView];
        }
    }];
}
- (void)ZWDShareButtonClick:(NSString *)ButtonName{
    NSArray *arr = @[@"微信",@"微信朋友圈",@"新浪微博",@"腾讯微博",@"QQ空间",@"QQ"];
    NSArray *UMName = @[UMShareToWechatSession,UMShareToWechatTimeline,UMShareToSina,UMShareToTencent,UMShareToQzone,UMShareToQQ];
    
    
    
    
    NSString *shareURL = [NSString stringWithFormat:@"http://bang.dahe.cn/forum.php?mod=viewthread&tid=%@",self.tid];
    [UMSocialWechatHandler setWXAppId:kWeichat_Appkey appSecret:kUmeng_Appkey url:shareURL];
    [UMSocialQQHandler setQQWithAppId:kQQ_Appkey appKey:kUmeng_Appkey url:shareURL];
    [UMSocialData defaultData].extConfig.title = self.threadInfo[@"subject"];
    
    __block CDFDisplayViewController *Weakself = self;

    
    NSString *second; //分享信息的第二个%@
    if (self.threadInfo[@"summary"]) {
        second = self.threadInfo[@"summary"];
    } else {
        second = @"";
    }
    NSString *name = UMName[[arr indexOfObject:ButtonName]];
    NSString *shareText =[NSString stringWithFormat:@"%@%@ %@",self.threadInfo[@"subject"],second, shareURL];
    UIImage *shareImage =[UIImage imageNamed:@"appicon120yuanjiao"];
    //设置分享内容，和回调对象
    [[UMSocialControllerService defaultControllerService] setShareText:shareText shareImage:shareImage socialUIDelegate:self];
    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:name];
    
    
    snsPlatform.snsClickHandler(self,[UMSocialControllerService defaultControllerService],YES);
    
    
    
}
#pragma mark - ZWD修改分享面板

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if (_isLoading) {
        return;
    }
    id segueViewController = segue.destinationViewController;
    if ([segueViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *nav = (UINavigationController *)segueViewController;
        segueViewController = nav.viewControllers[0];
    }
    
    if ([segueViewController respondsToSelector:@selector(displayViewController)]) {
        [segueViewController setValue:self forKey:@"displayViewController"];
    }
    
    //NSLog(@"%@", [senderView class]);
    
    if ([sender isKindOfClass:NSClassFromString(@"CDFReplyCellButton")]) {
        //回复楼层
        [MobClick event:@"Reply" label:@"楼层"];
        NSIndexPath *indexPath1 = [self.tableView indexPathForCell:[sender cell]];
        NSLog(@"%@",indexPath1);
        NSIndexPath *indexPath ;
        if (indexPath1.section == 0) {
            indexPath = [NSIndexPath indexPathForRow:indexPath1.row - 1 inSection:indexPath1.section];
        }else{
            indexPath = [NSIndexPath indexPathForRow:indexPath1.row inSection:indexPath1.section];
        }
        
        if ([segueViewController respondsToSelector:@selector(pid)]) {
            [segueViewController setValue:@"1" forKey:@"postType"];
            [segueViewController setValue:self.dataSource[indexPath.section][indexPath.row][@"pid"] forKey:@"pid"];
        }
//        NSLog(@"%@",self.dataSource[indexPath.section][indexPath.row]);
        NSMutableString *replyMsg = [NSMutableString stringWithString:self.dataSource[indexPath.section][indexPath.row][@"message"]];
        
        [replyMsg replaceOccurrencesOfString:@"<blockquote>[\\s|\\S]*</blockquote>" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [replyMsg length])];
        NSString *message = [NSString stringWithFormat:@"[quote]%@ 发表于 %@\n%@[/quote]" ,self.dataSource[indexPath.section][indexPath.row][@"author"], [Common formatDate:[self.dataSource[indexPath.section][indexPath.row][@"dbdateline"] longLongValue]], [replyMsg stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
        
        if ([segueViewController respondsToSelector:@selector(quote)]) {
            [segueViewController setValue:message forKey:@"quote"];
        }
        if ([segueViewController respondsToSelector:@selector(author)]) {
            [segueViewController setValue:[NSString stringWithFormat:@"%@",self.dataSource[indexPath.section][indexPath.row][@"author"]] forKey:@"author"];
        }
        if ([segueViewController respondsToSelector:@selector(uid)]) {
            [segueViewController setValue:[NSString stringWithFormat:@"%@",self.dataSource[indexPath.section][indexPath.row][@"location"][@"uid"]] forKey:@"uid"];
        }
    } else {
        //回复楼主
        
        if ([segueViewController respondsToSelector:@selector(pid)]) {
            [segueViewController setValue:self.dataSource[0][0][@"pid"] forKey:@"pid"];
        }
        if ([segueViewController respondsToSelector:@selector(author)]) {
            [segueViewController setValue:[NSString stringWithFormat:@"%@",self.dataSource[0][0][@"author"]] forKey:@"author"];
        }
//        NSLog(@"self.uid:%@", self.uid);
//        NSLog(@"th:%@", self.threadInfo);
        if ([segueViewController respondsToSelector:@selector(uid)]) {
            [segueViewController setValue:self.threadInfo[@"authorid"] forKey:@"uid"];
        }
    }
    if ([segueViewController respondsToSelector:@selector(tid)]) {
        
        [segueViewController setValue:self.tid forKey:@"tid"];
    }
    if([segueViewController respondsToSelector:@selector(fid)]) {
        
        [segueViewController setValue:self.fid forKey:@"fid"];
    }
    if ([sender isKindOfClass:[NSDictionary class]]) {
        if ([sender[@"type"] isEqualToString:@"user"]) {
            if ([segue.destinationViewController respondsToSelector:@selector(uid)]) {
                [segue.destinationViewController setValue:sender[@"uid"] forKey:@"uid"];
            }
        }
        if ([sender[@"type"] isEqualToString:@"post"]) {
            if ([segue.destinationViewController respondsToSelector:@selector(tid)]) {
                [segue.destinationViewController setValue:sender[@"tid"] forKey:@"tid"];
            }
            if ([segue.destinationViewController respondsToSelector:@selector(uid)]) {
                [segue.destinationViewController setValue:sender[@"pid"] forKey:@"pid"];
            }
        }
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    return !_isLoading;
}

- (void)getJumpID
{
    //    NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=getpostfloor&ptid=%@&pid=%@",kApiVersion,self.tid,self.pid];
    //    [[HXHttpClient shareInstance] grabURL:url success:^(id responseObject) {
    //        [[NSNotificationCenter defaultCenter] postNotificationName:kMESSAGE_BADGE object:nil userInfo:responseObject[@"Variables"][@"newmessage2"]];
    //        _currentPage = [responseObject[@"Variables"][@"floor"] integerValue];
    //
    //    } fail:^(NSError *error) {
    //        NSLog(@"%@",error);
    //    }];
    _currentPage = [_floor integerValue];
    [self loadDataAndJump];
}

- (void)setTitleLabel:(NSString *)title
{
    UILabel *lb = [[UILabel alloc] initWithFrame:CGRectZero];
    UIFont *f1;
    CGSize size;
//    NSLog(@"%@",NSStringFromCGSize(size));
    for (int i = 20; i > 12; i--) {
        f1 = [UIFont systemFontOfSize:i];
        size = [title sizeWithFont:f1 constrainedToSize:CGSizeMake(200, 100)];
        if (size.height < 25) {
            break;
        }
    }
    lb.textAlignment = UITextAlignmentCenter;
    lb.text = title;
    if ([Common systemVersion] >= 7.0) {
        lb.textColor = self.navigationController.navigationBar.tintColor;
    } else {
        lb.textColor  = [UIColor whiteColor];
    }
    lb.font = f1;
    lb.backgroundColor = [UIColor clearColor];
    lb.numberOfLines = 2;
    self.navigationItem.titleView = lb;
    lb.frame = CGRectMake(0, 0, 0, 44);
    lb.center = self.navigationController.navigationBar.center;
    lb.alpha = 0;
    [UIView animateWithDuration:.25 animations:^{
        lb.alpha = 1;
    }];
}
- (void)layoutLeftNavigationItem {
    UIButton *backBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    [backBtn setImage:[UIImage imageNamed:@"backOrange"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(handleLeftBarBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
}
- (void)handleLeftBarBtn:(UIButton *)btn {
    _isReply = NO;
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)loadDataAndJump
{
    if (_isLoading) {
        return;
    }
    _isLoading = YES;
    if (_currentPage == 0) {
        _currentPage = 1;
    }
    if (_currentPage == 1 && _totalPages > 1) {
        [self jump];
        _isLoading = NO;
        return;
    }
    self.tableView.tableFooterView = self.tableFooterView;
    [SVProgressHUD showWithStatus:@"正在加载回复楼层..."];
    NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=viewthread&tid=%@&ppp=10&page=%d",kApiVersion ,self.tid, _currentPage];
//    NSLog(@"%@",url);
    [[HXHttpClient shareInstance] grabURL:url success:^(id responseObject) {
        _isLoading = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:kMESSAGE_BADGE object:nil userInfo:responseObject[@"Variables"][@"newmessage2"]];
        //NSLog(@"%@",[Common dictionaryToJSON:responseObject]);
        if (responseObject[@"Message"]) {
            [SVProgressHUD showErrorWithStatus:responseObject[@"Message"][@"messagestr"]];
        } else {
            [SVProgressHUD dismiss];
            //[SVProgressHUD dismiss];
            _liked = [responseObject[@"Variables"][@"thread"][@"like"] boolValue];
            self.likeButton.enabled = !_liked;
            //            [self setTitleLabel:[NSString stringWithFormat:@"%@",responseObject[@"Variables"][@"thread"][@"subject"]]];
//            NSLog(@"huyahui = %@", self.title);
            [self setTitleLabel:@""];
            
            self.fid = responseObject[@"Variables"][@"fid"];
            _totalCells = [responseObject[@"Variables"][@"thread"][@"replies"] integerValue] + 1;
            _cellsPerSection = [responseObject[@"Variables"][@"ppp"] integerValue];
            _totalPages = _totalCells*1.0 / _cellsPerSection*1.0;
            if (_totalCells % _cellsPerSection > 0) {
                _totalPages += 1;
            }
            _hasMore = _currentPage < _totalPages;
            
            
            if (_hasMore) {
                self.tableView.tableFooterView = self.tableFooterView;
            } else {
                self.tableView.tableFooterView = nil;
            }
            
            //NSLog(@"%@",responseObject[@"Variables"][@"thread"][@"replies"]);
            NSArray *postList = [NSArray arrayWithArray:[NSArray arrayWithArray:responseObject[@"Variables"][@"postlist"]]];
            if (self.dataSource.count <= 0) {
                [self.dataSource insertObject:[NSArray arrayWithArray:responseObject[@"Variables"][@"postlist"]] atIndex:_currentPage - 1];
            } else {
                @try {
                    [self.dataSource replaceObjectAtIndex:_currentPage - 1 withObject:postList];
                }
                @catch (NSException *exception) {
                    
                }
                @finally {
                    
                }
            }
            
            [self.tableView reloadData];
            [self jump];
        }
        
    } fail:^(NSError *error) {
        _isLoading = NO;
        [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
    }];
    
}

- (void)jump
{
    int row = 0,section = 0;
    BOOL found = NO;
//    NSLog(@"self.datasource.count:%d", self.dataSource.count);
    for (int i = 0; i < self.dataSource.count; i++) {
        //NSLog(@"datasource count : %d",[self.dataSource[i] count]);
        for (int j = 0; j < [self.dataSource[i] count]; j++) {
//            NSLog(@"duibi:%@, %@", self.dataSource[i][j][@"pid"], self.pid);
            if ([[NSString stringWithFormat:@"%@",self.dataSource[i][j][@"pid"]] isEqualToString:[NSString stringWithFormat:@"%@",self.pid]]) {
                found = YES;
                section = i;
                row = j;
//                NSLog(@"%@",self.dataSource[i][j][@"pid"]);
                break;
            }
        }
    }
    if (found) {
//        NSLog(@"%d %d",row,section);
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:section] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    _jumped = YES;
}

- (void)loadData
{
    if (_isLoading) {
        return;
    }
    _isLoading = YES;

    self.tableView.tableFooterView = self.tableFooterView;
    NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=viewthread&tid=%@&ppp=10&page=%d",kApiVersion ,self.tid, _currentPage];
//    NSLog(@"%@",url);
//    NSLog(@"tid:%@", self.tid);
    
//    "special_activity" =         {
//        aboutmembers = 10;
//        activityclose = 0;
//        aid = 39243380;
//        allapplynum = 18;
//        applied = 0;
//        applylist =
    
    [[HXHttpClient shareInstance] grabURL:url success:^(id responseObject) {
//#pragma mark - 替换字符串@"&#"
        
//        for (NSDictionary *dict in responseObject[@"Variables"][@"postlist"]) {
//            if ([dict[@"message"] rangeOfString:@"&#"].length > 0) {
//                NSArray *arr = [dict[@"message"] componentsSeparatedByString:@"&#"];
//                
//                [dict setValue:[arr componentsJoinedByString:@""] forKey:@"message"];
//            }
//        }
        
        
//        NSLog(@"%@",responseObject);
        if ([responseObject[@"Variables"][@"postlist"] count] == 0) {
            return ;
        }
        self.mainData = responseObject[@"Variables"];

        if ([responseObject[@"Variables"][@"special_activity"] allValues].count > 0) {
            _activityDict = [NSDictionary dictionaryWithDictionary:responseObject[@"Variables"][@"special_activity"]];
            
            [self configTableHeaderView];
        }
        
        if ([responseObject[@"Variables"][@"special_activity"][@"activityclose"] boolValue]) {
            _isActivityClosed = YES;
        } else {
            _isActivityClosed = NO;
        }
        NSString *str = [NSString stringWithFormat:@"%@",responseObject[@"Variables"][@"postlist"][0][@"pid"]];
        self.ppid = str;
        self.numberOfperson = responseObject[@"Variables"][@"special_activity"][@"allapplynum"];
        self.activityTitle = responseObject[@"Variables"][@"thread"][@"subject"];
        if (isRefresh) {
            //[SVProgressHUD showSuccessWithStatus:@"刷新成功" duration:3];
        }
        self.title = [NSString stringWithFormat:@"【%@】",responseObject[@"Variables"][@"thread"][@"subject"]];
        //NSLog(@"%@",responseObject);
        activityInfo = [[NSMutableDictionary alloc] init];
        if (![responseObject[@"Variables"][@"activity"] isEqual:[NSNull null]]) {
            [activityInfo addEntriesFromDictionary:responseObject[@"Variables"][@"activity"]];
        }
        _isLoading = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:kMESSAGE_BADGE object:nil userInfo:responseObject[@"Variables"][@"newmessage2"]];
        [self showFooterView];
        //NSLog(@"%@",[Common dictionaryToJSON:responseObject]);
        
        if (responseObject[@"Message"]) {
            //显示错误信息
            [SVProgressHUD showErrorWithStatus:responseObject[@"Message"][@"messagestr"]];
        } else {
            
            [SVProgressHUD dismiss];
            _liked = [responseObject[@"Variables"][@"thread"][@"like"] boolValue];
            self.likeList = responseObject[@"Variables"][@"likelist"];
            self.likeButton.enabled = !_liked;
            [self setTitleLabel:@""];
            //            [self setTitleLabel:[NSString stringWithFormat:@"%@",responseObject[@"Variables"][@"thread"][@"subject"]]];
            
            self.fid = responseObject[@"Variables"][@"fid"];
            self.threadInfo = [NSMutableDictionary dictionaryWithDictionary:responseObject[@"Variables"][@"thread"]];
            _totalCells = [responseObject[@"Variables"][@"thread"][@"replies"] integerValue] + 1;
            _cellsPerSection = [responseObject[@"Variables"][@"ppp"] integerValue];
            _totalPages = _totalCells*1.0 / _cellsPerSection*1.0;
            //NSLog(@"%d", _totalCells % _cellsPerSection);
            if (_totalCells % _cellsPerSection > 0) {
                _totalPages += 1;
            }
            //NSLog(@"total pages : %d",_totalPages);
            _hasMore = _currentPage < _totalPages;
            if (!_jumped && self.pid) {
                [self getJumpID];
            }
            
            if (_hasMore) {
                self.tableView.tableFooterView = self.tableFooterView;
            } else {
                self.tableView.tableFooterView = nil;
            }
            
            //NSLog(@"%@",responseObject[@"Variables"][@"thread"][@"replies"]);
            NSArray *postList = [NSArray arrayWithArray:[NSArray arrayWithArray:responseObject[@"Variables"][@"postlist"]]];
//            NSLog(@"huyahui78%@", postList);
            if (self.dataSource.count <= 0) {
                [self.dataSource insertObject:[NSArray arrayWithArray:responseObject[@"Variables"][@"postlist"]] atIndex:_currentPage - 1];
            } else {
                if (self.dataSource.count >= _currentPage - 1) {
                    [self.dataSource replaceObjectAtIndex:_currentPage - 1 withObject:postList];
                }
            }
//            NSLog(@"%d--%d",self.dataSource.count,_currentPage);
            if (_currentPage == 2) {
//<<<<<<< HEAD
//=======
//                NSLog(@"%@",responseObject);

//>>>>>>> f30344495724dc9ff497527d304299a5a0ad6b1b
            }
            
            [self.tableView reloadData];
            if (_needJump) {
                [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:_currentPage - 1] atScrollPosition:UITableViewScrollPositionTop animated:YES];
                _needJump = NO;
            }
        }
//        [self.tableView reloadData];
        
    } fail:^(NSError *error) {
        _isLoading = NO;
        if ([[error localizedDescription] isEqualToString:@"Expected status code in (200-299), got 404"]) {
            [SVProgressHUD showErrorWithStatus:@"您访问的帖子已经被删除"];
        }else{
            
            [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
        }
    }];
}
- (void)changeDataSource{
    for (int i = 0; i < self.dataSource.count; i++) {
        for (int j = 0; j < [self.dataSource[i] count]; j++) {
            NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:self.dataSource[i][j]];
            if ([dict[@"message"] rangeOfString:@"&#"].length > 0) {
                NSArray *arr = [dict[@"message"] componentsSeparatedByString:@"&#"];
                [dict setValue:[arr componentsJoinedByString:@""] forKey:@"message"];
            }
            NSMutableArray *array = (NSMutableArray *)(self.dataSource[i]);
            [array replaceObjectAtIndex:j withObject:dict];
            
        }
    }
}
- (NSAttributedString *)heightForContentAtIndexPath:(NSString *)html atIndexPath:(NSIndexPath *)indexPath
{
    if (html == nil) {
        return nil;
    }
    
//    NSLog(@"html--%@",html);
    
    NSMutableString *str = [NSMutableString stringWithString:html];
    
    NSRange range;// = NSMakeRange(0, str.length);
    //[str replaceOccurrencesOfString:@"<br />" withString:@"" options:NSCaseInsensitiveSearch range:range];
    range = NSMakeRange(0, str.length);
    [str replaceOccurrencesOfString:@"blockquote" withString:@"quote" options:NSCaseInsensitiveSearch range:range];
    //匹配出附件 [attach]852156[/attach]
//    NSLog(@"str--%@",str);
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"\\[attach\\](\\d+)\\[/attach\\]" options:NSRegularExpressionCaseInsensitive error:NULL];
    
    NSDictionary *attachements = self.dataSource[indexPath.section][indexPath.row][@"attachments"];
//    NSLog(@"dict--%@",attachements);
    __block NSInteger offset = 0;
    NSMutableString *replaceStr = [NSMutableString stringWithFormat:@"%@",str];
    [regex enumerateMatchesInString:str options:0 range:NSMakeRange(0, str.length) usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
        
        if (result.numberOfRanges == 2) {
            
            NSString *attachid = [str substringWithRange:[result rangeAtIndex:1]];
            //NSLog(@"%@",attachid);
            NSDictionary *attachData = attachements[attachid];
            if (attachData && [attachData[@"url"] length] > 0 && [attachData[@"attachment"] length] > 0) {
                CGFloat width = [attachData[@"width"] floatValue];
                CGFloat height = [attachData[@"height"] floatValue];
                NSString *attachStr;
                
                if (width <= 30) {
                    attachStr = [NSString stringWithFormat:@"<img src='%@%@' width='%f' height='%f'>",attachData[@"url"],attachData[@"attachment"],width / 2,height / 2];
                } else {
                    attachStr = [NSString stringWithFormat:@"<div><img src='%@%@' width='%f' height='%f'></div>",attachData[@"url"],attachData[@"attachment"],width,height];
                }
                //NSLog(@"%@",attachStr);
                
                NSRange range = NSMakeRange(result.range.location + offset, result.range.length);
                
                [replaceStr replaceCharactersInRange:range withString:attachStr];
                offset += attachStr.length - result.range.length;
            }
        }
    }];
    //NSLog(@"%@",str);
    NSData *data = [replaceStr dataUsingEncoding:NSUTF8StringEncoding];
    
    void (^callBackBlock)(DTHTMLElement *element) = ^(DTHTMLElement *element) {
        
        // the block is being called for an entire paragraph, so we check the individual elements
        
        for (DTHTMLElement *oneChildElement in element.childNodes)
        {
            // if an element is larger than twice the font size put it in it's own block
            if (oneChildElement.displayStyle == DTHTMLElementDisplayStyleInline && oneChildElement.textAttachment.displaySize.height > 2.0 * oneChildElement.fontDescriptor.pointSize)
            {
                oneChildElement.displayStyle = DTHTMLElementDisplayStyleBlock;
                oneChildElement.paragraphStyle.minimumLineHeight = element.textAttachment.displaySize.height;
                oneChildElement.paragraphStyle.maximumLineHeight = element.textAttachment.displaySize.height;
            }
        }
    };
    
    
    NSMutableDictionary *options = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    [NSValue valueWithCGSize:CGSizeMake(286, 2000)], DTMaxImageSize,
                                    [NSNumber numberWithFloat:1.5], DTDefaultLineHeightMultiplier,
                                    [NSNumber numberWithFloat:16], DTDefaultFontSize,
                                    //@"Arial", DTDefaultFontFamily,
                                    @"Blue", DTDefaultLinkColor,
                                    @"none", DTDefaultLinkDecoration,
                                    [[DTCSSStylesheet alloc] initWithStyleBlock:@"quote{display:block;color:#666666;font-size:12px;padding:5px;}"], DTDefaultStyleSheet,
                                    callBackBlock, DTWillFlushBlockCallBack,
                                    nil];
    
    NSAttributedString *string = [[NSAttributedString alloc] initWithHTMLData:data options:options documentAttributes:NULL];
//    NSLog(@"%@",string);
    return string;
}

- (NSAttributedString *)formatHTMLString:(NSString *)html atIndexPath:(NSIndexPath *)indexPath
{
    if (html == nil) {
        return nil;
    }
    
//    NSLog(@"html--%@",html);
    
    NSMutableString *str = [NSMutableString stringWithString:html];
    
    NSRange range;// = NSMakeRange(0, str.length);
    //[str replaceOccurrencesOfString:@"<br />" withString:@"" options:NSCaseInsensitiveSearch range:range];
    range = NSMakeRange(0, str.length);
    [str replaceOccurrencesOfString:@"blockquote" withString:@"quote" options:NSCaseInsensitiveSearch range:range];
    //匹配出附件 [attach]852156[/attach]
//    NSLog(@"str--%@",str);
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"\\[attach\\](\\d+)\\[/attach\\]" options:NSRegularExpressionCaseInsensitive error:NULL];
    
    NSDictionary *attachements = self.dataSource[indexPath.section][indexPath.row][@"attachments"];
//    NSLog(@"dict--%@",attachements);
    __block NSInteger offset = 0;
    NSMutableString *replaceStr = [NSMutableString stringWithFormat:@"%@",str];
    [regex enumerateMatchesInString:str options:0 range:NSMakeRange(0, str.length) usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
        
        if (result.numberOfRanges == 2) {
            
            NSString *attachid = [str substringWithRange:[result rangeAtIndex:1]];
            //NSLog(@"%@",attachid);
            NSDictionary *attachData = attachements[attachid];
            if (attachData && [attachData[@"url"] length] > 0 && [attachData[@"attachment"] length] > 0) {
                CGFloat width = [attachData[@"width"] floatValue];
                CGFloat height = [attachData[@"height"] floatValue];
                NSString *attachStr;
                
                
                if (width <= 30) {
                    attachStr = [NSString stringWithFormat:@"<img src='%@%@' width='%f' height='%f'>",attachData[@"url"],attachData[@"attachment"],width / 2,height / 2];
                } else {
                    attachStr = [NSString stringWithFormat:@"<div><img src='%@%@' width='%f' height='%f'></div>",attachData[@"url"],attachData[@"attachment"],width,height];
                }
                
                
                NSRange range = NSMakeRange(result.range.location + offset, result.range.length);
                
                [replaceStr replaceCharactersInRange:range withString:attachStr];
                offset += attachStr.length - result.range.length;
            }
        }
    }];
    //NSLog(@"%@",str);
    NSData *data = [replaceStr dataUsingEncoding:NSUTF8StringEncoding];
    
    void (^callBackBlock)(DTHTMLElement *element) = ^(DTHTMLElement *element) {
        
        // the block is being called for an entire paragraph, so we check the individual elements
        
        for (DTHTMLElement *oneChildElement in element.childNodes)
        {
            // if an element is larger than twice the font size put it in it's own block
            if (oneChildElement.displayStyle == DTHTMLElementDisplayStyleInline && oneChildElement.textAttachment.displaySize.height > 2.0 * oneChildElement.fontDescriptor.pointSize)
            {
                oneChildElement.displayStyle = DTHTMLElementDisplayStyleBlock;
                oneChildElement.paragraphStyle.minimumLineHeight = element.textAttachment.displaySize.height;
                oneChildElement.paragraphStyle.maximumLineHeight = element.textAttachment.displaySize.height;
            }
        }
    };
    
    
    NSMutableDictionary *options = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    [NSValue valueWithCGSize:CGSizeMake(286, 2000)], DTMaxImageSize,
                                    [NSNumber numberWithFloat:1.5], DTDefaultLineHeightMultiplier,
                                    [NSNumber numberWithFloat:16], DTDefaultFontSize,
                                    //@"Arial", DTDefaultFontFamily,
                                    @"Blue", DTDefaultLinkColor,
                                    @"none", DTDefaultLinkDecoration,
                                    [[DTCSSStylesheet alloc] initWithStyleBlock:@"quote{display:block;color:#666666;font-size:12px;padding:5px;}"], DTDefaultStyleSheet,
                                    callBackBlock, DTWillFlushBlockCallBack,
                                    nil];
    
    NSAttributedString *string = [[NSAttributedString alloc] initWithHTMLData:data options:options documentAttributes:NULL];
//    NSLog(@"%@",string);
    
//    NSArray *familyNames = [UIFont familyNames];
//    for( NSString *familyName in familyNames ){
//        printf( "Family: %s \n", [familyName UTF8String] );
//        NSArray *fontNames = [UIFont fontNamesForFamilyName:familyName];
//        for( NSString *fontName in fontNames ){
//            printf( "\tFont: %s \n", [fontName UTF8String] );
//        }
//    }
    return string;
}



/**
 *  获取内容的高度
 *
 *  @param indexPath
 *
 *  @return CGfloat
 */
- (CGFloat)heightForContentAtIndexPath:(NSIndexPath *)indexPath
{
//    NSLog(@"%d---%d",indexPath.section,indexPath.row);
    //NSLog(@"_data:%@",_dataSource);
    NSString *message;
    if (indexPath.section == 0 && indexPath.row == 0) {
        message = [NSString stringWithFormat:@"%@%@",self.title,_dataSource[indexPath.section][indexPath.row][@"message"]];

    }else{
        message = [NSString stringWithFormat:@"%@",_dataSource[indexPath.section][indexPath.row][@"message"]];

    }
    
    
//    NSLog(@"%@",message);
    
 
    DTAttributedTextView *dtv = [[DTAttributedTextView alloc] initWithFrame:CGRectMake(0, 0, KDeviceSizeWidth - 34, 10)];

    if ([message rangeOfString:@"&#"].length > 0) {
        NSArray *arr = [message componentsSeparatedByString:@"&#"];
        message = [arr componentsJoinedByString:@""];
    }
    
    
//    NSLog(@"%@",message);
    dtv.attributedString = [self formatHTMLString:message atIndexPath:indexPath];
    
//    NSLog(@"%@",[self formatHTMLString:message atIndexPath:indexPath]);
    CGRect rect = dtv.attributedTextContentView.layoutFrame.frame;
//    NSLog(@"%f",CGRectGetMaxY(rect));
    return CGRectGetMaxY(rect);
}

- (void)avatarClick:(CDFAvatarImageView *)avatarView
{
    //NSLog(@"%@",avatarView.uid);
    [MobClick event:@"AvatarClick"];
    id userViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"userViewController"];
    if ([userViewController respondsToSelector:@selector(uid)]) {
        [userViewController setValue:avatarView.uid forKey:@"uid"];
    }
    [self.navigationController pushViewController:userViewController animated:YES];
}

- (void)handleRightItemAction:(id)sender
{
    //    IBActionSheet *standardIBAS = [[IBActionSheet alloc] initWithTitle:@"成都范儿" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitlesArray:@[@"刷新", @"翻页",@"收听楼主"]];
    //    standardIBAS.backgroundColor = [UIColor colorWithWhite:142.0f/255.0f alpha:.98];
    //    [standardIBAS showInView:[UIApplication sharedApplication].keyWindow];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"刷新", @"翻页",@"收听楼主", @"举报", nil];
    [actionSheet showFromTabBar:self.tabBarController.tabBar];
}

#pragma mark - IBActionSheet/UIActionSheet Delegate Method
static bool isRefresh = NO;
// the delegate method to receive notifications is exactly the same as the one for UIActionSheet
- (void)actionSheet:(IBActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
//    NSLog(@"Button at index: %d clicked\nIt's title is '%@'", buttonIndex, [actionSheet buttonTitleAtIndex:buttonIndex]);
    switch (buttonIndex) {
        case 0: {
            [MobClick event:@"DisplayRefresh"];
            _currentPage = 1;
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
            
            _totalPages = 0;
//            NSLog(@"%d",self.dataSource.count);
//            for (int i = 0; i < self.dataSource.count; i++) {
//                [self.dataSource replaceObjectAtIndex:i withObject:@[]];
//            }
            
            

            [self loadData];
            isRefresh = YES;
            break;
        }
        case 1: {
            [MobClick event:@"JumpPage"];
            [[PageInputView pageWithMaxPage:_totalPages currentPage:_showPage inView:self.tabBarController.view] addTarget:self action:@selector(jumpPage:) forControlEvents:UIControlEventValueChanged];
            break;
        }
        case 2: {
            //添加关注
            [MobClick event:@"FollowFromDispay"];
            NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=addfollow&hash=%@&fuid=%@", kApiVersion, [CDFMe shareInstance].formHash, self.threadInfo[@"authorid"]];
            [[HXHttpClient shareInstance] grabURL:url success:^(id responseObject) {
//                NSLog(@"%@",responseObject);
                if ([responseObject[@"Message"][@"messageval"] isEqualToString:@"follow_add_succeed"]) {
                    [SVProgressHUD showSuccessWithStatus:responseObject[@"Message"][@"messagestr"]];
                } else {
                    [SVProgressHUD showErrorWithStatus:responseObject[@"Message"][@"messagestr"]];
                }
            } fail:^(NSError *error) {
//                NSLog(@"%@",error);
                [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
            }];
            
            break;
        }
        case 3: {
            //举报帖子
            QViewController *QVC = [[QViewController alloc] init];
            QVC.tid = self.tid;
            QVC.pid = self.pid;
            QVC.uid = self.uid;
            QVC.fid = self.fid;
            QVC.navigationItem.title = @"举报";
            //            QVC.navigationItem.leftBarButtonItem.title = @"fanhui";
            [self.navigationController pushViewController:QVC animated:YES];
        }
        default:
            break;
    }
}

- (void)jumpPage:(PageInputView *)inputView
{
    if (_isLoading) {
        [SVProgressHUD showErrorWithStatus:@"上一个请求未完成，请稍后"];
        return;
    }
    _needJump = YES;
    
    if (inputView.currentPage <= _totalPages && inputView.currentPage >= 1) {
        if ([self.dataSource[inputView.currentPage - 1] count] > 0) {
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:inputView.currentPage - 1] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        } else {
            [SVProgressHUD showWithStatus:[NSString stringWithFormat:@"正在加载第 %d 页", inputView.currentPage]];
            _currentPage = inputView.currentPage;
            [self loadData];
        }
    }
}

- (void)showFooterView
{
    if (_footerShow) {
        return;
    }
    CGRect footerRect = self.footView.frame;
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(footerRect), .5)];
    line.backgroundColor = [UIColor colorWithWhite:213.0f/255.0f alpha:1];
    [self.footView addSubview:line];
    line = [[UIView alloc] initWithFrame:CGRectMake(0, 0.5, CGRectGetWidth(footerRect), 0.5)];
    line.backgroundColor = [UIColor whiteColor];
    [self.footView addSubview:line];
    line = [[UIView alloc] initWithFrame:CGRectMake(225, 0, .5, CGRectGetHeight(footerRect))];
    line.backgroundColor = [UIColor colorWithWhite:213.0f/255.0f alpha:1];
    [self.footView addSubview:line];
    line = [[UIView alloc] initWithFrame:CGRectMake(225.5, 0, .5, CGRectGetHeight(footerRect))];
    line.backgroundColor = [UIColor whiteColor];
    [self.footView addSubview:line];
    
    footerRect.origin.y = CGRectGetHeight(self.navigationController.view.bounds) + CGRectGetHeight(footerRect);
    self.footView.frame = footerRect;
    [self.navigationController.view addSubview:self.footView];
    footerRect.origin.y = CGRectGetHeight(self.navigationController.view.bounds) - CGRectGetHeight(footerRect);
    [UIView animateWithDuration:.25 animations:^{
        self.footView.frame = footerRect;
    } completion:^(BOOL finished) {
        _footerShow = YES;
    }];
}

- (void)hideFooterView
{
    CGRect pageRect = self.footView.frame;
    pageRect.origin.y = CGRectGetHeight(self.navigationController.view.bounds) + CGRectGetHeight(pageRect);
    [UIView animateWithDuration:.25 animations:^{
        self.footView.frame = pageRect;
    } completion:^(BOOL finished) {
        [self.footView removeFromSuperview];
        _footerShow = NO;
    }];
}

#pragma mark Super Methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    _isReply = YES;
    [self layoutLeftNavigationItem];
    _isFirstWarn = YES;
    _isActivityClosed = NO;
    [self loadData];
    
    UIButton *rightBarButtonItem = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBarButtonItem.frame = CGRectMake(0, 0, 30, 30);
    [rightBarButtonItem setImage:[UIImage imageNamed:@"more"] forState:UIControlStateNormal];
    [rightBarButtonItem addTarget:self action:@selector(handleRightItemAction:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarButtonItem];
    
    if ([Common systemVersion] < 7.0) {
        UIView *bg = [[UIView alloc] initWithFrame:self.tableView.bounds];
        bg.backgroundColor = bgColor;
        self.tableView.backgroundView = bg;
        self.navigationItem.rightBarButtonItem.tintColor = self.navigationController.navigationBar.tintColor;
        self.navigationItem.backBarButtonItem.tintColor = self.navigationController.navigationBar.tintColor;
    } else {
        rightBarButtonItem.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -25);
        self.view.backgroundColor = bgColor;
        //self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    }

#pragma mark - footView
    UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(13, 0,150, 44)];
    lable.text = @"回复也是一种鼓励...";
    lable.textColor = [Common colorWithHex:@"#CDCDCD" alpha:1];
    lable.font = [UIFont systemFontOfSize:13];
    [self.footView addSubview:lable];
    //DisplayFootViewPhoto@2x
    UIImageView *imgV = [[UIImageView alloc]initWithFrame:CGRectMake(190, 12, 23, 20)];
    imgV.image = [UIImage imageNamed:@"DisplayFootViewPhoto"];
    [self.footView addSubview:imgV];
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@""];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(returnAndRefreshData:) name:@"ReturnAndRefreshData" object:nil];
    [self showFooterView];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
#pragma mark - 头视图
    /*
    if (self.isActivity) {
        //UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.firstCell.bounds.size.width, self.firstCell.bounds.size.height)];
        //[view addSubview:self.firstCell];
//        self.tableView.tableHeaderView = self.firstCell;
//        self.firstCell.delegate = self;
        
        DaHeActivityDisplayHeader *h = [[[NSBundle mainBundle]loadNibNamed:@"DaHeActivityDisplayHeader" owner:self options:nil]lastObject];
        self.tableView.tableHeaderView = h;
        
        
        UIButton *btn = [[UIButton alloc] initWithFrame:self.rectOfjoinBtn];
        btn.backgroundColor = [UIColor clearColor];
        [btn addTarget:self action:@selector(handleApplyBtn:) forControlEvents:UIControlEventTouchUpInside];
        NSLog(@"%@",self.firstCell);
        //[self.tableView addSubview:btn];
    }
     */
}
- (void)configTableHeaderView{
#pragma mark - 头视图
    if (YES) {
        //UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.firstCell.bounds.size.width, self.firstCell.bounds.size.height)];
        //[view addSubview:self.firstCell];
        //        self.tableView.tableHeaderView = self.firstCell;
        //        self.firstCell.delegate = self;
        
        DaHeActivityDisplayHeader *h = [[[NSBundle mainBundle]loadNibNamed:@"DaHeActivityDisplayHeader" owner:self options:nil]lastObject];
       h.frame =CGRectMake(0, 0, self.tableView.bounds.size.width, [self detailTextHeight:self.mainData[@"thread"][@"subject"] ] + kHeightOfImage + 110 + 10);
        
//        h.frame =CGRectMake(0, 0, self.tableView.bounds.size.width , [self detailTextHeight:@"大家好大家好大家好大家好啊后恒大华府都撒谎返回冬奥会发到后发点哈佛大红色发达送饭哈达送话费辅导书发达送话费" ] + kHeightOfImage + 110 + 10 + 10);
        h.typeLable.backgroundColor = [Common colorWithHex:self.activityTypeColor alpha:1];
        [h showData:self.mainData];
        self.tableView.tableHeaderView = h;
        
        
        UIButton *btn = [[UIButton alloc] initWithFrame:self.rectOfjoinBtn];
        btn.backgroundColor = [UIColor clearColor];
        [btn addTarget:self action:@selector(handleApplyBtn:) forControlEvents:UIControlEventTouchUpInside];
//        NSLog(@"%@",self.firstCell);
        //[self.tableView addSubview:btn];
    }

}
- (void)returnAndRefreshData:(NSNotification *)sender{
    
//    NSLog(@"%@",sender.object);
    _floor = sender.object;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@""];
    [SVProgressHUD dismiss];
    [self hideFooterView];
    
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
//    if (self.isActivity && !_isReply) {
//        [self performSelector:@selector(postNoti) withObject:nil afterDelay:0.05];
//    }

}
//- (void)postNoti{
//    [[NSNotificationCenter defaultCenter] postNotificationName:ACTIVITYXQDISPLAYDISAPPER object:nil];
//}

- (void)likeHandle:(id)sender
{
    self.likeButton.enabled = NO;
}

#pragma mark UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //下拉加载更多
    if (scrollView.contentSize.height - scrollView.contentOffset.y < CGRectGetHeight(scrollView.frame) + 50) {
        if (_hasMore && !_isLoading) {
            _currentPage++;
            [self loadData];
        }
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//    NSLog(@"分区－－＝＝%d",_totalCells / _cellsPerSection + 1);
    return _totalCells / _cellsPerSection + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section > self.dataSource.count) {
        return 0;
    }
    if (self.isActivity) {
        if (section == 0) {
            return (self.dataSource.count == 0 ? 0 : ([self.dataSource[section] count]+1));
        }
    } else {
        if (section == 0) {
            return (self.dataSource.count == 0 ? 0 : ([self.dataSource[section] count]+1));
        }
    }
    return self.dataSource.count == 0 ? 0 : [self.dataSource[section] count];
}
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    if (section == 0) {
//        UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 100, 100)];
//        v.backgroundColor = [UIColor blackColor];
//        return v;
//    }
//    return nil;
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    if (section == 0) {
//        return 100.f;
//    }
//    return 0;
//}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSLog(@"%@",self.dataSource);
    
    if (self.isActivity) {
        //活动帖子
        /*
        //---------------------------
        if (indexPath.row == 0) {
            
            HYHActivityDetailFirstCell *cell = [[[NSBundle mainBundle]loadNibNamed:@"HYHActivityDetailFirstCell" owner:self options:nil]lastObject];
            cell.contentAttributedView.textDelegate = self;
            NSString *message = [NSString stringWithFormat:@"%@",self.dataSource[indexPath.section][indexPath.row][@"message"]];
            NSString *message0 = [NSString stringWithFormat:@"%@%@", self.title, message];
            cell.delegate = self;
            NSString *cacheKey = [NSString stringWithFormat:@"%d-%d",indexPath.section,indexPath.row];
            NSAttributedString *attrStr = [self.htmlCache objectForKey:cacheKey];
            
            if (!attrStr) {
                if (indexPath.row == 0) {
                    //                    attrStr = [self formatHTMLString:message0 atIndexPath:indexPath];
                    attrStr = [self formatHTMLString:message atIndexPath:indexPath];
                    
                } else {
                    attrStr = [self formatHTMLString:message atIndexPath:indexPath];
                }
                [self.htmlCache setObject:attrStr forKey:cacheKey];
            }
            cell.contentAttributedView.attributedString = attrStr;
            NSLog(@"%@", attrStr.string);
            cell.contentAttributedView.backgroundColor = [UIColor yellowColor];
            NSLog(@"%f",  [self heightForContentAtIndexPath:indexPath]);
            CGRect frame = cell.contentAttributedView.frame;
            CGFloat height = [self heightForContentAtIndexPath:indexPath];
//            cell.contentAttributedView.frame = CGRectMake(frame.origin.x, cell.pageNameLabel.frame.origin.y + kheightBettowTwoViews + cell.pageNameLabel.frame.size.width, KDeviceSizeWidth - frame.origin.x * 2, height);
 
            cell.contentAttributedView.frame = CGRectMake(frame.origin.x, 50, KDeviceSizeWidth - frame.origin.x * 2, height);
            cell.backgroundColor = [UIColor orangeColor];
            cell.applyBtn.frame = CGRectMake(cell.applyBtn.frame.origin.x, height - 80, cell.contentAttributedView.frame.size.width, cell.applyBtn.frame.size.height);
            NSLog(@"%f", cell.contentAttributedView.frame.origin.y);
            NSLog(@"%f", cell.contentAttributedView.frame.size.height);
            NSLog(@"%f", cell.contentAttributedView.attributedTextContentView.frame.size.height);
            NSLog(@"");
            return cell;
            
        } else {
            static NSString *CellIdentifier = @"Cell";
            CDFDisplayCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            
            NSString *date = [Common formatDate:[self.dataSource[indexPath.section][indexPath.row][@"dbdateline"] longLongValue]];
            
            NSString *message = [NSString stringWithFormat:@"%@",self.dataSource[indexPath.section][indexPath.row][@"message"]];
            NSString *message0 = [NSString stringWithFormat:@"%@%@", self.title, message];
            cell.contentCoreTextView.textDelegate = self;
            NSString *authorFaceURL = [Common formatAvatarWithUID:self.dataSource[indexPath.section][indexPath.row][@"authorid"] type:@"small"];
            cell.avatarImageView.uid = self.dataSource[indexPath.section][indexPath.row][@"authorid"];
            [cell.avatarImageView addTarget:self action:@selector(avatarClick:) forControlEvents:UIControlEventTouchUpInside];
            [cell.avatarImageView setImageWithURL:[NSURL URLWithString:authorFaceURL]];
            
            cell.authorLabel.text = [NSString stringWithFormat:@"%@",self.dataSource[indexPath.section][indexPath.row][@"author"]];
            
            NSString *str = [NSString stringWithFormat:@"%@ %@", date, self.dataSource[indexPath.section][indexPath.row][@"location"][@"location"]];
            
            if (self.dataSource[indexPath.section][indexPath.row][@"location"][@"location"]) {
                cell.timelineLabel.text = str;
            } else {
                cell.timelineLabel.text = date;
            }
            
            NSLog(@"displaylocation:%@", self.dataSource[indexPath.section][indexPath.row][@"location"][@"location"]);
            
            if ([self.dataSource[indexPath.section][indexPath.row][@"first"] boolValue]) {
                cell.replyButton.hidden = YES;
                cell.likeAndViewsControl.hidden = NO;
                if (!self.myLikeView) {
                    self.myLikeView = cell.likeAndViewsControl;
                    self.myLikeView.likes = [self.threadInfo[@"recommend_add"] integerValue];
                    self.myLikeView.views = [self.threadInfo[@"replies"] integerValue];
                    self.myLikeView.liked = [self.threadInfo[@"like"] boolValue];
                    self.myLikeView.tid = self.threadInfo[@"tid"];
                    [self.myLikeView addTarget:self action:@selector(likeHandle:) forControlEvents:UIControlEventEditingChanged];
                } else {
                    //cell.likeAndViewsControl = self.myLikeView;
                    cell.likeAndViewsControl.liked = !self.likeButton.enabled;
                    cell.likeAndViewsControl.likes = self.myLikeView.likes;
                    cell.likeAndViewsControl.views = self.myLikeView.views;
                    NSLog(@"%d %d",cell.likeAndViewsControl.liked, self.likeButton.enabled);
                }
                
            } else {
                
                cell.replyButton.hidden = NO;
                cell.likeAndViewsControl.hidden = YES;
            }
            
            NSString *cacheKey = [NSString stringWithFormat:@"%d-%d",indexPath.section,indexPath.row];
            NSAttributedString *attrStr = [self.htmlCache objectForKey:cacheKey];
            
            if (!attrStr) {
                if (indexPath.row == 0) {
                    attrStr = [self formatHTMLString:message0 atIndexPath:indexPath];
                } else {
                    attrStr = [self formatHTMLString:message atIndexPath:indexPath];
                }
                [self.htmlCache setObject:attrStr forKey:cacheKey];
            }
            cell.contentCoreTextView.attributedString = attrStr;
            
            return cell;
        }
        */
        //---------------------------
        
        NSIndexPath *newIndexPath;
        if (indexPath.row > 0 && indexPath.section == 0) {
            newIndexPath = [NSIndexPath indexPathForRow:indexPath.row - 1 inSection:indexPath.section];
        }
        if (indexPath.section == 0 && indexPath.row == 0) {
            newIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
        }
        if (indexPath.section > 0) {
            newIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
        }
        if (indexPath.section == 0) {
            if (indexPath.row == 1) {
                static NSString *ZWDCellID = @"zwdcellID";
                ZWDDisplayLikeCell *cell = [tableView dequeueReusableCellWithIdentifier:ZWDCellID];
                if (!cell) {
                    cell = [[ZWDDisplayLikeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ZWDCellID];
                    cell.navVC = self.navigationController;
                }
                [cell showData:self.likeList];
//                NSLog(@"%@",self.likeList);
                UIView *background = [[UIView alloc] initWithFrame:CGRectMake(10, 10, KDeviceSizeWidth - 20, [self heightForString:nil fontSize:FONT andWidth:[UIScreen mainScreen].bounds.size.width - 34]+30)];
                background.backgroundColor = [UIColor whiteColor];
                [cell addSubview:background];
                [cell sendSubviewToBack:background];
                cell.backgroundColor = [Common colorWithHex:@"#f3f3f3" alpha:1.0];

                return cell;
            }
        }
//        if (indexPath.row == 0 && indexPath.section == 0) {
//            HYHActivityDetailFirstCell *cell = [[[NSBundle mainBundle]loadNibNamed:@"HYHActivityDetailFirstCell" owner:self options:nil]firstObject];
//            
//#pragma mark - 活动详情数据填充
//            //NSLog(@"%@",self.mainData);
//            [cell showData:self.mainData index:indexPath data:self.dataSource];
//            cell.contentViewLabel.textDelegate = self;
//            cell.contentViewLabel.scrollEnabled = NO;
//            cell.delegate = self;
//            
//            UIView *background = [[UIView alloc] initWithFrame:CGRectMake(10, 0, KDeviceSizeWidth - 20, [self heightOfBlackBoardWithIndex:indexPath])];
//            background.backgroundColor = [UIColor whiteColor];
//            [cell addSubview:background];
//            [cell sendSubviewToBack:background];
//            cell.contentView.backgroundColor = [Common colorWithHex:@"#f3f3f3" alpha:1.0];
//            CGRect Frame = cell.avatarImageView.frame;
//            NSString *str1 = [NSString stringWithFormat:@"%f, %f, %f, %f", Frame.origin.x, Frame.origin.y, Frame.size.width, Frame.size.height];
//            NSLog(@"%@", str1);
//            return cell;
            
//        }
        NSString *date = [Common formatDate:[self.dataSource[newIndexPath.section][newIndexPath.row][@"dbdateline"] longLongValue]];
        
        static NSString *CellIdentifier = @"Cell";
        CDFDisplayCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:newIndexPath];
//        NSLog(@"dichyh=%@", self.title);
        
        NSString *message = [NSString stringWithFormat:@"%@",self.dataSource[newIndexPath.section][newIndexPath.row][@"message"]];
        
#pragma mark - 修改message
        if ([message rangeOfString:@"&#"].length > 0) {
            NSArray *arr = [message componentsSeparatedByString:@"&#"];
            message = [arr componentsJoinedByString:@""];
        }

        NSString *message0 = [NSString stringWithFormat:@"%@", message];
        cell.contentCoreTextView.textDelegate = self;
        //NSLog(@"%@",[Common dictionaryToJSON:self.dataSource[indexPath.section]]);
        NSString *authorFaceURL = [Common formatAvatarWithUID:self.dataSource[newIndexPath.section][newIndexPath.row][@"authorid"] type:@"small"];
        cell.avatarImageView.uid = self.dataSource[newIndexPath.section][newIndexPath.row][@"authorid"];
        cell.avatarImageView.layer.masksToBounds = YES;
        cell.avatarImageView.layer.cornerRadius = 15;
        [cell.avatarImageView addTarget:self action:@selector(avatarClick:) forControlEvents:UIControlEventTouchUpInside];
        [cell.avatarImageView setImageWithURL:[NSURL URLWithString:authorFaceURL]];
        
        cell.authorLabel.text = [NSString stringWithFormat:@"%@",self.dataSource[newIndexPath.section][newIndexPath.row][@"author"]];
        //    cell.timelineLabel.text = [Common formatDate:[self.dataSource[indexPath.section][indexPath.row][@"dbdateline"] longLongValue] withFormat:@"yyyy-MM-dd hh:mm"];
        
        NSString *str = [NSString stringWithFormat:@"%@ %@", date, self.dataSource[newIndexPath.section][newIndexPath.row][@"location"][@"location"]];
        
        if (self.dataSource[newIndexPath.section][newIndexPath.row][@"location"][@"location"]) {
            cell.timelineLabel.text = str;
        } else {
            cell.timelineLabel.text = date;
        }
        
//        NSLog(@"displaylocation:%@", self.dataSource[newIndexPath.section][newIndexPath.row][@"location"][@"location"]);
        
        if ([self.dataSource[newIndexPath.section][newIndexPath.row][@"first"] boolValue]) {
            cell.replyButton.hidden = YES;
            cell.likeAndViewsControl.hidden = NO;
            if (!self.myLikeView) {
                self.myLikeView = cell.likeAndViewsControl;
                self.myLikeView.likes = [self.threadInfo[@"recommend_add"] integerValue];
                self.myLikeView.views = [self.threadInfo[@"replies"] integerValue];
                self.myLikeView.liked = [self.threadInfo[@"like"] boolValue];
                self.myLikeView.tid = self.threadInfo[@"tid"];
                [self.myLikeView addTarget:self action:@selector(likeHandle:) forControlEvents:UIControlEventEditingChanged];
            } else {
                //cell.likeAndViewsControl = self.myLikeView;
                cell.likeAndViewsControl.liked = !self.likeButton.enabled;
                cell.likeAndViewsControl.likes = self.myLikeView.likes;
                cell.likeAndViewsControl.views = self.myLikeView.views;
//                NSLog(@"%d %d",cell.likeAndViewsControl.liked, self.likeButton.enabled);
            }
            
        } else {
            
            cell.replyButton.hidden = NO;
            cell.likeAndViewsControl.hidden = YES;
        }
        
        NSString *cacheKey = [NSString stringWithFormat:@"%d-%d",newIndexPath.section,newIndexPath.row];
        NSAttributedString *attrStr = [self.htmlCache objectForKey:cacheKey];
        if (!attrStr) {
            if (newIndexPath.row == 0 && newIndexPath.section == 0) {
                attrStr = [self formatHTMLString:message0 atIndexPath:newIndexPath];
            } else {
                attrStr = [self formatHTMLString:message atIndexPath:newIndexPath];
            }
            [self.htmlCache setObject:attrStr forKey:cacheKey];
        }
        cell.contentCoreTextView.attributedString = nil;
        cell.contentCoreTextView.attributedString = attrStr;
        
        cell.cellBackground.frame = CGRectMake(10, 10, KDeviceSizeWidth - 20,[self heightOfBlackBoardWithIndex:indexPath] + 10);
        
        cell.cellBackground.backgroundColor = [UIColor whiteColor];
        cell.contentView.backgroundColor = [Common colorWithHex:@"#f3f3f3" alpha:1.0];
        
        
        
        cell.isActivity = YES;
        if (indexPath.section == 0 && indexPath.row == 0) {
            if (![self.mainData[@"special_activity"][@"activityclose"] boolValue]) {
                cell.isEnd = NO;
            }else{
                cell.isEnd = YES;
            }
            [cell showButton];
            cell.delegate = self;
        }
        
        return cell;
        
    } else {
        //非活动帖子
        NSIndexPath *newIndexPath;
        if (indexPath.row > 0 && indexPath.section == 0) {
            newIndexPath = [NSIndexPath indexPathForRow:indexPath.row - 1 inSection:indexPath.section];
        }
        if (indexPath.section == 0 && indexPath.row == 0) {
            newIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
        }
        if (indexPath.section > 0) {
            newIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
        }
        if (indexPath.section == 0) {
            if (indexPath.row == 1) {
                static NSString *ZWDCellID = @"zwdcellID";
                ZWDDisplayLikeCell *cell = [tableView dequeueReusableCellWithIdentifier:ZWDCellID];
                if (!cell) {
                    cell = [[ZWDDisplayLikeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ZWDCellID];
                    cell.navVC = self.navigationController;
                }
                [cell showData:self.likeList];
//                NSLog(@"%@",self.likeList);
                UIView *background = [[UIView alloc] initWithFrame:CGRectMake(10, 10, KDeviceSizeWidth - 20, [self heightForString:nil fontSize:FONT andWidth:[UIScreen mainScreen].bounds.size.width - 34]+30)];
                background.backgroundColor = [UIColor whiteColor];
                [cell addSubview:background];
                [cell sendSubviewToBack:background];
                cell.backgroundColor = [Common colorWithHex:@"#f3f3f3" alpha:1.0];
                return cell;
            }
        }
        
        NSString *date = [Common formatDate:[self.dataSource[newIndexPath.section][newIndexPath.row][@"dbdateline"] longLongValue]];
        
        static NSString *CellIdentifier = @"Cell";
        CDFDisplayCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:newIndexPath];
//        NSLog(@"dichyh=%@", self.title);
        
        NSString *message = [NSString stringWithFormat:@"%@",self.dataSource[newIndexPath.section][newIndexPath.row][@"message"]];
#pragma mark - 修改message
        if ([message rangeOfString:@"&#"].length > 0) {
            NSArray *arr = [message componentsSeparatedByString:@"&#"];
            message = [arr componentsJoinedByString:@""];
        }

        NSString *message0 = [NSString stringWithFormat:@"%@%@", self.title, message];
//        NSLog(@"%@",self.title);
        cell.contentCoreTextView.textDelegate = self;
        //NSLog(@"%@",[Common dictionaryToJSON:self.dataSource[indexPath.section]]);
        NSString *authorFaceURL = [Common formatAvatarWithUID:self.dataSource[newIndexPath.section][newIndexPath.row][@"authorid"] type:@"small"];
        cell.avatarImageView.uid = self.dataSource[newIndexPath.section][newIndexPath.row][@"authorid"];
        cell.avatarImageView.layer.masksToBounds = YES;
        cell.avatarImageView.layer.cornerRadius = 15;
        [cell.avatarImageView addTarget:self action:@selector(avatarClick:) forControlEvents:UIControlEventTouchUpInside];
        [cell.avatarImageView setImageWithURL:[NSURL URLWithString:authorFaceURL]];
        
        cell.authorLabel.text = [NSString stringWithFormat:@"%@",self.dataSource[newIndexPath.section][newIndexPath.row][@"author"]];
        //    cell.timelineLabel.text = [Common formatDate:[self.dataSource[indexPath.section][indexPath.row][@"dbdateline"] longLongValue] withFormat:@"yyyy-MM-dd hh:mm"];
        NSString *str = [NSString stringWithFormat:@"%@ %@", date, self.dataSource[newIndexPath.section][newIndexPath.row][@"location"][@"location"]];
        
        
        if (self.dataSource[newIndexPath.section][newIndexPath.row][@"location"][@"location"]) {
            cell.timelineLabel.text = str;
        } else {
            cell.timelineLabel.text = date;
        }
        
//        NSLog(@"displaylocation:%@", self.dataSource[newIndexPath.section][newIndexPath.row][@"location"][@"location"]);
        
        if ([self.dataSource[newIndexPath.section][newIndexPath.row][@"first"] boolValue]) {
            cell.replyButton.hidden = YES;
            cell.likeAndViewsControl.hidden = NO;
            if (!self.myLikeView) {
                self.myLikeView = cell.likeAndViewsControl;
                self.myLikeView.likes = [self.threadInfo[@"recommend_add"] integerValue];
                self.myLikeView.views = [self.threadInfo[@"replies"] integerValue];
                self.myLikeView.liked = [self.threadInfo[@"like"] boolValue];
                self.myLikeView.tid = self.threadInfo[@"tid"];
                [self.myLikeView addTarget:self action:@selector(likeHandle:) forControlEvents:UIControlEventEditingChanged];
            } else {
                //cell.likeAndViewsControl = self.myLikeView;
                cell.likeAndViewsControl.liked = !self.likeButton.enabled;
                cell.likeAndViewsControl.likes = self.myLikeView.likes;
                cell.likeAndViewsControl.views = self.myLikeView.views;
//                NSLog(@"%d %d",cell.likeAndViewsControl.liked, self.likeButton.enabled);
            }
            
        } else {
            
            cell.replyButton.hidden = NO;
            cell.likeAndViewsControl.hidden = YES;
        }
        
        NSString *cacheKey = [NSString stringWithFormat:@"%d-%d",newIndexPath.section,newIndexPath.row];
        NSAttributedString *attrStr = [self.htmlCache objectForKey:cacheKey];
        //NSLog(@"%@",cacheKey);
        //NSLog(@"%@",attrStr);
        if (!attrStr) {
            if (newIndexPath.row == 0 && newIndexPath.section == 0) {
                attrStr = [self formatHTMLString:message0 atIndexPath:newIndexPath];
            } else {
                attrStr = [self formatHTMLString:message atIndexPath:newIndexPath];
            }
            [self.htmlCache setObject:attrStr forKey:cacheKey];
        }
        //NSLog(@"%d",[self.dataSource[newIndexPath.section] count]);
        cell.contentCoreTextView.attributedString = nil;
        cell.contentCoreTextView.attributedString = attrStr;
        //NSLog(@"%@",message0);
        
        cell.cellBackground.frame = CGRectMake(10, 10, KDeviceSizeWidth - 20,[self heightOfBlackBoardWithIndex:indexPath] + 10);
        
        cell.cellBackground.backgroundColor = [UIColor whiteColor];
        cell.contentView.backgroundColor = [Common colorWithHex:@"#f3f3f3" alpha:1.0];

        cell.isActivity = NO;
        
        return cell;
    }
    
    
    
    return nil;
}
- (void)CDFDisplaycellButtonClick{
    if (_isActivityClosed) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"抱歉,您来晚了,活动已经结束" delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alertView show];
    } else if ([CDFMe shareInstance].isLogin) {
//        NSLog(@"");
        ActivityCommitViewController *activityCommitVC = [[ActivityCommitViewController alloc]init];
        activityCommitVC.tid = self.tid;
        activityCommitVC.fid = self.fid;
        activityCommitVC.pid = self.ppid;
        activityCommitVC.title = self.activityTitle;
        activityCommitVC.numberOfPerson = self.numberOfperson;
        activityCommitVC.viewController = self;
        activityCommitVC.specialDict = _mainData[@"special_activity"];
        //        self.dataSource
//        NSLog(@"p:%@, t:%@, u:%@, f:%@", self.pid, self.tid, self.uid, self.fid);
        [self.navigationController pushViewController:activityCommitVC animated:YES];
        
    } else {
        //弹出登陆页面...
        //        if (_isFirstWarn) {
        //            _isFirstWarn = NO;
        //        }
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"温馨提醒" message:@"您还未登录,请登录后再进行活动报名" delegate:self cancelButtonTitle:@"返回" otherButtonTitles:@"登录", nil];
        alert.delegate = self;
        alert.tag = 999;
        [alert show];
    }

    
}
#pragma mark - 点击暗链接
- (UIView *)attributedTextContentView:(DTAttributedTextContentView *)attributedTextContentView viewForLink:(NSURL *)url identifier:(NSString *)identifier frame:(CGRect)frame{
    //NSLog(@"%@",url);
    
    //NSLog(@"%@",url.absoluteString);
    
    if (self.urlArr == nil) {
        self.urlArr = [NSMutableArray new];
    }
    [self.urlArr addObject:url];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    button.frame = frame;
    [button setTitle:[NSString stringWithFormat:@"%d",[self.urlArr count]] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(btn:) forControlEvents:UIControlEventTouchUpInside];
    [button setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    return button;
}
- (void)btn:(UIButton *)button{
    
    //NSLog(@"%@",button.titleLabel.text);
    //NSLog(@"%d",[self.urlArr count]);
    //NSLog(@"%@",self.urlArr[[button.titleLabel.text intValue]-1]);
    NSString *urlStr =[self.urlArr[[button.titleLabel.text intValue]-1] absoluteString];
    if ([urlStr rangeOfString:@"dahe.cn"].length > 0) {
        if ([urlStr rangeOfString:@"dahe.cn/thread"].length>0) {
            //帖子链接，需解析出tid//http://svnbang.dahe.cn/thread-7783800-1-1.html
            NSArray *arr = [urlStr componentsSeparatedByString:@"-"];
            NSString *tid = arr[1];
            //NSLog(@"%@",tid);
            if (tid) {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
                CDFDisplayViewController *dVC = [storyboard instantiateViewControllerWithIdentifier:@"DisplayViewContoller"];
                dVC.tid = tid;
                [self.navigationController pushViewController:dVC animated:YES];
            }else{
                ZWDWebViewController *web = [[ZWDWebViewController alloc]init];
                web.url = urlStr;
                [self.navigationController pushViewController:web animated:YES];
            }
            
        }else if ([urlStr rangeOfString:@"dahe.cn/space"].length > 0) {
            //http://bang.dahe.cn/space-uid-14632465.html
            //个人主页
            NSArray *arr = [urlStr componentsSeparatedByString:@"-"];
            NSString *lastStr = [arr lastObject];
            NSArray *arr2 = [lastStr componentsSeparatedByString:@"."];
            NSString *uid = [arr2 firstObject];
            if (uid) {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
                CDFMeViewController *MeVC = [storyboard instantiateViewControllerWithIdentifier:@"userViewController"];
                MeVC.uid = uid;
                [self.navigationController pushViewController:MeVC animated:YES];
            }else{
                ZWDWebViewController *web = [[ZWDWebViewController alloc]init];
                web.url = urlStr;
                [self.navigationController pushViewController:web animated:YES];
            }
            
        }
        
    }else{
        
        [SVProgressHUD showErrorWithStatus:@"非法链接" duration:1.5];
    }
}


- (CGFloat)heightOfBlackBoardWithIndex:(NSIndexPath *)indexPath {
    if (self.isActivity){
        NSIndexPath *newIndexPath;
        if (indexPath.row > 0 && indexPath.section == 0) {
            newIndexPath = [NSIndexPath indexPathForRow:indexPath.row - 1 inSection:indexPath.section];
        }
        if (indexPath.section == 0 && indexPath.row == 0) {
            newIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
        }
        if (indexPath.section > 0) {
            newIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
        }
        if (indexPath.section == 0 && indexPath.row == 1) {
            //NSLog(@"%f",[self heightForString:nil fontSize:12 andWidth:[UIScreen mainScreen].bounds.size.width - 25]);
            //NSLog(@"%f",[UIScreen mainScreen].bounds.size.width - 25);
            return [self heightForString:nil fontSize:FONT andWidth:[UIScreen mainScreen].bounds.size.width - 45 + 23]+30;
        }
        
        NSString *key = [NSString stringWithFormat:@"%d-%d",newIndexPath.section,newIndexPath.row];
        CGFloat height = [[self.heightCache objectForKey:key] floatValue];
        if (height <= 0.1f) {
            height = [self heightForContentAtIndexPath:newIndexPath] + 75;
            [self.heightCache setObject:[NSNumber numberWithFloat:height] forKey:key];
        }
        if (indexPath.section == 0 && indexPath.row == 0) {
            height += 80;
        }
        if ([activityInfo count] != 0 && newIndexPath.row == 0 && newIndexPath.section == 0) {
            // NSLog(@"%@",activityInfo);
            return height + 450;
            
        }else{
            // NSLog(@"%f",height);
            return height;
        }
        return 0;

    }else{
        NSIndexPath *newIndexPath;
        if (indexPath.row > 0 && indexPath.section == 0) {
            newIndexPath = [NSIndexPath indexPathForRow:indexPath.row - 1 inSection:indexPath.section];
        }
        if (indexPath.section == 0 && indexPath.row == 0) {
            newIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
        }
        if (indexPath.section > 0) {
            newIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
        }
        if (indexPath.section == 0 && indexPath.row == 1) {
            //NSLog(@"%f",[self heightForString:nil fontSize:12 andWidth:[UIScreen mainScreen].bounds.size.width - 25]);
            //NSLog(@"%f",[UIScreen mainScreen].bounds.size.width - 25);
            return [self heightForString:nil fontSize:FONT andWidth:[UIScreen mainScreen].bounds.size.width - 45 + 23]+30;
        }
        ///
        //    if (newIndexPath.row == 7) {
        //        newIndexPath = [NSIndexPath indexPathForRow:4 inSection:0];
        //    }else if (newIndexPath.row == 4){
        //        newIndexPath = [NSIndexPath indexPathForRow:7 inSection:0];
        //
        //    }
        ///
        
        NSString *key = [NSString stringWithFormat:@"%d-%d",newIndexPath.section,newIndexPath.row];
        CGFloat height = [[self.heightCache objectForKey:key] floatValue];
        if (height <= 0.0f) {
//            NSLog(@"%@",key);
            height = [self heightForContentAtIndexPath:newIndexPath] + 55;
            [self.heightCache setObject:[NSNumber numberWithFloat:height] forKey:key];
        }
        //NSLog(@"%@",_dataSource);
        //NSLog(@"%@",self.dataSource[newIndexPath.section][newIndexPath.row][@"imagelist"]);
        //    if (indexPath.section==0 && indexPath.row>1) {
        //        if ([self.dataSource[newIndexPath.section][newIndexPath.row][@"imagelist"] count] > 0) {
        //            height -= 30;
        //        }
        //    }
        
        if ([activityInfo count] != 0 && newIndexPath.row == 0 && newIndexPath.section == 0) {
            // NSLog(@"%@",activityInfo);
            return height + 450;
            
        }else{
            // NSLog(@"%f",height);
            return height;
        }
        
        return 0;
    }


}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
   // if (self.isActivity && indexPath.row == 0 && indexPath.section == 0) {
        
        //        NSString *key = [NSString stringWithFormat:@"%d-%d",indexPath.section,indexPath.row];
        //        CGFloat height = [[self.heightCache objectForKey:key] floatValue];
        //        if (height <= 0.0f) {
        //            height = [self heightForContentAtIndexPath:indexPath] + 55;
        //            [self.heightCache setObject:[NSNumber numberWithFloat:height] forKey:key];
        //        }
        //        if ([activityInfo count] != 0 && indexPath.row == 0 && indexPath.section == 0) {
        //
        //            return height + 450;//第一个cell高度
        //        }else{
        //
        //            return 1000;
        //            return height;
        //        }
    /*
     CGFloat height = 0.0;
     // NSString *str = self.mainData[@"postlist"][0][@"message"];
     //height += [self heightForString:str fontSize:16 andWidth:self.view.frame.size.width];
     
     height = [self heightForContentAtIndexPath:indexPath] + 55;
     
     
     //height += 60;
     return height;
     */
    if (self.isActivity){
        NSIndexPath *newIndexPath;
        if (indexPath.row > 0 && indexPath.section == 0) {
            newIndexPath = [NSIndexPath indexPathForRow:indexPath.row - 1 inSection:indexPath.section];
        }
        if (indexPath.section == 0 && indexPath.row == 0) {
            newIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
        }
        if (indexPath.section > 0) {
            newIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
        }
        if (indexPath.section == 0 && indexPath.row == 1) {
            //NSLog(@"%f",[self heightForString:nil fontSize:12 andWidth:[UIScreen mainScreen].bounds.size.width - 25]);
            //NSLog(@"%f",[UIScreen mainScreen].bounds.size.width - 25);
            return [self heightForString:nil fontSize:FONT andWidth:[UIScreen mainScreen].bounds.size.width - 34]+30 + 10;
        }
        
        NSString *key = [NSString stringWithFormat:@"%d-%d",newIndexPath.section,newIndexPath.row];
        CGFloat height = [[self.heightCache objectForKey:key] floatValue];
        if (height <= 0.1f) {
            height = [self heightForContentAtIndexPath:newIndexPath] + 95;
            [self.heightCache setObject:[NSNumber numberWithFloat:height] forKey:key];
        }
        if (indexPath.section == 0 && indexPath.row == 0) {
            height += 80;
        }
        if ([activityInfo count] != 0 && newIndexPath.row == 0 && newIndexPath.section == 0) {
            // NSLog(@"%@",activityInfo);
            return height + 450 + 20;
            
        }else{
            // NSLog(@"%f",height);
            return height + 20;
        }
        
        return 0;
        
        
    }else{
        NSIndexPath *newIndexPath;
        if (indexPath.row > 0 && indexPath.section == 0) {
            newIndexPath = [NSIndexPath indexPathForRow:indexPath.row - 1 inSection:indexPath.section];
        }
        if (indexPath.section == 0 && indexPath.row == 0) {
            newIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
        }
        if (indexPath.section > 0) {
            newIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
        }
        if (indexPath.section == 0 && indexPath.row == 1) {
            //NSLog(@"%f",[self heightForString:nil fontSize:12 andWidth:[UIScreen mainScreen].bounds.size.width - 25]);
            //NSLog(@"%f",[UIScreen mainScreen].bounds.size.width - 25);
            return [self heightForString:nil fontSize:FONT andWidth:[UIScreen mainScreen].bounds.size.width - 34]+30 + 10;
        }
        ///
        //    if (newIndexPath.row == 7) {
        //        newIndexPath = [NSIndexPath indexPathForRow:4 inSection:0];
        //    }else if (newIndexPath.row == 4){
        //        newIndexPath = [NSIndexPath indexPathForRow:7 inSection:0];
        //
        //    }
        ///
        
        NSString *key = [NSString stringWithFormat:@"%d-%d",newIndexPath.section,newIndexPath.row];
        CGFloat height = [[self.heightCache objectForKey:key] floatValue];
        if (height <= 0.0f) {
//            NSLog(@"%@",key);
            height = [self heightForContentAtIndexPath:newIndexPath] + 95-10;
            [self.heightCache setObject:[NSNumber numberWithFloat:height] forKey:key];
        }
        //NSLog(@"%@",_dataSource);
        //NSLog(@"%@",self.dataSource[newIndexPath.section][newIndexPath.row][@"imagelist"]);
        //    if (indexPath.section==0 && indexPath.row>1) {
        //        if ([self.dataSource[newIndexPath.section][newIndexPath.row][@"imagelist"] count] > 0) {
        //            height -= 30;
        //        }
        //    }
        
        if ([activityInfo count] != 0 && newIndexPath.row == 0 && newIndexPath.section == 0) {
            // NSLog(@"%@",activityInfo);
            return height + 450 + 20;
            
        }else{
            // NSLog(@"%f",height);
            return height + 20;
        }
        
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    _showPage = indexPath.section + 1;
}

#pragma mark DTCoreText

- (UIView *)attributedTextContentView:(DTAttributedTextContentView *)attributedTextContentView viewForAttributedString:(NSAttributedString *)string frame:(CGRect)frame
{
    NSDictionary *attributes = [string attributesAtIndex:0 effectiveRange:NULL];
    
    NSURL *URL = [attributes objectForKey:DTLinkAttribute];
    NSString *identifier = [attributes objectForKey:DTGUIDAttribute];
    
    
    DTLinkButton *button = [[DTLinkButton alloc] initWithFrame:frame];
    button.URL = URL;
    button.minimumHitSize = CGSizeMake(25, 25); // adjusts it's bounds so that button is always large enough
    button.GUID = identifier;
    
    // get image with normal link text
    UIImage *normalImage = [attributedTextContentView contentImageWithBounds:frame options:DTCoreTextLayoutFrameDrawingDefault];
    [button setImage:normalImage forState:UIControlStateNormal];
    
    // get image for highlighted link text
    UIImage *highlightImage = [attributedTextContentView contentImageWithBounds:frame options:DTCoreTextLayoutFrameDrawingDrawLinksHighlighted];
    [button setImage:highlightImage forState:UIControlStateHighlighted];
    
    // use normal push action for opening URL
    [button addTarget:self action:@selector(linkPushed:) forControlEvents:UIControlEventTouchUpInside];
    
    // demonstrate combination with long press
    //	UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(linkLongPressed:)];
    //	[button addGestureRecognizer:longPress];
    
    return button;
}

- (void)linkPushed:(DTLinkButton *)button
{
    NSURL *URL = button.URL;
    
    NSDictionary *dic = [Common decodeURL:URL.absoluteString];
    //NSLog(@"%@", dic);
    if (dic) {
        if ([dic[@"type"] isEqualToString:@"post"]) {
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
            [SVProgressHUD showWithStatus:@"加载中..."];
            _currentPage = 1;
            self.tid = dic[@"tid"];
            self.pid = dic[@"pid"];
            self.navigationItem.title = @"";
            [self.htmlCache removeAllObjects];
            [self.heightCache removeAllObjects];
            [self loadData];
            return;
        }
        if ([dic[@"type"] isEqualToString:@"user"]) {
            [self performSegueWithIdentifier:@"pushToMe" sender:dic];
            return;
        }
    }
    if ([[UIApplication sharedApplication] canOpenURL:[URL absoluteURL]])
    {
        
        //[[UIApplication sharedApplication] openURL:[URL absoluteURL]];
    }
    else
    {
        if (![URL host] && ![URL path])
        {
            
            // possibly a local anchor link
            NSString *fragment = [URL fragment];
            
            if (fragment)
            {
                //[_textView scrollToAnchorNamed:fragment animated:NO];
            }
        }
    }
}

- (UIView *)attributedTextContentView:(DTAttributedTextContentView *)attributedTextContentView viewForAttachment:(DTTextAttachment *)attachment frame:(CGRect)frame
{
    if ([attachment isKindOfClass:[DTVideoTextAttachment class]])
    {
        NSURL *url = (id)attachment.contentURL;
        
        // we could customize the view that shows before playback starts
        UIView *grayView = [[UIView alloc] initWithFrame:frame];
        grayView.backgroundColor = [DTColor blackColor];
        
        // find a player for this URL if we already got one
        MPMoviePlayerController *player = nil;
        for (player in self.mediaPlayers)
        {
            if ([player.contentURL isEqual:url])
            {
                break;
            }
        }
        
        if (!player)
        {
            player = [[MPMoviePlayerController alloc] initWithContentURL:url];
            [self.mediaPlayers addObject:player];
        }
        
#if __IPHONE_OS_VERSION_MAX_ALLOWED > __IPHONE_4_2
        NSString *airplayAttr = [attachment.attributes objectForKey:@"x-webkit-airplay"];
        if ([airplayAttr isEqualToString:@"allow"])
        {
            if ([player respondsToSelector:@selector(setAllowsAirPlay:)])
            {
                player.allowsAirPlay = YES;
            }
        }
#endif
        
        NSString *controlsAttr = [attachment.attributes objectForKey:@"controls"];
        if (controlsAttr)
        {
            player.controlStyle = MPMovieControlStyleEmbedded;
        }
        else
        {
            player.controlStyle = MPMovieControlStyleNone;
        }
        
        NSString *loopAttr = [attachment.attributes objectForKey:@"loop"];
        if (loopAttr)
        {
            player.repeatMode = MPMovieRepeatModeOne;
        }
        else
        {
            player.repeatMode = MPMovieRepeatModeNone;
        }
        
        NSString *autoplayAttr = [attachment.attributes objectForKey:@"autoplay"];
        if (autoplayAttr)
        {
            player.shouldAutoplay = YES;
        }
        else
        {
            player.shouldAutoplay = NO;
        }
        
        [player prepareToPlay];
        
        player.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        player.view.frame = grayView.bounds;
        [grayView addSubview:player.view];
        
        return grayView;
    }
    else if ([attachment isKindOfClass:[DTImageTextAttachment class]])
    {
        // if the attachment has a hyperlinkURL then this is currently ignored
        CDFLazyImageView *imageView = [[CDFLazyImageView alloc] initWithFrame:frame];
        imageView.backgroundColor = [UIColor colorWithWhite:.95 alpha:1];
        //		imageView.delegate = self;
        // sets the image if there is one
        imageView.image = [(DTImageTextAttachment *)attachment image];
        
        // url for deferred loading
        imageView.url = attachment.contentURL;
        
        // NOTE: this is a hack, you probably want to use your own image view and touch handling
        // also, this treats an image with a hyperlink by itself because we don't have the GUID of the link parts
        imageView.userInteractionEnabled = YES;
        
        
        // if there is a hyperlink then add a link button on top of this image
        if (attachment.hyperLinkURL)
        {
            DTLinkButton *button = [[DTLinkButton alloc] initWithFrame:imageView.bounds];
            button.minimumHitSize = CGSizeMake(25, 25); // adjusts it's bounds so that button is always large enough
            // use normal push action for opening URL
            [button addTarget:self action:@selector(linkPushed:) forControlEvents:UIControlEventTouchUpInside];
            [imageView addSubview:button];
            button.URL = attachment.hyperLinkURL;
            button.GUID = attachment.hyperLinkGUID;
            button.URL = imageView.url;
        } else {
            
            imageView.hqURL = [NSString stringWithFormat:@"%@",imageView.url];
        }
        
        return imageView;
    }
    else if ([attachment isKindOfClass:[DTIframeTextAttachment class]])
    {
        DTWebVideoView *videoView = [[DTWebVideoView alloc] initWithFrame:frame];
        videoView.attachment = attachment;
        
        return videoView;
    }
    else if ([attachment isKindOfClass:[DTObjectTextAttachment class]])
    {
        // somecolorparameter has a HTML color
        NSString *colorName = [attachment.attributes objectForKey:@"somecolorparameter"];
        UIColor *someColor = DTColorCreateWithHTMLName(colorName);
        
        UIView *someView = [[UIView alloc] initWithFrame:frame];
        someView.backgroundColor = someColor;
        someView.layer.borderWidth = 1;
        someView.layer.borderColor = [UIColor blackColor].CGColor;
        
        someView.accessibilityLabel = colorName;
        someView.isAccessibilityElement = YES;
        
        return someView;
    }
    return nil;
}

//设置引用回复的样式
- (BOOL)attributedTextContentView:(DTAttributedTextContentView *)attributedTextContentView shouldDrawBackgroundForTextBlock:(DTTextBlock *)textBlock frame:(CGRect)frame context:(CGContextRef)context forLayoutFrame:(DTCoreTextLayoutFrame *)layoutFrame
{
    //NSLog(@"%@",textBlock);
    CGContextSaveGState(context);
    [[UIColor colorWithWhite:.9 alpha:1] setStroke];
    CGContextSetLineWidth(context, .5);
    CGFloat dashArray[] = {2,2};
    CGContextSetLineDash(context, 3, dashArray, 4);
    CGContextStrokeRect(context, frame);
    CGContextRestoreGState(context);
    return YES;
}
- (float) heightForString:(NSString *)value fontSize:(float)fontSize andWidth:(float)width
{
    
    NSMutableString *str = [NSMutableString new];
    //[str appendString:@"👍  "];
    
    
    
    for (int i = 0; i < [self.likeList count]; i++) {
        NSDictionary *dict = self.likeList[i];
        if (i == 0) {
            [str appendString:[NSString stringWithFormat:@"<a href=\"title\">[%d赞] </a>",[self.likeList count]]];
            [str appendFormat:@"%@",dict[@"username"]];
            
        }else{
            [str appendFormat:@" , %@",dict[@"username"]];
        }
    }
    CGSize sizeToFit = [str sizeWithFont:[UIFont systemFontOfSize:fontSize]
                       constrainedToSize:CGSizeMake(width -16.0, CGFLOAT_MAX)
                           lineBreakMode:NSLineBreakByWordWrapping];
    //此处的换行类型（lineBreakMode）可根据自己的实际情况进行设置
    return sizeToFit.height + 16.0;
}

#pragma mark  点击cell上的button代理方法
- (void)handleApplyBtn:(UIButton *)sender {
    
    if (_isActivityClosed) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"抱歉,您来晚了,活动已经结束" delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alertView show];
    } else if ([CDFMe shareInstance].isLogin) {
//        NSLog(@"");
        ActivityCommitViewController *activityCommitVC = [[ActivityCommitViewController alloc]init];
        activityCommitVC.tid = self.tid;
        activityCommitVC.fid = self.fid;
        activityCommitVC.pid = self.ppid;
        activityCommitVC.title = self.activityTitle;
        activityCommitVC.numberOfPerson = self.numberOfperson;
        
        activityCommitVC.specialDict = _mainData[@"special_activity"];
//        self.dataSource
//        NSLog(@"p:%@, t:%@, u:%@, f:%@", self.pid, self.tid, self.uid, self.fid);
        [self.navigationController pushViewController:activityCommitVC animated:YES];

    } else {
        //弹出登陆页面...
//        if (_isFirstWarn) {
//            _isFirstWarn = NO;
//        }
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"温馨提醒" message:@"您还未登录,请登录后再进行活动报名" delegate:self cancelButtonTitle:@"返回" otherButtonTitles:@"登录", nil];
        alert.delegate = self;
        alert.tag = 999;
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1 && alertView.tag == 999) {
//        NSLog(@"");
//        NSLog(@"");
        [[CDFMe shareInstance] wellcomeLogin:self];

    }
}
- (void)handleZanBtn:(UIButton *)sender {
//    NSLog(@"");
    
}
- (CGFloat)detailTextHeight:(NSString *)text {
    CGRect rectToFit = [text boundingRectWithSize:CGSizeMake(KDeviceSizeWidth - 2 * kSideToLeft - 15, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:20.0f]} context:nil];
//    NSLog(@"rectToFit:%f", rectToFit.size.height);
    return rectToFit.size.height;
}


@end
