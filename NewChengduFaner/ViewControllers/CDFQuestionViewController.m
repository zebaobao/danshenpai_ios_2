//
//  CDFQuestionViewController.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 14-1-16.
//  Copyright (c) 2014年 www.huaxi100.com. All rights reserved.
//

#import "CDFQuestionViewController.h"

@interface CDFQuestionViewController () <UITextFieldDelegate, UIActionSheetDelegate>
@property (strong, nonatomic) IBOutlet UILabel *questionLabel;
@property (assign, nonatomic) NSInteger questionId;
@property (strong, nonatomic) IBOutlet UITextField *answerTextField;

@end

@implementation CDFQuestionViewController {
    NSArray *_questionArray;
    UIActionSheet *sheet;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    _questionArray = @[@"母亲的名字", @"爷爷的名字", @"父亲出生的城市", @"您其中一位老师的名字", @"您个人计算机的型号", @"您最喜欢的餐馆名称", @"驾驶执照最后四位数字"];
    sheet = [[UIActionSheet alloc] initWithTitle:@"安全提问" delegate:self cancelButtonTitle:nil destructiveButtonTitle:@"取消" otherButtonTitles: nil];
    for (NSString *question in _questionArray) {
        [sheet addButtonWithTitle:question];
    }
}

- (IBAction)login:(id)sender {
    [self.answerTextField resignFirstResponder];
    if (self.questionId <= 0) {
        [SVProgressHUD showErrorWithStatus:@"请选择安全提问"];
        return;
    } else if (self.answerTextField.text.length <=0) {
        [SVProgressHUD showErrorWithStatus:@"请填写答案"];
        return;
    }
    [SVProgressHUD showWithStatus:@"登录中..." maskType:SVProgressHUDMaskTypeGradient];
    NSString *url = [NSString stringWithFormat:@"%@&questionid=%d&answer=%@",self.loginURL,self.questionId,[self.answerTextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    //NSLog(@"%@%@",kBaseURL,url);
    [[HXHttpClient shareInstance] grabURL:url success:^(id responseObject) {
//        NSLog(@"%@",[Common dictionaryToJSON:responseObject]);
        if (responseObject[@"Message"]) {
            NSString *message = [NSString stringWithFormat:@"%@",responseObject[@"Message"][@"messagestr"]];
            //NSLog(@"%@",responseObject);
            if (![responseObject[@"Message"][@"messageval"] isEqualToString:@"login_succeed"]) {
                [SVProgressHUD showErrorWithStatus:message];
            } else {
                [SVProgressHUD showSuccessWithStatus:message];
                [Common login:responseObject];
                [self dismissModalViewControllerAnimated:YES];
            }
        }
    } fail:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"安全提问"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"安全提问"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0 && indexPath.section == 0) {
        [sheet showInView:self.view];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
//    NSLog(@"%d",buttonIndex);
    if (buttonIndex > 0) {
        self.questionId = buttonIndex;
        self.questionLabel.text = _questionArray[buttonIndex - 1];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
