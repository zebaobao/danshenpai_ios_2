//
//  CDFMeViewController.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-11.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDFMyDynCell.h"
#import "ZWDFirstLoginViewController.h"

@interface CDFMeTableHeaderView : UIView
@end

@interface CDFMeTableFooterView : UIView
@end

@interface CDFRoundButton : UIButton
@end

@interface CDFMeViewController : UITableViewController

@property (copy, nonatomic) id uid;

@end
