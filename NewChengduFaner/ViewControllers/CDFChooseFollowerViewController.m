//
//  CDFChooseFollowerViewController.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-12-27.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFChooseFollowerViewController.h"
#import "ChatViewController.h"
@interface CDFChooseFollowerViewController ()<UISearchBarDelegate, UISearchDisplayDelegate>
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) NSMutableArray *dataSource;
@property (strong, nonatomic) NSMutableArray *dataSourceForSearch;
@property (strong, nonatomic) NSArray *filter;
@end

@implementation CDFChooseFollowerViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (NSMutableArray *)dataSource
{
    if (!_dataSource) {
        _dataSource = [NSMutableArray new];
    }
    return _dataSource;
}

- (NSMutableArray *)dataSourceForSearch
{
    if (!_dataSourceForSearch) {
        _dataSourceForSearch = [NSMutableArray new];
    }
    return _dataSourceForSearch;
}

- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)loadData
{

    NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=following", kApiVersion];
    [SVProgressHUD showWithStatus:@"请稍后..."];
    [[HXHttpClient shareInstance] grabURL:url success:^(id responseObject) {
        
        if (responseObject[@"Message"]) {
            [SVProgressHUD showErrorWithStatus:responseObject[@"Message"][@"messagestr"]];
            return;
        }
        [self.dataSourceForSearch removeAllObjects];
        [self.dataSource removeAllObjects];
//        NSLog(@"3333%d", [responseObject[@"Variables"][@"list"] count]);
        if (![responseObject[@"Variables"][@"list"] isEqual:[NSNull null]] && [responseObject[@"Variables"][@"list"] count]) {
            
            NSDictionary *dictionary = [NSDictionary dictionaryWithDictionary:responseObject[@"Variables"][@"list"]];

            
            for (NSString *key in [dictionary allKeys]) {
                
                [self.dataSource addObject:@{@"key": key,@"items":dictionary[key]}];
                [_dataSourceForSearch addObjectsFromArray:dictionary[key]];
            }

            [_dataSource sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                return [obj1[@"key"] compare:obj2[@"key"]];
            }];
        }
        [self.tableView reloadData];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.frame), 30)];
        label.text = [NSString stringWithFormat:@"总共 %d 个联系人", self.dataSourceForSearch.count];
        label.textColor = [UIColor lightGrayColor];
        label.font = [UIFont systemFontOfSize:13];
        label.textAlignment = NSTextAlignmentCenter;
        self.tableView.tableFooterView = label;
        [SVProgressHUD dismiss];
    } fail:^(NSError *error) {
//        NSLog(@"%@",error);
        [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
    }];
}

#pragma mark - Super Methods
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"选择私信对象"];
    if ([Common systemVersion] >= 7.0) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
        [self setNeedsStatusBarAppearanceUpdate];
        
    }
    if ([Common systemVersion] < 7.0) {
        self.navigationItem.rightBarButtonItem.tintColor = self.navigationController.navigationBar.tintColor;
        self.navigationItem.backBarButtonItem.tintColor = self.navigationController.navigationBar.tintColor;
    } else {
        //self.navigationController.navigationBar.tintColor = [UIColor colorWithWhite:0 alpha:1];
        self.navigationController.navigationBar.barTintColor = BARTINCOLOR;
        self.navigationItem.rightBarButtonItem.tintColor = TITLECOLOR;
        self.searchBar.tintColor = self.navigationController.navigationBar.tintColor;
    }
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"选择私信对象"];
    [SVProgressHUD dismiss];
    if ([Common systemVersion] >= 7.0) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
        [self setNeedsStatusBarAppearanceUpdate];
    }
}



#pragma mark - Super Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.cancleButton.tintColor = TITLECOLOR;
    UILabel *titleLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 30)];
    titleLable.text =@"选择联系人";
    titleLable.textColor = TITLEGRAY;
    titleLable.textAlignment = NSTextAlignmentCenter;
    self.navigationItem.titleView = titleLable;

    
    [self loadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController respondsToSelector:@selector(touid)]) {
        [segue.destinationViewController setTitle:[NSString stringWithFormat:@"To：%@",sender[@"fusername"]]];
        [segue.destinationViewController setValue:sender[@"followuid"] forKey:@"touid"];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return tableView == self.tableView ? self.dataSource.count : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return tableView == self.tableView ? [self.dataSource[section][@"items"] count] : self.filter.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        CGFloat detaX = 0;
        if ([Common systemVersion] >= 7.0) {
            detaX = 15;
        }
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(5 + detaX, 7, 30, 30)];
        imageView.tag = 1;
        imageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        [cell.contentView addSubview:imageView];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(43 + detaX, 0, 257, CGRectGetHeight(cell.frame))];
        label.font = [UIFont systemFontOfSize:15];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor colorWithWhite:51.0f/255.0f alpha:1];
        label.tag = 2;
        [cell.contentView addSubview:label];
    }
    BOOL isSearchTable = (tableView != self.tableView);
    
    NSString *avatarURL = [Common formatAvatarWithUID:(isSearchTable ? self.filter[indexPath.row][@"followuid"] : self.dataSource[indexPath.section][@"items"][indexPath.row][@"followuid"]) type:@"normal"];
    
    UIImageView *imageView = (UIImageView *)[cell.contentView viewWithTag:1];
    [imageView setImageWithURL:[NSURL URLWithString:avatarURL]];
    
    UILabel *label = (UILabel *)[cell.contentView viewWithTag:2];
    label.text = [NSString stringWithFormat:@"%@",(isSearchTable ? self.filter[indexPath.row][@"fusername"] : self.dataSource[indexPath.section][@"items"][indexPath.row][@"fusername"])];
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return tableView == self.tableView ? self.dataSource[section][@"key"] : @"";
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *dictionary = tableView == self.tableView ? self.dataSource[indexPath.section][@"items"][indexPath.row] : self.filter[indexPath.row];
//    [self performSegueWithIdentifier:@"pushToSendPM" sender:dictionary];
    NSString *IMUid = [NSString stringWithFormat:@"dahe_%@", dictionary[@"followuid"]];
//    NSLog(@"dic:%@",dictionary);
//    NSLog(@"%@", IMUid);
    ChatViewController *chatVC = [[ChatViewController alloc] initWithChatter:IMUid isGroup:NO userName:dictionary[@"fusername"]];
    chatVC.title =dictionary[@"fusername"];
    chatVC.hidesBottomBarWhenPushed = YES;
    //chatVC.navigationItem.title = dictionary[@"fusername"];
    [self.navigationController pushViewController:chatVC animated:YES];
}

#pragma mark - Search Delegate
- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    self.filter = [self.dataSourceForSearch filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary * bindings){
        return [evaluatedObject[@"fusername"] rangeOfString:searchString].location != NSNotFound;
    }]];
    UITableView *tableView = self.searchDisplayController.searchResultsTableView;
    if (self.filter.count > 0) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 30)];
        label.text = [NSString stringWithFormat:@"查询到 %d 个联系人", self.filter.count];
        label.textColor = [UIColor lightGrayColor];
        label.font = [UIFont systemFontOfSize:13];
        label.textAlignment = UITextAlignmentCenter;
        tableView.tableFooterView = label;
    } else {
        tableView.tableFooterView = nil;
    }
    return YES;
}

@end
