//
//  CDFFollowViewController.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-12-30.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFFollowViewController.h"

@interface CDFFollowViewController ()<UIActionSheetDelegate>

@property (nonatomic, strong) NSMutableArray *dataSource;

@end

@implementation CDFFollowViewController {
    NSInteger   _isLoading;
    NSInteger   _currentPage;
    BOOL        _hasMore;
    CGPoint     _scrollPoint;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
       [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];
}

- (void)setup
{
    _currentPage = 1;
}

- (NSMutableArray *)dataSource
{
    if (!_dataSource) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}

- (void)loadData
{
    if (_isLoading) {
        return;
    }
    _isLoading = YES;
    
    if (self.uid == NULL) {
        
        [SVProgressHUD showErrorWithStatus:@"您还没有登录!请登录"];
        
    }else{
    
        [SVProgressHUD showWithStatus:@"请稍后..."];
        NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=%@&uid=%@&page=%d",kApiVersion,self.filter,self.uid,_currentPage];
//        NSLog(@"%@%@",kBaseURL,url);
        [[HXHttpClient shareInstance] grabURL:url success:^(id responseObject) {
            _isLoading = NO;
            [[NSNotificationCenter defaultCenter] postNotificationName:kMESSAGE_BADGE object:nil userInfo:responseObject[@"Variables"][@"newmessage2"]];
            if (responseObject[@"Message"]) {
                [SVProgressHUD showErrorWithStatus:responseObject[@"Message"][@"messagestr"]];
            } else {
                [SVProgressHUD dismiss];
                if (_currentPage == 1) {
                    [self.dataSource removeAllObjects];
                }
                
                for (NSDictionary *data in responseObject[@"Variables"][@"list"]) {
                    [self.dataSource addObject:[NSMutableDictionary dictionaryWithDictionary:data]];
                }
                
                _hasMore = self.dataSource.count < [responseObject[@"Variables"][@"count"] integerValue];
                if (self.dataSource.count > 0 && !_hasMore) {
                    UIView *ft = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.frame), 30)];
                    UILabel *label = [[UILabel alloc] initWithFrame:ft.bounds];
                    label.font = [UIFont systemFontOfSize:12];
                    label.backgroundColor = [UIColor clearColor];
                    label.textColor = [UIColor lightGrayColor];
                    label.textAlignment = UITextAlignmentCenter;
                    label.text = @"已经加载全部数据";
                    [ft addSubview:label];
                    self.tableView.tableFooterView = ft;
                }
                [self.tableView reloadData];
            }
            
        } fail:^(NSError *error) {
            _isLoading = NO;
            [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
        }];
    
    }

}

static NSInteger actionCell;
static UIButton *actionSender;
- (void)buttonAction:(UIButton *)sender
{
    actionCell = sender.tag;
    actionSender = sender;
    BOOL mutual = [self.dataSource[sender.tag][@"mutual"] boolValue];
    
    if (mutual) {
        //相互关注按钮被点击
        [self confirm];
    } else {
        if ([self.filter isEqualToString:@"myfollower"]) {
            if (!sender.accessibilityValue) {
                //收听按钮被点击
                [self follow];
            } else {
                [self confirm];
            }
        } else {
            //已收听按钮被点击
            if (!sender.accessibilityValue) {
                //收听按钮被点击
                [self confirm];
            } else {
                [self follow];
            }
            
        }
    }
}

- (void)confirm
{
    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"真的要取消对TA的收听？" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"确定", nil];
    [action showFromTabBar:self.tabBarController.tabBar];
}

- (void)follow
{
    NSString *idKey = [self.filter isEqualToString:@"myfollower"] ? @"uid" : @"followuid";
    NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=addfollow&hash=%@&fuid=%@", kApiVersion, [CDFMe shareInstance].formHash, self.dataSource[actionSender.tag][idKey]];
    [SVProgressHUD showWithStatus:@"请稍后..."];
    [[HXHttpClient shareInstance] grabURL:url success:^(id responseObject) {
//        NSLog(@"%@",responseObject);
        [[NSNotificationCenter defaultCenter] postNotificationName:kMESSAGE_BADGE object:nil userInfo:responseObject[@"Variables"][@"newmessage2"]];
        if ([responseObject[@"Message"][@"messageval"] isEqualToString:@"follow_add_succeed"]) {
            [SVProgressHUD showSuccessWithStatus:responseObject[@"Message"][@"messagestr"]];
            
            [self.dataSource[actionSender.tag] setObject:@"1" forKey:@"mutual"];
            
            [actionSender setImage:[UIImage imageNamed:[self.filter isEqualToString:@"myfollower"] ?@"followed_each" : @"followed"] forState:UIControlStateNormal];
            [actionSender setTitle:@"已收听" forState:UIControlStateNormal];
            [actionSender setTitleColor:[UIColor colorWithWhite:153.0f/255.0f alpha:1] forState:UIControlStateNormal];
            actionSender.accessibilityValue = @"operated";
            actionSender = nil;
        } else {
            [SVProgressHUD showErrorWithStatus:responseObject[@"Message"][@"messagestr"]];
        }
    } fail:^(NSError *error) {
//        NSLog(@"%@",error);
        [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
    }];
}

- (void)cancelFollow
{
    NSString *idKey = [self.filter isEqualToString:@"myfollower"] ? @"uid" : @"followuid";
    
    NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=delfollow&fuid=%@",kApiVersion,self.dataSource[actionCell][idKey]];
    [[HXHttpClient shareInstance] grabURL:url success:^(id responseObject) {
//        NSLog(@"%@",responseObject);
        [[NSNotificationCenter defaultCenter] postNotificationName:kMESSAGE_BADGE object:nil userInfo:responseObject[@"Variables"][@"newmessage2"]];
        if ([responseObject[@"Message"][@"messageval"] isEqualToString:@"follow_cancel_succeed"]) {
            [SVProgressHUD showSuccessWithStatus:responseObject[@"Message"][@"messagestr"]];
            [actionSender setImage:[UIImage imageNamed:@"green_plus"] forState:UIControlStateNormal];
            [actionSender setTitleColor:[Common colorWithHex:@"#339900" alpha:1] forState:UIControlStateNormal];
            [actionSender setTitle:@"收听" forState:UIControlStateNormal];
            [self.dataSource[actionCell] setObject:@"0" forKey:@"mutual"];
            [self.tableView reloadData];
            actionSender.accessibilityValue = @"operated";
            actionSender = nil;
        } else {
            [SVProgressHUD showErrorWithStatus:responseObject[@"Message"][@"messagestr"]];
        }
    } fail:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
//        NSLog(@"%@",error);
    }];
}

#pragma mark - UIActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [self cancelFollow];
    }
}

#pragma mark - Super Methods

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([sender isKindOfClass:[UITableViewCell class]]) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        NSString *idKey = [self.filter isEqualToString:@"myfollower"] ? @"uid" : @"followuid";
        if ([segue.destinationViewController respondsToSelector:@selector(uid)]) {
            [segue.destinationViewController setValue:self.dataSource[indexPath.row][idKey] forKey:@"uid"];
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self loadData];
    [MobClick beginLogPageView:@"收听列表"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"收听列表"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSString *nameKey = [self.filter isEqualToString:@"myfollower"] ? @"username" : @"fusername";
    NSString *idKey = [self.filter isEqualToString:@"myfollower"] ? @"uid" : @"followuid";
   
    UILabel *label = (UILabel *)[cell.contentView viewWithTag:2];
    label.text = [NSString stringWithFormat:@"%@", self.dataSource[indexPath.row][nameKey]];
    
    label = (UILabel *)[cell.contentView viewWithTag:3];
    label.text = [NSString stringWithFormat:@"%@",(self.dataSource[indexPath.row][@"recentnote"] ? self.dataSource[indexPath.row][@"recentnote"] : @"TA最近没有发帖:)")];
    
    UIImageView *avatarImageView = (UIImageView *)[cell.contentView viewWithTag:1];
    [avatarImageView setImageWithURL:[NSURL URLWithString:[Common formatAvatarWithUID:self.dataSource[indexPath.row][idKey] type:@"normal"]]];
    
    UIButton *button = (UIButton *)[cell.contentView viewWithTag:4];
    NSString *title;
    UIImage *image;
    UIColor *titleColor;
    if ([self.dataSource[indexPath.row][@"mutual"] boolValue]) {
        image = [UIImage imageNamed:@"followed_each"];
        title = @"已收听";
        titleColor = [UIColor colorWithWhite:153.0f/255.0f alpha:1];
    } else {
        if ([self.filter isEqualToString:@"myfollower"]) {
            image = [UIImage imageNamed:@"green_plus"];
            title = @"收听";
            titleColor = [Common colorWithHex:@"#339900" alpha:1];
        } else {
            image = [UIImage imageNamed:@"followed"];
            title = @"已收听";
            titleColor = [UIColor colorWithWhite:153.0f/255.0f alpha:1];
        }
    }
    [button setImage:image forState:UIControlStateNormal];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    button.tag = indexPath.row;
    [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    //下拉加载更多
    if (scrollView.contentSize.height - scrollView.contentOffset.y < CGRectGetHeight(scrollView.frame) + 50) {
        if (_hasMore && !_isLoading) {
            _currentPage++;
            _scrollPoint = CGPointMake(0, scrollView.contentOffset.y);
            [self loadData];
        }
    }
}

@end
