//
//  CDFForumViewController.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-22.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFForumViewController.h"

@implementation BoardCell
@end

@interface CDFForumViewController ()

@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet UIButton *allButton;

@end

@implementation CDFForumViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)setSelectedForum:(NSDictionary *)selectedForum
{
    _selectedForum = selectedForum;
    [self.tableView reloadData];
}

- (IBAction)changeBoard:(id)sender {
    NSDictionary *sendData;
    if (![sender isEqual:self.allButton]) {
        sendData = [NSDictionary dictionaryWithDictionary:self.dataSource[[sender tag]]];
        self.selectedForum = sendData;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kBOARD_CHANGED object:nil userInfo:sendData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor blackColor];
    
    UIView *bg = [[UIView alloc] initWithFrame:self.tableView.bounds];
//    bg.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_board"]];
    bg.backgroundColor = [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0];
    self.tableView.backgroundView = bg;
    
    UIView *headerLine = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.headerView.bounds), CGRectGetWidth(self.headerView.bounds), .5)];
    headerLine.backgroundColor = [UIColor colorWithWhite:1 alpha:.2];
    [self.headerView addSubview:headerLine];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"板块选择"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"板块选择"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setDataSource:(NSArray *)dataSource
{
    
    NSMutableArray *array = [NSMutableArray new];

    for (id obj in dataSource) {
//        if ([obj[@"show"] boolValue]) {
            [array addObject:obj];
//        } 
    }
    _dataSource = array;
    [self.tableView reloadData];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (BoardCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    BoardCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[BoardCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    UIView *bg = [[UIView alloc] initWithFrame:cell.bounds];
    bg.backgroundColor = [UIColor clearColor];
    cell.backgroundView = bg;
    cell.backgroundColor = [UIColor clearColor];
    
    [cell.button setTitle:[NSString stringWithFormat:@"%@",self.dataSource[indexPath.row][@"name"]] forState:UIControlStateNormal];
    cell.button.tag = indexPath.row;
    [cell.button addTarget:self action:@selector(changeBoard:) forControlEvents:UIControlEventTouchUpInside];
    cell.button.enabled = (self.selectedForum[@"fid"] != self.dataSource[indexPath.row][@"fid"]);
    return cell;
}


@end
