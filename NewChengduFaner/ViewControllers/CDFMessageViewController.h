//
//  CDFMessageViewController.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-12.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDFMessageCell : UITableViewCell

@end

@interface CDFMessageViewController : UIViewController
@property (nonatomic ,assign) MessageBodyType type;
@end
