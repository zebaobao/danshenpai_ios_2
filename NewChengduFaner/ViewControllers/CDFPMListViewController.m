//
//  CDFPMListViewController.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-12-24.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFPMListViewController.h"
#import "CRNavigationController.h"

@implementation CDFPMListViewCellContentView

- (void)drawRect:(CGRect)rect
{
    
}

@end

@implementation RoundTextView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];
}

- (UILabel *)placeHoldLabel
{
    if (!_placeHoldLabel) {
        _placeHoldLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, CGRectGetWidth(self.frame) - 10, CGRectGetHeight(self.frame))];
        _placeHoldLabel.text = @"回复：";
        _placeHoldLabel.font = [UIFont systemFontOfSize:12];
        _placeHoldLabel.textColor = [UIColor colorWithWhite:230.0f/255.0f alpha:1];
        _placeHoldLabel.backgroundColor = [UIColor clearColor];
    }
    return _placeHoldLabel;
}

- (void)setup
{
    self.layer.borderColor = [UIColor colorWithWhite:213.0f/255.0f alpha:1].CGColor;
    self.layer.borderWidth = 3;
    self.layer.cornerRadius = 15;
    self.layer.masksToBounds = YES;
    [self addSubview:self.placeHoldLabel];
}

@end

@implementation ReplyContentView

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    drawLinearGradient(context, rect, [UIColor colorWithWhite:246.0f/255.0f alpha:1].CGColor, [UIColor colorWithWhite:240.0f/255.0f alpha:1].CGColor, CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect)), CGPointMake(CGRectGetMidX(rect), CGRectGetMaxY(rect)));
    draw1PxStroke(context, CGPointMake(CGRectGetMinX(rect), CGRectGetMinY(rect)), CGPointMake(CGRectGetMaxX(rect), CGRectGetMinY(rect)), [UIColor colorWithWhite:190.0f/255.0f alpha:1].CGColor);
    draw1PxStroke(context, CGPointMake(CGRectGetMinX(rect), CGRectGetMinY(rect) + .5), CGPointMake(CGRectGetMaxX(rect), CGRectGetMinY(rect) + .5), [UIColor whiteColor].CGColor);
}

@end

@interface CDFPMListViewCell()
@property (strong, nonatomic) IBOutlet UIView *leftLine;
@property (strong, nonatomic) IBOutlet UIView *rightLine;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet CDFAvatarImageView *avatarImageView;
@property (strong, nonatomic) IBOutlet UILabel *messageLabel;
@property (strong, nonatomic) IBOutlet UIView *messageContentView;
@property (strong, nonatomic) IBOutlet UILabel *datelineLabel;
@property (strong, nonatomic) IBOutlet UIImageView *messageBackgroundView;

@end
@implementation CDFPMListViewCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];
}

- (void)setup
{
    CGRect rectLeft = self.leftLine.frame;
    CGRect rectRight = self.rightLine.frame;
    rectLeft.size.height = .5;
    rectRight.size.height = .5;
    self.leftLine.frame = rectLeft;
    self.rightLine.frame = rectRight;
}

@end

@interface CDFPMListViewController ()<UITextViewDelegate>

@property (nonatomic, strong) NSMutableArray *dataSource;
@property (strong, nonatomic) IBOutlet UIView *replyContentView;
@property (strong, nonatomic) IBOutlet RoundTextView *replyTextView;
@property (strong, nonatomic) IBOutlet UIButton *replyButton;
@end

@implementation CDFPMListViewController {
    NSDictionary *resultData;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (NSMutableArray *)dataSource
{
    if (!_dataSource) {
        _dataSource = [NSMutableArray new];
    }
    return _dataSource;
}

- (void)bindDataSource:(NSArray *)array
{
    [self.dataSource removeAllObjects];
    NSMutableDictionary *item = [NSMutableDictionary dictionary];
    for (NSDictionary *dic in array) {
        NSMutableArray *array = [NSMutableArray new];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:[dic[@"dateline"] longLongValue]];
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
        NSString *year = [NSString stringWithFormat:@"%d", [components year]];
        
        if ([item objectForKey:year]) {
            [[item objectForKey:year] addObject:dic];
        } else {
            [array addObject:dic];
            [item setObject:array forKey:year];
        }
    }
    for (NSString *key in [item allKeys]) {
        [self.dataSource addObject:@{@"key": key,@"value":item[key]}];
    }
    [self.dataSource sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [obj1[@"key"] compare:obj2[@"key"]];
    }];
    
    [self.tableView reloadData];
    if (self.dataSource.count > 0) {
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[[self.dataSource lastObject][@"value"] count]-1 inSection:self.dataSource.count - 1] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        self.replyTextView.text = @"";
        
    }
}

- (void)loadData
{
    NSString *url = [NSString stringWithFormat:@"api/mobile/index.php?version=%@&module=mypm&subop=view&touid=%@", kApiVersion, self.touid];
    [SVProgressHUD showWithStatus:@"请稍后..."];
    [[HXHttpClient shareInstance] grabURL:url success:^(id responseObject) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kMESSAGE_BADGE object:nil userInfo:responseObject[@"Variables"][@"newmessage2"]];
        resultData = responseObject[@"Variables"];
        if ([responseObject[@"Variables"][@"list"] isKindOfClass:[NSArray class]]) {
            
            [self bindDataSource:responseObject[@"Variables"][@"list"]];
        }
        [SVProgressHUD dismiss];
    } fail:^(NSError *error) {
//        NSLog(@"%@",[error localizedDescription]);
        [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
    }];
}

- (void)avatarClick:(CDFAvatarImageView *)avatarView
{
    //NSLog(@"%@",avatarView.uid);
    id userViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"userViewController"];
    if ([userViewController respondsToSelector:@selector(uid)]) {
        [userViewController setValue:avatarView.uid forKey:@"uid"];
    }
    [self.navigationController pushViewController:userViewController animated:YES];
}

- (IBAction)replyAction:(id)sender {
    
    if (self.replyTextView.text.length <= 0) {
        [SVProgressHUD showErrorWithStatus:@"回复内容不能为空！"];
        return;
    }
    NSDictionary *postData = @{@"message": self.replyTextView.text};
    NSString *url = [NSString stringWithFormat:@"api/mobile/?version=2&module=sendpm&pmid=%@&handlekey=pmsend&pmsubmit=true&formhash=%@", resultData[@"pmid"], [CDFMe shareInstance].formHash];
//    [SVProgressHUD showWithStatus:@"请稍后..."];
    [[HXHttpClient shareInstance] postURL:url postData:postData success:^(id responseObject) {
        
//        [SVProgressHUD dismiss];
        if ([responseObject[@"Message"][@"messageval"] isEqualToString:@"do_success"]) {
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self loadData];
            });
        } else {
            [SVProgressHUD showErrorWithStatus:responseObject[@"Message"][@"messagestr"]];
        }
        
    } fail:^(NSError *error) {
//        NSLog(@"%@",error);
        [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
    }];
}

#pragma mark - Super Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.replyButton.enabled = NO;
    
    if ([Common systemVersion] < 7.0) {
        UIView *bg = [[UIView alloc] initWithFrame:self.tableView.bounds];
        bg.backgroundColor = bgColor;
        self.tableView.backgroundView = bg;
    } else {
        self.view.backgroundColor = bgColor;
    }
    [self loadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"发送私信"];
    [self addKeyBoardNotification];
    
    if ([Common systemVersion] >= 7.0) {
        self.navigationController.navigationBar.tintColor = [UIColor colorWithWhite:153.0f/255.0f alpha:1];
        UILabel *titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.text = self.title;
        titleView.font = [UIFont systemFontOfSize:20];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.textAlignment = UITextAlignmentCenter;
        titleView.textColor = [UIColor colorWithWhite:153.0f/255.0f alpha:1];
        CGSize size = [titleView.text sizeWithFont:titleView.font];
        CGRect rect = CGRectMake(0, 0, size.width, size.height);
        titleView.frame = rect;
        self.navigationItem.titleView = titleView;
    }
    if ([Common systemVersion] >= 7.0) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
        [self setNeedsStatusBarAppearanceUpdate];
        CRNavigationController *nav = (CRNavigationController *)self.navigationController;
        [nav.navigationBar setBarTintColor:[UIColor whiteColor]];
    }
    
    CGRect footerRect = self.replyContentView.frame;
    footerRect.origin.y = CGRectGetHeight([UIScreen mainScreen].bounds) + CGRectGetHeight(footerRect);
    self.replyContentView.frame = footerRect;
    [[UIApplication sharedApplication].keyWindow addSubview:self.replyContentView];
    footerRect.origin.y = CGRectGetHeight([UIScreen mainScreen].bounds) - CGRectGetHeight(footerRect);
    [UIView animateWithDuration:.25 animations:^{
        self.replyContentView.frame = footerRect;
    }];

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"发送私信"];
    [SVProgressHUD dismiss];
    [self.replyTextView resignFirstResponder];
    if ([Common systemVersion] >= 7.0) {
        //self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque animated:YES];
        [self setNeedsStatusBarAppearanceUpdate];
        CRNavigationController *nav = (CRNavigationController *)self.navigationController;
        [nav.navigationBar setBarTintColor:[UIColor colorWithRed:1.0f green:48.0/255.0f blue:0.0f alpha:1]];
    }
    CGRect pageRect = self.replyContentView.frame;
    pageRect.origin.y = CGRectGetHeight([UIScreen mainScreen].bounds) + CGRectGetHeight(pageRect);
    [UIView animateWithDuration:.25 animations:^{
        self.replyContentView.frame = pageRect;
    } completion:^(BOOL finished) {
        [self.replyContentView removeFromSuperview];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UIKeyboardController Listener

//监听键盘隐藏和显示事件
- (void)addKeyBoardNotification {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowOrHide:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowOrHide:) name:UIKeyboardWillHideNotification object:nil];
}

static NSNotification *kbNote;
-(void)keyboardWillShowOrHide:(NSNotification *)notification {
    
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    kbNote = notification;
    BOOL isShow = [[notification name] isEqualToString:UIKeyboardWillShowNotification] ? YES : NO;
    
    CGRect formRect = self.replyContentView.frame;
    
    if (isShow) {
        formRect.origin.y -= keyboardSize.height - (CGRectGetHeight([UIApplication sharedApplication].keyWindow.frame) - CGRectGetMaxY(formRect));
    } else {
        formRect.origin.y += keyboardSize.height;
    }
    [UIView animateWithDuration:.25 animations:^{
        self.replyContentView.frame = formRect;
    }];
}

//注销监听事件
- (void)removeKeyBoardNotification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}


#pragma mark - Text View delegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
    [(RoundTextView *)textView placeHoldLabel].hidden = textView.text.length > 0;
    self.replyButton.enabled = textView.text.length > 0;
    CGSize size = textView.contentSize;
    size.height -= 33;
    CGRect rect = self.replyContentView.frame;
    rect.size.height = MIN(44 + size.height, 100);
    [UIView animateWithDuration:.15 animations:^{
        self.replyContentView.frame = rect;
    }];
//    [[NSNotificationCenter defaultCenter] postNotification:kbNote];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataSource[section][@"value"] count];
}

- (CGSize)messageSizeForIndexPath:(NSIndexPath *)indexPath
{
    NSString *msg = [[self.dataSource[indexPath.section][@"value"][indexPath.row][@"message"] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
    
    UIFont *f = [UIFont systemFontOfSize:15];
    CGSize size = [msg sizeWithFont:f constrainedToSize:CGSizeMake(195, 1000)];
    if (size.width < 52) {
        size.width = 52;
    }
    return size;
}

- (CDFPMListViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL isSelf = [self.dataSource[indexPath.section][@"value"][indexPath.row][@"authorid"] isEqual:[CDFMe shareInstance].uid];
    NSString *CellIdentifier = isSelf ? @"CellMe" : @"Cell";
    
    CDFPMListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (indexPath.row == 0) {
        cell.titleView.hidden = NO;
        cell.titleLabel.text = [NSString stringWithFormat:@"%@年", self.dataSource[indexPath.section][@"key"]];
    } else {
        cell.titleView.hidden = YES;
    }
    
    cell.avatarImageView.uid = self.dataSource[indexPath.section][@"value"][indexPath.row][@"authorid"];
    NSString *authorFaceURL = [Common formatAvatarWithUID:self.dataSource[indexPath.section][@"value"][indexPath.row][@"authorid"] type:@"normal"];
    [cell.avatarImageView addTarget:self action:@selector(avatarClick:) forControlEvents:UIControlEventTouchUpInside];
    [cell.avatarImageView setImageWithURL:[NSURL URLWithString:authorFaceURL]];
    //    cell.messageLabel.text = [NSString stringWithFormat:@"%@",[self.dataSource[indexPath.section][@"value"][indexPath.row][@"message"] stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""]];
//    NSLog(@"%@",[[self.dataSource[indexPath.section][@"value"][indexPath.row][@"message"] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "]);
    cell.messageLabel.text = [NSString stringWithFormat:@"%@",[[self.dataSource[indexPath.section][@"value"][indexPath.row][@"message"] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "]];
    cell.datelineLabel.text = [Common formatDate:[self.dataSource[indexPath.section][@"value"][indexPath.row][@"dateline"] longLongValue]];
    cell.messageBackgroundView.image = [[UIImage imageNamed:(isSelf ? @"bg_pm_self" : @"bg_pm")] stretchableImageWithLeftCapWidth:30 topCapHeight:30];
    CGSize size = [self messageSizeForIndexPath:indexPath];
    CGRect rect = cell.messageContentView.frame;
    CGRect rectAvatar = cell.avatarImageView.frame;
    rect.size.width = size.width + 30;
    rect.size.height = size.height + 30;
    if (isSelf) {
        rect.origin.x = 50 + (190 - size.width);
    } else {
        rect.origin.x = 45;
    }
    if (indexPath.row != 0) {
        rect.origin.y = 10;
        rectAvatar.origin.y = 10;
    } else {
        rect.origin.y = 28;
        rectAvatar.origin.y = 28;
    }
    cell.messageContentView.frame = rect;
    cell.avatarImageView.frame = rectAvatar;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self messageSizeForIndexPath:indexPath].height + (indexPath.row == 0 ? 54 : 40);
}
- (BOOL)isContainsEmoji:(NSString *)string {
    
    __block BOOL isEomji = NO;
    [string enumerateSubstringsInRange:NSMakeRange(0, [string length]) options:NSStringEnumerationByComposedCharacterSequences usingBlock:
     ^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
         const unichar hs = [substring characterAtIndex:0];
         if (0xd800 <= hs && hs <= 0xdbff) {
             if (substring.length > 1) {
                 const unichar ls = [substring characterAtIndex:1];
                 const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                 if (0x1d000 <= uc && uc <= 0x1f77f) {
                     isEomji = YES;
                 }
             }
         } else if (substring.length > 1) {
             const unichar ls = [substring characterAtIndex:1];
             if (ls == 0x20e3) {
                 isEomji = YES;
             }
         } else {
             if (0x2100 <= hs && hs <= 0x27ff && hs != 0x263b) {
                 isEomji = YES;
             } else if (0x2B05 <= hs && hs <= 0x2b07) {
                 isEomji = YES;
             } else if (0x2934 <= hs && hs <= 0x2935) {
                 isEomji = YES;
             } else if (0x3297 <= hs && hs <= 0x3299) {
                 isEomji = YES;
             } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50|| hs == 0x231a ) {
                 isEomji = YES;
             }
         }
     }];
    return isEomji;
}

@end
