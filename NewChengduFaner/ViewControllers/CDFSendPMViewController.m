//
//  CDFSendPMViewController.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-12-31.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFSendPMViewController.h"

@interface CDFSendPMViewController ()
@property (strong, nonatomic) IBOutlet UITextView *textView;

@end

@implementation CDFSendPMViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (IBAction)sendPM:(id)sender {
    if ([self.textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0) {
        [SVProgressHUD showErrorWithStatus:@"还是说两句吧..."];
        return;
    }
    NSDictionary *postData = @{@"message": self.textView.text};
    NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=sendpm&pmid=0&handlekey=pmsend&pmsubmit=true&formhash=%@&touid=%@",kApiVersion,[CDFMe shareInstance].formHash,self.touid];
    
    [[HXHttpClient shareInstance] postURL:url postData:postData success:^(id responseObject) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kMESSAGE_BADGE object:nil userInfo:responseObject[@"Variables"][@"newmessage2"]];
//        NSLog(@"%@",[Common dictionaryToJSON:responseObject]);
        if ([responseObject[@"Message"][@"messageval"] isEqualToString:@"do_success"]) {
            [SVProgressHUD showSuccessWithStatus:responseObject[@"Message"][@"messagestr"]];
            [self dismissModalViewControllerAnimated:YES];
        } else {
            [SVProgressHUD showErrorWithStatus:responseObject[@"Message"][@"messagestr"]];
        }
    } fail:^(NSError *error) {
//        NSLog(@"%@",error);
        [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
    }];
    
}

#pragma mark - Super Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    if ([self.navigationController.viewControllers[0] isEqual:self]) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStyleBordered target:self action:@selector(dismissModalViewControllerAnimated:)];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"发送私信"];
    [self.textView becomeFirstResponder];
    if ([Common systemVersion] >= 7.0) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
        [self setNeedsStatusBarAppearanceUpdate];
        
    }
    if ([Common systemVersion] < 7.0) {
        self.navigationItem.rightBarButtonItem.tintColor = self.navigationController.navigationBar.tintColor;
        self.navigationItem.backBarButtonItem.tintColor = self.navigationController.navigationBar.tintColor;
    } else {
        self.navigationController.navigationBar.tintColor = [UIColor colorWithWhite:0 alpha:1];
        self.navigationItem.rightBarButtonItem.tintColor = self.navigationController.navigationBar.tintColor;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"发送私信"];
    [self.textView resignFirstResponder];
    [SVProgressHUD dismiss];
    if ([Common systemVersion] >= 7.0) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque animated:YES];
        [self setNeedsStatusBarAppearanceUpdate];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
