//
//  CDFPMListViewController.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-12-24.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDFAvatarImageView.h"
@interface CDFPMListViewCellContentView : UIView
@end

@interface RoundTextView : UITextView
@property (nonatomic, strong) UILabel *placeHoldLabel;
@end

@interface ReplyContentView : UIView
@end

@interface CDFPMListViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *titleView;
@end

@interface CDFPMListViewController : UITableViewController

@property (nonatomic, strong) id touid;

@end
