//
//  CDFMessageViewController.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-12.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFMessageViewController.h"
#import "CDFSegmentControl.h"
#import "CDFMeViewController.h"

typedef enum {
    NOTICE_ACTION_NOTE,
    NOTICE_ACTION_MESSAGE
}NoticeAction;

@implementation CDFMessageCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setup];
        
    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];
}

- (void)setup
{
    UIView *bgv = [[UIView alloc] initWithFrame:self.bounds];
    bgv.backgroundColor = [UIColor whiteColor];
    self.backgroundView = bgv;
    
    UIView *sbgv = [[UIView alloc] initWithFrame:self.bounds];
    sbgv.backgroundColor = [UIColor colorWithWhite:230.0f/255.0f alpha:1];
    self.selectedBackgroundView = sbgv;
}

- (void)setHighlighted:(BOOL)highlighted
{
    [self setSelected:highlighted];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [self setSelected:highlighted animated:animated];
}

@end

@interface CDFMessageViewController ()<UITableViewDataSource, UITableViewDelegate, DTAttributedTextContentViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet CDFSegmentControl *navSegment;
@property (strong, nonatomic) NSMutableArray *dataSource;
@property (strong, nonatomic) id refreshHeaderView;
@property (strong, nonatomic) NSCache *noteCache;
@end

@implementation CDFMessageViewController {
    NSInteger       _currentPage;
    BOOL            _isLoading;
    BOOL            _hasMore;
    NoticeAction    _action;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)refreshHeaderView
{
    if (!_refreshHeaderView) {
        if ([Common systemVersion] < 6.0) {
        } else {
            UIRefreshControl *refresh = [[UIRefreshControl alloc] init];
            refresh.tintColor = [UIColor blackColor];
            refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"用力，不要停..."];
            [refresh addTarget:self
                        action:@selector(refreshView:)
              forControlEvents:UIControlEventValueChanged];
            _refreshHeaderView = refresh;
        }
    }
    return _refreshHeaderView;
}

- (NSCache *)noteCache
{
    if (!_noteCache) {
        _noteCache = [NSCache new];
    }
    return _noteCache;
}

- (NSMutableArray *)dataSource
{
    if (!_dataSource) {
        _dataSource = [NSMutableArray new];
    }
    return _dataSource;
}

- (void)segmentChanged:(CDFSegmentControl *)segment
{
    [MobClick event:@"ChangeMessageType" label:segment.items[segment.activeIndex]];
    _action = segment.activeIndex;
    _currentPage = 1;
    self.title = segment.items[_action];
    [self loadData];
}

/**
 *  刷新控件的逻辑方法
 *
 *  @param refresh 刷新控件
 */
- (void)refreshView:(UIRefreshControl *)refresh
{
    //刷新的逻辑代码
    if (!_isLoading) {
        refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"我正在很用力的加载..."];
        _currentPage = 1;
        [self loadData];
    } else {
        return;
    }
}

/**
 *  刷新完成以后调用
 *
 *  @param refresh 刷新控件
 *  @param message 显示的消息，如果没有特殊消息显示则会显示默认消息
 */
- (void)refreshView:(UIRefreshControl *)refresh endRefreshWithMessage:(NSString *)message
{
    [self performSelector:@selector(resetLoadFlag) withObject:nil afterDelay:3];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM.dd, hh:mm a"];
    NSString *lastUpdated = [NSString stringWithFormat:@"%@ %@", [formatter stringFromDate:[NSDate date]], (message.length > 0 ? message : @"刷新成功")];
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:lastUpdated];
    [refresh endRefreshing];
}

- (void)resetLoadFlag {
    _isLoading = NO;
}

- (void)loadData {
    if (_isLoading) {
        return;
    }
    
    if (![CDFMe shareInstance].isLogin) {
        
        [self refreshView:_refreshHeaderView endRefreshWithMessage:@"尚未登录"];
        UIStoryboard *user = [UIStoryboard storyboardWithName:@"welcome" bundle:nil];
        [self presentModalViewController:user.instantiateInitialViewController animated:YES];
        return;
    }
    
    _isLoading = YES;
    [SVProgressHUD showWithStatus:@"请稍后..." maskType:SVProgressHUDMaskTypeGradient];
#pragma mark - 根据self.type区分url
    //api/mobileversion=3&module=mynoticefollower消息列表api/mobileversion=3&module=mynotice&noticetype=followerlike消息列表
    //api/mobileversion=3&module=mynotice&noticetype=like
    NSString *url;
    if (self.type == newListen) {//新听众
        url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=%@&noticetype=follower",kApiVersion,(_action == NOTICE_ACTION_NOTE ? @"mynotice" : @"mypm") ];

    }else if (self.type == like){//喜欢
        url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=%@&noticetype=like",kApiVersion,(_action == NOTICE_ACTION_NOTE ? @"mynotice" : @"mypm") ];

    }else{//帖子回复
        url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=%@&page=%d",kApiVersion,(_action == NOTICE_ACTION_NOTE ? @"mynotice" : @"mypm") ,_currentPage];
    }
//    NSLog(@"%@%@", kBaseURL, url);
    [[HXHttpClient shareInstance] grabURL:url success:^(id responseObject) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kMESSAGE_BADGE object:nil userInfo:responseObject[@"Variables"][@"newmessage2"]];
//        NSLog(@"%@",responseObject);
        _isLoading = NO;
        if ([responseObject[@"Message"][@"messageval"] isEqualToString:@"login_before_enter_home"]) {
            
            [SVProgressHUD showErrorWithStatus:responseObject[@"Message"][@"messagestr"]];
            return;
        }
        [SVProgressHUD dismiss];
        
        if (_currentPage <= 1) {
            [self.dataSource removeAllObjects];
            [self.noteCache removeAllObjects];
        }
        
        [self.dataSource addObjectsFromArray:responseObject[@"Variables"][@"list"]];
        
        _hasMore = self.dataSource.count < ((NSNull *)responseObject[@"Variables"][@"count"] == [NSNull null] ? NO : [responseObject[@"Variables"][@"count"] integerValue]);
        
        [self.tableView reloadData];
        
        if (self.dataSource.count <= 0) {
            
            UIImageView *iv = [[UIImageView alloc] initWithFrame:self.tableView.bounds];
            iv.image = [UIImage imageNamed:@"bg_emptypm"];
            iv.contentMode = UIViewContentModeCenter;
            self.tableView.tableFooterView = iv;
            
        } else {
            
            self.tableView.tableFooterView = nil;
        }
        if (_currentPage <= 1 && self.dataSource.count > 0) {
            
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
        }
        
        [self refreshView:_refreshHeaderView endRefreshWithMessage:@"刷新成功"];
    } fail:^(NSError *error) {
        
        _isLoading = NO;
        [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
        [self refreshView:_refreshHeaderView endRefreshWithMessage:@"刷新失败"];
    }];
}

- (void)handleLogNotification:(NSNotification *)notification
{
    if ([notification.name isEqualToString: kLOGOUT_NOTIFICATION]) {
        //退出登录
        _currentPage = 1;
        _hasMore = NO;

    }
    
    if ([notification.name isEqualToString:kLOGIN_NOTIFICATION]) {
        //登录
        _currentPage = 1;
        _hasMore = NO;
        [self loadData];
    }
}

- (void)handleMessageChange:(NSNotification *)note
{
    

    NSInteger pm = [[[note userInfo] objectForKey:@"pmcount"] integerValue];
    NSInteger nt = [[[note userInfo] objectForKey:@"notice"] integerValue];
    if (pm > 0) {
        [self.navSegment setBadgeValue:[NSString stringWithFormat:@"%d", pm] atIndex:1];
    } else {
        [self.navSegment setBadgeValue:nil atIndex:1];
    }
    if (nt > 0) {
        [self.navSegment setBadgeValue:[NSString stringWithFormat:@"%d", nt] atIndex:0];
    } else {
        [self.navSegment setBadgeValue:nil atIndex:0];
    }
}

- (void)addNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLogNotification:) name:kLOGIN_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLogNotification:) name:kLOGOUT_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleMessageChange:) name:kMESSAGE_BADGE object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(networkDidReceiveMessage:) name:kJPFNetworkDidReceiveMessageNotification object:nil];
}

- (void)networkDidReceiveMessage:(NSNotification *)notification {
    [self loadData];
}

- (void)removeNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLOGIN_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLOGOUT_NOTIFICATION object:nil];
}

#pragma mark UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //下拉加载更多
    if (scrollView.contentSize.height - scrollView.contentOffset.y < CGRectGetHeight(scrollView.frame) + 50) {
        if (_hasMore && !_isLoading) {
            _currentPage++;
            [self loadData];
        }
    }
}

#pragma mark UIViewController Super Methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addNotification];

	_currentPage = 1;
    if ([Common systemVersion] < 7.0) {
        self.navigationItem.leftBarButtonItem.tintColor = self.navigationController.navigationBar.tintColor;
        self.navigationItem.rightBarButtonItem.tintColor = self.navigationController.navigationBar.tintColor;
        
        UIView *bg = [[UIView alloc] initWithFrame:self.tableView.bounds];
        bg.backgroundColor = bgColor;
        self.tableView.backgroundView = bg;
    } else {
        self.tableView.backgroundColor = bgColor;
        self.view.backgroundColor = bgColor;
    }
    
    self.navSegment.items = @[@"消息",@"私信"];
    [self.navSegment addTarget:self action:@selector(segmentChanged:) forControlEvents:UIControlEventValueChanged];
    
    self.tableView.rowHeight = 65;
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.tableFooterView = footerView;
    
    // Create a UITableViewController so we can use a UIRefreshControl.
    UITableViewController *tvc = [[UITableViewController alloc] initWithStyle:self.tableView.style];
    tvc.tableView = self.tableView;
    if ([tvc respondsToSelector:@selector(setRefreshControl:)]) {
        tvc.refreshControl = self.refreshHeaderView;
    }
    [self addChildViewController:tvc];
    
    [self loadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"消息"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"消息"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIndentier = [NSString stringWithFormat:@"Cell%d",self.navSegment.activeIndex];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIndentier];
    }
    
    if (self.navSegment.activeIndex == 0) {
        UILabel *label = (UILabel *)[cell.contentView viewWithTag:1];
//        NSLog(@"info:%@", self.dataSource[indexPath.row]);
        if ([self.dataSource[indexPath.row][@"authorid"] isEqualToString:@"0"]) {
            label.text = @"管理员操作";
        } else {
            label.text = [NSString stringWithFormat:@"%@",self.dataSource[indexPath.row][@"author"]];
        }
        label = (UILabel *)[cell.contentView viewWithTag:2];
        label.text = [Common formatDate:[self.dataSource[indexPath.row][@"dateline"] longLongValue]];
        DTAttributedLabel *attrLabel = (DTAttributedLabel *)[cell.contentView viewWithTag:3];
        attrLabel.attributedString = [self formatHTMLString:self.dataSource[indexPath.row][@"note"] atIndexPath:indexPath];
        //NSLog(@"%@",attrLabel.attributedString);
        attrLabel.delegate = self;
        
        UIImageView *avatarImageView = (UIImageView *)[cell.contentView viewWithTag:4];
        avatarImageView.layer.masksToBounds = YES;
        avatarImageView.layer.cornerRadius = 20;
        [avatarImageView setImageWithURL:[NSURL URLWithString:[Common formatAvatarWithUID:self.dataSource[indexPath.row][@"authorid"] type:@"normal"]]];
        ((CDFAvatarImageView *)avatarImageView).uid =self.dataSource[indexPath.row][@"authorid"];
        
        UIImageView *icon = (UIImageView *)[cell.contentView viewWithTag:5];
        icon.image = [UIImage imageNamed:self.dataSource[indexPath.row][@"type"]];
    } else {
        
        
        UILabel *lable = (UILabel *)[cell.contentView viewWithTag:1];
        lable.text = [NSString stringWithFormat:@"%@",self.dataSource[indexPath.row][@"tousername"]];
        
        lable = (UILabel *)[cell.contentView viewWithTag:2];
        lable.text = [Common formatDate:[self.dataSource[indexPath.row][@"lastdateline"] longLongValue]];
        lable = (UILabel *)[cell.contentView viewWithTag:3];
        lable.text = [NSString stringWithFormat:@"%@",self.dataSource[indexPath.row][@"lastsummary"]];
//        NSLog(@"%@",self.dataSource);

        UIImageView *avatarImageView = (UIImageView *)[cell.contentView viewWithTag:4];
        [avatarImageView setImageWithURL:[NSURL URLWithString:[Common formatAvatarWithUID:self.dataSource[indexPath.row][@"touid"] type:@"normal"]]];
    }
    CDFAvatarImageView *avatarImageView = (CDFAvatarImageView *)[cell.contentView viewWithTag:4];

    [avatarImageView addTarget:self action:@selector(headerImageClick:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
    
}
- (void)headerImageClick:(CDFAvatarImageView *)sender{

//    NSLog(@"%@",sender.uid);
    
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CDFMeViewController *vc = [s instantiateViewControllerWithIdentifier:@"userViewController"];
    vc.uid = sender.uid;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSLog(@"self.dataSource:%@", self.dataSource);
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (_action == NOTICE_ACTION_NOTE) {
        
        NSDictionary *dic;
        if ([self.dataSource[indexPath.row][@"from_idtype"] isEqualToString:@"following"]) {
            
            dic = @{@"type": @"user",@"uid":self.dataSource[indexPath.row][@"from_id"]};
//            NSLog(@"senderDic:%@", dic);
            [self performSegueWithIdentifier:@"pushToMe" sender:dic];
            return;
        } else {

            id tid = self.dataSource[indexPath.row][@"tid"];
            id pid = self.dataSource[indexPath.row][@"pid"];
            dic = @{@"type": @"post",@"tid": tid ? tid : self.dataSource[indexPath.row][@"from_id"],@"pid":pid ? pid : @"0"};
//            NSLog(@"senderDic2:%@", dic);
            [self performSegueWithIdentifier:@"pushToDetail" sender:dic];
            return;
        }
    }
}

#pragma mark - DTAttributedLabel
- (NSAttributedString *)formatHTMLString:(NSString *)html atIndexPath:(NSIndexPath *)indexPath
{
	if (html == nil) {
		return nil;
	}
    
    NSString *key = [NSString stringWithFormat:@"%d-%d",indexPath.section, indexPath.row];
    NSAttributedString *attrStr = [self.noteCache objectForKey:key];
    if (attrStr) {
        return attrStr;
    }
//    NSLog(@"%@",html);
    NSData *data = [html dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *options = [NSMutableDictionary dictionaryWithObjectsAndKeys:
									[NSValue valueWithCGSize:CGSizeMake(306, 2000)], DTMaxImageSize,
									[NSNumber numberWithFloat:1.5], DTDefaultLineHeightMultiplier,
									[NSNumber numberWithFloat:12], DTDefaultFontSize,
									//@"Arial", DTDefaultFontFamily,
									@"Gray", DTDefaultLinkColor,
									@"none", DTDefaultLinkDecoration,
									nil];
//    NSLog(@"%@",options);
	NSAttributedString *string = [[NSAttributedString alloc] initWithHTMLData:data options:options documentAttributes:NULL];
//    NSLog(@"%@",string);
    [self.noteCache setObject:string forKey:key];

	return string;
}

- (UIView *)attributedTextContentView:(DTAttributedTextContentView *)attributedTextContentView viewForAttributedString:(NSAttributedString *)string frame:(CGRect)frame
{

    NSDictionary *attributes = [string attributesAtIndex:0 effectiveRange:NULL];
	
	NSURL *URL = [attributes objectForKey:DTLinkAttribute];
	NSString *identifier = [attributes objectForKey:DTGUIDAttribute];
	
	DTLinkButton *button = [[DTLinkButton alloc] initWithFrame:frame];
	button.URL = URL;
	button.minimumHitSize = CGSizeMake(25, 25); // adjusts it's bounds so that button is always large enough
	button.GUID = identifier;
	
	// get image with normal link text
	UIImage *normalImage = [attributedTextContentView contentImageWithBounds:frame options:DTCoreTextLayoutFrameDrawingDefault];
	[button setImage:normalImage forState:UIControlStateNormal];
	
	// get image for highlighted link text
	UIImage *highlightImage = [attributedTextContentView contentImageWithBounds:frame options:DTCoreTextLayoutFrameDrawingDrawLinksHighlighted];
	[button setImage:highlightImage forState:UIControlStateHighlighted];
	
	// use normal push action for opening URL
	[button addTarget:self action:@selector(linkPushed:) forControlEvents:UIControlEventTouchUpInside];
	
	return button;
}

- (void)linkPushed:(DTLinkButton *)button
{
	NSURL *URL = button.URL;
	NSDictionary *dic = [Common decodeURL:URL.absoluteString];
//    NSLog(@"diccc:%@", dic);
//    NSLog(@"%@", URL);
    if (dic) {
        if ([dic[@"type"] isEqualToString:@"post"]) {
            [self performSegueWithIdentifier:@"pushToDetail" sender:dic];
            return;
        }
        if ([dic[@"type"] isEqualToString:@"user"]) {
            [self performSegueWithIdentifier:@"pushToMe" sender:dic];
            return;
        }
    }
	if ([[UIApplication sharedApplication] canOpenURL:[URL absoluteURL]])
	{
		//[[UIApplication sharedApplication] openURL:[URL absoluteURL]];
	}
	else
	{
		if (![URL host] && ![URL path])
		{
			// possibly a local anchor link
			NSString *fragment = [URL fragment];
			
			if (fragment)
			{
				//[_textView scrollToAnchorNamed:fragment animated:NO];
			}
		}
	}
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController respondsToSelector:@selector(touid)] && [sender isKindOfClass:[UITableViewCell class]]) {
        [MobClick event:@"SendPMFromMessage"];
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        [segue.destinationViewController setValue:self.dataSource[indexPath.row][@"touid"] forKey:@"touid"];
        [segue.destinationViewController setTitle:self.dataSource[indexPath.row][@"tousername"]];
    }
    if ([sender isKindOfClass:[NSDictionary class]]) {
        if ([sender[@"type"] isEqualToString:@"user"]) {
            
            if ([segue.destinationViewController respondsToSelector:@selector(uid)]) {
                [segue.destinationViewController setValue:sender[@"uid"] forKey:@"uid"];
            }
        }
        if ([sender[@"type"] isEqualToString:@"post"]) {
            
            if ([segue.destinationViewController respondsToSelector:@selector(tid)]) {
                [segue.destinationViewController setValue:sender[@"tid"] forKey:@"tid"];
            }
            
            if ([segue.destinationViewController respondsToSelector:@selector(pid)]) {
                [segue.destinationViewController setValue:sender[@"pid"] forKey:@"pid"];
            }
        }
    }
}



@end
