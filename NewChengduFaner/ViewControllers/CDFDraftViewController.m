//
//  CDFDraftViewController.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-29.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFDraftViewController.h"
#import "CDFDraft.h"

@implementation CDFDraftCell
@end

@interface CDFDraftViewController ()
@property (nonatomic, strong) NSMutableArray *dataSource;
@end

@implementation CDFDraftViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (NSMutableArray *)dataSource
{
    if (!_dataSource) {
        _dataSource = [NSMutableArray arrayWithArray:[CDFDraft allDrafts]];
    }
    return _dataSource;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
     self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
     self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    if ([Common systemVersion] >= 7.0) {
        //self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    }
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 30)];
    label.text = @"草稿箱";
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = TITLEGRAY;
    self.navigationItem.titleView = label;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"草稿箱"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"草稿箱"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([sender isKindOfClass:[UITableViewCell class]]) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        NSString *key = self.dataSource[indexPath.row][@"key"];
        if ([segue.destinationViewController respondsToSelector:@selector(setKey:)]) {
            [segue.destinationViewController setValue:key forKey:@"key"];
        }
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (CDFDraftCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    CDFDraftCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    NSLog(@"%@",self.dataSource[indexPath.row]);
    CDFDraft *draft = [[CDFDraft alloc] initWithDictionary:self.dataSource[indexPath.row]];
    cell.titleLabel.text = [NSString stringWithFormat:@"%@",draft.subject.length <= 0 ? @"(未标题)" : draft.subject];
    cell.summaryLabel.text = [NSString stringWithFormat:@"%@",draft.message];
    if (draft.attachements.count > 0) {
        cell.imageNumberLabel.text = [NSString stringWithFormat:@"总共 %d 张图片", draft.attachements.count];
        cell.imageNumberLabel.hidden = NO;
    } else {
        cell.imageNumberLabel.hidden = YES;
    }
    NSDateFormatter *df = [NSDateFormatter new];
    df.locale = [NSLocale currentLocale];
    df.formatterBehavior = NSDateFormatterBehavior10_4;
    df.dateFormat = @"yyyy-MM-dd hh:mm:ss";
    cell.dateLabel.text = [df stringFromDate:draft.timeline];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        CDFDraft *draft = [[CDFDraft alloc] initWithDictionary:self.dataSource[indexPath.row]];
        [draft destroy];
        [self.dataSource removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [[NSNotificationCenter defaultCenter] postNotificationName:kDRAFTS_CHANGED object:nil];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
