//
//  CDFForumViewController.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-22.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BoardCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *button;
@end

@interface CDFForumViewController : UITableViewController

@property (nonatomic, strong) NSArray *dataSource;
@property (nonatomic, strong) NSDictionary *selectedForum;

@end
