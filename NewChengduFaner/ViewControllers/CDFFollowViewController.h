//
//  CDFFollowViewController.h
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-12-30.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDFFollowViewController : UITableViewController

@property (nonatomic, strong) id uid;
@property (nonatomic, strong) NSString *filter;

@end
