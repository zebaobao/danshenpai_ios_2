//
//  CDFInfoModifyViewController.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 14-1-2.
//  Copyright (c) 2014年 www.huaxi100.com. All rights reserved.
//

#import "CDFInfoModifyViewController.h"

@interface CDFInfoModifyViewController ()<UITextViewDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *userSexLabel;
@property (strong, nonatomic) IBOutlet UITextView *signTextView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *saveButton;
@property (strong, nonatomic) IBOutlet UIButton *changeAvatarButton;

@end

@implementation CDFInfoModifyViewController {
    NSInteger gender;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (IBAction)saveModify:(id)sender {
    if (![self.userInfo[@"uid"] isEqual:[CDFMe shareInstance].uid]) {
        return;
    }
    NSString *sig = self.signTextView.text.length <= 0 ? @"" : self.signTextView.text;
    NSDictionary *postData = @{@"gender": [NSNumber numberWithInteger:gender],@"sightml":sig};
    NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=userinfo&formhash=%@&op=base&profilesubmit=true",kApiVersion,[CDFMe shareInstance].formHash];
//    NSLog(@"%@%@",kBaseURL,url);
    [[HXHttpClient shareInstance] postURL:url postData:postData success:^(id responseObject) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kMESSAGE_BADGE object:nil userInfo:responseObject[@"Variables"][@"newmessage2"]];
        if ([responseObject[@"Message"][@"messageval"] isEqualToString:@"editsuccess"]) {
//            [SVProgressHUD showSuccessWithStatus:responseObject[@"Message"][@"messagestr"]];
            [SVProgressHUD showSuccessWithStatus:@"设置成功"];
            NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:self.userInfo];
            [dic setObject:sig forKey:@"sightml"];
            [dic setObject:postData[@"gender"] forKey:@"gender"];
            //NSLog(@"%@",[Common dictionaryToJSON:dic]);
            [[NSNotificationCenter defaultCenter] postNotificationName:kINFO_CHANGED object:nil userInfo:dic];
            if ([[self.navigationController.viewControllers lastObject] isKindOfClass:[CDFInfoModifyViewController class]]) {
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
    } fail:^(NSError *error) {
//        NSLog(@"%@",error);
        [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
    }];
}
- (IBAction)changeSex:(id)sender {
    if ([self.userInfo[@"uid"] isEqual:[CDFMe shareInstance].uid]) {
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"请选择您的性别？" delegate:self cancelButtonTitle:@"让我再想想" destructiveButtonTitle:nil otherButtonTitles:@"男", @"女", nil];
        sheet.tag = 998;
        [sheet showFromTabBar:self.tabBarController.tabBar];
    }
}

- (IBAction)changeAvatar:(id)sender {
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"从相册读取", @"摄像头拍摄",  nil];
    sheet.tag = 999;
    [sheet showFromTabBar:self.tabBarController.tabBar];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.changeAvatarButton.hidden = ![self.userInfo[@"uid"] isEqual:[CDFMe shareInstance].uid];
    if ([Common systemVersion] < 7.0) {
        UIView *bg = [[UIView alloc] initWithFrame:self.tableView.bounds];
        bg.backgroundColor = bgColor;
        self.tableView.backgroundView = bg;
    } else {
        self.view.backgroundColor = bgColor;
    }
    if (self.userInfo) {
//        NSLog(@"%@",self.userInfo);
        [self showAvatar];
        self.userNameLabel.text = [NSString stringWithFormat:@"%@",self.userInfo[@"username"]];
        gender = [self.userInfo[@"gender"] integerValue];
        NSString *sex;
        switch (gender) {
            case 1: {
                sex = @"男";
                break;
            }
            case 2: {
                sex = @"女";
                break;
            }
            default:
                sex = @"你猜";
                break;
        }
        self.userSexLabel.text = sex;
        self.signTextView.text = ([self.userInfo[@"sightml"] length] <= 0 ? nonSign : self.userInfo[@"sightml"]);
        self.signTextView.editable = [self.userInfo[@"uid"] isEqual:[CDFMe shareInstance].uid];
        self.navigationItem.rightBarButtonItem = [self.userInfo[@"uid"] isEqual:[CDFMe shareInstance].uid] ? self.saveButton : nil;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"详细资料"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"详细资料"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UITextView Delegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text compare:@"\n"] == NSOrderedSame) {
        [textView resignFirstResponder];
    }
    return YES;
}

#pragma mark - UITableView Delegate
- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.frame), [self tableView:tableView heightForHeaderInSection:section])];
        header.backgroundColor = bgColor;
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, CGRectGetWidth(header.frame) - 20, CGRectGetHeight(header.frame))];
        label.backgroundColor = [UIColor clearColor];
        label.text = [super tableView:tableView titleForHeaderInSection:section];
        label.font = [UIFont systemFontOfSize:16];
        label.textColor = [UIColor colorWithWhite:51.0f/255.0f alpha:1];
        [header addSubview:label];
        return  header;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        return 50;
    }
    return [super tableView:tableView heightForHeaderInSection:section];
}

#pragma mark - UIActionSheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 998) {
        if (buttonIndex == 0) {
            gender = 1;
            self.userSexLabel.text = @"男";
        } else if (buttonIndex == 1) {
            gender = 2;
            self.userSexLabel.text = @"女";
        }
    } else if (actionSheet.tag == 999) {
        if (buttonIndex == 2) {
            return;
        }
        UIImagePickerController *imgPicker = [[UIImagePickerController alloc] init];
        imgPicker.delegate = self;
        imgPicker.allowsEditing = YES;
        if (buttonIndex == 0) {
            imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentModalViewController:imgPicker animated:YES];

        } else if (buttonIndex == 1) {
#if !TARGET_IPHONE_SIMULATOR
            imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
#endif
        }
    }
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissModalViewControllerAnimated:YES];
    
    [self handleImage:info];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissModalViewControllerAnimated:YES];
}

- (void)handleImage:(NSDictionary *)info
{
    [SVProgressHUD showWithStatus:@"上传头像中..." maskType:SVProgressHUDMaskTypeGradient];
    [[HXHttpClient shareInstance] grabURL:[NSString stringWithFormat:@"api/mobile/?version=%@&module=setavatar", kApiVersion] success:^(id responseObject) {
//        NSLog(@"%@",responseObject);
        if (responseObject[@"Variables"][@"avatar_param"]) {
            NSString *appid = responseObject[@"Variables"][@"avatar_param"][@"appid"];
            NSString *input = responseObject[@"Variables"][@"avatar_param"][@"input"];
            UIImage *image = info[UIImagePickerControllerEditedImage];
            UIImage *big = [Common resizeImage:image fitSize:CGSizeMake(200, 200)];
            UIImage *middle = [Common resizeImage:big fitSize:CGSizeMake(120, 120)];
            UIImage *small = [Common resizeImage:middle fitSize:CGSizeMake(48, 48)];
            
            NSDictionary *postData = @{@"appid": appid, @"input": input, @"big": big, @"middle": middle, @"small": small};
            
            [self uploadAvatar:postData];
        } else {
            [SVProgressHUD dismiss];
        }
    } fail:^(NSError *error) {
//        NSLog(@"%@",error);
        [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
    }];
}

- (void)uploadAvatar:(NSDictionary *)postData
{
//    NSLog(@"%@",postData);
    NSDictionary *sendDictionary = @{@"appid": postData[@"appid"],@"input": postData[@"input"]};
    
    [Common appendDefaultCookiesForRequest];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:kBaseURL]];
    NSMutableURLRequest *afRequest = [httpClient multipartFormRequestWithMethod:@"POST"
                                                                           path:@"uc_server/index.php?m=user&inajax=1&a=rectavatarcd&avatartype=virtual"
                                                                     parameters:sendDictionary
                                                      constructingBodyWithBlock:^(id <AFMultipartFormData>formData)
                                      {
                                          [formData appendPartWithFileData:UIImageJPEGRepresentation(postData[@"big"], .8)
                                                                      name:@"big"
                                                                  fileName:@"big"
                                                                  mimeType:@"image/jpeg"];
                                          [formData appendPartWithFileData:UIImageJPEGRepresentation(postData[@"middle"], .8)
                                                                      name:@"middle"
                                                                  fileName:@"middle"
                                                                  mimeType:@"image/jpeg"];
                                          [formData appendPartWithFileData:UIImageJPEGRepresentation(postData[@"small"], .8)
                                                                      name:@"small"
                                                                  fileName:@"small"
                                                                  mimeType:@"image/jpeg"];
                                      }];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:afRequest];
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten,  long long totalBytesExpectedToWrite) {
        
//        NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
        if (totalBytesWritten == totalBytesExpectedToWrite) {
            [SVProgressHUD dismiss];
            
        }
    }];
    
    [operation setCompletionBlock:^{
        [self showAvatar];
        [[NSNotificationCenter defaultCenter] postNotificationName:kINFO_CHANGED object:nil];
        //NSLog(@"%@", operation.responseData); //Gives a very scary warning
    }];
    
    [operation start];
}

- (void)showAvatar
{
    NSURL *url = [NSURL URLWithString:[Common formatAvatarWithUID:self.userInfo[@"uid"] type:@"middle"]];
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
    request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    [self.avatarImageView setImageWithURLRequest:request
                                placeholderImage:nil
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                             
                                         } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
//                                             NSLog(@"failed loading image: %@", error);
                                         }];
}

@end
