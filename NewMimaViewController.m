//
//  NewMimaViewController.m
//  yanyu
//
//  Created by caiyee on 15/4/24.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "NewMimaViewController.h"
#define kViewY  64
#define kHeightOfView   150
#define kHeightOfSmallView  kHeightOfView / 3.0
#define kLabelToLeft    20
#define KWidthOfLabel   80
#define kHeightToViewTopOfLabel    5
#define kHeightOfLabel      kHeightOfSmallView - 2 * kHeightToViewTopOfLabel
#define kTextFieldToLeft    kLabelToLeft + KWidthOfLabel + KWidthBettownLabelAndTextField
#define KWidthBettownLabelAndTextField  10
#define kWidthOfTextField       180
#define kHeightOfTextField       35
#define kHeightToViewTopOfTextField     kHeightToViewTopOfLabel
#define kBtnToLeft          (KDeviceSizeWidth - kWidthOfBtn) / 2.0
#define kWidthOfBtn         150
#define kHeightOfBtn        50
#define kHeightBettownBtnAndUpsideView    40
#define kBtnY               kViewY + kHeightOfView + kHeightBettownBtnAndUpsideView




@interface NewMimaViewController ()<UITextFieldDelegate, UIAlertViewDelegate>
@property (nonatomic, assign) BOOL isSucceed;
@end

@implementation NewMimaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self layoutSubViews];
//    [self layoutLeftNavigationItem];
    self.isSucceed = NO;
    // Do any additional setup after loading the view.
    
}

- (void)send {
    
    if (self.firstTextField.text.length <= 0) {
        //        [SVProgressHUD showWithStatus:@"请输入旧密码"];
        [SVProgressHUD showErrorWithStatus:@"请输入旧密码" duration:2.0];
        return;
    }
    if (self.secondTextField.text.length <=5) {
        //        [SVProgressHUD showWithStatus:@"新密码位数太短"];
        [SVProgressHUD showErrorWithStatus:@"新密码位数至少6位" duration:2.0];
        return;
    }
    if (![self.thirdTextField.text isEqualToString:self.secondTextField.text]) {
        //        [SVProgressHUD showWithStatus:@"请重新确认密码"];
        [SVProgressHUD showErrorWithStatus:@"两次输入的密码不一致" duration:2.0];
        
        return;
    }
    NSString *mima = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef) self.secondTextField.text,NULL,(CFStringRef) @"!*'();:@&=+$,/?%#[]",kCFStringEncodingUTF8));
    
    NSString *url = [NSString stringWithFormat:@"%@api/mobile/?version=%@&module=password&oldpassword=%@&newpassword=%@", kBaseURL, kApiVersion, self.firstTextField.text, mima];
    [SVProgressHUD showWithStatus:@"请稍等..."];
    [[HXHttpClient shareInstance] grabURL:url success:^(id responseObject) {
//        NSLog(@"%@", responseObject);
        NSString *message = responseObject[@"Variables"][@"message"];
        if ([responseObject[@"Variables"][@"result"] isEqualToString:@"-1"]) {
            self.isSucceed = YES;
        } else {
            self.isSucceed = NO;
        }
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
        alertView.delegate = self;
        [SVProgressHUD dismiss];
        [alertView show];
    } fail:^(NSError *error) {
//        NSLog(@"%@", error);
        [SVProgressHUD dismiss];
    }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (!self.isSucceed) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    } else {
        UITextField *textField = (UITextField *)[self.view viewWithTag:101];
        [textField becomeFirstResponder];
    }
}


- (IBAction)handleSureBtn:(id)sender {
    [self send];
    
}


- (void)layoutSubViews {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, kViewY, KDeviceSizeWidth, kHeightOfView)];
    UIView *firstView  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KDeviceSizeWidth, kHeightOfView / 3.0)];
    UIView *secondView = [[UIView alloc] initWithFrame:CGRectMake(0, firstView.frame.origin.y + firstView.frame.size.height, KDeviceSizeWidth, kHeightOfView / 3.0)];
    UIView *thirdView = [[UIView alloc] initWithFrame:CGRectMake(0, secondView.frame.origin.y + secondView.frame.size.height, KDeviceSizeWidth, kHeightOfView / 3.0)];
    [view addSubview:firstView];
    [view addSubview:secondView];
    [view addSubview:thirdView];
    [self.view addSubview:view];
    
    UILabel *firstLabel = [[UILabel alloc] initWithFrame:CGRectMake(kLabelToLeft, kHeightToViewTopOfLabel, KWidthOfLabel, kHeightOfLabel)];
    self.firstTextField = [[UITextField alloc] initWithFrame:CGRectMake(kTextFieldToLeft, ((kHeightOfView / 3.0) - kHeightOfTextField) / 2.0 , kWidthOfTextField, kHeightOfTextField)];
    _firstTextField.returnKeyType = UIReturnKeyNext;
    self.firstTextField.delegate = self;
    _firstTextField.secureTextEntry = YES;
    _firstTextField.clearsOnBeginEditing = YES;
    _firstTextField.tag = 101;
    self.firstTextField.borderStyle = UITextBorderStyleRoundedRect;
    [firstView addSubview:firstLabel];
    [firstView addSubview:self.firstTextField];
    
    UILabel *secondLabel = [[UILabel alloc] initWithFrame:CGRectMake(kLabelToLeft, kHeightToViewTopOfLabel, KWidthOfLabel, kHeightOfLabel)];
    self.secondTextField = [[UITextField alloc] initWithFrame:CGRectMake(kTextFieldToLeft, ((kHeightOfView / 3.0) - kHeightOfTextField) / 2.0 , kWidthOfTextField, kHeightOfTextField)];
    self.secondTextField.delegate = self;
    _secondTextField.secureTextEntry = YES;
    _secondTextField.clearsOnBeginEditing = YES;
    _secondTextField.returnKeyType = UIReturnKeyNext;
    _secondTextField.tag = 102;
    self.secondTextField.borderStyle = UITextBorderStyleRoundedRect;
    [secondView addSubview:secondLabel];
    [secondView addSubview:self.secondTextField];
    
    UILabel *thirdLabel = [[UILabel alloc] initWithFrame:CGRectMake(kLabelToLeft, kHeightToViewTopOfLabel, KWidthOfLabel, kHeightOfLabel)];
    self.thirdTextField = [[UITextField alloc] initWithFrame:CGRectMake(kTextFieldToLeft, ((kHeightOfView / 3.0) - kHeightOfTextField) / 2.0 , kWidthOfTextField, kHeightOfTextField)];
    self.thirdTextField.delegate = self;
    _thirdTextField.secureTextEntry = YES;
    _thirdTextField.clearsOnBeginEditing = YES;
    _thirdTextField.returnKeyType = UIReturnKeyDone;
    _thirdTextField.tag = 103;
    self.thirdTextField.borderStyle = UITextBorderStyleRoundedRect;
    [thirdView addSubview:thirdLabel];
    [thirdView addSubview:self.thirdTextField];
    
    self.btn = [[UIButton alloc] initWithFrame:CGRectMake(kBtnToLeft, kBtnY, kWidthOfBtn, kHeightOfBtn)];
    self.btn.backgroundColor = [UIColor whiteColor];
    self.btn.layer.masksToBounds = YES;
    self.btn.layer.cornerRadius = 5;
    self.btn.layer.borderWidth = 0.5;
    self.btn.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.btn.titleLabel.text = @"fafasfkalf";
    [self.btn addTarget:self action:@selector(handleSureBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn setTitle:@"确定" forState:UIControlStateNormal];
    [self.view addSubview:self.btn];
    [self.btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

    
    UIView *firstLineView = [[UIView alloc] initWithFrame:CGRectMake(0, secondView.frame.origin.y, KDeviceSizeWidth, 0.5)];
    firstLineView.backgroundColor = [UIColor lightGrayColor];
    UIView *secondLineView = [[UIView alloc] initWithFrame:CGRectMake(0, thirdView.frame.origin.y, KDeviceSizeWidth, 0.5)];
    secondLineView.backgroundColor = [UIColor lightGrayColor];
    
    [view addSubview:firstLineView];
    [view addSubview:secondLineView];
    
    
    view.backgroundColor = [UIColor whiteColor];
//    firstView.backgroundColor = [UIColor orangeColor];
//    secondView.backgroundColor = [UIColor orangeColor];
//    thirdView.backgroundColor = [UIColor clearColor];
//    firstLabel.backgroundColor = [UIColor greenColor];
//    secondLabel.backgroundColor = [UIColor greenColor];
//    thirdLabel.backgroundColor = [UIColor greenColor];
//    self.btn.backgroundColor = [UIColor redColor];

    
    firstLabel.text = @"旧密码";
    secondLabel.text = @"新密码";
    thirdLabel.text = @"确定密码";
    self.view.backgroundColor = [UIColor colorWithRed:208/256.0 green:208/256.0 blue:208/256.0 alpha:1.0];
    
    UIButton *bigBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, KDeviceSizeWidth, KDeviceSizeHeight)];
    [bigBtn addTarget:self action:@selector(handleResign:) forControlEvents:UIControlEventTouchUpInside];
    bigBtn.backgroundColor = [UIColor clearColor];
    [self.view addSubview:bigBtn];
    [self.view sendSubviewToBack:bigBtn];
    
}

- (void)handleResign:(UIButton *)sender {
    [self.view endEditing:YES];
}

//- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
//    [textField resignFirstResponder];
//    return YES;
//}
//- (BOOL)textFieldShouldReturn:(UITextField *)textField {
//    [textField resignFirstResponder];
//    return YES;
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    UITextField *field;

    if (textField.tag == 101) {
        field = (UITextField *)[self.view viewWithTag:102];
        [field becomeFirstResponder];
    } else if (textField.tag == 102) {
        field = (UITextField *)[self.view viewWithTag:103];
        [field becomeFirstResponder];
    } else if (textField.tag == 103) {
        [self send];
    }
    return YES;
}

- (void)layoutLeftNavigationItem {
    UIButton *backBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    [backBtn setImage:[UIImage imageNamed:@"backOrange"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(handleLeftBarBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
}
- (void)handleLeftBarBtn:(UIButton *)btn {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
