//
//  ZWDDisplayLikeCell.h
//  yanyu
//
//  Created by Case on 15/3/25.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZWDDisplayLikeCell : UITableViewCell<DTAttributedTextContentViewDelegate>
- (void)showData:(NSArray *)array;
@property (nonatomic,strong) UINavigationController *navVC;
@property (nonatomic ,copy) NSString *tid;
@end
