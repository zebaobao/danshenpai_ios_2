//
//  CDFWebViewController.h
//  yanyu
//
//  Created by 朱春锋 on 15/2/8.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDFWebViewController : UIViewController
@property (nonatomic, strong) NSString *url;
@end
