//
//  CDFIMViewController.m
//  yanyu
//
//  Created by caiyee on 15/2/28.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "CDFIMViewController.h"
#import "EMChatManagerChatDelegate.h"

@interface CDFIMViewController () <EMChatManagerChatDelegate, IChatManagerDelegate, IDeviceManagerDelegate>

@end

@implementation CDFIMViewController

// 向SDK中注册回调
- (void)registerEaseMobDelegate{
    // 此处先取消一次，是为了保证只将self注册过一次回调。
    [self unRegisterEaseMobDelegate];
    [[EaseMob sharedInstance].chatManager addDelegate:self delegateQueue:nil];
}

// 取消SDK中注册的回调
- (void)unRegisterEaseMobDelegate{
    [[EaseMob sharedInstance].chatManager removeDelegate:self];
}
//-(void)registerNotifications
//{
//    [self unregisterNotifications];
//    
//    [[EaseMob sharedInstance].chatManager addDelegate:self delegateQueue:nil];
////    [[EMSDKFull sharedInstance].callManager addDelegate:self delegateQueue:nil];
//}
//
//-(void)unregisterNotifications
//{
//    [[EaseMob sharedInstance].chatManager removeDelegate:self];
////    [[EMSDKFull sharedInstance].callManager removeDelegate:self];
//}

- (void)viewDidLoad {
    
#warning 以下三行代码必须写，注册为SDK的ChatManager的delegate
    [[[EaseMob sharedInstance] deviceManager] addDelegate:self onQueue:nil];
    [[EaseMob sharedInstance].chatManager removeDelegate:self];
    //注册为SDK的ChatManager的delegate
    [[EaseMob sharedInstance].chatManager addDelegate:self delegateQueue:nil];
    
//    [self registerNotifications];
    [self registerEaseMobDelegate];

    [super viewDidLoad];
    [self sendMessage];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)sendMessage {
//    EMChatText *txtChat = [[EMChatText alloc] initWithText:@"要发送的消息"];
//    EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithChatObject:txtChat];
//    
//    // 生成message
//    EMMessage *message = [[EMMessage alloc] initWithReceiver:@"bot" bodies:@[body]];
//    message.isGroup = NO; // 设置是否是群聊
//    // 发送消息
//    EMMessage *message2 =  [[EaseMob sharedInstance].chatManager asyncSendMessage:message progress:nil];
//    NSLog(@"message:%@", message2);

}

-(void)didSendMessage:(EMMessage *)message error:(EMError *)error {
    
}


-(void)didReceiveMessage:(EMMessage *)message {
    
}

@end
