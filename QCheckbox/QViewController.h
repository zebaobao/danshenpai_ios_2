//
//  QViewController.h
//  QCheckBoxDemo
//
//  Created by ivan on 13-7-18.
//  Copyright (c) 2013年 ivan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QCheckBox.h"

@interface QViewController : UIViewController <QCheckBoxDelegate>
@property (strong, nonatomic) id tid;
@property (strong, nonatomic) id uid;
@property (strong, nonatomic) id fid;
@property (strong, nonatomic) id pid;
@end
