//
//  QViewController.m
//  QCheckBoxDemo
//
//  Created by ivan on 13-7-18.
//  Copyright (c) 2013年 ivan. All rights reserved.
//

#import "QViewController.h"

@interface QViewController ()
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *yinhuiseqing;
@property (nonatomic, copy) NSString *baoliyinsu;
@property (nonatomic, copy) NSString *lajiguanggao;
@property (nonatomic, copy) NSString *neirongchaoxi;
@end

@implementation QViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.yinhuiseqing = @"";
    self.baoliyinsu = @"";
    self.lajiguanggao = @"";
    self.neirongchaoxi = @"";
    self.view.backgroundColor = [UIColor lightGrayColor];
	// Do any additional setup after loading the view, typically from a nib.
    
    QCheckBox *_check1 = [[QCheckBox alloc] initWithDelegate:self];
    _check1.frame = CGRectMake(30, 100, 80, 40);
    [_check1 setTitle:@"淫秽色情" forState:UIControlStateNormal];
    [_check1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_check1.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
    [self.view addSubview:_check1];
    [_check1 setChecked:NO];
    [_check1 release];
    
    QCheckBox *_check2 = [[QCheckBox alloc] initWithDelegate:self];
    _check2.frame = CGRectMake(30, 200, 80, 40);
    [_check2 setTitle:@"垃圾广告" forState:UIControlStateNormal];
    [_check2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_check2.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
    [self.view addSubview:_check2];
    [_check2 release];
    
    QCheckBox *_check3 = [[QCheckBox alloc] initWithDelegate:self];
    _check3.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 30 - 80, 100, 80, 40);
    [_check3 setTitle:@"暴力因素" forState:UIControlStateNormal];
    [_check3 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    [_check3 setTitleColor:[UIColor greenColor] forState:UIControlStateHighlighted];
//    [_check3 setTitleColor:[UIColor blueColor] forState:UIControlStateSelected];
    [_check3.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
//    [_check3 setImage:[UIImage imageNamed:@"uncheck_icon.png"] forState:UIControlStateNormal];
//    [_check3 setImage:[UIImage imageNamed:@"check_icon.png"] forState:UIControlStateSelected];
    [self.view addSubview:_check3];
    [_check3 setChecked:NO];
    [_check3 release];
    
    QCheckBox *_check4 = [[QCheckBox alloc] initWithDelegate:self];
    _check4.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 30 - 80, 200, 80, 40);
    [_check4 setTitle:@"内容抄袭" forState:UIControlStateNormal];
    [_check4 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    [_check4 setTitleColor:[UIColor greenColor] forState:UIControlStateHighlighted];
//    [_check4 setTitleColor:[UIColor blueColor] forState:UIControlStateSelected];
    [_check4.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
//    [_check4 setImage:[UIImage imageNamed:@"uncheck_icon.png"] forState:UIControlStateNormal];
//    [_check4 setImage:[UIImage imageNamed:@"check_icon.png"] forState:UIControlStateSelected];
    [self.view addSubview:_check4];
    [_check4 setChecked:NO];
    [_check4 release];
    
    UIButton *sureBtn = [[UIButton alloc] initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width - 100) / 2, 300, 100, 30)];
    [sureBtn setTitle:@"提交" forState:UIControlStateNormal];
    [sureBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [sureBtn addTarget:self action:@selector(handleSureToCommit:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:sureBtn];
    [sureBtn release];
}
- (void)handleSureToCommit:(UIButton *)sender {
    self.content = [NSString stringWithFormat:@"%@ %@ %@ %@", self.yinhuiseqing, self.baoliyinsu, self.lajiguanggao, self.neirongchaoxi];
//    NSLog(@"%@", self.content);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"感谢您的举报信息" message:@"我们将尽快处理您的举报信息,谢谢您对眼遇的支持!" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"请重新提交" message:@"至少选择一项举报信息进行提交!" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];

    if ([NSString stringWithFormat:@"%@%@%@%@", self.yinhuiseqing, self.baoliyinsu, self.lajiguanggao, self.neirongchaoxi].length >0) {
        [alert show];

    }else {
        [alert1 show];
    }
    

}

#pragma mark - QCheckBoxDelegate

- (void)didSelectedCheckBox:(QCheckBox *)checkbox checked:(BOOL)checked {
//    NSLog(@"did tap on CheckBox:%@ checked:%d", checkbox.titleLabel.text, checked);
    if ([checkbox.titleLabel.text isEqualToString:@"淫秽色情"]) {
        if (checked) {
            self.yinhuiseqing = @"淫秽色情";
        } else {
            self.yinhuiseqing = @"";
        }
        return;
    }
    if ([checkbox.titleLabel.text isEqualToString:@"暴力因素"]) {
        if (checked) {
            self.baoliyinsu = @"暴力因素";
        } else {
            self.baoliyinsu = @"";
        }
        return;
    }
    if ([checkbox.titleLabel.text isEqualToString:@"垃圾广告"]) {
        if (checked) {
            self.lajiguanggao = @"垃圾广告";
        } else {
            self.lajiguanggao = @"";
        }
        return;
    }
    if ([checkbox.titleLabel.text isEqualToString:@"内容抄袭"]) {
        if (checked) {
            self.neirongchaoxi = @"内容抄袭";
        } else {
            self.neirongchaoxi = @"";
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
