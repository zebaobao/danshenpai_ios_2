//
//  CDFLocationManager.h
//  yanyu
//
//  Created by caiyee on 15/2/16.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface CDFLocationManager : NSObject  <MKMapViewDelegate, CLLocationManagerDelegate> {
    CLLocationManager *locationManager;
    NSString *locationURL;

}

@property (nonatomic, strong) NSString *locationInfo;
@property (nonatomic, assign) BOOL isAllowLocation; //是否允许定位
+ (CDFLocationManager *)sharedInstance;
- (NSString *)locationURL;
@end
