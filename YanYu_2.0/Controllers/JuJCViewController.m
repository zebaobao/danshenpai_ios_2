//
//  JuJCViewController.m
//  yanyu
//
//  Created by Case on 15/4/10.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//
//页面层次：底层为一个scrollView，上层展示viewController的View部分

#import "JuJCViewController.h"
#import "ZWDSearchViewController.h"

@interface JuJCViewController ()<UIScrollViewDelegate>
{
    UIScrollView *_scrollView;
    CDFIndexViewController *_favViewController;//guanzhu
    CDFIndexViewController *_bestViewController;//jinghua
    CDFIndexViewController *_allViewController;//quanbu
    ShowViewController *_showViewController;//xiuchang
    NewsViewController *_newViewController;//xinwen
    ActivityTableViewController *_avc;//活动
    
    UIView *_bacgroundView;//titleBeijing
    
    NSArray *_viewControllers;
    
    UIScrollView *_topScrollView;
}
@end

@implementation JuJCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;

    // Do any additional setup after loading the view.
    self.view.backgroundColor = [Common colorWithHex:@"#E2E2E2" alpha:1];
//    self.view.backgroundColor = RGBACOLOR(243, 243, 243, 1);
//    self.view.backgroundColor = [UIColor redColor];
    [self showUI];
    [self showTopView];
    [self showNavi];
}
- (void)showNavi{
    UILabel *titleLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    titleLable.text = @"聚精彩";
    titleLable.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    titleLable.textAlignment = NSTextAlignmentCenter;
    titleLable.textColor = RGBACOLOR(250, 66, 4, 1.0);
    self.navigationItem.titleView = titleLable;
    
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    button2.frame = CGRectMake(0, 0, 25, 25);
    [button2 setImage:[UIImage imageNamed:@"icon_02"] forState:UIControlStateNormal];
    UIBarButtonItem *item2 = [[UIBarButtonItem alloc]initWithCustomView:button2];
    [button2 addTarget:self action:@selector(searchClick) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = item2;
    
}
#pragma mark - 点击搜索
- (void)searchClick{
    ZWDSearchViewController *vc = [[ZWDSearchViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)showTopView{
    _topScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, KDeviceSizeWidth, 50)];
    _topScrollView.bounces = YES;
    _topScrollView.delegate = self;
    [self.view addSubview:_topScrollView];
    _topScrollView.contentSize = CGSizeMake(KDeviceSizeWidth/5 * 6, 50);
    _topScrollView.backgroundColor = [UIColor whiteColor];
    _topScrollView.showsHorizontalScrollIndicator = NO;
    _topScrollView.showsVerticalScrollIndicator = NO;
    
    //JuJCTab@2x
    _bacgroundView = [[UIView alloc]initWithFrame:CGRectMake(0, 48, KDeviceSizeWidth/5, 2)];
    _bacgroundView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"JuJCTab"]];
//    _bacgroundView.backgroundColor = RGBACOLOR(251, 142, 142, 1.0);
//    _bacgroundView.layer.masksToBounds = YES;
//    _bacgroundView.layer.cornerRadius = 8;
    [_topScrollView addSubview:_bacgroundView];
    
    //NSArray *arr = @[@"活动",@"全部",@"关注",@"秀场",@"精华",@"新闻"];
    NSArray *arr = @[@"活动",@"关注",@"秀场",@"精华",@"全部",@"新闻"];

    for (int i = 0; i < arr.count; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
        button.frame = CGRectMake((KDeviceSizeWidth/5)*i , 0, KDeviceSizeWidth/5, 50);
        [button setTitle:arr[i] forState:UIControlStateNormal];
        [button setTintColor:[UIColor blackColor]];
        button.titleLabel.font = [UIFont systemFontOfSize:16];
        button.tag = 200 + i;
        //RGBACOLOR(251, 142, 142, 1.0)
        [button setBackgroundColor:[UIColor clearColor]];
        [_topScrollView addSubview:button];
        [button addTarget:self action:@selector(titleButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        if (i == 0) {
            _bacgroundView.center = CGPointMake(button.center.x, 49);
            [button setTintColor:RGBACOLOR(255, 0, 0, 1)];
        }
    }
}
- (void)showUI{
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64 + 50, KDeviceSizeWidth, KDeviceSizeHeight - 64 - 50 - 39)];
    _scrollView.backgroundColor = [Common colorWithHex:@"#E2E2E2" alpha:1];
//    _scrollView.backgroundColor = RGBACOLOR(243, 243, 243, 1);
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.contentSize = CGSizeMake(KDeviceSizeWidth * 6, _scrollView.bounds.size.height);
    _scrollView.bounces = NO;
    _scrollView.pagingEnabled = YES;
    _scrollView.delegate = self;
    [self.view addSubview:_scrollView];
    
#pragma mark - 创建界面
    //活动
    _avc = [[ActivityTableViewController alloc]init];
    
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    _allViewController = [storyboard instantiateViewControllerWithIdentifier:@"CDFIndexViewController"];
    _allViewController.JuJCType = allJuJC;
    _favViewController =[storyboard instantiateViewControllerWithIdentifier:@"CDFIndexViewController"];
    _favViewController.JuJCType = favJuJC;
    _bestViewController = [storyboard instantiateViewControllerWithIdentifier:@"CDFIndexViewController"];
    _bestViewController.JuJCType = bestJuJC;
    
    _viewControllers = @[_allViewController,_favViewController,_bestViewController];
    
    for (int i = 0; i < 6; i++) {
        
        if (i == 0) {
            _avc.view.frame =CGRectMake(KDeviceSizeWidth * i, -64 - 5, KDeviceSizeWidth, _scrollView.bounds.size.height);
            [_scrollView addSubview:_avc.view];
        }
        
        if (i == 4) {
            _allViewController.view.frame = CGRectMake(KDeviceSizeWidth * i, 5, KDeviceSizeWidth, _scrollView.bounds.size.height);
            [_scrollView addSubview:_allViewController.view];
        }
        if (i == 1) {
            _favViewController.view.frame = CGRectMake(KDeviceSizeWidth * i, 5, KDeviceSizeWidth, _scrollView.bounds.size.height);
            [_scrollView addSubview:_favViewController.view];
        }
        if (i == 3) {
            _bestViewController.view.frame = CGRectMake(KDeviceSizeWidth * i, 5, KDeviceSizeWidth, _scrollView.bounds.size.height);
            [_scrollView addSubview:_bestViewController.view];
        }
        if (i == 2) {
            _showViewController = [[ShowViewController alloc]init];
            _showViewController.view.frame  = CGRectMake(KDeviceSizeWidth * i, 5, KDeviceSizeWidth, _scrollView.bounds.size.height-10);
            [_scrollView addSubview:_showViewController.view];
        }
        if (i == 5) {
            _newViewController = [NewsViewController new];
            _newViewController.view.frame = CGRectMake(KDeviceSizeWidth * i, 5, KDeviceSizeWidth, _scrollView.bounds.size.height-10);
            [_scrollView addSubview:_newViewController.view];
        }
        
    }
#pragma mark - 把当前界面的所有视图控制器都push入navigationController
    self.navigationController.viewControllers = @[_avc,_allViewController,_favViewController,_bestViewController,_showViewController,self];
    self.navigationItem.hidesBackButton = YES;
}
//头试图button点击事件
- (void)titleButtonClick:(UIButton *)button{
    if (button.tag == 202) {//秀场
        [self showNaviRight];
    }else{
        self.navigationItem.rightBarButtonItem = nil;
    }
    
    _scrollView.contentOffset = CGPointMake((button.tag - 200)*_scrollView.bounds.size.width, 0);
    
    if (button.tag - 200 == 1) {//关注
        CDFIndexViewController *vc = (CDFIndexViewController *)_viewControllers[button.tag - 200];
        if ([vc respondsToSelector:@selector(apper)]) {
            [vc apper];
        }
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        _bacgroundView.center = CGPointMake(button.center.x, 49);

    } completion:^(BOOL finished) {
        if (finished) {
            for (int i = 0; i < 6; i++) {
                UIButton *btn = (UIButton *)[_topScrollView viewWithTag:200+i];
                [btn setTintColor:RGBACOLOR(91, 94, 106, 1)];
            }
            [button setTintColor:RGBACOLOR(255, 0, 0, 1)];
        }
    }];
}
#pragma mark - 滑动至秀场时右上角出现发帖按钮
- (void)showNaviRight{
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    button1.frame = CGRectMake(0, 0, 25, 25);
    [button1 setImage:[UIImage imageNamed:@"btn_post_unsel"] forState:UIControlStateNormal];
    [button1 setImage:[UIImage imageNamed:@"btn_post_sel"] forState:UIControlStateSelected];
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc]initWithCustomView:button1];
    [button1 addTarget:self action:@selector(handleButtonPostMessage:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItems = @[item1];
}
#pragma mark - scrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView == _topScrollView) {
        return;
    }
    [UIView animateWithDuration:0.5 animations:^{
        if (scrollView.contentOffset.x/scrollView.bounds.size.width == 5) {
            _topScrollView.contentOffset = CGPointMake(KDeviceSizeWidth/5, 0);
        }else if (scrollView.contentOffset.x == 0){
            _topScrollView.contentOffset = CGPointMake(0, 0);
        }
    }];
    
    
    NSInteger i = scrollView.contentOffset.x/scrollView.bounds.size.width;
    if (i == 2) {
#pragma mark - 秀场
        [self showNaviRight];
    }else{
        self.navigationItem.rightBarButtonItem = nil;
    }
    UIButton *button = (UIButton *)[_topScrollView viewWithTag:200+i];
    
    if (i == 1) {//关注
        CDFIndexViewController *vc = (CDFIndexViewController *)_viewControllers[i];
        if ([vc respondsToSelector:@selector(apper)]) {
            [vc apper];
        }
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        _bacgroundView.center = CGPointMake(button.center.x, 49);
        
    } completion:^(BOOL finished) {
        if (finished) {
            for (int i = 0; i < 6; i++) {
                UIButton *btn = (UIButton *)[_topScrollView viewWithTag:200+i];
                [btn setTintColor:RGBACOLOR(91, 94, 106, 1)];
            }
            
            [button setTintColor:RGBACOLOR(255, 0, 0, 1)];
        }
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - 点击发帖button
- (void)handleButtonPostMessage:(UIButton *)sender {
    if (![CDFMe shareInstance].isLogin) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"温馨提醒" message:@"您还没有登录哦" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"登录", nil];
        [alertView show];
        return;
    }
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CDFNavigationController *VC = [storyboard instantiateViewControllerWithIdentifier:@"CDFNavigationController"];
    CDFPostingViewController *pvc = (CDFPostingViewController *)VC.topViewController;
    pvc.isShowViewController = YES;
   // CDFPostingViewController *pVC = [storyboard instantiateViewControllerWithIdentifier:@"CDFPostingViewController"];
    
    //pVC.isShowViewController = YES;
    NSArray *arr = @[
                     @{@"name":@"美食",@"gid":@"402",@"fid":@"1205",@"typeid":@"0",@"color":@"ff638e",@"icon":@"food",@"show":@"0",@"default":@"0"},
                     @{@"name":@"快旅",@"gid":@"441",@"fid":@"53",@"typeid":@"0",@"color":@"ffa300",@"food":@"food",@"show":@"1",@"default":@"0"},
                     @{@"name":@"骑友",@"gid":@"441",@"fid":@"593",@"typeid":@"138",@"color":@"ffa300",@"icon":@"food",@"show":@"1",@"default":@"0"},
                     @{@"name":@"拍客",@"gid":@"441",@"fid":@"780",@"typeid":@"1390",@"color":@"ffa300",@"icon":@"food",@"show":@"1",@"default":@"0"},
                     @{@"name":@"亲子",@"gid":@"402",@"fid":@"528",@"typeid":@"0",@"color":@"ffa300",@"icon":@"food",@"show":@"1",@"default":@"0"},
                     @{@"name":@"败家",@"gid":@"402",@"fid":@"496",@"typeid":@"214",@"color":@"ffa300",@"icon":@"food",@"show":@"1",@"default":@"0"},
                     @{@"name":@"贴图",@"gid":@"1010",@"fid":@"569",@"typeid":@"0",@"color":@"ffa300",@"icon":@"food",@"show":@"1",@"default":@"0"},
                     @{@"name":@"精彩",@"gid":@"441",@"fid":@"1006",@"typeid":@"214",@"color":@"ffa300",@"icon":@"food",@"show":@"1",@"default":@"0"},
                     @{@"name":@"车友",@"gid":@"1008",@"fid":@"117",@"typeid":@"0",@"color":@"ffa300",@"icon":@"food",@"show":@"1",@"default":@"0"}];
    
    
    NSDictionary *dict;
        dict = @{@"name":@"秀场",@"gid":@"1008",@"fid":@"1321",@"typeid":@"0",@"color":@"ffa300",@"icon":@"food",@"show":@"1",@"default":@"1"};
   
    
//    NSLog(@"%@",dict);
    /*
     NSLog(@"%@",self.fid);
     NSMutableArray *Marr = [NSMutableArray array];
     for (int i = 0; i < arr.count; i++) {
     
     NSDictionary *dict = arr[i];
     NSMutableDictionary *Mdict = [NSMutableDictionary dictionaryWithDictionary:dict];
     
     if ([dict[@"fid"]isEqualToString:self.fid]) {
     [Mdict setValue:@"1" forKey:@"default"];
     }
     [Marr addObject:Mdict];
     NSLog(@"%d",i);
     }
     NSLog(@"%@",Marr);
     */
    NSMutableArray *Marr = [NSMutableArray array];
    [Marr addObject:dict];
    //[Marr addObjectsFromArray:arr];
    [CDFMe shareInstance].board = Marr;
    
    [self presentViewController:VC animated:YES completion:nil];
    //[self.navigationController pushViewController:pVC animated:YES];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [[CDFMe shareInstance] wellcomeLogin:self];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
