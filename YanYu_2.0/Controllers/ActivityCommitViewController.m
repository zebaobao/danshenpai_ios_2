//
//  ActivityCommitViewController.m
//  yanyu
//
//  Created by caiyee on 15/4/15.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//


#import "ActivityCommitViewController.h"
#import "CDFDisplayViewController.h"
#define kHeightOfTitleLabel     40
//#define kWidthOfTitleLabel      KDeviceSizeWidth - kLeftSideTitleLabel * 2
#define kWidthOfTitleLabel      50
#define kWidthOfPhoneLabel      50
#define kWidthOfGenderLabel     50
#define kWidthOfMessageLabel    50

#define kWidthOfNameTextField   KDeviceSizeWidth - kWidthOfTitleLabel - kLeftSideTitleLabel * 3
#define kWidthOfPhoneTextField  KDeviceSizeWidth - kWidthOfPhoneLabel - kLeftSideTitleLabel * 3
#define kWidthOfGenderTextField KDeviceSizeWidth - kWidthOfGenderLabel - kLeftSideTitleLabel * 3
#define kWidthOfMessageTextView KDeviceSizeWidth - kWidthOfMessageLabel - kLeftSideTitleLabel * 3

#define kLeftSideTitleLabel     10
#define kUpSideTitleLabel       10
#define kHeightBettwonTitleLabelAndCountOfPerson    15
#define kWidthOfButton          100
#define kWidthToSideOfButton    30


@interface ActivityCommitViewController ()<UIScrollViewDelegate, UITextFieldDelegate,UIGestureRecognizerDelegate, UIAlertViewDelegate, UITextViewDelegate>

@end

@implementation ActivityCommitViewController {
    UILabel *_titleLabel;
    UILabel *_countOfperson;
    UILabel *_tinyNameLabel;
    UILabel *_phoneLabel;
    UITextField *_tinyNameTextField;
    UITextField *_phoneNumberTextField;
    UILabel *_genderLabel;
    UILabel *_messageLabel;
    UITextView *_messageTextField;
    UILabel *_genderTextField;
    NSString *_genderNum;
    UIButton *_backgroundButton;
    UIScrollView *_scrollView;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    UITextField *textFieldOfName = (UITextField *)[_scrollView viewWithTag:100];
    UITextField *textFieldOfPhone = (UITextField *)[_scrollView viewWithTag:101];

    if (textFieldOfName == textField) {
        [textFieldOfName resignFirstResponder];
        [textFieldOfPhone becomeFirstResponder];
    } else if (textField == textFieldOfPhone) {
        [textFieldOfPhone resignFirstResponder];
    }
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customizedNavigationItem];
    [self setupViews];
    [self layoutLeftNavigationItem];
    _genderNum = @"0";
}

- (void)customizedNavigationItem {
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    titleLabel.font = [UIFont fontWithName:@"" size:20];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor orangeColor];
//    titleLabel.text = self.title;
    titleLabel.text = @"活动报名";
    [titleView addSubview:titleLabel];
    self.navigationItem.titleView = titleView;
}

- (void)setupViews {
    
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, KDeviceSizeWidth, KDeviceSizeHeight)];
    _scrollView.backgroundColor = [UIColor whiteColor];
    _scrollView.delegate = self;
    _scrollView.alwaysBounceVertical = YES;
    
    NSString *titleStr = [NSString stringWithFormat:@"《%@》报名表", self.title];
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(kLeftSideTitleLabel, kUpSideTitleLabel, KDeviceSizeWidth - kLeftSideTitleLabel * 2, [[self class] detailTextHeight:titleStr])];
    _titleLabel.text = titleStr;
    _titleLabel.font = [UIFont fontWithName:nil size:20];
    _titleLabel.numberOfLines = 0;
    
    _scrollView.contentSize = CGSizeMake(KDeviceSizeWidth, 286 + [[self class] detailTextHeight:titleStr]);
    
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _countOfperson = [[UILabel alloc] initWithFrame:CGRectMake(kLeftSideTitleLabel, _titleLabel.frame.size.height + kHeightBettwonTitleLabelAndCountOfPerson + kUpSideTitleLabel, 300, kHeightOfTitleLabel)];
    _countOfperson.font = [UIFont fontWithName:nil size:15];
    _countOfperson.textColor = [UIColor lightGrayColor];
    if (!self.numberOfPerson) {
        self.numberOfPerson = @"0";
    }
    _countOfperson.text = [NSString stringWithFormat:@"截止到目前,共有%@人参加", self.numberOfPerson];
    _tinyNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(kLeftSideTitleLabel, _countOfperson.frame.origin.y + kHeightOfTitleLabel + kHeightBettwonTitleLabelAndCountOfPerson, kWidthOfTitleLabel, kHeightOfTitleLabel)];
    _tinyNameLabel.textColor = [UIColor grayColor];
    _tinyNameLabel.text = @"姓名 *";
    _tinyNameTextField = [[UITextField alloc] initWithFrame:CGRectMake(kLeftSideTitleLabel * 2.0 + kWidthOfTitleLabel, _tinyNameLabel.frame.origin.y, kWidthOfNameTextField, kHeightOfTitleLabel)];
    _tinyNameTextField.borderStyle = UITextBorderStyleRoundedRect;
    _tinyNameTextField.tag = 100;
    _tinyNameTextField.returnKeyType = UIReturnKeyNext;
    _tinyNameTextField.delegate = self;
    _phoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(kLeftSideTitleLabel, _tinyNameTextField.frame.origin.y + _tinyNameTextField.frame.size.height + kHeightBettwonTitleLabelAndCountOfPerson, kWidthOfPhoneLabel, kHeightOfTitleLabel)];
    _phoneLabel.text = @"手机 *";
    _phoneLabel.textColor = [UIColor grayColor];
    _phoneNumberTextField = [[UITextField alloc] initWithFrame:CGRectMake(kLeftSideTitleLabel * 2 + kWidthOfPhoneLabel, _phoneLabel.frame.origin.y, kWidthOfPhoneTextField, kHeightOfTitleLabel)];
    
    _phoneNumberTextField.borderStyle = UITextBorderStyleRoundedRect;
    _phoneNumberTextField.tag = 101;
    _phoneNumberTextField.delegate = self;
    _phoneNumberTextField.keyboardType = UIKeyboardTypeNumberPad;
    
    _genderLabel = [[UILabel alloc] initWithFrame:CGRectMake(kLeftSideTitleLabel, _phoneLabel.frame.origin.y + _phoneLabel.frame.size.height + kHeightBettwonTitleLabelAndCountOfPerson, kWidthOfGenderLabel, kHeightOfTitleLabel)];

    _genderLabel.text = @"性别 *";
    _genderLabel.textColor = [UIColor grayColor];
    _genderTextField = [[UILabel alloc] initWithFrame:CGRectMake(kLeftSideTitleLabel * 2 + kWidthOfGenderLabel, _genderLabel.frame.origin.y, kWidthOfGenderTextField, kHeightOfTitleLabel)];
    _genderTextField.layer.masksToBounds = YES;
    _genderTextField.layer.cornerRadius = 5;
    _genderTextField.layer.borderWidth = 0.6;
    _genderTextField.userInteractionEnabled = YES;
    _genderTextField.layer.borderColor = [[UIColor colorWithRed:193.0/255 green:193.0/255 blue:193.0/255 alpha:0.8] CGColor];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapLabel:)];
    [_genderTextField addGestureRecognizer:tapGesture];
    
    _messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(kLeftSideTitleLabel, _genderTextField.frame.origin.y + kHeightBettwonTitleLabelAndCountOfPerson + _genderTextField.frame.size.height, kWidthOfMessageLabel, kHeightOfTitleLabel)];
    _messageLabel.text = @"留言";
    _messageLabel.textColor = [UIColor grayColor];
    
    _messageTextField = [[UITextView alloc] initWithFrame:CGRectMake(kLeftSideTitleLabel * 2 + kWidthOfMessageLabel, _messageLabel.frame.origin.y , kWidthOfMessageTextView, kHeightOfTitleLabel)];
    _messageTextField.layer.masksToBounds = YES;
    _messageTextField.layer.cornerRadius = 5;
    _messageTextField.layer.borderColor = [[UIColor colorWithRed:193.0/255 green:193.0/255 blue:193.0/255 alpha:0.8] CGColor];
    _messageTextField.layer.borderWidth = 0.6;
    _messageTextField.tag = 103;
    _messageTextField.font = [UIFont fontWithName:nil size:18.0];
    _messageTextField.delegate = self;

    UIButton *commitBtn = [[UIButton alloc] initWithFrame:CGRectMake(kWidthToSideOfButton, _messageTextField.frame.origin.y + kHeightBettwonTitleLabelAndCountOfPerson * 2 + _messageTextField.frame.size.height + 10, kWidthOfButton, kHeightOfTitleLabel)];
    commitBtn.layer.masksToBounds = YES;
    commitBtn.layer.cornerRadius = 5;
    [commitBtn addTarget:self action:@selector(handleCommitBtn:) forControlEvents:UIControlEventTouchUpInside];
    [commitBtn setTitle:@"提交" forState:UIControlStateNormal];
    [commitBtn setBackgroundColor:[UIColor orangeColor]];
    [commitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIButton *resultBtn = [[UIButton alloc] initWithFrame:CGRectMake(KDeviceSizeWidth - kWidthOfButton - kWidthToSideOfButton, commitBtn.frame.origin.y, commitBtn.frame.size.width, commitBtn.frame.size.height)];
    [resultBtn addTarget:self action:@selector(handleResultBtn:) forControlEvents:UIControlEventTouchUpInside];
    [resultBtn setTitle:@"查看结果" forState:UIControlStateNormal];
    [resultBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [resultBtn setBackgroundColor:[UIColor orangeColor]];
    resultBtn.layer.masksToBounds = YES;
    resultBtn.layer.cornerRadius = 5;
    
    [_scrollView addSubview:_tinyNameTextField];
    [_scrollView addSubview:_titleLabel];
    [_scrollView addSubview:_countOfperson];
    [_scrollView addSubview:_tinyNameLabel];
    [_scrollView addSubview:_phoneNumberTextField];
    [_scrollView addSubview:_phoneLabel];
    [_scrollView addSubview:_genderLabel];
    [_scrollView addSubview:_genderTextField];
    
    [_scrollView addSubview:_messageTextField];
    [_scrollView addSubview:_messageLabel];
    
    [_scrollView addSubview:commitBtn];
    [_scrollView addSubview:resultBtn];
    
    [self.view addSubview:_scrollView];
    
    _backgroundButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, _scrollView.frame.size.width, _scrollView.contentSize.height)];
    [_backgroundButton addTarget:self action:@selector(handleBackgroundBtn:) forControlEvents:UIControlEventTouchUpInside];
    [_scrollView addSubview:_backgroundButton];
    [_scrollView sendSubviewToBack:_backgroundButton];
    
}
- (void)handleBackgroundBtn:(UIButton *)btn {
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];  //这句代码可以 隐藏键盘
}

- (void)handleTapLabel:(UITapGestureRecognizer *)tap {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"选择性别" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"男",@"女", nil];
    alert.delegate = self;
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        _genderTextField.text = @" 男";
        _genderNum = @"1";
    } else if (buttonIndex == 2) {
        _genderTextField.text = @" 女";
        _genderNum = @"2";
    }
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    CGFloat heightOf = [[self class] detailTextHeight:[NSString stringWithFormat:@"《%@》报名表", self.title]];
//    _scrollView.contentSize = CGSizeMake(KDeviceSizeWidth, 686 + [[self class] detailTextHeight:titleStr]);
    _scrollView.contentSize = CGSizeMake(KDeviceSizeWidth, _scrollView.contentSize.height+350);

    [_scrollView setContentOffset:CGPointMake(0, heightOf + 70) animated:YES];
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    
//    _scrollView.contentSize = CGSizeMake(KDeviceSizeWidth, 686 + [[self class] detailTextHeight:titleStr]);
//    _scrollView.frame = CGRectMake(0, 0, KDeviceSizeWidth, KDeviceSizeHeight);
    _scrollView.contentSize = CGSizeMake(KDeviceSizeWidth, _scrollView.contentSize.height - 350);
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    NSString *titleStr = [NSString stringWithFormat:@"《%@》报名表", self.title];
    CGFloat heightOf = [[self class] detailTextHeight:[NSString stringWithFormat:@"《%@》报名表", self.title]];
    //

    _scrollView.contentSize = CGSizeMake(KDeviceSizeWidth, _scrollView.contentSize.height+350);
    
    if (textField.tag == 100) {
        [_scrollView setContentOffset:CGPointMake(0, heightOf - 110) animated:YES];
    } else if (textField.tag == 101) {
        [_scrollView setContentOffset:CGPointMake(0, heightOf - 5) animated:YES];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    NSString *titleStr = [NSString stringWithFormat:@"《%@》报名表", self.title];
//    _scrollView.contentSize = CGSizeMake(KDeviceSizeWidth, 686 + [[self class] detailTextHeight:titleStr]);
    _scrollView.contentSize = CGSizeMake(KDeviceSizeWidth, _scrollView.contentSize.height - 350);
//    _scrollView.frame = CGRectMake(0, 0, KDeviceSizeWidth, KDeviceSizeHeight);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//验证电话号码是否有效
- (BOOL)isMobileNumber:(NSString *)mobileNum
{
    NSString * MOBILE = @"^1[3|4|5|7|8][0-9]\\d{8}$";
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    if ([regextestmobile evaluateWithObject:mobileNum] == YES)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}
- (void)handleCommitBtn:(UIButton *)sender {
    NSString *str = _tinyNameTextField.text;
    
    if (_tinyNameTextField.text.length < 2) {
        [SVProgressHUD showErrorWithStatus:@"姓名信息填写有误" duration:3];
        return;
    } else if (![self isMobileNumber:_phoneNumberTextField.text]) {
        [SVProgressHUD showErrorWithStatus:@"手机号码填写有误" duration:3];
        return;
    } else if (_genderTextField.text.length <= 0) {
        [SVProgressHUD showErrorWithStatus:@"请填写性别" duration:3];
        return;
    }
    
    NSMutableDictionary *sendData = [NSMutableDictionary new];
    [sendData setObject:self.fid forKey:@"fid"];
    [sendData setObject:self.tid forKey:@"tid"];
    [sendData setObject:self.pid forKey:@"pid"];
    [sendData setObject:[CDFMe shareInstance].formHash forKey:@"formhash"];
//    [sendData setObject:(NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef) _tinyNameTextField.text,NULL,(CFStringRef) @"!*'();:@&=+$,/?%#[]",kCFStringEncodingUTF8)) forKey:@"realname"];
    [sendData setObject:_tinyNameTextField.text forKey:@"realname"];

    [sendData setObject:_phoneNumberTextField.text forKey:@"mobile"];
//    NSLog(@"%@", _genderNum);
    [sendData setObject:_genderNum forKey:@"gender"];
    [sendData setObject:_messageTextField.text forKey:@"message"];
    
    NSString *url = [NSString stringWithFormat:@"%@api/mobile/?version=%@&module=activityapplies", kBaseURL, kApiVersion];
    //NSString *loginURL;
    //loginURL = [NSString stringWithFormat:@"api/mobile/?version=%@&module=login&loginsubmit=yes&loginfield=auto&submodule=checkpost&lssubmit=yes&username=%@&password=%@&mobiletype=2",kApiVersion ,(NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef) self.userNameTextField.text,NULL,(CFStringRef) @"!*'();:@&=+$,/?%#[]",kCFStringEncodingUTF8)),(NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef) self.passwordTextField.text,NULL,(CFStringRef) @"!*'();:@&=+$,/?%#[]",kCFStringEncodingUTF8))];
    
    [SVProgressHUD showWithStatus:@"提交中..."];
    [[HXHttpClient shareInstance] postURL:url postData:sendData success:^(id responseObject) {
        NSString *message = responseObject[@"Message"][@"messagestr"];
        NSString *messageval = responseObject[@"Message"][@"messageval"];
        
        [SVProgressHUD showSuccessWithStatus:message duration:3];
        //提交成功, 报名人数+1
        if ([messageval isEqualToString:@"activity_completion"]) {
            
            [(CDFDisplayViewController *)self.viewController performSelector:@selector(loadData) withObject:nil];
            [self.navigationController popViewControllerAnimated:YES];
        }

    } fail:^(NSError *error) {
        
    }];
    
}
- (void)handleResultBtn:(UIButton *)sender {
    ZWDActivityResultViewController *vc = [[ZWDActivityResultViewController alloc]init];
    vc.specialDict = self.specialDict;
//    NSLog(@"%@",self.specialDict);
    [self.navigationController pushViewController:vc animated:YES];
    
}

+ (CGFloat)detailTextHeight:(NSString *)text {
    CGRect rectToFit = [text boundingRectWithSize:CGSizeMake(KDeviceSizeWidth - 2 * kLeftSideTitleLabel, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:20.0f]} context:nil];
//    NSLog(@"rectToFit:%f", rectToFit.size.height);
    return rectToFit.size.height;
}
- (void)layoutLeftNavigationItem {
    UIButton *backBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    [backBtn setImage:[UIImage imageNamed:@"backOrange"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(handleLeftBarBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
}
- (void)handleLeftBarBtn:(UIButton *)btn {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
