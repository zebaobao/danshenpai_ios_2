//
//  ShowViewController.m
//  yanyu
//
//  Created by Case on 15/4/10.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "ShowViewController.h"
#import "MJRefresh.h"
#import "PSCollectionViewCell.h"
#import "CellView.h"
#import "UIImageView+WebCache.h"
#import "CDFDisplayViewController.h"


#define SHOWCELLID @"SHOWVIEWCONTROLLERCELLID"


@interface ShowViewController ()
{
    int _currentPage;
    
    //UICollectionView *_collectionView;
    
    //ZBWaterView *_waterView;
    
    id _mainData;
    
//    NSMutableArray *_dataArr;
}
@end

@implementation ShowViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self reset];
    // Do any additional setup after loading the view.
//    _dataArr = [NSMutableArray array];
    [self showUI];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(handlePostResult) name:kPOST_SUCCESS object:nil];
    
    [self loadData];
    self.view.backgroundColor = [Common colorWithHex:@"#E2E2E2" alpha:1];
}

- (void)handlePostResult{
    _currentPage = 1;
    self.collectionView.contentOffset = CGPointZero;
    [self loadData];
}

- (void)reset{
    self.items = [NSMutableArray array];

    _currentPage = 1;
}
- (void)loadData{
    //    NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=forumdisplay&page=%d%@%@",kApiVersion ,_currentPage, [NSString stringWithFormat:@"&fid=%@",self.fid], self.orderBy];

    NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=topicnew&fid=1321&page=%d",kApiVersion ,_currentPage];
    //NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=forumdisplay&fid=1321&page=%d",kApiVersion ,_currentPage];

    
    [[HXHttpClient shareInstance]grabURL:url success:^(id responseObject) {
        _mainData = responseObject;
        if (_currentPage == 1 && [responseObject[@"Variables"][@"forum_threadlist"] count]>0) {
            [self.items removeAllObjects];
        }
        NSArray *arr = responseObject[@"Variables"][@"forum_threadlist"];
        for (int i = 0 ; i< arr.count; i++) {
            NSDictionary *dict = arr[i];
            if ([[dict allKeys] containsObject:@"attachments"]) {
//                [_dataArr addObject:dict];
                [self.items addObject:dict];
            }
        }
        [self.collectionView reloadData];
//        [_waterView endLoadMore];
//        [_waterView reloadData];
    } fail:^(NSError *error) {
//        NSLog(@"showviewcontroller-----down fail");
    }];
    
}
- (void)showUI{
    self.collectionView = [[PullPsCollectionView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:self.collectionView];
    self.collectionView.collectionViewDelegate = self;
    self.collectionView.collectionViewDataSource = self;
    self.collectionView.pullDelegate=self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    
    self.collectionView.numColsPortrait = 2;
    self.collectionView.numColsLandscape = 3;
    
    self.collectionView.pullArrowImage = [UIImage imageNamed:@"blackArrow"];
    self.collectionView.pullBackgroundColor = [UIColor whiteColor];
    self.collectionView.pullTextColor = [UIColor grayColor];
    //    UIView *headerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 45)];
    //    [headerView setBackgroundColor:[UIColor redColor]];
    //    self.collectionView.headerView=headerView;
    UILabel *loadingLabel = [[UILabel alloc] initWithFrame:self.collectionView.bounds];
    loadingLabel.text = @"Loading...";
    loadingLabel.textAlignment = NSTextAlignmentCenter;
    self.collectionView.loadingView = loadingLabel;
    
    //    [self loadDataSource];
    if(!self.collectionView.pullTableIsRefreshing) {
        self.collectionView.pullTableIsRefreshing = YES;
        [self performSelector:@selector(refreshTable) withObject:nil afterDelay:0];
    }
    
    
    
//    _waterView = [[ZBWaterView alloc]  initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height - 64 - 49 - 50)];
//    _waterView.waterDataSource = self;
//    _waterView.waterDelegate = self;
//    _waterView.isDataEnd = NO;
//    [self.view addSubview:_waterView];

//    ZWDShowLayOut *layout = [[ZWDShowLayOut alloc]init];
//    layout.dataArr = _dataArr;
//    
//    _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, KDeviceSizeWidth, KDeviceSizeHeight - 64 - 49 - 50) collectionViewLayout:layout];
//    _collectionView.delegate =self;
//    _collectionView.dataSource = self;
//    [_collectionView registerClass:[ZWDShowCell class] forCellWithReuseIdentifier:SHOWCELLID];
//    _collectionView.backgroundColor = [UIColor whiteColor];
//    [self.view addSubview:_collectionView];
    
}
- (void) refreshTable
{
    /*
     
     Code to actually refresh goes here.
     
     */
    
    [self.items removeAllObjects];
    _currentPage = 1;
    [self loadData];
    self.collectionView.pullLastRefreshDate = [NSDate date];
    self.collectionView.pullTableIsRefreshing = NO;
    [self.collectionView reloadData];
}
- (void) loadMoreDataToTable
{
    /*
     
     Code to actually load more data goes here.
     
     */
    //    [self loadDataSource];
    _currentPage++;
    //[self.items addObjectsFromArray:self.items];
    //[self.collectionView reloadData];
    [self loadData];
    self.collectionView.pullTableIsLoadingMore = NO;
}

#pragma mark - PullTableViewDelegate

- (void)pullPsCollectionViewDidTriggerRefresh:(PullPsCollectionView *)pullTableView
{
    [self performSelector:@selector(refreshTable) withObject:nil afterDelay:0];
}

- (void)pullPsCollectionViewDidTriggerLoadMore:(PullPsCollectionView *)pullTableView
{
    [self performSelector:@selector(loadMoreDataToTable) withObject:nil afterDelay:0];
}
- (void)viewDidUnload
{
    [self setCollectionView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}
- (PSCollectionViewCell *)collectionView:(PSCollectionView *)collectionView viewAtIndex:(NSInteger)index {
    
    NSDictionary *item = [self getImageDict:[self.items objectAtIndex:index]];
    NSDictionary *dict = [self.items objectAtIndex:index];
    
    
    // You should probably subclass PSCollectionViewCell
    CellView *v = (CellView *)[self.collectionView dequeueReusableView];
    //    if (!v) {
    //        v = [[[PSCollectionViewCell alloc] initWithFrame:CGRectZero] autorelease];
    //    }
    if(v == nil) {
        NSArray *nib =
        [[NSBundle mainBundle] loadNibNamed:@"CellView" owner:self options:nil];
        v = [nib objectAtIndex:0];
        
    }
    
    //    [v fillViewWithObject:item];
    
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", [item objectForKey:@"url"], [item objectForKey:@"attachment"]]];
    //    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    
//    [v.picView  setImageWithURL:URL placeholderImage:[UIImage imageNamed:@"bg_imageloader"]];
    [v showData:dict width:self.collectionView.colWidth];
    
//    [v.picView sd_setImageWithURL:URL placeholderImage:[UIImage imageNamed:@""] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//        v.picView.image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    }];
    v.picView.contentMode = UIViewContentModeScaleToFill;
    [v.picView sd_setImageWithURL:URL placeholderImage:[[UIImage imageNamed:@"bg_imageloader"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] options:SDWebImageProgressiveDownload progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
        
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];

    
    v.titleLabel.text = dict[@"subject"];
    v.titleLabel.textColor = TITLEGRAY;
    
    v.viewsLable.text = dict[@"views"];
    if ([dict[@"views"] length] >= 4) {
        CGRect frame = v.viewsLable.frame;
        frame.size.width += 10;
        frame.origin.x -= 10;
        v.viewsLable.frame = frame;
    }else{
        CGRect frame = v.viewsLable.frame;
        if (frame.size.width > 40) {
            frame.origin.x += 10;
            frame.size.width -= 10;
        }
        v.viewsLable.frame = frame;
        
    }
    
    v.viewsLable.backgroundColor = [UIColor blackColor];
    v.viewsLable.alpha = 0.4;
    v.viewsLable.layer.masksToBounds = YES;
    v.viewsLable.layer.cornerRadius = 5;
    return v;
    
}
- (CGFloat)heightForViewAtIndex:(NSInteger)index {
    NSDictionary *item = [self.items objectAtIndex:index];
//    NSDictionary *item = [self getImageDict:[self.items objectAtIndex:index]];

    // You should probably subclass PSCollectionViewCell
    return [PSCollectionViewCell heightForViewWithObject:item inColumnWidth:self.collectionView.colWidth];
}

- (void)collectionView:(PSCollectionView *)collectionView didSelectView:(PSCollectionViewCell *)view atIndex:(NSInteger)index {
    // Do something with the tap
    NSDictionary *dict = self.items[index];
    //        UIViewController *displayViewController = [nav.storyboard instantiateViewControllerWithIdentifier:@"DisplayViewContoller"];

    UIStoryboard *s = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CDFDisplayViewController *vc = [s instantiateViewControllerWithIdentifier:@"DisplayViewContoller"];
    vc.tid = dict[@"tid"];
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (NSInteger)numberOfViewsInCollectionView:(PSCollectionView *)collectionView {
    return [self.items count];
}
/*
#pragma mark - ZBWaterViewDatasource
- (NSInteger)numberOfFlowViewInWaterView:(ZBWaterView *)waterView
{
    return [_dataArr count];
}
- (CustomWaterInfo *)infoOfWaterView:(ZBWaterView*)waterView
{
    CustomWaterInfo *info = [[CustomWaterInfo alloc] init];
    info.topMargin = 0;
    info.leftMargin = 5;
    info.bottomMargin = 0;
    info.rightMargin = 5;
    info.horizonPadding = 5;
    info.veticalPadding = 5;
    info.numOfColumn = 2;
    return info;
}
- (ZBFlowView *)waterView:(ZBWaterView *)waterView flowViewAtIndex:(NSInteger)index
{
    ZBFlowView *flowView = [waterView dequeueReusableCellWithIdentifier:SHOWCELLID];
    if (flowView == nil) {
        flowView = [[ZBFlowView alloc] initWithFrame:CGRectZero];
        flowView.reuseIdentifier = SHOWCELLID;
    }
    flowView.index = index;
    //flowView.backgroundColor = data.color;
    [flowView showData:_dataArr[index]];
    return flowView;
}

- (CGFloat)waterView:(ZBWaterView *)waterView heightOfFlowViewAtIndex:(NSInteger)index
{
    NSDictionary *dict = _dataArr[index];
    NSDictionary *imgDict = [self getImageDict:dict];
    float height = [imgDict[@"height"] floatValue]/[imgDict[@"width"] floatValue] * (KDeviceSizeWidth-15)/2;
    return height;
}

#pragma mark - ZBWaterViewDelegate
- (void)needLoadMoreByWaterView:(ZBWaterView *)waterView;
{
    _currentPage ++ ;
    [self loadData];
    NSLog(@"loadmore");
}
- (void)phoneWaterViewDidScroll:(ZBWaterView *)waterView
{
    //do what you want to do
    return;
}

- (void)waterView:(ZBWaterView *)waterView didSelectAtIndex:(NSInteger)index
{
    NSLog(@"didSelectAtIndex%d",index);
}

- (float) heightForString:(NSString *)value fontSize:(float)fontSize andWidth:(float)width
{
    CGSize sizeToFit = [value sizeWithFont:[UIFont systemFontOfSize:fontSize]
                         constrainedToSize:CGSizeMake(width -16.0, CGFLOAT_MAX)
                             lineBreakMode:NSLineBreakByWordWrapping];
    //此处的换行类型（lineBreakMode）可根据自己的实际情况进行设置
    return sizeToFit.height + 16.0;
}
*/

/*
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSLog(@"%d",_dataArr.count);
    return _dataArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    ZWDShowCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:SHOWCELLID forIndexPath:indexPath];
    
    
    [cell showData:_dataArr[indexPath.row]];
    NSLog(@"%d------%d",indexPath.section,indexPath.row);
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"%d------%d",indexPath.section,indexPath.row);

}
 */
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSDictionary *)getImageDict:(NSDictionary *)dict{
    
    NSDictionary *retDict = dict[@"attachments"];
    NSDictionary *imgDict = retDict[[[retDict allKeys] firstObject]];
    return imgDict;
}
@end
