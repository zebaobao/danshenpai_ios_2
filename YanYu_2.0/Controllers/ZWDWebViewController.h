//
//  ZWDWebViewController.h
//  yanyu
//
//  Created by Case on 15/4/18.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "ParentViewController.h"
#import "MJRefresh.h"
#import "CDFMeViewController.h"

@interface ZWDWebViewController : ParentViewController

@property (nonatomic,copy)NSString *url;

@end
