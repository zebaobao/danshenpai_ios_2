//
//  ZWDResetPasswordViewController.m
//  yanyu
//
//  Created by Case on 15/4/24.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "ZWDResetPasswordViewController.h"

@interface ZWDResetPasswordViewController ()
{
    UITextField *_username;
    UITextField *_phoneNumber;
    UITableView *_tableView;
}
@end

@implementation ZWDResetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self showUI];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    ZWDResetCell *cell = (ZWDResetCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [cell.textField becomeFirstResponder];
}
- (void)showUI{
    /*
    UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(5, 64, KDeviceSizeWidth - 10, 20)];
    lable.text = @"用户名:";
    [self.view addSubview:lable];
    
    
    _username = [[UITextField alloc]initWithFrame:CGRectMake(lable.frame.origin.x, lable.frame.origin.y + lable.frame.size.height + 5, lable.frame.size.width, 30)];
    _username.clearButtonMode = UITextFieldViewModeWhileEditing;
    _username.placeholder = @"请输入用户名";
    [self.view addSubview:_username];
    
    UILabel *phoneLable = [[UILabel alloc]initWithFrame:CGRectMake(5, _username.frame.origin.y + _username.frame.size.height + 20, _username.frame.size.width, 30)];
    phoneLable.text = @"手机:";
    [self.view addSubview:phoneLable];
    
    _phoneNumber = [[UITextField alloc]initWithFrame:CGRectMake(5, phoneLable.frame.origin.y + phoneLable.frame.size.height + 5, phoneLable.frame.size.width, 30)];
    _phoneNumber.clearButtonMode = UITextFieldViewModeUnlessEditing;
    _phoneNumber.placeholder = @"请输入密码";
    [self.view addSubview:_phoneNumber];
     */
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, KDeviceSizeWidth, KDeviceSizeHeight - 64) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    [button setTitle:@"X" forState:UIControlStateNormal];
    button.frame = CGRectMake(KDeviceSizeWidth - 50, 30, 30, 30);
    button.layer.masksToBounds = YES;
    button.layer.cornerRadius = 15;
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.backgroundColor = TITLEGRAY;
    [self.view addSubview:button];
    [button addTarget:self action:@selector(button) forControlEvents:UIControlEventTouchUpInside];
}
- (void)button{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - 点击确定按钮
- (void)sureButtonClick{
    ZWDResetCell *cell1 = (ZWDResetCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    NSString *username = cell1.textField.text;
    ZWDResetCell *cell2 = (ZWDResetCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    NSString *phoneNumber = cell2.textField.text;
    
    if (username.length <= 0) {
        [SVProgressHUD showErrorWithStatus:@"请输入用户名！" duration:5];
        return;
    }else if (phoneNumber.length <= 0){
        [SVProgressHUD showErrorWithStatus:@"请输入手机号！" duration:5];
        return;
    }else if (![self isMobileNumber:phoneNumber]){
        [SVProgressHUD showErrorWithStatus:@"请输入正确的手机号！" duration:5];
        return;
    }
    ////////////
    NSMutableString *formhash = [NSMutableString new];
    NSString *url0 = [NSString stringWithFormat:@"%@api/mobile/?version=%@&module=hxregister&mod=registeryy_yanyu&mobiletype=2",kBaseURL,kApiVersion];
    [SVProgressHUD showWithStatus:@"请稍后..."];
    [[HXHttpClient shareInstance]grabURL:url0 success:^(id responseObject) {
        [formhash appendString:responseObject[@"formhash"]];
        
        NSString *url = [NSString stringWithFormat:@"%@api/mobile/?version=%@&module=find_password&formhash=%@&username=%@&mobiles=%@",kBaseURL,kApiVersion,formhash,(NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef) username,NULL,(CFStringRef) @"!*'();:@&=+$,/?%#[]",kCFStringEncodingUTF8)),phoneNumber];
        NSLog(@"%@",url);
        [[HXHttpClient shareInstance]grabURL:url success:^(id responseObject) {
            NSLog(@"%@",responseObject);
            [SVProgressHUD showWithStatus:responseObject[@"Message"][@"messagestr"]];
            [self performSelector:@selector(dismissSVpro) withObject:nil afterDelay:5];
        } fail:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:@"网络连接错误！" duration:5];
            
        }];
       
        
        /*
        NSDictionary *dict = @{@"formhash":formhash
                               ,@"username":(NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef) username,NULL,(CFStringRef) @"!*'();:@&=+$,/?%#[]",kCFStringEncodingUTF8)),
                               @"mobiles":phoneNumber};
//        NSLog(@"%@",dict);
        
        NSLog(@"%@",dict);
        NSString *url = [NSString stringWithFormat:@"%@api/mobile/?version=%@&module=find_password",kBaseURL,kApiVersion];
        
        
        [[HXHttpClient shareInstance]postURL:url postData:dict success:^(id responseObject) {
            
//            [SVProgressHUD showSuccessWithStatus:responseObject[@"Message"][@"messagestr"] duration:5];
            [SVProgressHUD showWithStatus:responseObject[@"Message"][@"messagestr"]];
            [self performSelector:@selector(dismissSVpro) withObject:nil afterDelay:5];
        } fail:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:@"网络连接错误！" duration:5];
        }];
*/
    } fail:^(NSError *error) {
        
    }];
//<<<<<<< HEAD
//=======
    
////////////////////////
    
//>>>>>>> f30344495724dc9ff497527d304299a5a0ad6b1b
}
- (void)dismissSVpro{
    [SVProgressHUD dismiss];
}
//验证电话号码是否有效
- (BOOL)isMobileNumber:(NSString *)mobileNum
{
    NSString * MOBILE = @"^1[3|4|5|7|8][0-9]\\d{8}$";
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    if ([regextestmobile evaluateWithObject:mobileNum] == YES)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}
#pragma mark - tableVIewDelegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 2) {
        static NSString *cellID = @"ZWDResetOtherCell";
        ZWDResetOtherCell *cell = [tableView dequeueReusableHeaderFooterViewWithIdentifier:cellID];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"ZWDResetOtherCell" owner:self options:nil]lastObject];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
        return cell;
    }
    
    static NSString *cellID = @"ZWDResetCell";
    ZWDResetCell *cell = [tableView dequeueReusableHeaderFooterViewWithIdentifier:cellID];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"ZWDResetCell" owner:self options:nil]lastObject];
    }
    
    if (indexPath.row == 0) {
        cell.iconImg.image = [UIImage imageNamed:@"username"];
        cell.textField.placeholder = @"请输入用户名";
    }else if (indexPath.row == 1){
        cell.iconImg.image = [UIImage imageNamed:@"iphone"];
        cell.textField.placeholder = @"请输入手机号";
    }
    
    return cell;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 2) {
        return 100;
    }
    return 64;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
