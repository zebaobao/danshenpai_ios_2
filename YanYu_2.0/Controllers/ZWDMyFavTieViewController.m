//
//  ZWDMyFavTieViewController.m
//  yanyu
//
//  Created by Case on 15/4/18.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "ZWDMyFavTieViewController.h"

#import "CircleImageView.h"
#import "CDFMeViewController.h"
#import "CDFDisplayViewController.h"

#import "AFNetworking.h"

@interface ZWDMyFavTieViewController ()<UITableViewDataSource,UITableViewDelegate,CircleImageVIewDelegate>
{
    UITableView *_tableView;
    
    NSMutableArray *_dataArr;
    
    NSMutableArray *_deleteArr;
    
    int _currentPage;
    
    id _mainData;
    
    BOOL _hasMore;
    
    BOOL _isLoading;
}
@end

@implementation ZWDMyFavTieViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    _dataArr = [NSMutableArray new];
    _deleteArr = [NSMutableArray new];
    // Do any additional setup after loading the view.
    [self setup];
    [self loadData];
    [self showUI];
    [self showNavi];
    
}
- (void)showNavi{
    //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editAction)];
    //self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self creatMyEditButton];

}

#pragma mark - 创建编辑按钮
- (void)creatMyEditButton{
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"编辑" style:UIBarButtonItemStylePlain target:self action:@selector(myButtonClick:)];
    self.navigationItem.rightBarButtonItem = item;
}
- (void)myButtonClick:(UIBarButtonItem *)item{
    //    NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=forumdisplay&page=%d%@%@",kApiVersion ,_currentPage, [NSString stringWithFormat:@"&fid=%@",self.fid], self.orderBy];

    if (_tableView.editing) {
        item.title = @"编辑";
//        NSLog(@"删除操作");
        //http://svnbang.dahe.cn/api/mobile/?version=3&module=favthread&id=7783838
#warning 删除收藏帖子－－－－需优化
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            if (_deleteArr.count > 0) {
                for (int i = 0; i < _deleteArr.count; i++) {
                    NSDictionary *dict = _deleteArr[i];
                    NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=delfav&favid=%@&formhash=%@",kApiVersion,dict[@"favid"],[CDFMe shareInstance].formHash];
//                    NSLog(@"%@",url);
                    [[HXHttpClient shareInstance]grabURL:url success:^(id responseObject) {
//                        NSLog(@"%@",responseObject);
                        if ([responseObject[@"messageval"] isEqualToString:@"do_success"]) {
//                            NSLog(@"删除成功");
                        }
                        
                    } fail:^(NSError *error) {
//                        NSLog(@"shibai");
                        
                    }];
                }
            }
        });
        if (_deleteArr.count > 0) {
            for (int i = 0; i < _deleteArr.count; i++) {
                NSDictionary *dict = _deleteArr[i];
                [_dataArr removeObject:dict];
            }
            [_tableView reloadData];
        }
        
    }else{
        item.title = @"删除";

    }
    [_tableView setEditing:!_tableView.editing animated:YES];
}
#pragma mark - 删除
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    NSLog(@"%d",indexPath.row);
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == _dataArr.count) {
        return UITableViewCellEditingStyleNone;
    }
    
    return UITableViewCellEditingStyleDelete|UITableViewCellEditingStyleInsert;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == _dataArr.count) {
        return;
    }
    
    if (tableView.editing) {
//        NSLog(@"编辑状态");
        [_deleteArr addObject:_dataArr[indexPath.row]];
        
    }else{
//        NSLog(@"非编辑状态");
        UIStoryboard *s = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        CDFDisplayViewController *displayVC = [s instantiateViewControllerWithIdentifier:@"DisplayViewContoller"];
        displayVC.tid = _dataArr[indexPath.row][@"id"];
        [self.navigationController pushViewController:displayVC animated:YES];
    }
}
    
- (void)loadData{
    if (_isLoading) {
        return;
    }
    _isLoading = YES;
    //    NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=topicnew&page=%d%@%@",kApiVersion ,_currentPage, self.selectedForum ? [NSString stringWithFormat:@"&fid=%@",_selectedForum[@"fid"]] : @"", _orderBy ? [NSString stringWithFormat:@"&order=%@",_orderBy] : @"&order=dateline"];
    [SVProgressHUD showWithStatus:@"加载中"];
    NSString *url = [NSString stringWithFormat:@"%@api/mobile/?version=%@&module=myfavthread&page=%d",kBaseURL,kApiVersion,_currentPage];
    [[HXHttpClient shareInstance]grabURL:url success:^(id responseObject) {
        _isLoading = NO;
//        NSLog(@"%@",responseObject[@"Variables"][@"list"]);
        if ([responseObject[@"Variables"][@"list"] isKindOfClass:[NSArray class]]) {
            _mainData = responseObject[@"Variables"];
            
            [_dataArr addObjectsFromArray:responseObject[@"Variables"][@"list"]];
            [SVProgressHUD showSuccessWithStatus:@"下载成功" duration:2];
            [_tableView reloadData];
            
            if ([_mainData[@"count"] integerValue] > _dataArr.count) {
                _hasMore = YES;
            }else{
                _hasMore = NO;
            }
        }else{
            [SVProgressHUD showErrorWithStatus:responseObject[@"Message"][@"messagestr"] duration:3];
        }
        
        
    } fail:^(NSError *error) {
        _isLoading = NO;
//        NSLog(@"下载失败");
        [SVProgressHUD showErrorWithStatus:@"下载失败" duration:2];
        if (_currentPage > 1) {
            _currentPage --;
        }
    }];
    
    
}
- (void)setup{
    _currentPage = 1;
    _hasMore = YES;
    _isLoading = NO;
}
- (void)showUI{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, KDeviceSizeWidth, KDeviceSizeHeight - 64 - 49) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView registerClass:[IndexViewCell class] forCellReuseIdentifier:@"ZWDMYFAVCELL"];
    [self.view addSubview:_tableView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)loadMore{
    if (_hasMore && !_isLoading) {
        _currentPage ++;
        [self loadData];
    }
}
#pragma  mark - tableViewDelegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger count;
    
    
//    NSLog(@"%d---%d",indexPath.row,_dataArr.count);
    if (indexPath.row == _dataArr.count && _dataArr.count > 0) {
        //加载更多cell
        static NSString *cellID = @"ZWDLoadMoreCell";
        
        ZWDLoadMoreCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"ZWDLoadMoreCell" owner:self options:nil]lastObject];
        }
        
        //if ([self.mainData[@"forum_threadlist"][@"count"] integerValue] > self.dataSource.count) {
        if (_hasMore){
            cell.contentLable.text = @"上拉加载更多……";
            [cell.activty startAnimating];
            [self loadMore];
        }else{
            [cell.activty stopAnimating];
            cell.contentLable.text = @"已加载全部";
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    
    IndexViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"ZWDMYFAVCELL" forIndexPath:indexPath];
    cell.avataImage.delegate = self;
    cell.avataImage.layer.cornerRadius = 20;
    cell.contentLabel.text = _dataArr[indexPath.row][@"title"];
    [cell.avataImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://bang.dahe.cn/uc_server/avatar.php?uid=%@&size=big",_dataArr[indexPath.row][@"authorid"]]] placeholderImage:nil];
    
    CGRect frame = cell.contentLabel.frame;
    CGFloat heightOfLabel = [IndexViewCell detailTextHeight:_dataArr[indexPath.row][@"title"]];
    if (heightOfLabel < 20.0) {
        heightOfLabel = heightOfLabel + 10;
    }
    cell.contentLabel.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, heightOfLabel);
    cell.contentLabel.numberOfLines = 0;
    cell.contentLabel.font = [UIFont fontWithName:nil size:15];
    cell.replyAndViewsView.frame = CGRectMake(cell.contentLabel.frame.origin.x, cell.contentLabel.frame.origin.y + cell.contentLabel.frame.size.height + 12, 100, 10);
    cell.replyAndViewLable.text = [NSString stringWithFormat:@"%@   浏览%@",_dataArr[indexPath.row][@"replies"], _dataArr[indexPath.row][@"views"]?_dataArr[indexPath.row][@"views"]:@"0"];
    
    CGFloat heightOfCell = [IndexViewCell heightForCellWithStr:_dataArr[indexPath.row][@"title"]];
    CGRect frameOfAvatar = cell.avataImage.frame;
    cell.avataImage.frame = CGRectMake(frameOfAvatar.origin.x, (heightOfCell - (KAvataImageHeightOrWidth + 17)) / 2.0 , frameOfAvatar.size.width, frameOfAvatar.size.height);
    
    CGRect avataFrame = cell.avataImage.frame;
    cell.nameLabel.frame = CGRectMake(avataFrame.origin.x, avataFrame.origin.y + avataFrame.size.height + 2, 100, 15);
    cell.nameLabel.text = _dataArr[indexPath.row][@"author"];
    CGPoint pointOfNameLabel = CGPointMake(cell.avataImage.center.x, cell.nameLabel.center.y);
    [cell.nameLabel setCenter:pointOfNameLabel];
//    NSLog(@"%@", _dataArr[indexPath.row][@"dateline"]);
    NSInteger dateline = [_dataArr[indexPath.row][@"dateline"] integerValue];
//    NSLog(@"%d", dateline);
    NSString *time = [ZWDMyFavTieViewController formatDate:dateline];
    cell.timeLabel.text = time;
    CGRect timeLabelFrame = cell.replyAndViewsView.frame;
    cell.timeLabel.frame = CGRectMake(timeLabelFrame.origin.x + 95, timeLabelFrame.origin.y - 3, 100, 15);
    cell.timeLabel.font = [UIFont fontWithName:nil size:12];
    cell.timeLabel.textAlignment = NSTextAlignmentRight;
    cell.timeLabel.textColor = [UIColor lightGrayColor];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (_dataArr.count == 0) {
        return 0;
    }
    
    if (_dataArr.count <= 10) {
        return _dataArr.count;
    }
    
    return _dataArr.count+1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == _dataArr.count) {
        return 60;
    }
    
    return [ZWDMyFavTieViewController heightForCellWithStr:_dataArr[indexPath.row][@"title"]];
}
+ (CGFloat)heightForCellWithStr:(NSString *)str {
    return (CGFloat)fmaxf(80.0f, (float)[self detailTextHeight:str] + 45.0f);
}
+ (CGFloat)detailTextHeight:(NSString *)text {
    CGRect rectToFit = [text boundingRectWithSize:CGSizeMake(KContentLabelWidth, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15.0f]} context:nil];
//    NSLog(@"rectToFit:%f", rectToFit.size.height);
    return rectToFit.size.height;
}




//点击cell上的头像 实现的代理方法
- (void)handleCircleImageViewTapGesture:(UITapGestureRecognizer *)gesture {
    IndexViewCell *cell = (IndexViewCell *)gesture.view.superview;
    NSIndexPath *path = [_tableView indexPathForCell:cell];
    NSString *str = [NSString stringWithFormat:@"section:%d, row:%d, superClass:%@", path.section, path.row, gesture.view.superview];
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CDFMeViewController *vc = [s instantiateViewControllerWithIdentifier:@"userViewController"];
    vc.uid = _dataArr[path.row][@"authorid"];
//    NSLog(@"%@",vc.uid);
//    [(CDFTabBarController *)[[UIApplication sharedApplication].delegate window].rootViewController setHidden:YES];//隐藏tabbar
    //[(CDFTabBarController *)[[UIApplication sharedApplication].delegate window].rootViewController setHidden:YES];//隐藏tabbar
    [self.navigationController pushViewController:vc animated:YES];
}
+ (NSString *)formatDate:(NSTimeInterval)timeInterval {
    NSDate *myDate = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [NSLocale currentLocale];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm";
    NSString *dateStr = [dateFormatter stringFromDate:myDate];//目标时间, 字符串格式
    NSDate *nowDate = [NSDate date];
    NSString *nowDateStr = [dateFormatter stringFromDate:nowDate]; //当前的时间, 字符串格式
    NSTimeInterval nowTime = [[NSDate date] timeIntervalSince1970]; //当前时间到1970年的秒数
    long long int nowtime = (long long) nowTime;
    long long int cha = nowtime - timeInterval;
//    NSLog(@"%@", dateStr);
    //对时间进行优化显示
    if ([[dateStr substringWithRange:NSMakeRange(0, 4)] isEqualToString:[nowDateStr substringWithRange:NSMakeRange(0, 4)]]) {
        if ([[dateStr substringWithRange:NSMakeRange(5, 5)] isEqualToString:[nowDateStr substringWithRange:NSMakeRange(5, 5)]]) {
            if (cha / 3600.0 <= 1) {
                if (cha/60 == 0) {
                    dateStr = [NSString stringWithFormat:@"%ld秒之前", (long int)cha];
                } else {
                    dateStr = [NSString stringWithFormat:@"%ld分钟之前", (long int)cha/60];
                }
            }else if (cha / 3600.0<= 2 && cha / 3600.0 > 1) {
                dateStr = @"1小时前";
            }else if (cha / 3600.0<=3 && cha / 3600.0 > 2) {
                dateStr = @"2小时前";
            } else {
                dateStr = [NSString stringWithFormat:@"今天%@", [dateStr substringWithRange:NSMakeRange(11, 5)]];
                //                NSLog(@"今天 %@", [dateStr substringWithRange:NSMakeRange(11, 5)]);
            }
        }else if ([[nowDateStr substringWithRange:NSMakeRange(8, 2)] integerValue] - [[dateStr substringWithRange:NSMakeRange(8, 2)] integerValue] == 1) {
//            NSLog(@"昨天%@", [dateStr substringWithRange:NSMakeRange(11, 5)]);
            dateStr = [NSString stringWithFormat:@"昨天%@", [dateStr substringWithRange:NSMakeRange(11, 5)]];
        } else {
//            NSLog(@"%@", [dateStr substringWithRange:NSMakeRange(5, 11)]);
//            NSLog(@"%@", [dateStr substringWithRange:NSMakeRange(5, 5)]);
            //            dateStr = [NSString stringWithFormat:@"%@", [dateStr substringWithRange:NSMakeRange(5, 11)]]; //如果是前天或者更早的日期, 显示日期和具体时间
            dateStr = [NSString stringWithFormat:@"%@", [dateStr substringWithRange:NSMakeRange(5, 5)]];//如果是前天或者更早的日期, 只显示日期, 不显示具体时间
        }
    } else {
//        NSLog(@"dateStr:%@", dateStr);
        dateStr = [dateStr substringWithRange:NSMakeRange(0, 10)];
    }
    return dateStr;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
