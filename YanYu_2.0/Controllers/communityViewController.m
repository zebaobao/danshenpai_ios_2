//
//  communityViewController.m
//  yanyu_2.0
//
//  Created by Case on 15/4/7.
//
//

#import "communityViewController.h"
#import "CDFIndexViewController.h"
#import "PlateViewController.h"

#import "ZWDMyFavTieViewController.h"
#import "ZWDSearchViewController.h"

#define LEFTWEDITH [UIScreen mainScreen].bounds.size.width/4

@interface communityViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_leftTableView;
    UITableView *_rightTableView;
    NSMutableArray *_dataArrLeft;
    NSMutableArray *_dataArrRight;
    NSMutableArray *_dataFav;//以关注帖子id
    NSMutableArray *_dataFavDict;
    
    BOOL _isRefresh;
    
    NSIndexPath *_leftCurrentIndexPath;
 
}
@end

@implementation communityViewController
- (void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:FAV_SUCCESS object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:FAV_DELETESUCCESS object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLOGIN_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLOGOUT_NOTIFICATION object:nil];
}
- (void)viewDidLoad {
    _leftCurrentIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self registeNotifacion];
    // Do any additional setup after loading the view.
    _isRefresh = NO;
    

    
    [self creatTableView];
    [self loadData];
    [self UIConfig];
}
- (void)UIConfig{
    self.view.backgroundColor = [UIColor whiteColor];

    UILabel *titleLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    titleLable.text = @"圈子";
    titleLable.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    titleLable.textAlignment = NSTextAlignmentCenter;
    titleLable.textColor = RGBACOLOR(250, 66, 4, 1.0);
    self.navigationItem.titleView = titleLable;
    
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    button1.frame = CGRectMake(0, 0, 25, 25);
    [button1 setImage:[UIImage imageNamed:@"icon_01"] forState:UIControlStateNormal];
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc]initWithCustomView:button1];
    
    
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    button2.frame = CGRectMake(0, 0, 25, 25);
    [button2 setImage:[UIImage imageNamed:@"icon_02"] forState:UIControlStateNormal];
    UIBarButtonItem *item2 = [[UIBarButtonItem alloc]initWithCustomView:button2];
    [button2 addTarget:self action:@selector(searchClick) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItems = @[item2];
    
    [button1 addTarget:self action:@selector(ceshi) forControlEvents:UIControlEventTouchUpInside];
}
#pragma mark - 点击搜索
- (void)searchClick{
    ZWDSearchViewController *vc = [[ZWDSearchViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)ceshi{
    ZWDMyFavTieViewController *vc = [ZWDMyFavTieViewController new];
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)favSuccess:(NSNotification *)noti{
    //关注成功接受通知
   // NSLog(@"%@",noti.userInfo);
    [_dataFav addObject:noti.userInfo[@"fid"]];
    [self refresh];
    [self loadData];
    
    
}
- (void)favDeleteSuccess:(NSNotification *)noti{
    //取消关注成功
    [_dataFav removeObject:noti.userInfo[@"fid"]];
    [self refresh];
}
- (void)registeNotifacion{
    //注册关注成功通知，关注成功后将以关注板块fid加入_datafav数据源数组
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(favSuccess:) name:FAV_SUCCESS object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(favDeleteSuccess:) name:FAV_DELETESUCCESS object:nil];
    
    //注册登录成功通知，在登录成功时刷新当前页面数据
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSuccess) name:kLOGIN_NOTIFICATION object:nil];
    
    //注册推出通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logoutSuccess) name:kLOGOUT_NOTIFICATION object:nil];
}
- (void)logoutSuccess{
    [_dataFav removeAllObjects];
    [_dataFavDict removeAllObjects];
    [_rightTableView reloadData];
}
- (void)loginSuccess{
    [self performSelector:@selector(loadData) withObject:nil afterDelay:0.5];
    //[self loadData];
}
- (void)loadData{
  
    //[self refresh];
    _dataArrLeft = [NSMutableArray new];
    _dataArrRight = [NSMutableArray new];
    _dataFav = [NSMutableArray new];
    _dataFavDict = [NSMutableArray new];
    
    NSString *favUrl = [NSString stringWithFormat:@"api/mobile/?version=%@&module=myfavforum",kApiVersion];
    
    if ([self isDownFav]) {
        [[HXHttpClient shareInstance]grabURL:favUrl success:^(id responseObject) {
//            NSLog(@"%@",responseObject);
            if ([responseObject[@"Variables"][@"list"] isKindOfClass:[NSArray class]]) {
                for (NSDictionary *dict in responseObject[@"Variables"][@"list"]) {
                    [_dataFav addObject:dict[@"id"]];
                }
                _dataFavDict = [NSMutableArray arrayWithArray:responseObject[@"Variables"][@"list"]];
            }
            
            
            [self writeFav:responseObject];
            [self downList];
        } fail:^(NSError *error) {
            //[self getData];

        }];
    }else{

        NSDictionary *responseObject = [self getFavData];
        if ([responseObject[@"Variables"][@"list"] isKindOfClass:[NSArray class]]) {
            for (NSDictionary *dict in responseObject[@"Variables"][@"list"]) {
                [_dataFav addObject:dict[@"id"]];
            }
            _dataFavDict = [NSMutableArray arrayWithArray:responseObject[@"Variables"][@"list"]];
        }
        
        [self downList];
    }
    
    
}
- (void)downList{
    NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=forumindex",kApiVersion];
    if ([self isDownFromInter]) {
        [[HXHttpClient shareInstance] grabURL:url success:^(id responseObject) {

            if (responseObject) {
                [self write:responseObject];
            }
//            NSLog(@"%@",responseObject);
            if (responseObject[@"Variables"]) {
//                NSLog(@"%@",responseObject[@"Variables"][@"catlist"]);
                NSArray *arr =[responseObject[@"Variables"] allKeys];
                if ([arr containsObject:@"favfids"]) {
//                    [_dataFav addObjectsFromArray:responseObject[@"Variables"][@"favfids"]];
                }
                if (responseObject[@"Variables"][@"forum_favlist"]) {
                    
                    /*
                     "forum_favlist" =         {
                     6079 =             {
                     dateline = 1428562530;
                     description = "";
                     favid = 6079;
                     id = 1006;
                     idtype = fid;
                     spaceuid = 0;
                     title = "\U773c\U9047\U7cbe\U5f69";
                     uid = 14633085;
                     };
                     */
                    
//                    _dataFavDict = [NSMutableDictionary dictionaryWithDictionary:responseObject[@"Variables"][@"forum_favlist"]];
                }
                if (responseObject[@"Variables"][@"catlist"]) {
                    [_dataArrLeft removeAllObjects];
                    [_dataArrLeft addObjectsFromArray:responseObject[@"Variables"][@"catlist"]];
                    [_leftTableView reloadData];
                    [_leftTableView selectRowAtIndexPath:_leftCurrentIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
                }
                if (responseObject[@"Variables"][@"forumlist"]) {
                    [_dataArrRight removeAllObjects];

                    [_dataArrRight addObjectsFromArray:responseObject[@"Variables"][@"forumlist"]];
                    [_rightTableView reloadData];
                }
                
            }
            
        } fail:^(NSError *error) {

//            NSLog(@"下载失败");
        }];
    }else{

        NSDictionary *responseObject = [self getData];
//        NSLog(@"%@",responseObject);
        if (responseObject[@"Variables"]) {
//            NSLog(@"%@",responseObject[@"Variables"][@"catlist"]);
            NSArray *arr =[responseObject[@"Variables"]allKeys];
            if ([arr containsObject:@"favfids"]) {
                [_dataFav addObjectsFromArray:responseObject[@"Variables"][@"favfids"]];
            }
            if (responseObject[@"Variables"][@"forum_favlist"]) {
                
                /*
                 "forum_favlist" =         {
                 6079 =             {
                 dateline = 1428562530;
                 description = "";
                 favid = 6079;
                 id = 1006;
                 idtype = fid;
                 spaceuid = 0;
                 title = "\U773c\U9047\U7cbe\U5f69";
                 uid = 14633085;
                 };
                 */
                
               // _dataFavDict = [NSMutableDictionary dictionaryWithDictionary:responseObject[@"Variables"][@"forum_favlist"]];
            }
            if (responseObject[@"Variables"][@"catlist"]) {
                [_dataArrLeft removeAllObjects];
                [_dataArrLeft addObjectsFromArray:responseObject[@"Variables"][@"catlist"]];
                [_leftTableView reloadData];
                [_leftTableView selectRowAtIndexPath:_leftCurrentIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
            }
            if (responseObject[@"Variables"][@"forumlist"]) {
                [_dataArrRight removeAllObjects];
                [_dataArrRight addObjectsFromArray:responseObject[@"Variables"][@"forumlist"]];
                [_rightTableView reloadData];
            }
            
        }
        
    }

}
#pragma mark - 数据缓存相关方法 起
- (void)writeFav:(NSDictionary *)dict{
    //把已关注信息保存至本地
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachesDir = [paths objectAtIndex:0];

    
    NSString *path = [NSString stringWithFormat:@"%@/CommunityFavCache%@.plist",cachesDir,[CDFMe shareInstance].uid];
    
        NSLog(@"%@",cachesDir);
    
    NSString *jsonStr = [Common dictionaryToJSON:dict];
    //    NSString *path = [NSString stringWithFormat:@"%@/Documents/searchKeys%d.plist",NSHomeDirectory(),_changeBtn.tag];
    NSData *data = [jsonStr dataUsingEncoding:NSUTF8StringEncoding];
    
    if ([data writeToFile:path atomically:NO]) {
//        NSLog(@"写入成功");
    }
    //[_dataFav writeToFile:path atomically:YES];
    
}
- (NSDictionary *)getFavData{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachesDir = [paths objectAtIndex:0];
    

    NSString *path = [NSString stringWithFormat:@"%@/CommunityFavCache%@.plist",cachesDir,[CDFMe shareInstance].uid];
    
    
    return [NSDictionary dictionaryWithContentsOfFile:path];
    
}
- (BOOL)isDownFav{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachesDir = [paths objectAtIndex:0];
    

    NSString *path = [NSString stringWithFormat:@"%@/CommunityFavCache%@.plist",cachesDir,[CDFMe shareInstance].uid];
    
    
    
    NSFileManager *fm = [NSFileManager defaultManager];
    if ([fm fileExistsAtPath:path]) {
        NSDictionary *attrDict = [fm fileAttributesAtPath:path traverseLink:YES];
        NSDate *date = [attrDict fileCreationDate];
//        NSLog(@"date--%@",date);
        NSDate *now = [NSDate date];
        
        NSTimeInterval time=[now timeIntervalSinceDate:date];
        
        if (time > 24 * 60 * 60 ) {//如果大于两天
            return YES;
        }else{
            
            return NO;
        }
    }else{
        return YES;
    }
}

//点击关注或取消关注时删除本地文件，下次重新下载
- (void)refresh{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachesDir = [paths objectAtIndex:0];
    

    
    NSString *path = [NSString stringWithFormat:@"%@/CommunityCache%@.plist",cachesDir,[CDFMe shareInstance].uid];
    NSString *path2 = [NSString stringWithFormat:@"%@/CommunityFavCache%@.plist",cachesDir,[CDFMe shareInstance].uid];

    NSFileManager *fm = [NSFileManager defaultManager];
    [fm removeItemAtPath:path error:nil];
    [fm removeItemAtPath:path2 error:nil];

}
- (BOOL)isDownFromInter{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachesDir = [paths objectAtIndex:0];
    

    
    NSString *path = [NSString stringWithFormat:@"%@/CommunityCache%@.plist",cachesDir,[CDFMe shareInstance].uid];
    NSFileManager *fm = [NSFileManager defaultManager];
    if ([fm fileExistsAtPath:path]) {
        NSDictionary *attrDict = [fm fileAttributesAtPath:path traverseLink:YES];
        NSDate *date = [attrDict fileCreationDate];
//        NSLog(@"date--%@",date);
        NSDate *now = [NSDate date];
        
        NSTimeInterval time=[now timeIntervalSinceDate:date];
        
        if (time > 24 * 60 * 60 ) {//如果大于两天
            return YES;
        }else{
            return NO;
        }
    }else{
        return YES;
    }
}
- (void)write:(NSDictionary *)dict{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachesDir = [paths objectAtIndex:0];
    

    
    NSString *path = [NSString stringWithFormat:@"%@/CommunityCache%@.plist",cachesDir,[CDFMe shareInstance].uid];
//    NSLog(@"%@",dict);
    NSString *jsonStr = [Common dictionaryToJSON:dict];
    //    NSString *path = [NSString stringWithFormat:@"%@/Documents/searchKeys%d.plist",NSHomeDirectory(),_changeBtn.tag];
    NSData *data = [jsonStr dataUsingEncoding:NSUTF8StringEncoding];
    
    if ([data writeToFile:path atomically:NO]) {
//        NSLog(@"写入成功");
    }
    
}
- (NSDictionary *)getData{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachesDir = [paths objectAtIndex:0];
    

    
    NSString *path = [NSString stringWithFormat:@"%@/CommunityCache%@.plist",cachesDir,[CDFMe shareInstance].uid];
    NSData *data = [[NSData alloc]initWithContentsOfFile:path];
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    return dict;
}
#pragma mark - 数据缓存相关方法 至
- (void)creatTableView{
    _leftTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, LEFTWEDITH, [UIScreen mainScreen].bounds.size.height - 64 - 49) style:UITableViewStylePlain];
    _leftTableView.dataSource = self;
    _leftTableView.delegate = self;
    _leftTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _leftTableView.showsVerticalScrollIndicator = NO;
    _leftTableView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:_leftTableView];
    
    _rightTableView = [[UITableView alloc]initWithFrame:CGRectMake(LEFTWEDITH, 64, [UIScreen mainScreen].bounds.size.width - LEFTWEDITH, _leftTableView.bounds.size.height) style:UITableViewStylePlain];
    _rightTableView.dataSource = self;
    _rightTableView.delegate = self;
    _rightTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_rightTableView];
}
#pragma mark - TableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == _leftTableView) {
        [_rightTableView reloadData];
        _leftCurrentIndexPath = indexPath;
    }else{
        //_dataArrRight[j][@"fid"]
//        UIStoryboard *s = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
//        CDFIndexViewController *vc = [s instantiateViewControllerWithIdentifier:@"CDFIndexViewController"];
//        NSDictionary *dict = @{@"color":@"ffa300",@"default":@"0",@"fid":_dataArrRight[indexPath.row][@"fid"],@"gid":@"441",@"icon":@"food",@"name":_dataArrRight[indexPath.row][@"name"],@"show":@"1",@"typeid":@"138"};
//        vc.selectedForum = dict;
//        [self.navigationController pushViewController:vc animated:YES];
        NSIndexPath *leftIndex = _leftTableView.indexPathForSelectedRow;
        NSArray *arr = _dataArrLeft[leftIndex.row][@"forums"];
//        NSLog(@"%@",arr);
        NSString *name;
        NSString *fid = arr[indexPath.row];
        for (int j = 0; j < _dataArrRight.count; j++) {
            if ([_dataArrRight[j][@"fid"] isEqualToString:fid]) {
//                NSLog(@"%d",[_dataFav containsObject:fid]);
                //[cell showData:_dataArrRight[j] isGuZhu:[_dataFav containsObject:fid]];
                name =_dataArrRight[j][@"name"];
            }
        }
        
        
        PlateViewController *plateVC = [[PlateViewController alloc] initWithStyle:UITableViewStylePlain];
        
        //plateVC.fid = [NSString stringWithFormat:@"%@", _dataArrRight[indexPath.row][@"fid"]];
        plateVC.fid = arr[indexPath.row];
        plateVC.title = name;
//        NSLog(@"%@%@",plateVC.fid,plateVC.title);
        [self.navigationController pushViewController:plateVC animated:YES];
    }
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == _leftTableView) {
        return _dataArrLeft.count;
    }else{
        NSIndexPath *index = [_leftTableView indexPathForSelectedRow];
        if (index) {
            return [_dataArrLeft[index.row][@"forums"] count];

        }else{
            return 0;
        }
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == _leftTableView) {
        return 44;
    }else{
        return 65;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == _leftTableView) {
        static NSString *LeftCellID = @"ZWDCommLeftCell";
        ZWDCommLeftCell *cell = [tableView dequeueReusableCellWithIdentifier:LeftCellID];
        if (!cell) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"ZWDCommLeftCell" owner:self options:nil]lastObject];
        }
        cell.backgroundColor = RGBACOLOR(241, 241, 241, 1.0);
        UIView *view = [[UIView alloc]initWithFrame:cell.bounds];
        view.backgroundColor = [UIColor whiteColor];
        cell.selectedBackgroundView = view;
        cell.nameLable.text = _dataArrLeft[indexPath.row][@"name"];
        return cell;
    }else{
        static NSString *RightCellID = @"ZWDCommRightCell";
        ZWDCommRightCell *cell = [tableView dequeueReusableCellWithIdentifier:RightCellID];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:RightCellID owner:self options:nil]lastObject];
        }
        NSIndexPath *leftIndex = _leftTableView.indexPathForSelectedRow;
        NSArray *arr = _dataArrLeft[leftIndex.row][@"forums"];
//        NSLog(@"%@",arr);

        NSString *fid = arr[indexPath.row];
        for (int j = 0; j < _dataArrRight.count; j++) {
            if ([_dataArrRight[j][@"fid"] isEqualToString:fid]) {
//                NSLog(@"%d",[_dataFav containsObject:fid]);
                [cell showData:_dataArrRight[j] isGuZhu:[_dataFav containsObject:fid]];
            }
        }
        cell.dataDict = _dataFavDict;
//        NSLog(@"%@",_dataFavDict);
        cell.delegate = self;
        return cell;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



@end
