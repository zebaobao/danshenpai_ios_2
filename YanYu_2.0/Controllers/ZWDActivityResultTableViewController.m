//
//  ZWDActivityResultTableViewController.m
//  yanyu
//
//  Created by Case on 15/4/22.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "ZWDActivityResultTableViewController.h"
#import "ZWDActivityResultCell.h"


@interface ZWDActivityResultTableViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;
}
@end

@implementation ZWDActivityResultTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self showUI];
    
}
- (void)showUI{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KDeviceSizeWidth, self.view.bounds.size.height) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    if (self.dataSource.count == 0) {
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200, 40)];
        lable.font = [UIFont systemFontOfSize:17];
        lable.center = CGPointMake(KDeviceSizeWidth/2, 200);
        lable.text = [NSString stringWithFormat:@"暂时还没有哦！"];
        lable.textColor = TITLEGRAY;
        lable.textAlignment = NSTextAlignmentCenter;
        [self.view addSubview:lable];
        
        
    }
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}
#pragma mark - tableViewdelegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellID = @"ZWDActivityResultCell";
    ZWDActivityResultCell *cell = [tableView dequeueReusableHeaderFooterViewWithIdentifier:cellID];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"ZWDActivityResultCell" owner:self options:nil]firstObject];
    }
    [cell showdata:(NSDictionary *)self.dataSource[indexPath.row]];
    
     return cell;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CDFMeViewController *vc = [s instantiateViewControllerWithIdentifier:@"userViewController"];
    
    vc.uid = self.dataSource[indexPath.row][@"uid"];
    
    [self.navigationController pushViewController:vc animated:YES];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
