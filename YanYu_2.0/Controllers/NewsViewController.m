//
//  NewsViewController.m
//  yanyu
//
//  Created by Case on 15/4/10.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "NewsViewController.h"
#define URL @"http://4g.dahe.cn/"

//#define URL @"https://www.baidu.com/"

@interface NewsViewController ()<UIWebViewDelegate>
{
    UIWebView *_webView;
    
    NSURLRequest *_urlRequest;
}
@end

@implementation NewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self showUI];
}
- (void)showUI{
    _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - 49 - 64 - 50)];
    _webView.delegate =self;
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:URL]]];
    [self.view addSubview:_webView];
    
    __weak UIScrollView *scrollView = _webView.scrollView;
    [scrollView addLegendHeaderWithRefreshingBlock:^{
        if (_urlRequest) {
            [_webView loadRequest:_urlRequest];
        }else{
            [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:URL]]];
        }
    }];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [webView.scrollView.header endRefreshing];
   [ _webView stringByEvaluatingJavaScriptFromString:@"ad_close()"];
}
- (void)webViewDidStartLoad:(UIWebView *)webView{
  //[_webView stringByEvaluatingJavaScriptFromString:@"ad_close()"];

}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    _urlRequest = request;
    //[_webView stringByEvaluatingJavaScriptFromString:@"ad_close()"];
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
