//
//  JuJCViewController.h
//  yanyu
//
//  Created by Case on 15/4/10.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "ParentViewController.h"
#import "CDFIndexViewController.h"
#import "ShowViewController.h"
#import "NewsViewController.h"
#import "CDFPostingViewController.h"
#import "CDFNavigationController.h"
#import "ActivityTableViewController.h"

@interface JuJCViewController : ParentViewController<UIAlertViewDelegate>

@end
