//
//  ZWDFirstLoginViewController.m
//  yanyu
//
//  Created by Case on 15/4/23.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "ZWDFirstLoginViewController.h"
#import "ZWDFirstLoginView.h"

#define BJWidth 5
#define VIEWHEIGHT 80
//#define VIEWHEIGHT (KDeviceSizeWidth - 4*BJWidth)/3

@interface ZWDFirstLoginViewController ()<ZWDFirstLoginViewDelegate>
{
    NSMutableArray *_allForum;
    NSMutableArray *_forum;
    
    NSMutableArray *_postArr;
}
@end

@implementation ZWDFirstLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _postArr = [NSMutableArray new];
    // Do any additional setup after loading the view.
    [self loadData];
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 20, KDeviceSizeWidth, 44)];
    view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:view];
    
    self.view.backgroundColor = [Common colorWithHex:@"#F7F7F7" alpha:1];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 20, 100, 44)];
    label.text = @"关注板块";
    label.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label];
    label.center = CGPointMake(KDeviceSizeWidth/2, 42);
    
    
    [self addButton];

}
- (void)addButton{
    CGFloat height = 20.f;
    if (KDeviceSizeHeight > 490) {
        height = 44.f;
    }
    
    
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    [button setTitle:@"下一步" forState:UIControlStateNormal];
    [self.view addSubview:button];
    [button setTitleColor:RGBACOLOR(249, 63, 0, 1) forState:UIControlStateNormal];
    button.frame = CGRectMake(KDeviceSizeWidth - 110, 20 , 100, 44);
    //button.layer.masksToBounds = YES;
    //button.layer.cornerRadius = 5;
    //button.layer.borderWidth = 0.5;
    //button.layer.borderColor = TITLEGRAY.CGColor;
    [button addTarget:self action:@selector(ButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *changeButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [changeButton setTitle:@"换一批看看" forState:UIControlStateNormal];
    changeButton.frame = CGRectMake(20 , KDeviceSizeHeight - 84, (KDeviceSizeWidth - 40), 44);
    //[self.view addSubview:changeButton];
    changeButton.layer.masksToBounds = YES;
    changeButton.layer.cornerRadius = 5;
    //changeButton.layer.borderWidth = 0.5;
    //changeButton.layer.borderColor = TITLEGRAY.CGColor;
    [changeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [changeButton addTarget:self action:@selector(showUI) forControlEvents:UIControlEventTouchUpInside];
    changeButton.backgroundColor = RGBACOLOR(252, 146, 37, 1.f);
    //完成
    
    UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(0, KDeviceSizeHeight - 34, KDeviceSizeWidth, 20)];
    lable.text = @"关注后到 聚精彩->关注中查看";
    lable.textAlignment = NSTextAlignmentCenter;
    lable.textColor = TITLEGRAY;
    lable.font = [UIFont systemFontOfSize:13];
    //[self.view addSubview:lable];
    
}
- (void)ButtonClick{
#pragma mark - 在此批量关注
    //favids
    NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=favforums&formhash=%@",kApiVersion,[CDFMe shareInstance].formHash];
//    NSLog(@"%@",_postArr);
    NSDictionary *dict = @{@"favids":_postArr};
    [[HXHttpClient shareInstance]postURL:url postData:dict success:^(id responseObject) {
//        NSLog(@"%@",responseObject);
//        [SVProgressHUD showWithStatus:responseObject[@"Message"][@"messagestr"]];
    } fail:^(NSError *error) {
        
    }];
    
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)loadData{
    NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=forumindex",kApiVersion];
    
    [[HXHttpClient shareInstance]grabURL:url success:^(id responseObject) {
        _allForum = [NSMutableArray arrayWithArray:responseObject[@"Variables"][@"forumlist"]];
        _forum = [NSMutableArray arrayWithArray:responseObject[@"Variables"][@"forumlist"]];;
        [self showUI];
        
    } fail:^(NSError *error) {
        
    }];
    
}
- (void)showUI{
    
    //创建安scrollView  将button放到上边
    UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, KDeviceSizeWidth, KDeviceSizeHeight - 64)];
    [self.view addSubview:scrollView];
    scrollView.contentSize = CGSizeMake(0, (_allForum.count + 1) * (VIEWHEIGHT+BJWidth)/2);
    
    
    NSInteger lines;
    if (KDeviceSizeHeight > 490) {
        lines = 10;
    }else{
        lines = 8;
    }
    
    if (_allForum.count < lines) {
        [_allForum addObjectsFromArray:_forum];
    }
    
    for (int i = 0;  i < lines; i++) {
        UIView *v = [self.view viewWithTag:100+i];
        [v removeFromSuperview];
    }
    
    for (int i = 0; i < _forum.count; i++) {
        if (_allForum.count == 0) {
            [_allForum addObjectsFromArray:_forum];
        }
        
        NSInteger SJInt = -1;
        for (int k = 0; k < _allForum.count; k++) {
            NSDictionary *dict = _allForum[k];
            if ([dict[@"good"] boolValue]) {
                SJInt = k;
            }
        }
        
        if (SJInt == -1) {
            SJInt = arc4random()%_allForum.count;
        }
        
        ZWDFirstLoginView *v = [[ZWDFirstLoginView alloc]initWithFrame:CGRectMake(BJWidth+((KDeviceSizeWidth-3*BJWidth)/2 +BJWidth)*(i%2),  10 + (VIEWHEIGHT + BJWidth) * (i/2), (KDeviceSizeWidth-3*BJWidth)/2, VIEWHEIGHT) withDict:_allForum[SJInt]];
        v.delegate = self;
        if ([_postArr containsObject:_allForum[SJInt][@"fid"]]) {
            [v.button setTitle:@"关注成功" forState:UIControlStateNormal];
            
        }else{
            [v.button setTitle:@"关注" forState:UIControlStateNormal];

        }
        [_allForum removeObjectAtIndex:SJInt];
        v.layer.masksToBounds = YES;
        v.layer.cornerRadius = 5;
        [scrollView addSubview:v];
        v.tag = 100 + i;
        v.backgroundColor = [UIColor whiteColor];
        
    }
    
    
}
#pragma mark - ZWDFirstLoginDelegate
- (void)ZWDFirstLoginViewBtnClick:(NSDictionary *)dict{
    //fid
    [_postArr addObject:dict[@"fid"]];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
