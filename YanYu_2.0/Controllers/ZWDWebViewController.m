//
//  ZWDWebViewController.m
//  yanyu
//
//  Created by Case on 15/4/18.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "ZWDWebViewController.h"

@interface ZWDWebViewController ()<UIWebViewDelegate>
{
    NSURLRequest *_urlRequest;
    UIWebView *web;
}
@end

@implementation ZWDWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self showUI];
    
}
static bool isPresent = NO;
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (isPresent) {
        [web loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
        isPresent = NO;
    }
    
    if (![CDFMe shareInstance].isLogin) {
        [[CDFMe shareInstance]wellcomeLogin:self];
        isPresent = YES;
    }
    
}
- (void)showUI{
    web = [[UIWebView alloc]initWithFrame:self.view.bounds];
    web.delegate = self;
    [web loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
    [self.view addSubview:web];
    
    __weak UIScrollView *scrollView = web.scrollView;
    [scrollView addLegendHeaderWithRefreshingBlock:^{
        if (_urlRequest) {
            [web loadRequest:_urlRequest];
        }else{
            [web loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
        }
        
    }];
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    _urlRequest = request;
    return YES;
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [webView.scrollView.header endRefreshing];
    [webView stringByEvaluatingJavaScriptFromString:@"ad_close()"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
