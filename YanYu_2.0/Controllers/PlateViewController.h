//
//  PlateViewController.h
//  yanyu
//
//  Created by caiyee on 15/4/10.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PlateViewController : UITableViewController
@property (nonatomic, copy) NSString *fid;
@property (nonatomic, copy) NSString *title;

@property (nonatomic,assign) BOOL isClickSeg;
@end
