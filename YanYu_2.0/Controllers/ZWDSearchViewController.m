//
//  ZWDSearchViewController.m
//  yanyu
//
//  Created by Case on 15/4/21.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "ZWDSearchViewController.h"
#import "CDFSearchResultViewController.h"

//边距
#define BJWidth 5

@interface ZWDSearchViewController ()<UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
{
    UISearchBar *_searchBar;
    UIButton *_changeBtn;
    
    UITableView *_tableView;
    NSMutableArray *_dataArr;
    
    UIScrollView *_scrollView;
}
@end

@implementation ZWDSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self showUI];
    [self showKeys];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_searchBar becomeFirstResponder];
}
- (void)showKeys{
    for (UIView *v in _scrollView.subviews) {
        [v removeFromSuperview];
    }
    
    for (int i = 0; i<_dataArr.count; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
        button.frame = CGRectMake(BJWidth+((KDeviceSizeWidth-4*BJWidth)/3 +BJWidth)*(i%3), BJWidth + (50 + BJWidth) * (i/3), (KDeviceSizeWidth-4*BJWidth)/3, 50);
        [button setTitle:_dataArr[i] forState:UIControlStateNormal];
        [_scrollView addSubview:button];
        button.titleLabel.numberOfLines = 0;
        [button setTitleColor:TITLEGRAY forState:UIControlStateNormal];
        button.tag = 1000+i;
        button.backgroundColor = RGBACOLOR(237, 237, 237, 1.f);
        [button addTarget:self action:@selector(keyClick:) forControlEvents:UIControlEventTouchUpInside];
        button.layer.masksToBounds = YES;
        button.layer.cornerRadius = 5;
    }
    
    int i = _dataArr.count/3;
    if (_dataArr.count%3 != 0) {
        i++;
    }
    
    if (_dataArr.count > 0) {
        UIButton *clearButton = [UIButton buttonWithType:UIButtonTypeSystem];
        clearButton.frame = CGRectMake(BJWidth, i * (50 + BJWidth) + BJWidth*4, KDeviceSizeWidth - 2*BJWidth, 50);
        [clearButton setTitle:@"清除历史纪录" forState:UIControlStateNormal];
        clearButton.backgroundColor = [UIColor redColor];
        [clearButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [clearButton addTarget:self action:@selector(clearClick) forControlEvents:UIControlEventTouchUpInside];
        [_scrollView addSubview:clearButton];
    }
    _scrollView.contentSize = CGSizeMake(0, i * (50 + BJWidth) + BJWidth + 50 + BJWidth*4);

}
#pragma mark - 关键词点击
- (void)keyClick:(UIButton *)button{
//    NSLog(@"%@",_dataArr[button.tag - 1000]);
    _searchBar.text = button.titleLabel.text;
    [self persent];
    
}
#pragma mark - 清除按钮点击
- (void)clearClick{
    [_dataArr removeAllObjects];
    [self wirtePilst:_dataArr];
    [self showKeys];
    
}
- (void)showUI{
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64 + 70, KDeviceSizeWidth, KDeviceSizeHeight - 64 - 49 - 70)];
    _scrollView.backgroundColor = [UIColor whiteColor];
    _scrollView.bounces = YES;
    [self.view addSubview:_scrollView];
    
    
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, KDeviceSizeWidth, 70)];
    headerView.backgroundColor =RGBACOLOR(237, 237, 237, 1.f);
//    headerView.layer.masksToBounds = YES;
//    headerView.layer.cornerRadius = 8;
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(10, 15, KDeviceSizeWidth - 20, 40)];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.masksToBounds = YES;
    view.layer.cornerRadius = 20;
    view.clipsToBounds = YES;
    [headerView addSubview:view];
    [self.view addSubview:headerView];
    
    _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(60, -1, KDeviceSizeWidth - 20 - 60, 42)];
    _searchBar.backgroundColor = [UIColor whiteColor];
    _searchBar.barTintColor = [UIColor whiteColor];
    _searchBar.delegate = self;
    _searchBar.placeholder = @"搜帖子";
    [view addSubview:_searchBar];
    
    _changeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _changeBtn.frame = CGRectMake(0, -5 , 60, 50);
//    [_changeBtn setTitle:@"帖子" forState:UIControlStateNormal];85 38／／userSel／／tieSel0
    [_changeBtn setImage:[UIImage imageNamed:@"userSel"] forState:UIControlStateNormal];
//    _changeBtn.backgroundColor = RGBACOLOR(199, 199, 204, 1.f);
    _changeBtn.layer.borderWidth = 0.5;
    _changeBtn.layer.borderColor = RGBACOLOR(237, 237, 237, 1.f).CGColor;
    _changeBtn.tag = tieZi;
    [_changeBtn addTarget:self action:@selector(changeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:_changeBtn];
    
    
    /*
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KDeviceSizeWidth, KDeviceSizeHeight) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableHeaderView = headerView;
    [self.view addSubview:_tableView];
     */
    _dataArr = [NSMutableArray arrayWithArray:[self getArrFromPlist]];

}
- (void)changeBtnClick:(UIButton *)button{
    
    
    if (button.tag == user) {
        
        [button setImage:[UIImage imageNamed:@"userSel"] forState:UIControlStateNormal];
        _searchBar.placeholder = @"搜帖子";
        button.tag = tieZi;
    }else{
        [button setImage:[UIImage imageNamed:@"tieSel"] forState:UIControlStateNormal];
        _searchBar.placeholder = @"搜用户";
        button.tag = user;

    }
    _dataArr = [NSMutableArray arrayWithArray:[self getArrFromPlist]];
//    NSLog(@"%@",_dataArr);
    [self showKeys];
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    if (searchBar.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"请输入关键词" duration:2];
        return;
    }
    NSRange range;
    range.location = 0;
    range.length = 1;
    for (int i = 0; i < searchBar.text.length; i++) {
        NSString *s = [searchBar.text substringWithRange:range];
        if (![s isEqualToString:@" "]) {
            break;
        }
        else if (i == searchBar.text.length - 1){
            [SVProgressHUD showErrorWithStatus:@"不能全部为空格" duration:2];
            return;
        }
        range.location ++;
    }
    
    
    if (![_dataArr containsObject:searchBar.text]) {
        if (_dataArr.count == 9) {
            [_dataArr removeObjectAtIndex:0];
        }
        [_dataArr addObject:searchBar.text];
    }
    [self showKeys];
    [self wirtePilst:_dataArr];
    [self persent];
    
}
#pragma mark - tableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArr.count + 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"ZWDSearchCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellID];
    }
    if (_dataArr.count == 0) {
        cell.textLabel.text = @"清除历史纪录";
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
    }else{
        if (indexPath.row < _dataArr.count) {
            cell.textLabel.text = _dataArr[indexPath.row];
            cell.textLabel.textAlignment = NSTextAlignmentLeft;

        }else{
            cell.textLabel.text = @"清除历史纪录";
            cell.textLabel.textAlignment = NSTextAlignmentCenter;

        }
    }
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_dataArr.count == 0) {
        return;
    }
    if (indexPath.row == _dataArr.count) {
        [_dataArr removeAllObjects];
        [self wirtePilst:_dataArr];
        [_tableView reloadData];
        return;
    }
    _searchBar.text = _dataArr[indexPath.row];
    [self persent];
}
- (void)persent{
    if (_searchBar.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"请输入关键词" duration:2];
        return;
    }
    if (_changeBtn.tag == user) {
        //搜索用户
        if (![CDFMe shareInstance].isLogin) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"温馨提示" message:@"登录后可搜索用户" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            [alert show];
            
            return;
        }
        
    }
    
    if (_changeBtn.tag == user) {
        ZWDSearchUserResultViewController *userVC = [ZWDSearchUserResultViewController new];
        userVC.key = _searchBar.text;
        [self.navigationController pushViewController:userVC animated:YES];
        
    }else{
        UIStoryboard *s = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        CDFSearchResultViewController *vc = [s instantiateViewControllerWithIdentifier:@"CDFSearchResultViewController"];
        vc.type = tieZi;
        vc.keyword = _searchBar.text;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [[CDFMe shareInstance] wellcomeLogin:self];
    }else{
        
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)wirtePilst:(NSArray *)arr{
    NSString *path = [NSString stringWithFormat:@"%@/Documents/searchKeys%d.plist",NSHomeDirectory(),_changeBtn.tag];
    [arr writeToFile:path atomically:YES];
}
- (NSArray *)getArrFromPlist{
    NSString *path = [NSString stringWithFormat:@"%@/Documents/searchKeys%d.plist",NSHomeDirectory(),_changeBtn.tag];
    NSArray *arr = [NSArray arrayWithContentsOfFile:path];
    return arr;
    
}
@end
