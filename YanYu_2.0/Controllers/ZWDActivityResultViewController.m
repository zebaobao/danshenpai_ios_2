//
//  ZWDActivityResultViewController.m
//  yanyu
//
//  Created by Case on 15/4/22.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "ZWDActivityResultViewController.h"
#import "ZWDActivityResultTableViewController.h"

@interface ZWDActivityResultViewController ()<UIScrollViewDelegate>
{
    UIScrollView *_scrollView;
    ZWDActivityResultTableViewController *_applylistTable;
    ZWDActivityResultTableViewController *_applylistverifiedTable;
    
    UIView *_bacView;
    
}
@end

@implementation ZWDActivityResultViewController
- (void)layoutLeftNavigationItem {
    UIButton *backBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    [backBtn setImage:[UIImage imageNamed:@"backOrange"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(handleLeftBarBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
}
- (void)handleLeftBarBtn:(UIButton *)btn {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self layoutLeftNavigationItem];
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    // Do any additional setup after loading the view.
    [self showUI];
    [self customizedNavigationItem];
}
- (void)customizedNavigationItem {
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    titleLabel.font = [UIFont fontWithName:@"" size:20];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor orangeColor];
    //    titleLabel.text = self.title;
    titleLabel.text = @"活动报名结果";
    [titleView addSubview:titleLabel];
    self.navigationItem.titleView = titleView;
}
- (void)showUI{
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64 + 50, KDeviceSizeWidth, KDeviceSizeHeight - 64 - 50)];
    _scrollView.backgroundColor = [UIColor grayColor];
    _scrollView.delegate = self;
    _scrollView.contentSize = CGSizeMake(2*KDeviceSizeWidth, 0);
    _scrollView.pagingEnabled = YES;
    [self.view addSubview:_scrollView];
    
    
    _bacView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, KDeviceSizeWidth/2 , 50)];
    _bacView.backgroundColor = RGBACOLOR(251, 142, 142, 1.0);
    _bacView.layer.masksToBounds = YES;
    _bacView.layer.cornerRadius = 8;
    [self.view addSubview:_bacView];
    
    
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeSystem];
    button1.frame = CGRectMake(0, 64, KDeviceSizeWidth/2, 50);
    [button1 setTitle:@"已审核" forState:UIControlStateNormal];
    button1.tag = 101;
    [button1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button1 addTarget:self action:@selector(titleClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button1];
    
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeSystem];
    button2.frame = CGRectMake(KDeviceSizeWidth/2, 64, KDeviceSizeWidth/2, 50);
    [button2 setTitle:@"未审核" forState:UIControlStateNormal];
    button2.tag = 102;
    [button2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button2 addTarget:self action:@selector(titleClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button2];
    
    _applylistTable  = [ZWDActivityResultTableViewController new];
    _applylistTable.dataSource = self.specialDict[@"applylist"];
    _applylistTable.view.frame = CGRectMake(0, 0, KDeviceSizeWidth, _scrollView.bounds.size.height);
    [_scrollView addSubview:_applylistTable.view];
    
    _applylistverifiedTable = [ZWDActivityResultTableViewController new];
    _applylistverifiedTable.dataSource = self.specialDict[@"applylistverified"];
    _applylistverifiedTable.view.frame = CGRectMake(KDeviceSizeWidth, 0, KDeviceSizeWidth, _scrollView.bounds.size.height);
    [_scrollView addSubview:_applylistverifiedTable.view];
    
#pragma mark - 判断，如果已审核数量为0跳至 未审核
    if (_applylistTable.dataSource.count <=0 && _applylistverifiedTable.dataSource.count > 0) {
        _scrollView.contentOffset = CGPointMake(KDeviceSizeWidth, 0);
        UIButton *button = (UIButton *)[self.view viewWithTag:102];
        [UIView animateWithDuration:0.5 animations:^{
            
            _bacView.center = button.center;
            
        } completion:nil];
    }
    
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    UIButton *button1 = (UIButton *)[self.view viewWithTag:101];
    UIButton *button2 = (UIButton *)[self.view viewWithTag:102];
    NSInteger i = scrollView.contentOffset.x/KDeviceSizeWidth;
    if (i == 0) {
        [UIView animateWithDuration:0.5 animations:^{
            
            _bacView.center = button1.center;
            
        } completion:nil];
    }else{
        [UIView animateWithDuration:0.5 animations:^{
            
            _bacView.center = button2.center;
            
        } completion:nil];
    }
    
}
- (void)titleClick:(UIButton *)button{
    if (button.tag == 101) {
        _scrollView.contentOffset = CGPointMake(0, 0);
    }else{
        _scrollView.contentOffset = CGPointMake(KDeviceSizeWidth, 0);
    }
    [UIView animateWithDuration:0.5 animations:^{
        _bacView.center = button.center;
        
    } completion:nil];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
