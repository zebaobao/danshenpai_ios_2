//
//  IndexViewController.m
//  yanyu_2.0
//
//  Created by Case on 15/4/7.
//
//

#import "IndexViewController.h"
#import "IndexViewHeaderVIew.h"
#import "IndexViewCell.h"
#import "CircleImageView.h"
#import "CDFIndexViewController.h"
#import "CDFDisplayViewController.h"
#import "CDFTabBarController.h"
#import "ZWDLoadMoreCell.h"
#import "SVProgressHUD.h"
#import "PlateViewController.h"
#import "CDFMeViewController.h"
#import "CDFWebViewController.h"
#import "ActivityTableViewController.h"
#import "CDFWelcomeScrollView.h"

@interface IndexViewController () < IndexViewHeaderVIewDelegate, CircleImageVIewDelegate>
//@property (nonatomic, strong) UITableView *tableView;
@property (strong, nonatomic) id refreshHeaderView;
@property (retain, nonatomic) NSMutableArray *adDataSource;
@property (retain, nonatomic) NSMutableArray *kwDataSource;
@property (retain, nonatomic) NSMutableArray *picDatSource;
@property (strong, nonatomic) CDFWelcomeScrollView *welcomeScrollView;


@end

@implementation IndexViewController {
    BOOL _isLoading;
    NSInteger   _currentPage;
    CGPoint     _scrollPoint;
    BOOL        _hasMore;
    BOOL        _loadMoreOver;//结束上拉加载
}

/**
 *  实现refreshHeaderView的get方法
 *
 *  @return refreshHeaderView
 */
- (id)refreshHeaderView
{
    if (!_refreshHeaderView) {
        if ([Common systemVersion] < 6.0) {
            
        } else {
            UIRefreshControl *rc = [[UIRefreshControl alloc] init];
            rc.attributedTitle = [[NSAttributedString alloc] initWithString:@"用力，不要停..."];
            [rc addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
            _refreshHeaderView = rc;
        }
    }
    return _refreshHeaderView;
}

/**
 *  刷新控件的逻辑方法
 *
 *  @param refresh 刷新控件
 */
- (void)refreshView:(UIRefreshControl *)refresh
{
    //刷新的逻辑代码
    if (!_isLoading) {
        refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"我正在很用力的加载..."];
        _currentPage = 1;
        _loadMoreOver = NO;
        [self setupTableView];
        [self loadData];
    } else {
        return;
    }
}
/**
 *  刷新完成以后调用
 *
 *  @param refresh 刷新控件
 *  @param message 显示的消息，如果没有特殊消息显示则会显示默认消息
 */
- (void)refreshView:(UIRefreshControl *)refresh endRefreshWithMessage:(NSString *)message
{
    [self performSelector:@selector(resetLoadFlag) withObject:nil afterDelay:3];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM.dd, hh:mm a"];
    NSString *lastUpdated = [NSString stringWithFormat:@"%@ %@", [formatter stringFromDate:[NSDate date]], (message.length > 0 ? message : @"刷新成功")];
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:lastUpdated];
    [refresh endRefreshing];
}
- (void)resetLoadFlag
{
    _isLoading = NO;
}

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    //下拉加载更多
//    if (scrollView.contentSize.height - scrollView.contentOffset.y < CGRectGetHeight(scrollView.frame) + 50) {
//        if (_hasMore && !_isLoading) {
//            _currentPage++;
//            _scrollPoint = CGPointMake(0, scrollView.contentOffset.y);
//            [self loadData];
//        }
//    }
//}

//- (UITableView *)tableView {
//    if (!_tableView) {
//        _tableView = [[UITableView alloc] initWithFrame:[[UIScreen mainScreen] bounds] style:UITableViewStylePlain];
//    }
//    return _tableView;
//}



- (void)viewDidLoad {
    [super viewDidLoad];
    [self isFirst];
    [self setup];
    [self setupTableView];
    [self customizedNavigationItem];
    [self.tableView registerClass:[IndexViewCell class] forCellReuseIdentifier:@"indexViewCell"];
    if ([Common systemVersion] >= 6.0) {
        self.refreshControl = self.refreshHeaderView;
    }
    [self handleReadData];
    [self loadData];
    
}
- (void)isFirst{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *lastVersion = [defaults objectForKey:@"lastVersion"];
    NSString *currentVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    
//    NSLog(@"%@ %@",lastVersion, currentVersion);
    if (![lastVersion isEqualToString:currentVersion]) {
        //新安装版本
        [defaults setObject:currentVersion forKey:@"lastVersion"];
        [self presentModalViewController:self.welcomeScrollView animated:YES];
    }
}
- (CDFWelcomeScrollView *)welcomeScrollView
{
    if (!_welcomeScrollView) {
        _welcomeScrollView = [CDFWelcomeScrollView new];
    }
    return _welcomeScrollView;
}
- (void)setupHeaderView {
    
}

- (void)setupTableView {
    //    self.tableView.dataSource = self;
    //    self.tableView.delegate = self;
    IndexViewHeaderVIew *headerView = [[IndexViewHeaderVIew alloc] initWithFrame:CGRectMake(0, 64, [[UIScreen mainScreen] bounds].size.width, kHeightOfColumns + kHeightOfScrollView)];
    headerView.delegate = self;
    self.tableView.tableHeaderView = headerView;
    //    [self.view addSubview:self.tableView];
}
- (void)customizedNavigationItem {
    
    UILabel *titleLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    titleLable.text = @"眼遇";
    titleLable.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    titleLable.textAlignment = NSTextAlignmentCenter;
    titleLable.textColor = RGBACOLOR(250, 66, 4, 1.0);
    UIImageView *titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"眼遇"]];
    titleView.contentMode = UIViewContentModeScaleAspectFill;
    titleView.frame = CGRectMake(0, 0, 50, 25);
    self.navigationItem.titleView = titleView;
    CGRect rect = self.navigationItem.titleView.frame;
    self.navigationItem.titleView.frame = CGRectMake(rect.origin.x, rect.origin.y, 50, 25);
    
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    button2.frame = CGRectMake(0, 0, 25, 25);
    [button2 setImage:[UIImage imageNamed:@"icon_02"] forState:UIControlStateNormal];
    [button2 addTarget:self action:@selector(searchClick) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item2 = [[UIBarButtonItem alloc]initWithCustomView:button2];
    self.navigationItem.leftBarButtonItems = @[item2];
    
    //    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_01.png"] style:UIBarButtonItemStylePlain target:self action:@selector(handlePop:)];
    //    self.navigationItem.leftBarButtonItem = leftItem;
#pragma mark - 签到按钮
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 70, 30)];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(QDBtnClick)];
    view.userInteractionEnabled = YES;
    [view addGestureRecognizer:tap];
    
    UIButton *qdBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [qdBtn setImage:[UIImage imageNamed:@"QDImage"] forState:UIControlStateNormal];
    qdBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    //qdBtn.backgroundColor = [UIColor grayColor];
    qdBtn.frame = CGRectMake(0, 0, 70, 15);
    [qdBtn setTitleColor:RGBACOLOR(249, 84, 53, 1.f) forState:UIControlStateNormal];
    [qdBtn setTitle:@"签到  +" forState:UIControlStateNormal];
    qdBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -30, 0, 0);
    qdBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 55, 0, 0);
    [view addSubview:qdBtn];
    qdBtn.userInteractionEnabled = NO;
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 15, 70, 15)];
    label.font = [UIFont systemFontOfSize:9];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"每日签到送积分";
    label.textColor  = TITLEGRAY;
    [view addSubview:label];
    
    //    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    //    button1.frame = CGRectMake(0, 0, 30, 30);
    //    [button1 setImage:[UIImage imageNamed:@"icon_01"] forState:UIControlStateNormal];
    //    [button1 addTarget:self action:@selector(QDBtnClick) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc]initWithCustomView:view];
    self.navigationItem.rightBarButtonItem = item1;
    
}
#pragma mark - 点击搜索
- (void)searchClick {
    ZWDSearchViewController *vc = [[ZWDSearchViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - 签到点击
- (void)QDBtnClick {
    
    if (![CDFMe shareInstance].isLogin) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"温馨提醒" message:@"您还没有登录哦" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"登录", nil];
        [alertView show];
        return;
    }
    [self qiandao];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [[CDFMe shareInstance] wellcomeLogin:self];
    }
}
- (void)qiandao {
    if ([CDFMe shareInstance].isLogin) {
        ZWDWebViewController *web = [ZWDWebViewController new];
        web.url = [NSString stringWithFormat:@"%@plugin.php?id=k_misign:sign",kBaseURL];
        //    web.url = @"http://svnbang.dahe.cn/plugin.php?id=k_misign:sign";
        [self.navigationController pushViewController:web animated:YES];
    }
}
#pragma mark ---- UITableViewDelegate && UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == self.picDatSource.count) {
        return 60;
    }
    
    return [IndexViewCell heightForCellWithStr:self.picDatSource[indexPath.row][@"title"]];
}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//
//    return headerView;
//}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == self.picDatSource.count) {
        return;
    }
    
    
    NSString *str = self.picDatSource[indexPath.row][@"url"];
    NSString *newStr = [str substringWithRange:NSMakeRange(0, 4)];
    
    if (![newStr isEqualToString:@"http"]) {
        //url不是以http开头, 打开帖子
        UIStoryboard *s = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        CDFDisplayViewController *displayVC = [s instantiateViewControllerWithIdentifier:@"DisplayViewContoller"];
        displayVC.tid = self.picDatSource[indexPath.row][@"id"];
        [(CDFTabBarController *)[[UIApplication sharedApplication].delegate window].rootViewController setHidden:YES];
        [self.navigationController pushViewController:displayVC animated:YES];
        
    } else {
        //url以http开头, 打开WebView
        //获取uid
        CDFMe *me = [CDFMe shareInstance];
        id uid = me.uid;
//        NSLog(@"uid = %@", [NSString stringWithFormat:@"%@", uid]);
        CDFWebViewController *webView = [[CDFWebViewController alloc] init];
        webView.url = [uid isEqualToString:@""] ? str : [NSString stringWithFormat:@"%@&uid=%@", str, uid];
//        NSLog(@"webView.url = %@", webView.url);
        webView.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:webView animated:YES];
    }
}
static bool isShow = NO;
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [(CDFTabBarController *)[[UIApplication sharedApplication].delegate window].rootViewController setHidden:NO];//隐藏tabbar
    
#pragma mark - 首页已经出现时判断是否为第一次登陆
    if (!isShow) {
        if ([CDFMe shareInstance].isFirstLogin) {
            //第一次登陆
            
            ZWDFirstLoginViewController *vc = [[ZWDFirstLoginViewController alloc]init];
            [self presentViewController:vc animated:YES completion:nil];
            [CDFMe shareInstance].isFirstLogin = NO;
            isShow = YES;
        }else{
            
            
        }
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == self.picDatSource.count && self.picDatSource.count > 0) {
        //加载更多cell
        static NSString *cellID = @"ZWDLoadMoreCell";
        
        ZWDLoadMoreCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"ZWDLoadMoreCell" owner:self options:nil]lastObject];
        }
        
        //if ([self.mainData[@"forum_threadlist"][@"count"] integerValue] > self.dataSource.count) {
        if (!_loadMoreOver){
            cell.contentLable.text = @"上拉加载更多……";
            [self loadMore];
        }else{
            cell.contentLable.text = @"所有数据加载完毕!";
            cell.activty.hidden = YES;
        }
        
        return cell;
    }
    
//    NSLog(@":: %@", self.picDatSource);
    IndexViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"indexViewCell" forIndexPath:indexPath];
    cell.avataImage.delegate = self;
    cell.avataImage.layer.cornerRadius = 20;
    cell.contentLabel.text = self.picDatSource[indexPath.row][@"title"];
//    NSLog(@"url:%@", [NSString stringWithFormat:@"%@uc_server/avatar.php?uid=%@&size=big",kBaseURL,self.picDatSource[indexPath.row][@"fields"][@"authorid"]]);
    [cell.avataImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@uc_server/avatar.php?uid=%@&size=big",kBaseURL,self.picDatSource[indexPath.row][@"fields"][@"authorid"]]] placeholderImage:[UIImage imageNamed:@"河宝娃"]];
    CGRect frame = cell.contentLabel.frame;
    CGFloat heightOfLabel = [IndexViewCell detailTextHeight:self.picDatSource[indexPath.row][@"title"]];
    if (heightOfLabel < 20.0) {
        heightOfLabel = heightOfLabel + 10;
    }
    cell.contentLabel.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, heightOfLabel);
    cell.contentLabel.numberOfLines = 0;
    cell.contentLabel.font = [UIFont fontWithName:nil size:15];
    cell.replyAndViewsView.frame = CGRectMake(cell.contentLabel.frame.origin.x, cell.contentLabel.frame.origin.y + cell.contentLabel.frame.size.height + 12, 100, 10);
    cell.replyAndViewLable.text = [NSString stringWithFormat:@"%@   浏览%@",self.picDatSource[indexPath.row][@"fields"][@"replies"], self.picDatSource[indexPath.row][@"fields"][@"views"]];
    
    CGFloat heightOfCell = [IndexViewCell heightForCellWithStr:self.picDatSource[indexPath.row][@"title"]];
    CGRect frameOfAvatar = cell.avataImage.frame;
    cell.avataImage.frame = CGRectMake(frameOfAvatar.origin.x, (heightOfCell - (KAvataImageHeightOrWidth + 17)) / 2.0 , frameOfAvatar.size.width, frameOfAvatar.size.height);
    CGRect avataFrame = cell.avataImage.frame;
    
    cell.nameLabel.frame = CGRectMake(avataFrame.origin.x, avataFrame.origin.y + avataFrame.size.height + 2, 100, 15);
    cell.nameLabel.text = self.picDatSource[indexPath.row][@"fields"][@"author"];
    CGPoint pointOfNameLabel = CGPointMake(cell.avataImage.center.x, cell.nameLabel.center.y);
    [cell.nameLabel setCenter:pointOfNameLabel];
    NSInteger dateline = [self.picDatSource[indexPath.row][@"fields"][@"dateline"] integerValue];
    NSString *time = [[self class] formatDate:dateline];
    cell.timeLabel.text = time;
    CGRect timeLabelFrame = cell.replyAndViewsView.frame;
    cell.timeLabel.frame = CGRectMake(timeLabelFrame.origin.x + 95, timeLabelFrame.origin.y - 3, 100, 15);
    cell.timeLabel.font = [UIFont fontWithName:nil size:12];
    cell.timeLabel.textAlignment = NSTextAlignmentRight;
//    cell.timeLabel.textColor = [Common colorWithHex:@"#cdcdcd" alpha:1.0];
    return cell;
}
- (NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger)section {
//    NSLog(@"self:%d", self.picDatSource.count);
    if (self.picDatSource.count > 0) {
        return self.picDatSource.count + 1;
    }else{
        return self.picDatSource.count;
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

//点击模块的代理实现
- (void)handleActionColumnWithTag:(NSInteger)tag {
    if (tag<109) {
        //八个模块
        int fid = 0;
        NSString *title ;
        switch (tag) {
//            case 110:
//                fid = 759;
//                title = @"大河公益";
            case 101:
                fid = 593;
                title = @"骑友";
                break;
            case 102:
                title = @"亲子";
                fid = 528;
                break;
            case 103:
                fid = 117;
                title = @"车友";
                break;
            case 104:
                fid = 780;
                title = @"摄影";
                break;
            case 105:
                fid = 53;
                title = @"驴友";
                break;
            case 106:
                title = @"美食";
                fid = 1205;
                break;
            case 107:
                title = @"购物";
                fid = 496;
                break;
            case 108:
                title = @"情感";
                fid = 265;
                break;
            default:
                break;
        }
        PlateViewController *plateVC = [[PlateViewController alloc] initWithStyle:UITableViewStylePlain];
        
        plateVC.fid = [NSString stringWithFormat:@"%d", fid];
        plateVC.title = title;
//        NSLog(@"%@, %@, tag:%d", plateVC.title, plateVC.fid, tag);
        [self.navigationController pushViewController:plateVC animated:YES];
    } else {
        //活动
        if (tag == 109) {
            //跳转活动页面
            //            ActivityTableViewController *activityVC = [[ActivityTableViewController alloc] initWithStyle:UITableViewStylePlain];
            ActivityTableViewController *activityVC = [[ActivityTableViewController alloc] init];
            //            [(CDFTabBarController *)[[UIApplication sharedApplication].delegate window].rootViewController setHidden:YES];//隐藏tabbar
            
            [self.navigationController pushViewController:activityVC animated:YES];
        } else {
            int fid = 759;
            NSString *title;
            title = @"大河公益";
            
            PlateViewController *plateVC = [[PlateViewController alloc] initWithStyle:UITableViewStylePlain];
            
            plateVC.fid = [NSString stringWithFormat:@"%d", fid];
            plateVC.title = title;
//            NSLog(@"%@, %@, tag:%d", plateVC.title, plateVC.fid, tag);
            [self.navigationController pushViewController:plateVC animated:YES];
            
            
            
//            //跳转笑话页面
//            CDFWebViewController *jokeWebView = [[CDFWebViewController alloc] init];
//            jokeWebView.url = @"http://xiaohua.dahe.cn";
//            UILabel *titleLabel  = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
//            titleLabel.text = @"笑话";
//            titleLabel.tintColor = KThemeColor;
//            jokeWebView.navigationItem.titleView = titleLabel;
//            [self.navigationController pushViewController:jokeWebView animated:YES];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//点击cell上的头像 实现的代理方法
- (void)handleCircleImageViewTapGesture:(UITapGestureRecognizer *)gesture {
    IndexViewCell *cell = (IndexViewCell *)gesture.view.superview;
    NSIndexPath *path = [self.tableView indexPathForCell:cell];
    NSString *str = [NSString stringWithFormat:@"section:%d, row:%d, superClass:%@", path.section, path.row, gesture.view.superview];
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CDFMeViewController *vc = [s instantiateViewControllerWithIdentifier:@"userViewController"];
    vc.uid = self.picDatSource[path.row][@"fields"][@"authorid"];
    [(CDFTabBarController *)[[UIApplication sharedApplication].delegate window].rootViewController setHidden:YES];//隐藏tabbar
    [self.navigationController pushViewController:vc animated:YES];
}

- (NSMutableArray *)adDataSource {
    if (!_adDataSource) {
        _adDataSource = [NSMutableArray arrayWithCapacity:0];
    }
    return _adDataSource;
}
- (NSMutableArray *)kwDataSource {
    if (!_kwDataSource) {
        _kwDataSource = [NSMutableArray arrayWithCapacity:0];
    }
    return _kwDataSource;
}
- (NSMutableArray *)picDatSource {
    if (!_picDatSource) {
        _picDatSource = [NSMutableArray arrayWithCapacity:0];
    }
    return _picDatSource;
}
- (void)loadData
{
    
    [Common unarchiveForKey:@"indexDic"];
    
    ///return;
    if (_isLoading) {
        return;
    }
    _isLoading = YES;
    NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=plaza&page=%d",kApiVersion, _currentPage];
    
    //    [SVProgressHUD showWithStatus:@"请稍后..."];
    [[HXHttpClient shareInstance] grabURL:url success:^(id responseObject) {
        _isLoading = NO;
//        NSLog(@"%@", responseObject);
        [CDFMe shareInstance].formHash = responseObject[@"Variables"][@"formhash"];
//        NSLog(@"%@",[CDFMe shareInstance].formHash);
        [[NSNotificationCenter defaultCenter] postNotificationName:kMESSAGE_BADGE object:nil userInfo
                                                                  :responseObject[@"Variables"][@"newmessage2"]];

        if (responseObject[@"Message"]) {
            [SVProgressHUD showErrorWithStatus:responseObject[@"Message"][@"messagestr"]];
            [self refreshView:_refreshHeaderView endRefreshWithMessage:responseObject[@"Message"][@"messagestr"]];
        } else {
            [self refreshView:_refreshHeaderView endRefreshWithMessage:@""];
//            [SVProgressHUD dismiss];
            if (self.adDataSource.count > 0) {
                [self.adDataSource removeAllObjects];
            }
            if (self.kwDataSource.count > 0) {
                [self.kwDataSource removeAllObjects];
            }
            NSMutableArray *adArr = [NSMutableArray arrayWithArray:responseObject[@"Variables"][@"plaza_ad"]];
            NSMutableArray *kwArr = [NSMutableArray arrayWithArray:responseObject[@"Variables"][@"plaza_keywords"]];
            self.adDataSource = adArr;
            self.kwDataSource = kwArr;
            if (_currentPage == 1) {
                [self.picDatSource removeAllObjects];
                NSMutableArray *picArr = [NSMutableArray arrayWithArray:responseObject[@"Variables"][@"plaza_keywords_pic"]];
                self.picDatSource = picArr;
            } else {
                if ([responseObject[@"Variables"][@"plaza_keywords_pic"] isKindOfClass:[NSArray class]]) {
                    NSArray *arr = responseObject[@"Variables"][@"plaza_keywords_pic"];
                    [self.picDatSource addObjectsFromArray:arr];
                } else {
                    _loadMoreOver = YES;
                }
            }
        }
        [self refreshView:_refreshHeaderView endRefreshWithMessage:@"刷新成功"];
        [self.tableView reloadData];
        
        if (_currentPage == 1) {
//            [self guidangWithDic:responseObject];
            [Common archive:responseObject withKey:@"indexDic"];
        }
        
    } fail:^(NSError *error) {
        _isLoading = NO;
        [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
//        NSLog(@"%@",error);
        [self refreshView:_refreshHeaderView endRefreshWithMessage:@"刷新失败"];
    }];
    //[self.tableView reloadData];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}
//- (void)setPicDatSource:(NSMutableArray *)picDatSource
//{
//    _picDatSource = picDatSource;
//    [self.tableView reloadData];
//}


//- (void)setAdDataSource:(NSArray *)adDataSource
//{
//    _adDataSource = adDataSource;
//}

+ (NSString *)formatDate:(NSTimeInterval)timeInterval {
    NSDate *myDate = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [NSLocale currentLocale];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm";
    NSString *dateStr = [dateFormatter stringFromDate:myDate];//目标时间, 字符串格式
    NSDate *nowDate = [NSDate date];
    NSString *nowDateStr = [dateFormatter stringFromDate:nowDate]; //当前的时间, 字符串格式
    NSTimeInterval nowTime = [[NSDate date] timeIntervalSince1970]; //当前时间到1970年的秒数
    long long int nowtime = (long long) nowTime;
    long long int cha = nowtime - timeInterval;
//    NSLog(@"%@", dateStr);
    //对时间进行优化显示
    if ([[dateStr substringWithRange:NSMakeRange(0, 4)] isEqualToString:[nowDateStr substringWithRange:NSMakeRange(0, 4)]]) {
        
        if ([[dateStr substringWithRange:NSMakeRange(5, 5)] isEqualToString:[nowDateStr substringWithRange:NSMakeRange(5, 5)]]) {
            if (cha / 3600.0 <= 1) {
                dateStr = [NSString stringWithFormat:@"%ld分钟之前", (long int)cha/60];
            }else if (cha / 3600.0<= 2 && cha / 3600.0 > 1) {
                dateStr = @"1小时前";
            }else if (cha / 3600.0<=3 && cha / 3600.0 > 2) {
                dateStr = @"2小时前";
            } else {
                dateStr = [NSString stringWithFormat:@"今天%@", [dateStr substringWithRange:NSMakeRange(11, 5)]];
                //                NSLog(@"今天 %@", [dateStr substringWithRange:NSMakeRange(11, 5)]);
            }
        }else if ([[nowDateStr substringWithRange:NSMakeRange(8, 2)] integerValue] - [[dateStr substringWithRange:NSMakeRange(8, 2)] integerValue] == 1) {
//            NSLog(@"昨天%@", [dateStr substringWithRange:NSMakeRange(11, 5)]);
            dateStr = [NSString stringWithFormat:@"昨天%@", [dateStr substringWithRange:NSMakeRange(11, 5)]];
        } else {
//            NSLog(@"%@", [dateStr substringWithRange:NSMakeRange(5, 11)]);
//            NSLog(@"%@", [dateStr substringWithRange:NSMakeRange(5, 5)]);
            //            dateStr = [NSString stringWithFormat:@"%@", [dateStr substringWithRange:NSMakeRange(5, 11)]]; //如果是前天或者更早的日期, 显示日期和具体时间
            dateStr = [NSString stringWithFormat:@"%@", [dateStr substringWithRange:NSMakeRange(5, 5)]];//如果是前天或者更早的日期, 只显示日期, 不显示具体时间
        }
    } else {
        dateStr = [dateStr substringWithRange:NSMakeRange(0, 10)];
    }
    return dateStr;
}

- (void)reloadAdView
{
    
}

- (void)loadMore{
    if (_hasMore && !_isLoading) {
        _currentPage ++;
        [self loadData];
    }
}
-(void)setup{
    _currentPage = 1;
    _isLoading = NO;
    _hasMore = YES;
    _loadMoreOver = NO;
    
    //    self.refreshControl = self.refreshHeaderView;
}

//- (void)guidangWithDic:(NSDictionary *)dic{
//    NSMutableData *mData = [NSMutableData data];
//    //创建对象
//    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:mData];
//    //归档
//    [archiver encodeObject:dic forKey:@"index"];
//    //3.结束归档.结束归档之后在归档无效
//    [archiver finishEncoding];
//    BOOL isSucess = [mData writeToFile:[self getFilePath] atomically:YES];
////    NSLog(@"%d", isSucess);
//
//}

- (void)handleReadData {
//    //1.从本地读取数据
//    NSData *mData = [NSData dataWithContentsOfFile:[self getFilePath]];
//    //2.创建反归档对象
//    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:mData];
//    //3.反归档
    NSDictionary *indexDic = [Common unarchiveForKey:@"indexDic"];
//    //4.结束反规档
//    [unarchiver finishDecoding];
    if (indexDic) {
        NSMutableArray *adArr = [NSMutableArray arrayWithArray:indexDic[@"Variables"][@"plaza_ad"]];
        NSMutableArray *kwArr = [NSMutableArray arrayWithArray:indexDic[@"Variables"][@"plaza_keywords"]];
        self.adDataSource = adArr;
        self.kwDataSource = kwArr;
        NSMutableArray *picArr = [NSMutableArray arrayWithArray:indexDic[@"Variables"][@"plaza_keywords_pic"]];
        self.picDatSource = picArr;
        [self.tableView reloadData];
    }
   
}
//
//- (NSString *)getFilePath {
//    //1.获取Documents文件夹路径
//    NSString *filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
//    //2.拼接上文件路径
//    NSString *newFilePath = [filePath stringByAppendingPathComponent:@"index.data"];
//    return newFilePath;
//}


@end
