//
//  ActivityTableViewController.m
//  yanyu
//
//  Created by caiyee on 15/4/14.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "ActivityTableViewController.h"
#import "ActivityTableViewCell.h"
#import "CDFDisplayViewController.h"
#import "KxMenu.h"
#import "UIImageView+EMWebCache.h"
#import "ZWDLoadMoreCell.h"
#define kSideToLeft     16
#define kHeightOfImage  (KDeviceSizeWidth - 2.0 * kSideToLeft) / 1.8

@interface ActivityTableViewController ()<UITableViewDataSource, UITableViewDelegate> {
    UIButton *_btn1;
    UITableView *_tableView;
    NSString *_typeid;
    
    NSIndexPath *_indexPath;
    
    int _currentPage;
    BOOL _isLoading;
    
    BOOL _hasMore;
    BOOL _loadMoreOver;
    

}
@property (retain, nonatomic) UILabel *titleLabel;
@property (retain, nonatomic) NSMutableArray *dataSource;
@property (retain, nonatomic) NSArray *typeArr;
@property (retain, nonatomic) NSMutableArray *keyArr;
@property (retain, nonatomic) NSMutableArray *valueArr;
@property (retain, nonatomic) NSMutableDictionary *sixColorDic;
@end

@implementation ActivityTableViewController

- (NSArray *)typeArr {
    if (!_typeArr) {
        _typeArr = [[NSArray alloc] init];
    }
    return _typeArr;
}

- (NSMutableDictionary *)sixColorDic {
    if (!_sixColorDic) {
        _sixColorDic = [[NSMutableDictionary alloc] init];
    }
    return _sixColorDic;
}

- (void)setupSixColor {
    
    NSArray *colors = @[@"#f63375", @"#31bd80", @"#ff443f", @"#3ea3ff", @"#ffb30f", @"#ff7d01", @"#f63375", @"#31bd80", @"#ff443f", @"#ff7d01"];
    for (int i = 0; i < [self.keyArr count]; i++) {
        [self.sixColorDic setObject:colors[i] forKey:self.valueArr[i]];
//        NSLog(@"%@", self.valueArr);
    }
}
- (void)displayDisapper{
    [_tableView reloadRowsAtIndexPaths:@[_indexPath] withRowAnimation:UITableViewRowAnimationNone];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(displayDisapper) name:ACTIVITYXQDISPLAYDISAPPER object:nil];
    self.automaticallyAdjustsScrollViewInsets = NO;
    _typeid = @"";
    [self setup];
    [self loadData];
    _loadMoreOver = NO;
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    _titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.textColor = [UIColor orangeColor];
    _titleLabel.text = @"精彩活动";
    [titleView addSubview:_titleLabel];
    self.navigationItem.titleView = titleView;
    [self setupRightButton];
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, KDeviceSizeWidth, KDeviceSizeHeight - 64 - 49) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
    [_tableView addLegendHeaderWithRefreshingBlock:^{
        _currentPage = 1;
        if (!_isLoading) {
            [self loadData];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.dataSource.count > 0) {
        return self.dataSource.count + 1;
    }else{
        return self.dataSource.count;
    }
}

+ (NSString *)formatDate:(NSTimeInterval)timeInterval withFormat:(NSString*)format {
    NSDate *myDate = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.locale = [NSLocale currentLocale];
    df.dateFormat = format;
    NSString *date = [df stringFromDate:myDate];
    return date;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == self.dataSource.count && self.dataSource.count > 0) {
        //加载更多cell
        static NSString *cellID = @"ZWDLoadMoreCell";
        
        ZWDLoadMoreCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"ZWDLoadMoreCell" owner:self options:nil]lastObject];
        }
        
        //if ([self.mainData[@"forum_threadlist"][@"count"] integerValue] > self.dataSource.count) {
        if (!_loadMoreOver){
            cell.contentLable.text = @"上拉加载更多……";
            [self loadMore];
        }else{
            cell.contentLable.text = @"所有数据已加载完毕!";
            cell.activty.hidden = YES;
        }
        cell.userInteractionEnabled = NO;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    ActivityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"activityCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"ActivityTableViewCell" owner:self options:nil]lastObject];
    }
    if (self.dataSource.count > 0) {
        NSInteger startTime = [self.dataSource[indexPath.row][@"activity"][@"starttimefrom"] integerValue];
        NSInteger toTime = [self.dataSource[indexPath.row][@"activity"][@"starttimeto"] integerValue];
        NSString *startTimeStr = [[self class] formatDate:startTime withFormat:@"yyyy.MM.dd"];
        NSString *toTimeStr = [[self class] formatDate:toTime withFormat:@"yyyy.MM.dd"];
        
        if (toTime <= 0) {
            
//            cell.activityTitleLabel.text = self.dataSource[indexPath.row][@"title"];
            
            cell.activityTime.text = [NSString stringWithFormat:@"活动时间: %@开始", startTimeStr];
        } else {
            cell.activityTime.text = [NSString stringWithFormat:@"活动时间: %@ - %@", startTimeStr, toTimeStr];
        }
        cell.activityTime.font = [UIFont fontWithName:nil size:15];
        cell.activityTime.textColor= [UIColor lightGrayColor];
        NSString *imageURL = [NSString stringWithFormat:@"%@%@", kBaseURL, self.dataSource[indexPath.row][@"activity"][@"attachurl"]];
        [cell.activityImage sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:[UIImage imageNamed:@"bg_imageloader"]];
        
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString: [NSString stringWithFormat:@"已有%d人感兴趣", [self.dataSource[indexPath.row][@"fields"][@"views"] integerValue]]];
        int index = [((NSString *)[NSString stringWithFormat:@"%d",[self.dataSource[indexPath.row][@"fields"][@"views"] integerValue]]) length];
//        [str addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:NSMakeRange(0,2)];
        [str addAttribute:NSForegroundColorAttributeName value:[UIColor orangeColor] range:NSMakeRange(2,index)];
//        [str addAttribute:NSForegroundColorAttributeName value:[UIColor greenColor] range:NSMakeRange(2 + index,4)];
        
        cell.countOfperson.attributedText = str;
        
//        cell.countOfperson.text = [NSString stringWithFormat:@"已有%d人感兴趣", [self.dataSource[indexPath.row][@"views"] integerValue]];
    }
//    NSLog(@"%@",self.dataSource[indexPath.row]);
  
    cell.activityTypeLabel.layer.masksToBounds = YES;
    cell.activityTypeLabel.layer.cornerRadius = 5;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.activityTitleLabel.text = [NSString stringWithFormat:@"%@",self.dataSource[indexPath.row][@"title"]];
    if ([self.dataSource[indexPath.row][@"classname"] length] > 0) {
        cell.activityTypeLabel.text = self.dataSource[indexPath.row][@"classname"];
    } else {
        cell.activityTypeLabel.text = @"大河活动";
    }
    CGFloat heightOfTitle = [[self class] detailTextHeight:[NSString stringWithFormat:@"%@",self.dataSource[indexPath.row][@"title"]]];
//    NSLog(@"%f", heightOfTitle);
    if (heightOfTitle < 22.0) {
        cell.activityTitleLabel.textAlignment = NSTextAlignmentCenter;
    } else {
        cell.activityTitleLabel.textAlignment = NSTextAlignmentLeft;
    }
    
//    cell.activityTime.frame = CGRectMake(cell.activityTime.frame.origin.x, cell.activityTitleLabel.frame.origin.y + cell.activityTitleLabel.frame.size.height , cell.activityTime.frame.size.width, cell.activityTime.frame.size.height);
//    cell.activityImage.frame = CGRectMake(cell.activityImage.frame.origin.x, cell.activityTime.frame.size.height + cell.activityTime.frame.origin.y + 10, cell.activityImage.frame.size.width, kHeightOfImage);
//    cell.countOfperson.frame = CGRectMake(cell.activityImage.frame.origin.x, cell.activityImage.frame.origin.y + cell.activityImage.frame.size.height + 10, cell.countOfperson.frame.size.width, cell.countOfperson.frame.size.height);
//    cell.joinLabel.frame = CGRectMake(cell.joinLabel.frame.origin.x, cell.countOfperson.frame.origin.y, cell.joinLabel.frame.size.width, cell.joinLabel.frame.size.height);
//    
//    cell.joinLabel.hidden = NO;
    [cell.activityTitleLabel setFrame:CGRectMake(cell.activityTitleLabel.frame.origin.x, cell.activityTitleLabel.frame.origin.y, KDeviceSizeWidth - 2 * cell.activityTitleLabel.frame.origin.x - 15 - 5, heightOfTitle)];
    cell.activityTypeLabel.frame = CGRectMake(cell.activityTitleLabel.frame.origin.x, cell.activityTitleLabel.frame.origin.y + cell.activityTitleLabel.frame.size.height + 5, cell.activityTypeLabel.frame.size.width, cell.activityTypeLabel.frame.size.height);
    cell.activityTime.frame = CGRectMake(cell.activityTypeLabel.frame.origin.x + cell.activityTypeLabel.frame.size.width + 5, cell.activityTypeLabel.frame.origin.y, KDeviceSizeWidth - cell.activityTime.frame.origin.x, cell.activityTypeLabel.frame.size.height);
    cell.activityImage.frame = CGRectMake(cell.activityTitleLabel.frame.origin.x, cell.activityTypeLabel.frame.size.height + cell.activityTypeLabel.frame.origin.y + 5, KDeviceSizeWidth - 2.0 * cell.activityImage.frame.origin.x, kHeightOfImage);
    cell.countOfperson.frame = CGRectMake(cell.activityTitleLabel.frame.origin.x, cell.activityImage.frame.origin.y + cell.activityImage.frame.size.height + 5, KDeviceSizeWidth - 2.0 * cell.countOfperson.frame.origin.x, cell.countOfperson.frame.size.height);
    cell.joinLabel.frame = CGRectMake(cell.activityTitleLabel.frame.origin.x, cell.countOfperson.frame.origin.y + cell.countOfperson.frame.size.height + 5, KDeviceSizeWidth - 2.0 * cell.joinLabel.frame.origin.x, cell.joinLabel.frame.size.height);
    cell.whiteBackgroundView.frame = CGRectMake(cell.whiteBackgroundView.frame.origin.x, cell.whiteBackgroundView.frame.origin.y, KDeviceSizeWidth - 2.0 * cell.whiteBackgroundView.frame.origin.x, [[self class] detailTextHeight:[NSString stringWithFormat:@"%@",self.dataSource[indexPath.row][@"title"]]] + kHeightOfImage + 110 + 15);
    cell.activityStateImageView.frame = CGRectMake(KDeviceSizeWidth - cell.whiteBackgroundView.frame.origin.x - cell.activityStateImageView.frame.size.width - 1, 11, cell.activityStateImageView.frame.size.width, cell.activityStateImageView.frame.size.height);
    
    cell.activityTime.font = [UIFont fontWithName:nil size:13.0];
    
    cell.activityTitleLabel.numberOfLines = 0;
//    cell.activityTitleLabel.backgroundColor = [UIColor yellowColor];
//    cell.activityTime.backgroundColor = [UIColor greenColor];
//    cell.activityImage.image = [UIImage imageNamed:@"图片1"];
//    cell.countOfperson.backgroundColor = [UIColor orangeColor];
    cell.joinLabel.backgroundColor = [UIColor orangeColor];
    
    [cell sendSubviewToBack:cell.whiteBackgroundView];
    
    cell.backgroundColor = [Common colorWithHex:@"#f3f3f3" alpha:1.0];
    
    if ([self.dataSource[indexPath.row][@"activity"][@"close"] boolValue]) {
        cell.activityStateImageView.image = [UIImage imageNamed:@"已结束"];
        cell.joinImageView.image = [UIImage imageNamed:@"已结束立即参加"];
    } else {
        cell.activityStateImageView.image = [UIImage imageNamed:@"进行中"];
        cell.joinImageView.image = [UIImage imageNamed:@"立即参加"];
    }
    cell.joinImageView.frame = cell.joinLabel.frame;
    cell.joinLabel.hidden = YES;
    
    if (self.sixColorDic[cell.activityTypeLabel.text]) {
//        NSLog(@"cell:%@", cell.activityTypeLabel.text);
        cell.activityTypeLabel.backgroundColor = [Common colorWithHex:self.sixColorDic[cell.activityTypeLabel.text] alpha:1.0];
    } else {
//        NSLog(@"cell:%@", cell.activityTypeLabel.text);
        cell.activityTypeLabel.backgroundColor = [UIColor orangeColor];
    }
    cell.activityTypeLabel.textColor = [UIColor whiteColor];
    return cell;
}

+ (CGFloat)heightForCellWithStr:(NSString *)str {
    return (CGFloat)fmaxf(80.0f, (float)[self detailTextHeight:str] + 45.0f);
}

+ (CGFloat)detailTextHeight:(NSString *)text {
    CGRect rectToFit = [text boundingRectWithSize:CGSizeMake(KDeviceSizeWidth - 2 * kSideToLeft - 15 - 5, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:17.0f]} context:nil];
//    NSLog(@"rectToFit:%f", rectToFit.size.height);
    return rectToFit.size.height;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [SVProgressHUD dismiss];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == self.dataSource.count) {
        return 60;
    }
    
    return [[self class] detailTextHeight:[NSString stringWithFormat:@"%@",self.dataSource[indexPath.row][@"title"]]] + kHeightOfImage + 110 + 10 + 15 + 10;
}
- (NSMutableArray *)dataSource {
    if (!_dataSource) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}

- (void)loadData {
//%@api/mobile/?version=%@&module=activitylist&fid=1322&filter=typeid&typeid=%@&page=%d
    NSString *url = [NSString stringWithFormat:@"%@api/mobile/?version=%@&module=activitylist&classid=%@&page=%d", kBaseURL, kApiVersion, _typeid, _currentPage];
//    NSLog(@"%@", url);
//    NSLog(@"%@", _typeid);
    [SVProgressHUD showWithStatus:@"加载中..."];
    [[HXHttpClient shareInstance] grabURL:url success:^(id responseObject) {
//        NSLog(@"%@", responseObject);
        
        if (_currentPage == 1) {
            [self.dataSource removeAllObjects];
        }
        
        if ([responseObject[@"Variables"][@"forum_threadlist"] isKindOfClass:[NSArray class]]) {
            NSArray *arr =responseObject[@"Variables"][@"forum_threadlist"];

            [self.dataSource addObjectsFromArray:arr];

        } else {
            _loadMoreOver = YES;
        }
//        [self.dataSource addObjectsFromArray:arr];
//        self.typeArr = responseObject[@"Variables"][@"threadtypes"][@"types"];
        self.typeArr = responseObject[@"Variables"][@"activityclass"];
//        NSLog(@"%@", self.dataSource);
        [_tableView reloadData];
        [SVProgressHUD dismiss];
        [self resetLoadFlag];
        if (_currentPage == 1 && [_typeid isEqualToString:@""]) {
            [self guidangWithDic:responseObject];
        }
        
        /*
         *活动之前的代码
         
         self.valueArr = [NSMutableArray arrayWithArray:[self.typeArr allValues]];
         self.keyArr = [NSMutableArray arrayWithArray:[self.typeArr allKeys]];

         */
        
        if ([self.valueArr count] <= 0) {
            for (int i = 0; i < [self.typeArr count]; i++) {
                [self.valueArr addObject:self.typeArr[i][@"name"]];
                [self.keyArr addObject:self.typeArr[i][@"classid"]];
            }
        }
        
        [self setupSixColor];

        [_tableView.header endRefreshing];
        
        if (_currentPage == 1 && [self.dataSource count]> 0) {
            [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
        }
        
    } fail:^(NSError *error) {
//        NSLog(@"%@", error);
        [self resetLoadFlag];
        if (_currentPage == 1) {
            [self handleReadData];
        }
        [SVProgressHUD showErrorWithStatus:@"刷新失败" duration:2];
        [_tableView.header endRefreshing];
    }];
}

- (NSMutableArray *)valueArr {
    if (!_valueArr) {
        _valueArr = [NSMutableArray array];
    }
    return _valueArr;
}

- (NSMutableArray *)keyArr {
    if (!_keyArr) {
        _keyArr = [NSMutableArray array];
    }
    return _keyArr;
}

- (void)setupRightButton {
    _btn1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _btn1.frame = CGRectMake(0, 3, 63, 27);
    [_btn1 setTitleColor: [UIColor grayColor] forState:UIControlStateNormal];
//    [_btn1 setTitle:@"活动类型" forState:UIControlStateNormal];
    _btn1.backgroundColor = [UIColor orangeColor];
    UIImageView *btnImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"活动类型"]];
    _btn1.layer.masksToBounds = YES;
    _btn1.layer.cornerRadius = 5;
    _btn1.backgroundColor = [UIColor orangeColor];
    btnImageView.frame = CGRectMake(0, 0, _btn1.frame.size.width, _btn1.frame.size.height);
    [_btn1 addSubview:btnImageView];
    
    [_btn1 addTarget:self action:@selector(showMenu:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btn1];
    
    UIBarButtonItem *bar = [[UIBarButtonItem alloc] initWithCustomView:_btn1];
    self.navigationItem.rightBarButtonItem = bar;
    
    //    [KxMenu setTintColor: [UIColor colorWithRed:15/255.0f green:97/255.0f blue:33/255.0f alpha:1.0]];//渐变色
    //    [KxMenu setTitleFont:[UIFont systemFontOfSize:14]];
    
    
    UIButton *backBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    [backBtn setImage:[UIImage imageNamed:@"backOrange"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(handleLeftBarBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
    
}

- (void)handleLeftBarBtn:(UIButton *)btn {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showMenu:(UIButton *)sender
{
    NSMutableArray *menuItems = [NSMutableArray arrayWithCapacity:1];
    for (int i = 0; i < self.valueArr.count; i++) {
        [menuItems addObject:[KxMenuItem menuItem:self.valueArr[i]
                                            image:[UIImage imageNamed:@"action_icon"]
                                           target:self
                                           action:@selector(pushMenuItem:)]];
    }
    
    //    NSArray *menuItems =
    //    @[
    //
    //      [KxMenuItem menuItem:@"房产"
    //                     image:[UIImage imageNamed:@"action_icon"]
    //                    target:self
    //                    action:@selector(pushMenuItem:)],
    //
    //      [KxMenuItem menuItem:@"选秀"
    //                     image:[UIImage imageNamed:@"check_icon"]
    //                    target:self
    //                    action:@selector(pushMenuItem:)],
    //
    //      [KxMenuItem menuItem:@"买车"
    //                     image:[UIImage imageNamed:@"reload"]
    //                    target:self
    //                    action:@selector(pushMenuItem:)],
    //
    //      [KxMenuItem menuItem:@"驴友"
    //                     image:[UIImage imageNamed:@"search_icon"]
    //                    target:self
    //                    action:@selector(pushMenuItem:)],
    //
    //      [KxMenuItem menuItem:@"交友"
    //                     image:[UIImage imageNamed:@"home_icon"]
    //                    target:self
    //                    action:@selector(pushMenuItem:)],
    //      ];
    
    [KxMenu showMenuInView:self.view
                  fromRect:sender.frame
                 menuItems:menuItems];
}

- (void)pushMenuItem:(id)sender
{
//    NSLog(@"%@", sender);
    KxMenuItem *afa = sender;
//    NSLog(@"%@", afa.title);
//    NSLog(@"%@", self.valueArr);
    for (int i = 0; i < self.valueArr.count; i++) {
        NSString *value = self.valueArr[i];
        if ([afa.title isEqualToString:value]) {
            _typeid = self.keyArr[i];
//            NSLog(@"%@", _typeid);
            _currentPage = 1;
            [self loadData];
            self.titleLabel.text = value;
            return;
        }
    }
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ActivityTableViewCell *cell = (ActivityTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    _indexPath = indexPath;
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CDFDisplayViewController *displayVC = [s instantiateViewControllerWithIdentifier:@"DisplayViewContoller"];
    displayVC.tid = self.dataSource[indexPath.row][@"fields"][@"tid"];
//    NSLog(@"%@", self.dataSource[indexPath.row]);
    if ([self.dataSource[indexPath.row][@"fields"][@"special"] isEqualToString:@"4"]) {
//        NSLog(@"%@", displayVC.tid);
        displayVC.isActivity = YES;
    } else {
        displayVC.isActivity = NO;
    }
    NSString *colorStr = self.dataSource[indexPath.row][@"classname"];
    if ([colorStr length]>0) {
        displayVC.activityTypeColor = self.sixColorDic[colorStr];
    } else {
        displayVC.activityTypeColor = @"#ffd200";
    }
    displayVC.rectOfjoinBtn = cell.joinImageView.frame;
    [self.navigationController pushViewController:displayVC animated:YES];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (!_indexPath) {
        return;
    }
    if (_indexPath.row == 0) {
        [_tableView reloadData];
    }else if(_indexPath.row > 0){
        [_tableView reloadRowsAtIndexPaths:@[_indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
}

//
//- (id)refreshHeaderView
//{
//    if (!_refreshHeaderView) {
//        if ([Common systemVersion] < 6.0) {
//
//        } else {
//            UIRefreshControl *rc = [[UIRefreshControl alloc] init];
//            rc.attributedTitle = [[NSAttributedString alloc] initWithString:@"用力，不要停..."];
//            [rc addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
//            _refreshHeaderView = rc;
//        }
//    }
//    return _refreshHeaderView;
//}
//- (void)refreshView:(UIRefreshControl *)refresh
//{
//    //刷新的逻辑代码
//    if (!_isLoading) {
//        refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"我正在很用力的加载..."];
//        _currentPage = 1;
//        [self loadData];
//    } else {
//        return;
//    }
//}
//- (void)refresh:(id)sender
//{
//#warning 处理刷新crash
//    _currentPage = 1;
//    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
//    [SVProgressHUD showWithStatus:@"刷新中..."];
//    [MobClick event:@"IndexRefresh"];
//    [self loadData];
//}
//#pragma mark Private Methods
///**
// *  刷新完成以后调用
// *
// *  @param refresh 刷新控件
// *  @param message 显示的消息，如果没有特殊消息显示则会显示默认消息
// */
//- (void)refreshView:(UIRefreshControl *)refresh endRefreshWithMessage:(NSString *)message
//{
//    [self performSelector:@selector(resetLoadFlag) withObject:nil afterDelay:3];
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"MM.dd, hh:mm a"];
//    NSString *lastUpdated = [NSString stringWithFormat:@"%@ %@", [formatter stringFromDate:[NSDate date]], (message.length > 0 ? message : @"刷新成功")];
//    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:lastUpdated];
//    [refresh endRefreshing];
//}
- (void)loadMore{
    if (_hasMore && !_isLoading) {
        _currentPage ++;
        [self loadData];
    }
}
-(void)setup{
    _currentPage = 1;
    _isLoading = NO;
    _hasMore = YES;
    
//    self.refreshControl = self.refreshHeaderView;
}

- (void)resetLoadFlag
{
    _isLoading = NO;
}


- (void)guidangWithDic:(NSDictionary *)dic{
    NSMutableData *mData = [NSMutableData data];
    //创建对象
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:mData];
    //归档
    [archiver encodeObject:dic forKey:@"activityHuanCun"];
    //3.结束归档.结束归档之后在归档无效
    [archiver finishEncoding];
    BOOL isSucess = [mData writeToFile:[self getFilePath] atomically:YES];
//    NSLog(@"%d", isSucess);
    
}

- (void)handleReadData {
    //1.从本地读取数据
    NSData *mData = [NSData dataWithContentsOfFile:[self getFilePath]];
    //2.创建反归档对象
    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:mData];
    //3.反归档
    NSDictionary *indexDic = [unarchiver decodeObjectForKey:@"activityHuanCun"];
    //4.结束反规档
    [unarchiver finishDecoding];
//    NSLog(@"%@", indexDic);
    
    NSArray *arr =indexDic[@"Variables"][@"forum_threadlist"];
    [self.dataSource addObjectsFromArray:arr];
    self.typeArr = indexDic[@"Variables"][@"threadtypes"][@"types"];
//    NSLog(@"%@", self.dataSource);
    [_tableView reloadData];
}

- (NSString *)getFilePath {
    //1.获取Documents文件夹路径
    NSString *filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    //2.拼接上文件路径
    NSString *newFilePath = [filePath stringByAppendingPathComponent:@"activityHuanCun.data"];
    return newFilePath;
}

@end
