//
//  ZWDSearchUserResultViewController.h
//  yanyu
//
//  Created by Case on 15/4/22.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "ParentViewController.h"
#import "ZWDSearchUserCell.h"
#import "CDFMeViewController.h"

@interface ZWDSearchUserResultViewController : ParentViewController
@property (nonatomic,copy)NSString *key;
@end
