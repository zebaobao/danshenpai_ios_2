//
//  ShowViewController.h
//  yanyu
//
//  Created by Case on 15/4/10.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "ParentViewController.h"
//#import "ZWDShowCell.h"
//#import "ZWDShowLayOut.h"

//#import "UIScrollView+LoadingMore.h"
//#import "ZBFlowView.h"
//#import "ZBWaterView.h"
#import "PSCollectionView.h"
#import "PullPsCollectionView.h"


@interface ShowViewController : ParentViewController<PSCollectionViewDelegate,PSCollectionViewDataSource,UIScrollViewDelegate,PullPsCollectionViewDelegate>
@property(nonatomic,retain) PullPsCollectionView *collectionView;
@property(nonatomic,retain)NSMutableArray *items;
//-(void)loadDataSource;
@end
