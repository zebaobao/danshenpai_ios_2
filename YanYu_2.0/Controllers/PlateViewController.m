//
//  PlateViewController.m
//  yanyu
//
//  Created by caiyee on 15/4/10.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "PlateViewController.h"
#import "HappyJourneyViewCell.h"
#import "CircleImageView.h"
#import "CDFTabBarController.h"
#import "CDFDisplayViewController.h"
#import "PPiFlatSegmentedControl.h"
#import "NSString+FontAwesome.h"
#import "UITableView+Wave.h"
#import "UITableView+AnimateReloadData.h"
#import "CDFPostingViewController.h"
#import "CDFNavigationController.h"

#import "ZWDLoadMoreCell.h"

#define KHeightOfSegment   30

@interface PlateViewController ()<CircleImageVIewDelegate,UIAlertViewDelegate>{
    int _currentPage;
    BOOL _isLoading;
    
    CGPoint     _scrollPoint;
    BOOL        _hasMore;
    //判断是否点击的tableView的头视图上的按钮
    BOOL flag;
}
@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, copy) NSString *orderBy;
@property (nonatomic, strong) UISegmentedControl *segment;

//刷新所需属性
@property (nonatomic,strong) id refreshHeaderView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingActInd;
@property (strong, nonatomic) IBOutlet UILabel *loadingLabel;
@property (strong, nonatomic) IBOutlet UIView *tableHeaderView;


@property (strong, nonatomic) id mainData;

@end

@implementation PlateViewController

-(void)setup{
    _currentPage = 1;
    _isLoading = NO;
    _hasMore = YES;
    
    self.refreshControl = self.refreshHeaderView;
    
    CGRect frame = self.tableView.frame;
    frame.size.height += 49;
    self.tableView.frame = frame;
}
- (id)refreshHeaderView
{
    if (!_refreshHeaderView) {
        if ([Common systemVersion] < 6.0) {
            
        } else {
            UIRefreshControl *rc = [[UIRefreshControl alloc] init];
            rc.attributedTitle = [[NSAttributedString alloc] initWithString:@"用力，不要停..."];
            [rc addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
            _refreshHeaderView = rc;
        }
    }
    return _refreshHeaderView;
}
- (void)refreshView:(UIRefreshControl *)refresh
{
    //刷新的逻辑代码
    if (!_isLoading) {
        refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"我正在很用力的加载..."];
        _currentPage = 1;
        [self loadData];
    } else {
        return;
    }
}
- (void)refresh:(id)sender
{
#warning 处理刷新crash
    _currentPage = 1;
    //[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    [SVProgressHUD showWithStatus:@"刷新中..."];
    [MobClick event:@"IndexRefresh"];
    [self loadData];
}
#pragma mark Private Methods
/**
 *  刷新完成以后调用
 *
 *  @param refresh 刷新控件
 *  @param message 显示的消息，如果没有特殊消息显示则会显示默认消息
 */
- (void)refreshView:(UIRefreshControl *)refresh endRefreshWithMessage:(NSString *)message
{
    [self performSelector:@selector(resetLoadFlag) withObject:nil afterDelay:3];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM.dd, hh:mm a"];
    NSString *lastUpdated = [NSString stringWithFormat:@"%@ %@", [formatter stringFromDate:[NSDate date]], (message.length > 0 ? message : @"刷新成功")];
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:lastUpdated];
    [refresh endRefreshing];
}
- (void)loadMore{
    if (_hasMore && !_isLoading) {
        _currentPage ++;
        [self loadData];
    }
}
/*
 - (void)scrollViewDidScroll:(UIScrollView *)scrollView
 {
 //shang拉加载更多
 if (scrollView.contentSize.height - scrollView.contentOffset.y < CGRectGetHeight(scrollView.frame) + 50) {
 if (_hasMore && !_isLoading) {
 _currentPage++;
 _scrollPoint = CGPointMake(0, scrollView.contentOffset.y);
 [self loadData];
 }
 }
 }
 */
#pragma mark - 发送帖子成功接受通知
- (void)handlePostSuccess:(NSNotification *)note
{
    [self refresh:note];
    
    //[self.tabBarController setSelectedIndex:0];
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)resetLoadFlag
{
    _isLoading = NO;
}
//- (UISegmentedControl *)segment {
//    if (!_segment) {
//        NSArray *arr = @[@"最新回复", @"最新发帖", @"精华帖"];
//        _segment = [[UISegmentedControl alloc] initWithItems:arr];
//        [_segment addTarget:self action:@selector(controllerPressed:) forControlEvents:UIControlEventValueChanged];
//        _segment.frame = CGRectMake(0, 0, KDeviceSizeWidth, KHeightOfSegment);
//        _segment.backgroundColor = [UIColor whiteColor];
//        _segment.segmentedControlStyle= UISegmentedControlStyleBar;
//        _segment.tintColor = [UIColor orangeColor];
//    }
//    return _segment;
//}

- (void)customizedNavigationItem {
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"tab背景"]forBarMetrics:UIBarMetricsDefault];
    
    UILabel *titleLable = [[UILabel alloc]initWithFrame:CGRectMake(KDeviceSizeWidth/2, 20, 30, 30)];
    titleLable.text = self.title;
    titleLable.textAlignment = NSTextAlignmentCenter;
    titleLable.textColor = RGBACOLOR(250, 66, 4, 1.0);
    self.navigationItem.titleView = titleLable;
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    button1.frame = CGRectMake(KDeviceSizeWidth-30, 0, 25, 25);
    [button1 setImage:[UIImage imageNamed:@"btn_post_unsel"] forState:UIControlStateNormal];
    [button1 setImage:[UIImage imageNamed:@"btn_post_sel"] forState:UIControlStateSelected];
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(0, 0, 10, 20);
    [backBtn setImage:[UIImage imageNamed:@"backOrange"] forState:UIControlStateNormal];
    UIBarButtonItem *ba = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = ba;
    
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc]initWithCustomView:button1];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    button2.frame = CGRectMake(0, 0, 25, 25);
    [button2 setImage:[UIImage imageNamed:@"icon_02"] forState:UIControlStateNormal];
    UIBarButtonItem *item2 = [[UIBarButtonItem alloc]initWithCustomView:button2];
    [button1 addTarget:self action:@selector(handleButtonPostMessage:) forControlEvents:UIControlEventTouchUpInside];
    [backBtn addTarget:self action:@selector(BlackBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [button2 addTarget:self action:@selector(handleButtonToSearch:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItems = @[item1];
}
//返回
- (void)BlackBtnClick:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
//点击搜索
- (void)handleButtonToSearch:(UIButton *)sender {
//    NSLog(@"");
}

#pragma mark - 点击发帖button

- (void)handleButtonPostMessage:(UIButton *)sender {
    //判断是否登陆
    if (![CDFMe shareInstance].isLogin) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"温馨提醒" message:@"您还没有登录哦" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"登录", nil];
        alertView.delegate = self;
        [alertView show];
        return;
    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CDFNavigationController *VC = [storyboard instantiateViewControllerWithIdentifier:@"CDFNavigationController"];
    CDFPostingViewController *pVC = [storyboard instantiateViewControllerWithIdentifier:@"CDFPostingViewController"];
    pVC.isShowViewController = YES;
    NSArray *arr = @[
                     @{@"name":@"美食",@"gid":@"402",@"fid":@"1205",@"typeid":@"0",@"color":@"ff638e",@"icon":@"food",@"show":@"0",@"default":@"0"},
                     @{@"name":@"快旅",@"gid":@"441",@"fid":@"53",@"typeid":@"0",@"color":@"ffa300",@"food":@"food",@"show":@"1",@"default":@"0"},
                     @{@"name":@"骑友",@"gid":@"441",@"fid":@"593",@"typeid":@"138",@"color":@"ffa300",@"icon":@"food",@"show":@"1",@"default":@"0"},
                     @{@"name":@"拍客",@"gid":@"441",@"fid":@"780",@"typeid":@"1390",@"color":@"ffa300",@"icon":@"food",@"show":@"1",@"default":@"0"},
                     @{@"name":@"亲子",@"gid":@"402",@"fid":@"528",@"typeid":@"0",@"color":@"ffa300",@"icon":@"food",@"show":@"1",@"default":@"0"},
                     @{@"name":@"败家",@"gid":@"402",@"fid":@"496",@"typeid":@"214",@"color":@"ffa300",@"icon":@"food",@"show":@"1",@"default":@"0"},
                     @{@"name":@"贴图",@"gid":@"1010",@"fid":@"569",@"typeid":@"0",@"color":@"ffa300",@"icon":@"food",@"show":@"1",@"default":@"0"},
                     @{@"name":@"精彩",@"gid":@"441",@"fid":@"1006",@"typeid":@"214",@"color":@"ffa300",@"icon":@"food",@"show":@"1",@"default":@"0"},
                     @{@"name":@"车友",@"gid":@"1008",@"fid":@"117",@"typeid":@"0",@"color":@"ffa300",@"icon":@"food",@"show":@"1",@"default":@"0"}];
    
    
    NSDictionary *dict;
    if ([self.mainData[@"threadtypes"][@"required"] boolValue]) {
        // dict = @{@"name":self.title,@"gid":@"1008",@"fid":self.fid,@"typeid":[[self.mainData[@"threadtypes"][@"types"] allKeys] firstObject],@"color":@"ffa300",@"icon":@"food",@"show":@"1",@"default":@"1"};
        dict = @{@"name":self.title,@"gid":@"1008",@"fid":self.fid,@"typeid":@"0",@"color":@"ffa300",@"icon":@"food",@"show":@"1",@"default":@"1"};
        
    }else{
        dict = @{@"name":self.title,@"gid":@"1008",@"fid":self.fid,@"typeid":@"0",@"color":@"ffa300",@"icon":@"food",@"show":@"1",@"default":@"1"};
        
    }
    
//    NSLog(@"%@",dict);
//    NSLog(@"%@--",self.fid);
//    NSLog(@"%@",self.mainData);
    /*
     NSLog(@"%@",self.fid);
     NSMutableArray *Marr = [NSMutableArray array];
     for (int i = 0; i < arr.count; i++) {
     
     NSDictionary *dict = arr[i];
     NSMutableDictionary *Mdict = [NSMutableDictionary dictionaryWithDictionary:dict];
     
     if ([dict[@"fid"]isEqualToString:self.fid]) {
     [Mdict setValue:@"1" forKey:@"default"];
     }
     [Marr addObject:Mdict];
     NSLog(@"%d",i);
     }
     NSLog(@"%@",Marr);
     */
    NSMutableArray *Marr = [NSMutableArray array];
    [Marr addObject:dict];
    //[Marr addObjectsFromArray:arr];
    [CDFMe shareInstance].board = Marr;
    
    [self presentViewController:VC animated:YES completion:nil];
    //[self.navigationController pushViewController:pVC animated:YES];
    
}
//点击alertView的确定键去登陆
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [[CDFMe shareInstance] wellcomeLogin:self];
    }
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [SVProgressHUD dismiss];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //    [(CDFTabBarController *)[[UIApplication sharedApplication].delegate window].rootViewController setHidden:NO];//隐藏tabbar
    [self customizedNavigationItem];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}
- (NSMutableArray *)dataSource {
    if (!_dataSource) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}
- (void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:kPOST_SUCCESS object:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
//    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, KDeviceSizeWidth, KDeviceSizeHeight) style:UITableViewStylePlain];
    [self setup];
    [self handleRead];
    //self.orderBy = @"&filter=author&orderby=dateline";//按照全部来请求数据
    //默认最新发帖
    self.orderBy = @"&filter=author&orderby=dateline";//最新
    [self hreadView];
//    self.segment.selectedSegmentIndex = 0;
    [self loadData];
    
    [self.tableView registerClass:[HappyJourneyViewCell class] forCellReuseIdentifier:@"HappyJourneyViewCell"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handlePostSuccess:) name:kPOST_SUCCESS object:nil];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    //    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KDeviceSizeWidth, KHeightOfSegment)];
    //    [headerView addSubview:self.segment];
    //    return headerView;
    return _tableHeaderView;
}
//设置tableView的头标题视图
- (void)hreadView{
    _tableHeaderView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KDeviceSizeWidth, KHeightOfSegment+20)];
    _tableHeaderView.tag = 999;
    _tableHeaderView.userInteractionEnabled = YES;
    _tableHeaderView.backgroundColor = [UIColor whiteColor];
    UIImageView *im = [[UIImageView alloc]initWithFrame:CGRectMake(0, 40, KDeviceSizeWidth, 10)];
    im.image = [UIImage imageNamed:@"分割线"];
    [_tableHeaderView addSubview:im];
    NSArray *titArr = @[@"最新回复", @"最新发帖", @"精华帖"];
    for (NSInteger i = 0;i < titArr.count ; i ++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [btn setTitle:titArr[i] forState:UIControlStateNormal];
        btn.frame = CGRectMake(KDeviceSizeWidth/3 * i, 0, KDeviceSizeWidth/3, 38);
        btn.tag = 100 + i;
        if (btn.tag == 101) {
            [btn setTintColor:[UIColor redColor]];
        } else {
            [btn setTintColor:[UIColor grayColor]];
        }
        [btn addTarget:self action:@selector(onHeardViewBtn:) forControlEvents:UIControlEventTouchUpInside];
        [_tableHeaderView addSubview:btn];
    }
    //按钮下红线
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(KDeviceSizeWidth/3, 38, KDeviceSizeWidth/3, 2)];
    label.backgroundColor = [UIColor grayColor];
    [_tableHeaderView addSubview:label];
    label.tag = 200 ;
    label.backgroundColor = [UIColor redColor];
    [_tableHeaderView addSubview:label];
}
//点击按钮后改变状态的事件
- (void)onHeardViewBtn:(UIButton *)btn{
    NSInteger index = btn.tag-100;
    UIView *bgView = (UIView *)[self.view viewWithTag:999];
    for (UIView *view in bgView.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            //拿到btn
            UIButton *currentBtn = (UIButton *)view;
            //变色
            if (currentBtn.tag == btn.tag) {
                //为选中状态
                if (flag==NO) {
                    [currentBtn setTintColor:[UIColor redColor]];
                    flag = YES;
                }
            }else{
                //常态
                [currentBtn setTintColor:[UIColor grayColor]];
                flag = NO;
            }
        }
    }
    //改变tipLabel的横坐标
    UILabel *tipLabel = (UILabel *)[bgView viewWithTag:200];
    
    CGRect frame = tipLabel.frame;
    //改变x
    frame.origin.x = index*(KDeviceSizeWidth/3);
    //将更改后的frame重新赋值给label
    tipLabel.frame = frame;
    if (tipLabel.frame.origin.x == (KDeviceSizeWidth/3)) {
        [btn setTintColor:[UIColor redColor]];
        self.orderBy = @"";
    }
    //改变数据
    if (btn.tag == 100) {
        self.orderBy = @"";
    } else if (btn.tag == 101) {
        self.orderBy = @"&filter=author&orderby=dateline";//最新
    } else {
        self.orderBy = @"&filter=digest&digest=1&orderby=dateline";//精华
    }
    _currentPage = 1;
    [self loadData];
}

//- (void)controllerPressed:(id)sender {
//    int index = ((UISegmentedControl *)sender).selectedSegmentIndex;
//    if (index == 0) {
//        ((UISegmentedControl *)sender).selectedSegmentIndex = 0;
//        self.orderBy = @"";
//    } else if (index == 1) {
//        ((UISegmentedControl *)sender).selectedSegmentIndex = 1;
//        self.orderBy = @"&filter=author&orderby=dateline";//最新
//    } else {
//        ((UISegmentedControl *)sender).selectedSegmentIndex = 2;
//        self.orderBy = @"&filter=digest&digest=1&orderby=dateline";//精华
//    }
//    self.isClickSeg = YES;
//    _currentPage = 1;
//    [self loadData];
//    //    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
//    //[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
//    
//}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return KHeightOfSegment+20;
}

- (void)loadData {
    _isLoading = YES;
    NSLog(@"%@", self.orderBy);
    NSString *url = [NSString stringWithFormat:@"%@api/mobile/?version=%@&module=forumdisplay&page=%d%@%@",kBaseURL,kApiVersion ,_currentPage, [NSString stringWithFormat:@"&fid=%@",self.fid], self.orderBy];
//    NSLog(@"%@",url);
    [SVProgressHUD showWithStatus:@"加载中..."];
    [[HXHttpClient shareInstance] grabURL:url success:^(id responseObject) {
        [SVProgressHUD dismiss];
//        NSLog(@"%@", responseObject);
        self.mainData = responseObject[@"Variables"];
        
        if (self.isClickSeg && self.dataSource.count > 0) {
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
            self.isClickSeg = NO;
        }
        
        if (_currentPage == 1) {
            [self.dataSource removeAllObjects];
            [self refreshView:self.refreshHeaderView endRefreshWithMessage:@"刷新成功"];
        }
//        NSMutableArray *arr;
        if ([self.orderBy isEqualToString:@""]) {
            //最新回复
            NSLog(@"%@", self.dataSource);
            [self.dataSource addObjectsFromArray:responseObject[@"Variables"][@"forum_threadlist"]];
        }else{
            for (NSDictionary *dict in responseObject[@"Variables"][@"forum_threadlist"]) {
                if (![dict[@"displayorder"] boolValue]) {
                    [self.dataSource addObject:dict];
                }
            }
        }
//        [self.dataSource addObjectsFromArray:arr];
//        flag = YES;
        
        [self.tableView reloadData];
        [SVProgressHUD dismiss];
        [self resetLoadFlag];
        
        if (_currentPage == 1 && self.dataSource.count > 0) {
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
        
        if (_currentPage == 1 && [_orderBy isEqualToString: @"&filter=author&orderby=dateline"]) {
            NSLog(@"%@", responseObject);
            [Common archive:responseObject withKey:[NSString stringWithFormat:@"huanCunFida%@", self.fid]];
        }
        
    } fail:^(NSError *error) {
        [SVProgressHUD dismiss];
        
        [SVProgressHUD showErrorWithStatus:@"数据刷新失败!" duration:2];
        if (_currentPage == 1) {
            [self refreshView:self.refreshHeaderView endRefreshWithMessage:@"数据刷新失败"];
        }else{
            _currentPage -- ;
        }
        
        [self resetLoadFlag];
    }];
}
//
- (void)handleRead {
    NSDictionary *dic = [Common unarchiveForKey:[NSString stringWithFormat:@"huanCunFida%@", self.fid]];
    NSLog(@"%@",dic);
    if (dic) {
        self.mainData = dic[@"Variables"];
        [self.dataSource removeAllObjects];
        NSArray *arr =dic[@"Variables"][@"forum_threadlist"];
        [self.dataSource addObjectsFromArray:arr];
        [self.tableView reloadData];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete method implementation.
    if (self.dataSource.count > 0) {
        return self.dataSource.count + 1;
    }else{
        return self.dataSource.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    NSLog(@"%d---%d",indexPath.row,self.dataSource.count);
    if (indexPath.row == self.dataSource.count && self.dataSource.count > 0) {
        //加载更多cell
        static NSString *cellID = @"ZWDLoadMoreCell";
        
        ZWDLoadMoreCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"ZWDLoadMoreCell" owner:self options:nil]lastObject];
        }
        
        //if ([self.mainData[@"forum_threadlist"][@"count"] integerValue] > self.dataSource.count) {
        if (YES){
            cell.contentLable.text = @"上拉加载更多……";
            [self loadMore];
        }else{
            cell.contentLable.text = @"加载完毕……";
        }
        
        return cell;
    }
    
    
    HappyJourneyViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"HappyJourneyViewCell" forIndexPath:indexPath];
    cell.avataImage.delegate = self;
    cell.avataImage.layer.cornerRadius = 20;
    cell.contentLabel.text = self.dataSource[indexPath.row][@"subject"];
    [cell.avataImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://bang.dahe.cn/uc_server/avatar.php?uid=%@&size=big",self.dataSource[indexPath.row][@"authorid"]]] placeholderImage:[UIImage imageNamed:@"河宝娃"]];
    CGRect frame = cell.contentLabel.frame;
    CGFloat heightOfLabel = [HappyJourneyViewCell detailTextHeight:self.dataSource[indexPath.row][@"subject"]];
    if (heightOfLabel < 20.0) {
        heightOfLabel = heightOfLabel + 10;
    }
    cell.contentLabel.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, heightOfLabel);
    cell.contentLabel.numberOfLines = 0;
    cell.contentLabel.font = [UIFont fontWithName:nil size:15];
    //回复
    cell.replyAndViewsView.frame = CGRectMake(cell.contentLabel.frame.origin.x, cell.contentLabel.frame.origin.y + cell.contentLabel.frame.size.height + 12, 100, 10);
    cell.replyAndViewLable.text = [NSString stringWithFormat:@"%@  浏览%@",self.dataSource[indexPath.row][@"replies"],self.dataSource[indexPath.row][@"views"]];
//    //点赞
//    cell.AgreeView.frame = CGRectMake(cell.contentLabel.frame.origin.x, cell.contentLabel.frame.origin.y + cell.contentLabel.frame.size.height + 12, 100, 10);
//    cell.AgreeLable.text = [NSString stringWithFormat:@"%@",self.dataSource[indexPath.row][@"recommend_add"]];
    
    CGFloat heightOfCell = [HappyJourneyViewCell heightForCellWithStr:self.dataSource[indexPath.row][@"subject"]];
    CGRect frameOfAvatar = cell.avataImage.frame;
    cell.avataImage.frame = CGRectMake(frameOfAvatar.origin.x, (heightOfCell - (KAvataImageHeightOrWidth + 17)) / 2.0 , frameOfAvatar.size.width, frameOfAvatar.size.height);
    
    CGRect avataFrame = cell.avataImage.frame;
    cell.nameLabel.frame = CGRectMake(avataFrame.origin.x, avataFrame.origin.y + avataFrame.size.height + 2, 100, 15);
    cell.nameLabel.text = self.dataSource[indexPath.row][@"author"];
    CGPoint pointOfNameLabel = CGPointMake(cell.avataImage.center.x, cell.nameLabel.center.y);
    [cell.nameLabel setCenter:pointOfNameLabel];
//    NSLog(@"%@", self.dataSource[indexPath.row][@"dateline"]);
    NSInteger dateline = [self.dataSource[indexPath.row][@"dbdateline"] integerValue];
//    NSLog(@"%d", dateline);
    NSString *time = [[self class] formatDate:dateline];
    cell.timeLabel.text = time;
    CGRect timeLabelFrame = cell.replyAndViewsView.frame;
    cell.timeLabel.frame = CGRectMake(timeLabelFrame.origin.x + 95, timeLabelFrame.origin.y - 3, 100, 15);
    cell.timeLabel.font = [UIFont fontWithName:nil size:12];
    cell.timeLabel.textAlignment = NSTextAlignmentRight;
    cell.timeLabel.textColor = [Common colorWithHex:@"#949494" alpha:1];
    
#pragma mark - 置顶标志暂时取消
    /*
    if ([self.dataSource[indexPath.row][@"displayorder"] boolValue]) {
        
        [cell addSubview:cell.ZDImgV];
    }else{
        [cell.ZDImgV removeFromSuperview];
    }
    */
    return cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == self.dataSource.count) {
        return 60;
    }
    
    return [HappyJourneyViewCell heightForCellWithStr:self.dataSource[indexPath.row][@"subject"]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == self.dataSource.count) {
        return;
    }
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CDFDisplayViewController *displayVC = [s instantiateViewControllerWithIdentifier:@"DisplayViewContoller"];
    displayVC.tid = self.dataSource[indexPath.row][@"tid"];
    if ([self.dataSource[indexPath.row][@"special"] isEqualToString:@"4"]) {
        displayVC.isActivity = YES;
    }else{
        displayVC.isActivity  = NO;

    }
    //[(CDFTabBarController *)[[UIApplication sharedApplication].delegate window].rootViewController setHidden:YES];
    [self.navigationController pushViewController:displayVC animated:YES];
}

+ (NSString *)formatDate:(NSTimeInterval)timeInterval {
    NSDate *myDate = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [NSLocale currentLocale];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm";
    NSString *dateStr = [dateFormatter stringFromDate:myDate];//目标时间, 字符串格式
    NSDate *nowDate = [NSDate date];
    NSString *nowDateStr = [dateFormatter stringFromDate:nowDate]; //当前的时间, 字符串格式
    NSTimeInterval nowTime = [[NSDate date] timeIntervalSince1970]; //当前时间到1970年的秒数
    long long int nowtime = (long long) nowTime;
    long long int cha = nowtime - timeInterval;
//    NSLog(@"%@", dateStr);
    //对时间进行优化显示
    if ([[dateStr substringWithRange:NSMakeRange(0, 4)] isEqualToString:[nowDateStr substringWithRange:NSMakeRange(0, 4)]]) {
        if ([[dateStr substringWithRange:NSMakeRange(5, 5)] isEqualToString:[nowDateStr substringWithRange:NSMakeRange(5, 5)]]) {
            if (cha / 3600.0 <= 1) {
                if (cha/60 == 0) {
                    dateStr = [NSString stringWithFormat:@"%ld秒之前", (long int)cha];
                } else {
                    dateStr = [NSString stringWithFormat:@"%ld分钟之前", (long int)cha/60];
                }
            }else if (cha / 3600.0<= 2 && cha / 3600.0 > 1) {
                dateStr = @"1小时前";
            }else if (cha / 3600.0<=3 && cha / 3600.0 > 2) {
                dateStr = @"2小时前";
            } else {
                dateStr = [NSString stringWithFormat:@"今天%@", [dateStr substringWithRange:NSMakeRange(11, 5)]];
                //                NSLog(@"今天 %@", [dateStr substringWithRange:NSMakeRange(11, 5)]);
            }
        }else if ([[nowDateStr substringWithRange:NSMakeRange(8, 2)] integerValue] - [[dateStr substringWithRange:NSMakeRange(8, 2)] integerValue] == 1) {
//            NSLog(@"昨天%@", [dateStr substringWithRange:NSMakeRange(11, 5)]);
            dateStr = [NSString stringWithFormat:@"昨天%@", [dateStr substringWithRange:NSMakeRange(11, 5)]];
        } else {
//            NSLog(@"%@", [dateStr substringWithRange:NSMakeRange(5, 11)]);
//            NSLog(@"%@", [dateStr substringWithRange:NSMakeRange(5, 5)]);
            //            dateStr = [NSString stringWithFormat:@"%@", [dateStr substringWithRange:NSMakeRange(5, 11)]]; //如果是前天或者更早的日期, 显示日期和具体时间
            dateStr = [NSString stringWithFormat:@"%@", [dateStr substringWithRange:NSMakeRange(5, 5)]];//如果是前天或者更早的日期, 只显示日期, 不显示具体时间
        }
    } else {
//        NSLog(@"dateStr:%@", dateStr);
        dateStr = [dateStr substringWithRange:NSMakeRange(0, 10)];
    }
    return dateStr;
}

//点击cell上的头像 实现的代理方法
- (void)handleCircleImageViewTapGesture:(UITapGestureRecognizer *)gesture {
    HappyJourneyViewCell *cell = (HappyJourneyViewCell *)gesture.view.superview;
    NSIndexPath *path = [self.tableView indexPathForCell:cell];
    NSString *str = [NSString stringWithFormat:@"section:%d, row:%d, superClass:%@", path.section, path.row, gesture.view.superview];
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CDFMeViewController *vc = [s instantiateViewControllerWithIdentifier:@"userViewController"];
    vc.uid = self.dataSource[path.row][@"authorid"];
//    NSLog(@"%@",vc.uid);
    [(CDFTabBarController *)[[UIApplication sharedApplication].delegate window].rootViewController setHidden:YES];//隐藏tabbar
    //[(CDFTabBarController *)[[UIApplication sharedApplication].delegate window].rootViewController setHidden:YES];//隐藏tabbar
    [self.navigationController pushViewController:vc animated:YES];
}

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


//- (void)guidangWithDic:(NSDictionary *)dic name:(NSString *)name {
//    NSMutableData *mData = [NSMutableData data];
//    //创建对象
//    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:mData];
//    //归档
//    [archiver encodeObject:dic forKey:name];
//    //3.结束归档.结束归档之后在归档无效
//    [archiver finishEncoding];
////    NSLog(@"%@", name);
//    BOOL isSucess = [mData writeToFile:[self getFilePathWithName:name] atomically:YES];
////    NSLog(@"%d", isSucess);
//    
//}
//
//- (NSDictionary *)handleReadDataWithName:(NSString *)name {
//    //1.从本地读取数据
////    NSData *mData = [NSData dataWithContentsOfFile:[self getFilePathWithName:name]];
////    //2.创建反归档对象
////    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:mData];
////    //3.反归档
//    NSDictionary *indexDic = [Common unarchiveForKey:@""];
////    //4.结束反规档
////    [unarchiver finishDecoding];
//////    NSLog(@"%@", indexDic);
//    
//    return indexDic;
//}
//
//- (NSString *)getFilePathWithName:(NSString *)name {
//    //1.获取Documents文件夹路径
//    NSString *filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
//    //2.拼接上文件路径
//    NSString *newFilePath = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.data", name]];
//    return newFilePath;
//}

@end
