//
//  ZWDSearchUserResultViewController.m
//  yanyu
//
//  Created by Case on 15/4/22.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "ZWDSearchUserResultViewController.h"

@interface ZWDSearchUserResultViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;
    
    NSDictionary *_dataDict;
    
    NSArray *_dataArr;
}
@end

@implementation ZWDSearchUserResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self showUI];
    [self loadData];
}
- (void)loadData{
    
    NSString *url = [NSString stringWithFormat:@"%@api/mobile/?version=%@&module=searchuser&username=%@",kBaseURL,kApiVersion,self.key];
    NSString *s = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //[self.keyword stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
//    NSLog(@"%@",s);
//    NSLog(@"%@",url);
    
    [[HXHttpClient shareInstance]grabURL:s success:^(id responseObject) {
        if (responseObject[@"Variables"][@"searchuser"]) {
            _dataDict = responseObject[@"Variables"][@"searchuser"];
            _dataArr = [_dataDict allKeys];
            [_tableView reloadData];
            
        }
        
    } fail:^(NSError *error) {
//        NSLog(@"xiazaishibai ");
    }];
    
}
- (void)showUI{
    self.title = @"搜索结果";
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KDeviceSizeWidth, self.view.bounds.size.height) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    
}
#pragma mark - 搜索用户
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CDFMeViewController *vc = [s instantiateViewControllerWithIdentifier:@"userViewController"];
    vc.uid = _dataDict[_dataArr[indexPath.row]][@"uid"];
    [self.navigationController pushViewController:vc animated:YES];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellID = @"ZWDSearchUserCell";
    ZWDSearchUserCell *cell = [tableView dequeueReusableHeaderFooterViewWithIdentifier:cellID];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"ZWDSearchUserCell" owner:self options:nil]firstObject];
    }
    [cell showData:_dataDict[_dataArr[indexPath.row]]];
    
    return cell;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [_dataDict allKeys].count;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
