//
//  ActivityCommitViewController.h
//  yanyu
//
//  Created by caiyee on 15/4/15.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZWDActivityResultViewController.h"

@interface ActivityCommitViewController : UIViewController
@property (strong, nonatomic) id tid;
@property (strong, nonatomic) id uid;
@property (strong, nonatomic) id fid;
@property (strong, nonatomic) id pid;
@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *numberOfPerson;

@property (weak , nonatomic) UIViewController *viewController;

@property (strong,nonatomic) NSDictionary *specialDict;
@end
