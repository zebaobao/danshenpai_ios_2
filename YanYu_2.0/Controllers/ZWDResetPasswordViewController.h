//
//  ZWDResetPasswordViewController.h
//  yanyu
//
//  Created by Case on 15/4/24.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "ParentViewController.h"
#import "ZWDResetCell.h"
#import "ZWDResetOtherCell.h"

@interface ZWDResetPasswordViewController : ParentViewController<UITableViewDataSource,UITableViewDelegate,ZWDResetOtherCellDelegate>

@end
