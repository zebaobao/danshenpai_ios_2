//
//  Timer.h
//  MedicalAPP
//
//  Created by NPHD on 14-11-3.
//  Copyright (c) 2014年 NPHD. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TimerAction : NSObject

@property (nonatomic,copy) void(^callback)();
@property (nonatomic,assign) int timer;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,assign) BOOL isValid;

@end


@interface Timer : NSObject

+ (Timer *)shareTimer;

-(void)registerAction:(void(^)())block andTimer:(int)timer andName:(NSString *)name;

-(void)setValid:(BOOL)isvalid ForName:(NSString *)name;
@end
