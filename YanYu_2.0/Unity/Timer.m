//
//  Timer.m
//  MedicalAPP
//
//  Created by NPHD on 14-11-3.
//  Copyright (c) 2014年 NPHD. All rights reserved.
//

#import "Timer.h"


@implementation TimerAction

@end

@interface Timer ()
{
    NSTimer *_timer;
    NSMutableArray *_actionArray;
}
@end

@implementation Timer
+(Timer *)shareTimer{
    static Timer *t = nil;
    if (t == nil) {
        t = [[Timer alloc]init];
    }
    return t;
}
- (id)init{
    if (self = [super init]) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:1.0/60.0 target:self selector:@selector(timerClick) userInfo:nil repeats:YES];
        _actionArray = [[NSMutableArray alloc]init];
    }
    return self;
}
static int count = 0;

- (void)timerClick{
    count ++;
    for (TimerAction *a in _actionArray) {
        if (a.isValid && count%a.timer == 0) {
            a.callback();
        }
    }
}
-(void)registerAction:(void(^)())block andTimer:(int)timer andName:(NSString *)name
{
    TimerAction *action = [[TimerAction alloc]init];
    action.callback = block;
    action.timer = timer;
    action.name = name;
    action.isValid = YES;
    [_actionArray addObject:action];
}

-(void)setValid:(BOOL)isvalid ForName:(NSString *)name
{
    for (TimerAction *a in _actionArray) {
        if ([a.name isEqualToString:name]) {
            a.isValid = isvalid;
        }
    }
}
@end
