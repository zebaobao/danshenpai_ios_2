//
//  ApplyViewController.m
//  yanyu
//
//  Created by caiyee on 15/4/17.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "ApplyViewController.h"

@interface ApplyViewController ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *countOfPerson;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *otherMessageTextField;
@property (weak, nonatomic) IBOutlet UIView *genderView;
@property (weak, nonatomic) IBOutlet UIButton *commitBtn;
@property (weak, nonatomic) IBOutlet UIButton *resultBtn;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation ApplyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.scrollView.contentSize = CGSizeMake(KDeviceSizeWidth, 1000);
    self.scrollView.scrollEnabled = YES;
    self.scrollView.backgroundColor = [UIColor redColor];
    // Do any additional setup after loading the view from its nib.
}
- (IBAction)handleCommit:(id)sender {
}
- (IBAction)handleResult:(id)sender {
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
