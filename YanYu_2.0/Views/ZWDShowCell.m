//
//  ZWDShowCell.m
//  yanyu
//
//  Created by Case on 15/4/16.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "ZWDShowCell.h"
#import "UIImageView+WebCache.h"

@implementation ZWDShowCell
{
    UIImageView *_imageView;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self uiConfig];
    }
    return self;
}
-(void)uiConfig
{
    _imageView = [[UIImageView alloc]initWithFrame:CGRectMake(5, 5, self.contentView.frame.size.width - 10, self.contentView.frame.size.height - 10)];
    _imageView.contentMode = UIViewContentModeScaleAspectFit;

    //_imageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
    
    [self.contentView addSubview:_imageView];

}

- (void)showData:(NSDictionary *)data{
    
    _imageView.userInteractionEnabled = YES;
    _imageView.image = nil;
    
    _imageView.frame = CGRectMake(5, 5, self.contentView.frame.size.width - 10, self.contentView.frame.size.height - 10);
    
//    NSLog(@"%@",NSStringFromCGRect(self.contentView.frame));
    NSDictionary *dict = [self getImageDict:data];
    //[_imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",dict[@"url"],dict[@"attachment"]]] placeholderImage:[UIImage imageNamed:@"bg_imageloader"]];
    
    [_imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",dict[@"url"],dict[@"attachment"]]] placeholderImage:[UIImage imageNamed:@"bg_imageloader"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        _imageView.image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }];
    
    _imageView.backgroundColor = [UIColor grayColor];
}
- (NSDictionary *)getImageDict:(NSDictionary *)dict{
    
    NSDictionary *retDict = dict[@"attachments"];
    NSDictionary *imgDict = retDict[[[retDict allKeys] firstObject]];
    return imgDict;
}
@end
