//
//  ActivityTableViewCell.h
//  yanyu
//
//  Created by caiyee on 15/4/14.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+EMWebCache.h"
@protocol ActivityTableViewCellDelegate <NSObject>
- (void)handleJoinLabelAction;
@end

@interface ActivityTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *activityTypeLabel;//活动类型
@property (weak, nonatomic) IBOutlet UILabel *activityTitleLabel;//活动简介
@property (weak, nonatomic) IBOutlet UILabel *activityTime;//活动时间
@property (weak, nonatomic) IBOutlet UIImageView *activityStateImageView;//活动状态
@property (weak, nonatomic) IBOutlet UIImageView *activityImage;//大图
@property (weak, nonatomic) IBOutlet UILabel *countOfperson;//参见人数
@property (weak, nonatomic) IBOutlet UILabel *joinLabel;
@property (weak, nonatomic) IBOutlet UIView *whiteBackgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *joinImageView;
@property (assign, nonatomic) id<ActivityTableViewCellDelegate> delegate;



@end
