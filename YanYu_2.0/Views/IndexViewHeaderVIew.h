//
//  IndexViewHeaderVIew.h
//  yanyu_2.0
//
//  Created by caiyee on 15/4/7.
//
//

#import <UIKit/UIKit.h>
#import "ImageWall.h"
@protocol IndexViewHeaderVIewDelegate <NSObject>
- (void)handleActionColumnWithTag:(NSInteger)tag;
@end

@interface IndexViewHeaderVIew : UITableViewHeaderFooterView <ImageWallDelegate>
@property (nonatomic, assign) id <IndexViewHeaderVIewDelegate>delegate;
- (instancetype)initWithFrame:(CGRect)frame;

@end
