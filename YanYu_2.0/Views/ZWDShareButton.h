//
//  ZWDShareButton.h
//  yanyu
//
//  Created by dahe on 15/4/28.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZWDShareButtonDelegate <NSObject>

- (void)ZWDShareButtonClick:(NSString *)ButtonName;

@end

@interface ZWDShareButton : UIView
@property (nonatomic,weak) id<ZWDShareButtonDelegate>delegate;
- (instancetype)initWithFrame:(CGRect)frame name:(NSString *)name imageName:(NSString *)imgName;
@end
