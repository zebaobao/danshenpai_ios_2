//
//  IndexCellModel.m
//  yanyu_2.0
//
//  Created by caiyee on 15/4/8.
//
//

#import "IndexCellModel.h"

@implementation IndexCellModel
- (instancetype)initWithDic:(NSDictionary *)dic {
    self = [super init];
    if (self) {
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    if ([key isEqualToString:@"views"]) {
        if ([value isKindOfClass:[NSNumber class]]) {
            self.views = [NSString stringWithFormat:@"%lld", [value longLongValue]];
        } else {
            self.views = value;
        }
    }
    if ([key isEqualToString:@"dateline"]) {
        if ([value isKindOfClass:[NSNumber class]]) {
            self.dateline = [NSString stringWithFormat:@"%lld", [value longLongValue]];
        } else {
            self.dateline = value;
        }
    }
    if ([key isEqualToString:@"replies"]) {
        if ([value isKindOfClass:[NSNumber class]]) {
            self.replies = [NSString stringWithFormat:@"%lld", [value longLongValue]];
        } else {
            self.replies = value;
        }
    }
}
    @end
