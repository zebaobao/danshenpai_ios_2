//
//  ZWDResetOtherCell.m
//  yanyu
//
//  Created by Case on 15/4/24.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "ZWDResetOtherCell.h"

@implementation ZWDResetOtherCell

- (void)awakeFromNib {
    // Initialization code
    self.sureButton.backgroundColor = RGBACOLOR(53, 156, 0, 1.f);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)buttonClick:(id)sender {
    if ([self.delegate respondsToSelector:@selector(sureButtonClick)]) {
        [self.delegate sureButtonClick];
    }
}
@end
