//
//  ZWDCommLeftCell.m
//  yanyu
//
//  Created by Case on 15/4/8.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "ZWDCommLeftCell.h"

@implementation ZWDCommLeftCell

- (void)awakeFromNib {
    // Initialization code
    self.nameLable.center = self.center;
    UIView *line1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 1)];
    line1.backgroundColor = RGBACOLOR(230, 230, 230, 1.0);
    [self addSubview:line1];
    
    UIView *line2 = [[UIView alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/4 - 1, 0, 1, self.contentView.bounds.size.height)];
    line2.backgroundColor = RGBACOLOR(230, 230, 230, 1.0);
    [self addSubview:line2];
//    [self.contentView.layer setBorderWidth:1];
//    [self.contentView.layer setBorderColor:RGBACOLOR(230, 230, 230, 1.0).CGColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
