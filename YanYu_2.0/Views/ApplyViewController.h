//
//  ApplyViewController.h
//  yanyu
//
//  Created by caiyee on 15/4/17.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ApplyViewController : UIViewController
@property (strong, nonatomic) id tid;
@property (strong, nonatomic) id uid;
@property (strong, nonatomic) id fid;
@property (strong, nonatomic) id pid;
@end
