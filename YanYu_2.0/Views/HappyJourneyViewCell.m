//
//  HappyJourneyViewCell.m
//  yanyu
//
//  Created by Case on 15/4/29.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "HappyJourneyViewCell.h"
#import "CircleImageView.h"
#import "IndexCellModel.h"

@implementation HappyJourneyViewCell

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(KAvataImageX, KAvataImageY + KAvataImageHeightOrWidth + 2, 100, 15)];
        _nameLabel.font = [UIFont fontWithName:nil size:12];
        _nameLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_nameLabel];
    }
    return _nameLabel;
}
//回复
- (UIView *)replyAndViewsView {
    if (!_replyAndViewsView) {
        self.replyAndViewsView = [[UIView alloc] initWithFrame:CGRectMake(KContentLabelX,KContentLabelY + 12, 100, 10)];
        UIImageView *replyImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        replyImage.image = [UIImage imageNamed:@"回复"];
        self.replyAndViewLable = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, 200, 10)];
        _replyAndViewLable.font = [UIFont fontWithName:nil size:10];
        [_replyAndViewsView addSubview:replyImage];
        [_replyAndViewsView addSubview:_replyAndViewLable];
        
        [self.contentView addSubview:_replyAndViewsView];
        _replyAndViewLable.textColor = [Common colorWithHex:@"#949494" alpha:1.0];
    }
    return _replyAndViewsView;
}
//点赞
- (UIView *)AgreeView {
    if (!_AgreeView) {
        self.AgreeView = [[UIView alloc] initWithFrame:CGRectMake(KContentLabelX+100,KContentLabelY + 12, 100, 10)];
        UIImageView *AgreeImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        AgreeImage.image = [UIImage imageNamed:@"赞"];
        self.AgreeLable = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, 200, 10)];
        _AgreeLable.font = [UIFont fontWithName:nil size:10];
        [_AgreeView addSubview:AgreeImage];
        [_AgreeView addSubview:_AgreeLable];
        
        [self.contentView addSubview:_AgreeView];
        _AgreeLable.textColor = [Common colorWithHex:@"#949494" alpha:1.0];
    }
    return _AgreeView;
}

//头像
- (UIImageView *)avataImage {
    
    if (!_avataImage) {
        self.avataImage = [[CircleImageView alloc] initWithFrame:CGRectMake(KAvataImageX, KAvataImageY, KAvataImageHeightOrWidth, KAvataImageHeightOrWidth)];
        [self.contentView addSubview:_avataImage];
    }
    return _avataImage;
}
//时间
- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(KContentLabelX + 100,KContentLabelY + 12, 100, 15)];
        [self.contentView addSubview:_timeLabel];
    }
    return _timeLabel;
}
//内容
- (UILabel *)contentLabel {
    if (!_contentLabel) {
        self.contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(KContentLabelX, KContentLabelY, KContentLabelWidth, 30)];
        [self.contentView addSubview:_contentLabel];
//        self.contentLabel.textColor = [Common colorWithHex:@"#949494" alpha:1.0];
        self.contentLabel.textColor = [UIColor blackColor];
    }
    return _contentLabel;
}
- (UIImageView *)ZDImgV{
    if (!_ZDImgV) {
        _ZDImgV = [[UIImageView alloc]initWithFrame:CGRectMake(self.bounds.size.width - 40, 0, 40, 40)];
        _ZDImgV.image = [UIImage imageNamed:@"PlateZDImg"];
    }
    return _ZDImgV;
}
- (void)setIndexCellModel:(IndexCellModel *)indexCellModel {
    if (_indexCellModel != indexCellModel) {
        _indexCellModel = indexCellModel;
    }
    self.avataImage.backgroundColor = [UIColor whiteColor];
    self.contentLabel.text = indexCellModel.fulltitle;
    self.timeLabel.text = @"";
    
}

+ (CGFloat)heightForCellWithStr:(NSString *)str {
    return (CGFloat)fmaxf(65.0f, (float)[self detailTextHeight:str] + 35.0f);
}

+ (CGFloat)detailTextHeight:(NSString *)text {
    CGRect rectToFit = [text boundingRectWithSize:CGSizeMake(KContentLabelWidth, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15.0f]} context:nil];
//    NSLog(@"rectToFit:%f", rectToFit.size.height);
    return rectToFit.size.height;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
