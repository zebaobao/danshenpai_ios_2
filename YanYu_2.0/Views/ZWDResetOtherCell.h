//
//  ZWDResetOtherCell.h
//  yanyu
//
//  Created by Case on 15/4/24.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZWDResetOtherCellDelegate <NSObject>

- (void)sureButtonClick;

@end

@interface ZWDResetOtherCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *sureButton;



- (IBAction)buttonClick:(id)sender;
@property (nonatomic,weak) id <ZWDResetOtherCellDelegate>delegate;
@end
