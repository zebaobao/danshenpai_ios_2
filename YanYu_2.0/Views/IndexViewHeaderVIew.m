//
//  IndexViewHeaderVIew.m
//  yanyu_2.0
//
//  Created by caiyee on 15/4/7.
//
//

#import "IndexViewHeaderVIew.h"
#import "ZWDWebViewController.h"
#import "CDFDisplayViewController.h"

@implementation IndexViewHeaderVIew

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupColumns];
    }
    return self;
}

//小模块布局
- (void)setupColumns {
    [self setupScrollView];
    
    NSArray *imagesArr = @[@[@"骑友", @"亲子", @"车友", @"摄影"], @[@"驴友", @"美食", @"购物", @"情感"]];
    UIView *columnsView = [[UIView alloc] initWithFrame:CGRectMake(0, kHeightOfScrollView, [[UIScreen mainScreen] bounds].size.width, kHeightOfColumns)];
    
    columnsView.backgroundColor = [UIColor colorWithRed:208/256.0 green:208/256.0 blue:208/256.0 alpha:1.0];
    columnsView.backgroundColor = [UIColor whiteColor];
    NSArray *textArr= @[@[@"骑友", @"亲子", @"车友", @"摄影"], @[@"驴友", @"美食", @"购物", @"情感"]];
    for (int j = 0; j < 2; j++) {
        for (int i = 0; i < 4; i++) {
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(((i % 4) *(kWidthOfColumn + KWidthOfLine)), KWidthOfLine + j * (kHeightOfColumn + KWidthOfLine), kWidthOfColumn, kHeightOfColumn)];
            view.tag =  j==0 ? 101 + i : 105 + i;
            view.backgroundColor = [UIColor whiteColor];
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake((view.frame.size.width - 40)/2.0, 10, 40, 40)];
            imageView.image = [UIImage imageNamed:imagesArr[j][i]];
            [view addSubview:imageView];
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 50, view.frame.size.width, 20)];
            label.text = textArr[j][i];
            label.font = [UIFont fontWithName:nil size:13];
            label.textColor = [UIColor grayColor];
            label.textAlignment = NSTextAlignmentCenter;
            [view addSubview:label];
//            view.backgroundColor = [UIColor colorWithRed:arc4random() % 255 / 256.0 green:arc4random() % 255 / 256.0 blue:arc4random() % 255 / 256.0 alpha:1.0];
            UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGestureWithGesture:)];
            [view addGestureRecognizer:gesture];
            view.userInteractionEnabled = YES;
            [columnsView addSubview:view];
        }
    }
    UIView *lineImage = [[UIView alloc]initWithFrame:CGRectMake(10, 10 + KWidthOfLine + (kHeightOfColumn + KWidthOfLine) + kHeightOfColumn - 1, KDeviceSizeWidth - 20, 1)];
    lineImage.backgroundColor = [UIColor colorWithRed:218.0/255 green:218.0/255 blue:218.0/255 alpha:1.0];
    [columnsView addSubview:lineImage];
//    UIView *secondLine = [[UIView alloc]initWithFrame:CGRectMake(20, (KWidthOfLine) + 2 * (kHeightOfColumn + KWidthOfLine) + kHeightOfColumn - 1, KDeviceSizeWidth - 20 * 2, 1)];
//    secondLine.backgroundColor = [UIColor orangeColor];
//    [columnsView addSubview:secondLine];
    
    UIView *actionView = [[UIView alloc] initWithFrame:CGRectMake(0, (KWidthOfLine) + 2 * (kHeightOfColumn + KWidthOfLine) + 10, ([[UIScreen mainScreen]bounds].size.width - KWidthOfLine) / 2.0, kHeightOfColumn)];
    actionView.backgroundColor = [UIColor whiteColor];
    UIImageView *actionFirstImage = [[UIImageView alloc] initWithFrame:CGRectMake(50, 16, 68, 18)];
    actionFirstImage.image = [UIImage imageNamed:@"newActionText"];
    [actionView addSubview:actionFirstImage];
    UIImageView *actionSecondImage = [[UIImageView alloc] initWithFrame:CGRectMake(15, 20, 30, 30)];
    actionSecondImage.image = [UIImage imageNamed:@"newActive"];
    [actionView addSubview:actionSecondImage];
    UILabel *actionLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 40, 150, 20)];
    actionLabel.text = @"汇聚各类最好玩的活动";
    actionLabel.font = [UIFont fontWithName:nil size:10];
    actionLabel.textColor = [Common colorWithHex:@"#a5a5a5" alpha:1.0];
    [actionView addSubview:actionLabel];
    actionView.tag = 109;
    actionView.userInteractionEnabled = YES;
    UITapGestureRecognizer *gesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGestureWithGesture:)];
    [actionView addGestureRecognizer:gesture1];
//    actionView.backgroundColor = [UIColor yellowColor];
    UIView *jokeView = [[UIView alloc] initWithFrame:CGRectMake(([[UIScreen mainScreen]bounds].size.width - KWidthOfLine) / 2.0 + KWidthOfLine, actionView.frame.origin.y, ([[UIScreen mainScreen]bounds].size.width - KWidthOfLine) / 2.0, kHeightOfColumn)];
    jokeView.backgroundColor = [UIColor whiteColor];
    UIImageView *jokeFirstImage = [[UIImageView alloc] initWithFrame:CGRectMake(15, 16, 68, 18)];
    jokeFirstImage.image = [UIImage imageNamed:@"henanGood"];
    [jokeView addSubview:jokeFirstImage];
    UIImageView *jokeSecondImage = [[UIImageView alloc] initWithFrame:CGRectMake(110, 20, 30, 30)];
    jokeSecondImage.image = [UIImage imageNamed:@"publicGood"];
    [jokeView addSubview:jokeSecondImage];
    UILabel *jokeLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 40, 150, 20)];
    jokeLabel.text = @"让爱心洒满中原";
    jokeLabel.font = [UIFont fontWithName:nil size:10];
    jokeLabel.textColor = [Common colorWithHex:@"#a5a5a5" alpha:1.0];
    [jokeView addSubview:jokeLabel];
    jokeView.tag = 110;
    jokeView.userInteractionEnabled = YES;
    UITapGestureRecognizer *gesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGestureWithGesture:)];
    [jokeView addGestureRecognizer:gesture2];
    UIView *shuLine = [[UIView alloc] initWithFrame:CGRectMake(0, 7, 1, jokeView.frame.size.height - 14)];
    shuLine.backgroundColor = [Common colorWithHex:@"#dadada" alpha:1.0];
    [jokeView addSubview:shuLine];
    
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, (KWidthOfLine) + 2 * (kHeightOfColumn + KWidthOfLine) + kHeightOfColumn + KWidthOfLine + 10, [[UIScreen mainScreen] bounds].size.width, 50)];
    UIView *thirdLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KDeviceSizeWidth, 10)];
    thirdLine.backgroundColor = [Common colorWithHex:@"#eeeeee" alpha:1.0];
    [titleView addSubview:thirdLine];
    UIImageView *leftLineViewImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, 5, titleView.frame.size.height - 10)];
    leftLineViewImage.image = [UIImage imageNamed:@"readLeft"];
    [titleView addSubview:leftLineViewImage];
    UIImageView *readView = [[UIImageView alloc] initWithFrame:CGRectMake(17, 18, titleView.frame.size.height - 32, titleView.frame.size.height - 26)];
    readView.image = [UIImage imageNamed:@"reading"];
    [titleView addSubview:readView];
    titleView.backgroundColor = [UIColor whiteColor];
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, titleView.frame.size.height, KDeviceSizeWidth, 1)];
    lineView.backgroundColor = [Common colorWithHex:@"#cdcdcd" alpha:1.0];
    [titleView addSubview:lineView];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(45, 15, 200, 35)];
//    titleLabel.textAlignment = NSTextAlignmentJustified;
    titleLabel.text = @"推荐阅读";
    titleLabel.textColor = [UIColor blackColor];
    [titleView addSubview:titleLabel];
    [columnsView addSubview:titleView];
    [columnsView addSubview:actionView];
    [columnsView addSubview:jokeView];
    [self addSubview:columnsView];
}
- (void)handleGestureWithGesture:(UITapGestureRecognizer *)gesture{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(handleActionColumnWithTag:)]) {
        UIImageView *imageView = (UIImageView *)gesture.view;
        [self.delegate handleActionColumnWithTag:imageView.tag];
    }
}

//轮播图
- (void)setupScrollView {
    ImageWall *image = [[ImageWall alloc]initWithFrame:CGRectMake(0, 0, kWidthOfScrollView, kHeightOfScrollView) Url:[NSString stringWithFormat:@"%@api/mobile/?version=3&module=plaza",kBaseURL]];
    image.delegate = self;
    //@"http://svnbang.dahe.cn/api/mobile/?version=3&module=plaza"
    [self addSubview:image];
}
- (void)ImageWallClick:(NSString *)ID commonCount:(NSString *)count{
//    NSLog(@"%@---%@",ID,count);
    NSString *urlStr = ID;
    //forum.php?mod=viewthread&tid=7817088
    if ([urlStr hasPrefix:@"http"]) {
        ZWDWebViewController *web = [[ZWDWebViewController alloc]init];
        web.url = urlStr;
        UIViewController *vc = (UIViewController *)self.delegate;
        
        [vc.navigationController pushViewController:web animated:YES];
    }else{
        if ([urlStr rangeOfString:@"mod=viewthread"].length > 0) {
            NSArray *arr = [urlStr componentsSeparatedByString:@"="];
            NSString *tid = [arr lastObject];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            CDFDisplayViewController *dVC = [storyboard instantiateViewControllerWithIdentifier:@"DisplayViewContoller"];
            dVC.tid = tid;
            UIViewController *vc = (UIViewController *)self.delegate;
            [vc.navigationController pushViewController:dVC animated:YES];
            
        }
        
    }
//        if ([urlStr rangeOfString:@"dahe.cn/thread"].length>0) {
//            NSArray *arr = [urlStr componentsSeparatedByString:@"-"];
//            NSString *tid = arr[1];
//            NSLog(@"%@",tid);
//            if (tid) {
//                
//                //[self.delegate.navigationController pushViewController:dVC animated:YES];
//            }else{
//                ZWDWebViewController *web = [[ZWDWebViewController alloc]init];
//                web.url = urlStr;
//                //[self.navigationController pushViewController:web animated:YES];
//            }
//            
//        }else if ([urlStr rangeOfString:@"dahe.cn/space"].length > 0) {
//            //http://bang.dahe.cn/space-uid-14632465.html
//            //个人主页
//            NSArray *arr = [urlStr componentsSeparatedByString:@"-"];
//            NSString *lastStr = [arr lastObject];
//            NSArray *arr2 = [lastStr componentsSeparatedByString:@"."];
//            NSString *uid = [arr2 firstObject];
//            if (uid) {
//                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
//                CDFMeViewController *MeVC = [storyboard instantiateViewControllerWithIdentifier:@"userViewController"];
//                MeVC.uid = uid;
//                //[self.navigationController pushViewController:MeVC animated:YES];
//            }else{
//                
//            }
//            
//        }
    
    
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
