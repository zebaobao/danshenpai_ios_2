//
//  ZWDSearchUserCell.h
//  yanyu
//
//  Created by Case on 15/4/22.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZWDSearchUserCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconImg;
@property (weak, nonatomic) IBOutlet UILabel *nameLable;

- (void)showData:(NSDictionary *)dict;

@end
