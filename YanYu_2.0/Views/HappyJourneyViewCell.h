//
//  HappyJourneyViewCell.h
//  yanyu
//
//  Created by Case on 15/4/29.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IndexCellModel;
@class CircleImageView;
@interface HappyJourneyViewCell : UITableViewCell

@property (nonatomic, strong) IndexCellModel *indexCellModel;
@property (nonatomic, strong) CircleImageView *avataImage;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UIView *replyAndViewsView;
@property (nonatomic, strong) UIView *AgreeView;//点赞
@property (nonatomic, strong) UILabel *AgreeLable;//点赞
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *replyAndViewLable;
@property (nonatomic, strong) UILabel *timeLabel;

@property (nonatomic ,strong) UIImageView *ZDImgV;

+ (CGFloat)heightForCellWithStr:(NSString *)str;
+ (CGFloat)detailTextHeight:(NSString *)text;
@end

