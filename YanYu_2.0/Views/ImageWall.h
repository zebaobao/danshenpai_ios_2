//
//  ImageWall.h
//  MedicalAPP
//
//  Created by NPHD on 14-11-3.
//  Copyright (c) 2014年 NPHD. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ImageWallDelegate <NSObject>

- (void)ImageWallClick:(NSString *)ID commonCount:(NSString *)count;

@end

@interface ImageWall : UIView<UIScrollViewDelegate>
{
     UILabel *noteTitle;
     NSArray *titleArray;
}
@property (nonatomic,assign) id<ImageWallDelegate>delegate;
- (id)initWithFrame:(CGRect)frame Url:(NSString *)url;
- (void)refreshData:(NSString *)url;
- (void)initWithFrame:(CGRect)frame arr:(NSArray *)arr;
@end
