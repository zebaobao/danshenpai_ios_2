//
//  IndexViewCell.m
//  yanyu_2.0
//
//  Created by caiyee on 15/4/8.
//
//

#import "IndexViewCell.h"
#import "CircleImageView.h"
#import "IndexCellModel.h"


@interface IndexViewCell ()
@property (nonatomic, strong) UIView *likeAndCommentView; //后期用1.4版本的替换

@end
@implementation IndexViewCell
- (UILabel *)nameLabel {
    if (!_nameLabel) {
        self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(KAvataImageX, KAvataImageY + KAvataImageHeightOrWidth + 2, 100, 15)];
        _nameLabel.font = [UIFont fontWithName:nil size:10];
        _nameLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_nameLabel];
    }
    return _nameLabel;
}

- (UIView *)replyAndViewsView {
    if (!_replyAndViewsView) {
        self.replyAndViewsView = [[UIView alloc] initWithFrame:CGRectMake(KContentLabelX,KContentLabelY + 12, 100, 10)];
        
        UIImageView *replyImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        replyImage.image = [UIImage imageNamed:@"回复"];
         self.replyAndViewLable = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, 200, 10)];
        _replyAndViewLable.font = [UIFont fontWithName:nil size:10];
        [_replyAndViewsView addSubview:replyImage];
        [_replyAndViewsView addSubview:_replyAndViewLable];

        [self.contentView addSubview:_replyAndViewsView];
        _replyAndViewLable.textColor = [Common colorWithHex:@"#949494" alpha:1.0];
    }
    return _replyAndViewsView;
}
- (UIImageView *)avataImage {
    
    if (!_avataImage) {
        self.avataImage = [[CircleImageView alloc] initWithFrame:CGRectMake(KAvataImageX, KAvataImageY, KAvataImageHeightOrWidth, KAvataImageHeightOrWidth)];
        [self.contentView addSubview:_avataImage];
    }
    return _avataImage;
}
- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(KContentLabelX + 100,KContentLabelY + 12, 100, 15)];
        [self.contentView addSubview:_timeLabel];
        _timeLabel.textColor = [Common colorWithHex:@"#949494" alpha:1.0];
    }
    return _timeLabel;
}

- (UILabel *)contentLabel {
    if (!_contentLabel) {
        self.contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(KContentLabelX, KContentLabelY, KContentLabelWidth, 30)];
        [self.contentView addSubview:_contentLabel];
        self.contentLabel.textColor = [UIColor blackColor];
    }
    return _contentLabel;
}

- (void)setIndexCellModel:(IndexCellModel *)indexCellModel {
    if (_indexCellModel != indexCellModel) {
        _indexCellModel = indexCellModel;
    }
    self.avataImage.backgroundColor = [UIColor whiteColor];
    self.contentLabel.text = indexCellModel.fulltitle;
    self.timeLabel.text = @"";
    
}

+ (CGFloat)heightForCellWithStr:(NSString *)str {
    return (CGFloat)fmaxf(65.0f, (float)[self detailTextHeight:str] + 35.0f);
}

+ (CGFloat)detailTextHeight:(NSString *)text {
    CGRect rectToFit = [text boundingRectWithSize:CGSizeMake(KContentLabelWidth, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15.0f]} context:nil];
//    NSLog(@"rectToFit:%f", rectToFit.size.height);
    return rectToFit.size.height;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
