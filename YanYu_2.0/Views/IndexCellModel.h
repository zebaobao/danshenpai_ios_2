//
//  IndexCellModel.h
//  yanyu_2.0
//
//  Created by caiyee on 15/4/8.
//
//

#import <Foundation/Foundation.h>

@interface IndexCellModel : NSObject
@property (nonatomic, copy) NSString *fulltitle;//title
@property (nonatomic, copy) NSString *avatar;//头像url
@property (nonatomic, copy) NSString *dateline;//时间戳
@property (nonatomic, copy) NSString *replies;//回复数
@property (nonatomic, copy) NSString *author;//用户名
@property (nonatomic, copy) NSString *views;//浏览数

@end
