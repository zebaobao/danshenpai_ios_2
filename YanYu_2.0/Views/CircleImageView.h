//
//  CircleImageView.h
//  yanyu_2.0
//
//  Created by caiyee on 15/4/8.
//
//

#import <UIKit/UIKit.h>
@protocol CircleImageVIewDelegate <NSObject>
- (void)handleCircleImageViewTapGesture:(UITapGestureRecognizer *)gesture;
@end
@interface CircleImageView : UIImageView
@property (nonatomic, assign) id<CircleImageVIewDelegate>delegate;

@end
