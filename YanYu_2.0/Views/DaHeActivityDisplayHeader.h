//
//  DaHeActivityDisplayHeader.h
//  yanyu
//
//  Created by dahe on 15/4/30.
//  Copyright (c) 2015年 dahe.cn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DaHeActivityDisplayHeader : UIView
@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet UILabel *typeLable;
@property (weak, nonatomic) IBOutlet UILabel *startTimeLable;
@property (weak, nonatomic) IBOutlet UIImageView *imgV;
@property (weak, nonatomic) IBOutlet UILabel *countLable;
@property (weak, nonatomic) IBOutlet UIButton *joinButton;
@property (weak, nonatomic) IBOutlet UIImageView *rightImageV;


- (void)showData:(NSDictionary *)dict;


@end
