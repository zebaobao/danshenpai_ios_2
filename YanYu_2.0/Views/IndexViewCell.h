//
//  IndexViewCell.h
//  yanyu_2.0
//
//  Created by caiyee on 15/4/8.
//
//

#import <UIKit/UIKit.h>


@class IndexCellModel;
@class CircleImageView;

@interface IndexViewCell : UITableViewCell
@property (nonatomic, strong) IndexCellModel *indexCellModel;
@property (nonatomic, strong) CircleImageView *avataImage;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UIView *replyAndViewsView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *replyAndViewLable;
@property (nonatomic, strong) UILabel *timeLabel;
+ (CGFloat)heightForCellWithStr:(NSString *)str;
+ (CGFloat)detailTextHeight:(NSString *)text;
@end
