//
//  ActivityTableViewCell.m
//  yanyu
//
//  Created by caiyee on 15/4/14.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "ActivityTableViewCell.h"

@implementation ActivityTableViewCell

- (void)awakeFromNib {
    // Initialization code
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self.joinLabel addGestureRecognizer:tapGesture];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)handleTap:(UITapGestureRecognizer *)gesture {
    if (self.delegate && [self.delegate respondsToSelector:@selector(handleJoinLabelAction)]) {
        [self.delegate handleJoinLabelAction];
    }
}

@end
