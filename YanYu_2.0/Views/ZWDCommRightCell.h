//
//  ZWDCommRightCell.h
//  yanyu
//
//  Created by Case on 15/4/8.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZWDCommRightCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconImgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLable;
@property (weak, nonatomic) IBOutlet UILabel *countLable;
@property (weak, nonatomic) IBOutlet UIButton *guanZhuButton;
@property (weak,nonatomic) id delegate;
@property (retain,nonatomic) NSArray *dataDict;
- (void)showData:(NSDictionary *)dict isGuZhu:(BOOL)isGuZhu;

@end
