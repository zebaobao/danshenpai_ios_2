//
//  CircleImageView.m
//  yanyu_2.0
//
//  Created by caiyee on 15/4/8.
//
//

#import "CircleImageView.h"

@implementation CircleImageView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.cornerRadius = 15;
        self.layer.masksToBounds = YES;
        self.userInteractionEnabled = YES;
        [self addGestureForImageView];
    }
    return self;
}

- (void)addGestureForImageView {
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self addGestureRecognizer:tapGesture];
    
}

- (void)handleTap:(UITapGestureRecognizer *)gesture {
    if (self.delegate && [self.delegate respondsToSelector:@selector(handleCircleImageViewTapGesture:)]) {
        [self.delegate handleCircleImageViewTapGesture:gesture];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
