//
//  ZWDFirstLoginView.m
//  yanyu
//
//  Created by Case on 15/4/23.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "ZWDFirstLoginView.h"
#import "UIImageView+WebCache.h"

@implementation ZWDFirstLoginView
{
    NSDictionary *_dict;
}
- (id)initWithFrame:(CGRect)frame withDict:(NSDictionary *)dict{
    
    if ([super initWithFrame:frame]) {
        
        _dict = dict;
        [self showUI];
    }
    return self;
    
}
- (void)showUI{
    
    UIImageView *imageV = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, self.bounds.size.height - 20, self.bounds.size.height - 20)];
    [imageV sd_setImageWithURL:[NSURL URLWithString:_dict[@"icon"]] placeholderImage:[UIImage imageNamed:@"bg_imageloader"]];
    imageV.layer.masksToBounds = YES;
    imageV.layer.cornerRadius = imageV.bounds.size.width/2;
    [self addSubview:imageV];
    
    UILabel *titleLable = [[UILabel alloc]initWithFrame:CGRectMake(imageV.frame.origin.x + imageV.frame.size.width + 5, 8, self.bounds.size.width - imageV.frame.size.width - 8 - 10, 20)];
    titleLable.text = _dict[@"name"];
    titleLable.textColor = TITLEGRAY;
    titleLable.numberOfLines = 0;
    titleLable.font = [UIFont systemFontOfSize:13];
    [self addSubview:titleLable];
    
//    UILabel *desLable = [[UILabel alloc]initWithFrame:CGRectMake(titleLable.frame.origin.x, titleLable.frame.origin.y + titleLable.frame.size.height + 5, titleLable.bounds.size.width, self.bounds.size.height - titleLable.bounds.size.height-10)];
//    desLable.text = _dict[@"description"];
//    desLable.textColor = TITLEGRAY;
//    desLable.numberOfLines = 0;
//    desLable.font = [UIFont systemFontOfSize:10];
//    [self addSubview:desLable];
    
    
    self.button = [UIButton buttonWithType:UIButtonTypeSystem];
    //[self.button setTitle:@"关注" forState:UIControlStateNormal];
    [self.button setTitleColor:RGBACOLOR(115, 185, 229, 1) forState:UIControlStateNormal];
    self.button.frame = CGRectMake(titleLable.frame.origin.x, titleLable.frame.size.height + titleLable.frame.origin.y + 5, titleLable.bounds.size.width, self.bounds.size.height - (titleLable.frame.size.height + titleLable.frame.origin.y + 5) - 5);
    self.button.titleLabel.numberOfLines = 0;
//    button.titleLabel.font = [UIFont systemFontOfSize:12];
    self.button.layer.masksToBounds = YES;
    self.button.layer.cornerRadius = 4;
    self.button.layer.borderColor = TITLEGRAY.CGColor;
    self.button.layer.borderWidth = 0.3;
    [self addSubview:self.button];
    [self.button addTarget:self action:@selector(guanzhu:) forControlEvents:UIControlEventTouchUpInside];
}
- (void)guanzhu:(UIButton *)button{
    if ([self.delegate respondsToSelector:@selector(ZWDFirstLoginViewBtnClick:)]) {
        [self.delegate performSelector:@selector(ZWDFirstLoginViewBtnClick:) withObject:_dict];
    }
    
    button.userInteractionEnabled = NO;
    [button setTitle:@"关注成功" forState:UIControlStateNormal];
/*
    NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=favforum&id=%@&formhash=%@",kApiVersion,_dict[@"fid"],[CDFMe shareInstance].formHash];
    [[HXHttpClient shareInstance]grabURL:url success:^(id responseObject) {
        
        
    } fail:^(NSError *error) {
        
        button.userInteractionEnabled = YES;
    }];
    */
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
