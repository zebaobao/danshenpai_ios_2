//
//  ImageWall.m
//  MedicalAPP
//
//  Created by NPHD on 14-11-3.
//  Copyright (c) 2014年 NPHD. All rights reserved.
//

#import "ImageWall.h"
#import "UIImageView+WebCache.h"
#import "Timer.h"


#define NAME @"PageWall"
@interface ImageWall ()
{
    NSString *_url;
    NSArray *_dataArray;
    
    UIScrollView *_scrollView;
    UIPageControl *_pageControl;
    
    NSArray *_ImageArr;
}
@end

@implementation ImageWall
- (void)dealloc
{
    [[Timer shareTimer] setValid:NO ForName:NAME];
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (id)initWithFrame:(CGRect)frame Url:(NSString *)url{
    if (self = [super initWithFrame:frame]) {
        _url = url;
        [self showUI];
        [self addTimer];
        
    }
    return self;
}
- (void)initWithFrame:(CGRect)frame arr:(NSArray *)arr{
    _ImageArr = [NSArray arrayWithArray:arr];
    
    
}
- (void)refreshData:(NSString *)url
{
        _url = url;
        [self showUI];
        [self addTimer];
}
- (void)change{
    CGPoint point = _scrollView.contentOffset;
    NSInteger currentPage = _pageControl.currentPage;
    
    if (point.x < (_dataArray.count) * self.frame.size.width) {
        point.x += self.frame.size.width;
        currentPage ++;
    }else{
        point.x = self.frame.size.width;
        currentPage = 0;
        
    }
    _scrollView.contentOffset = point;
    _pageControl.currentPage = currentPage;
}
- (void)addTimer{
    __weak ImageWall *blockSelf = self;
    [[Timer shareTimer] registerAction:^{
        [blockSelf change];
    } andTimer:300 andName:NAME];//5秒换一次图片
}
- (void)showUI{
    [self startRequest:_url];
    
}
- (void)UIConfig{
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.bounces = NO;
    _scrollView.backgroundColor = [UIColor whiteColor];
    _scrollView.contentSize = CGSizeMake(self.frame.size.width * (_dataArray.count + 2) , self.frame.size.height);
    _scrollView.delegate = self;
    _scrollView.pagingEnabled = YES;
    _scrollView.contentOffset = CGPointMake(self.frame.size.width, 0);
    
    [self addSubview:_scrollView];
    if (_dataArray.count == 1) {
        _scrollView.scrollEnabled = NO;
        UIImageView *imgV = [[UIImageView alloc]initWithFrame:CGRectMake(self.frame.size.width * 1, 0, self.frame.size.width, self.frame.size.height)];
        [imgV sd_setImageWithURL:[NSURL URLWithString:[_dataArray lastObject][@"pic"]] placeholderImage:[UIImage imageNamed:@"bg_imageloader"]];
        //            imgV.contentMode = UIViewContentModeScaleAspectFit;
        [_scrollView addSubview:imgV];
        imgV.userInteractionEnabled = YES;
        
//        NSLog(@"%@", [_dataArray lastObject][@"pic"]);
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer  alloc]initWithTarget:self action:@selector(imgClick:)];
        [imgV addGestureRecognizer:tap];
        imgV.tag = 100 + 1;
        [_scrollView addSubview:imgV];
        //说明文字层
        UIView *noteView=[[UIView alloc] initWithFrame:CGRectMake(0, self.bounds.size.height-33,self.bounds.size.width,33)];
        //    [noteView setBackgroundColor:[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:0.5]];
        [noteView setBackgroundColor:[UIColor colorWithWhite:1/255.0 alpha:0.35]];
        NSUInteger pageCount=[_dataArray count]+1;
        float pageControlWidth=(pageCount-2)*10.0f+40.f;
        float pagecontrolHeight=20.0f;
        noteTitle=[[UILabel alloc] initWithFrame:CGRectMake(5, 6, self.frame.size.width-pageControlWidth-15, pagecontrolHeight)];
        noteTitle.textColor = [UIColor whiteColor];
        [noteTitle setText:[[_dataArray objectAtIndex:0] objectForKey:@"title"]];
//        NSLog(@"%@",[[_dataArray objectAtIndex:0] objectForKey:@"title"]);
        [noteTitle setBackgroundColor:[UIColor clearColor]];
        [noteTitle setFont:[UIFont systemFontOfSize:13]];
        [noteView addSubview:noteTitle];
        
        [self addSubview:noteView];
        
        _pageControl = [[UIPageControl alloc]initWithFrame:CGRectMake(noteTitle.frame.size.width+noteTitle.frame.origin.x+5, 6, pageControlWidth, pagecontrolHeight)];
        //    _pageControl.center = CGPointMake(self.frame.size.width / 2, self.frame.size.height - 20);
        _pageControl.numberOfPages = _dataArray.count;
        [noteView addSubview:_pageControl];
        _pageControl.userInteractionEnabled = NO;
        return;
    }
    
    for (int i = 0; i < _dataArray.count + 2; i++) {
//        NSLog(@"%@", [_dataArray lastObject][@"pic"]);

        if (i == 0) {
            UIImageView *imgV = [[UIImageView alloc]initWithFrame:CGRectMake(self.frame.size.width * i, 0, self.frame.size.width, self.frame.size.height)];
            [imgV sd_setImageWithURL:[NSURL URLWithString:[_dataArray lastObject][@"pic"]] placeholderImage:[UIImage imageNamed:@"bg_imageloader"]];
//            imgV.contentMode = UIViewContentModeScaleAspectFit;
            [_scrollView addSubview:imgV];
            imgV.userInteractionEnabled = YES;
            continue;
        }
        if (i == _dataArray.count + 1) {
            UIImageView *imgV = [[UIImageView alloc]initWithFrame:CGRectMake(self.frame.size.width * i, 0, self.frame.size.width, self.frame.size.height)];
            [imgV sd_setImageWithURL:[NSURL URLWithString:[_dataArray firstObject][@"pic"]] placeholderImage:[UIImage imageNamed:@"bg_imageloader"]];
//            imgV.contentMode = UIViewContentModeScaleAspectFit;
            [_scrollView addSubview:imgV];
            imgV.userInteractionEnabled = YES;
            continue;
        }
        UIImageView *imgV = [[UIImageView alloc]initWithFrame:CGRectMake(self.frame.size.width * i, 0, self.frame.size.width, self.frame.size.height)];
        [imgV sd_setImageWithURL:[NSURL URLWithString:_dataArray[i-1][@"pic"]] placeholderImage:[UIImage imageNamed:@"bg_imageloader"]];
//        imgV.contentMode = UIViewContentModeScaleAspectFit;
        imgV.userInteractionEnabled = YES;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer  alloc]initWithTarget:self action:@selector(imgClick:)];
        [imgV addGestureRecognizer:tap];
        imgV.tag = 100 + i;
        [_scrollView addSubview:imgV];
    }
    
            //说明文字层
    UIView *noteView=[[UIView alloc] initWithFrame:CGRectMake(0, self.bounds.size.height-33,self.bounds.size.width,33)];
//    [noteView setBackgroundColor:[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:0.5]];
    [noteView setBackgroundColor:[UIColor colorWithWhite:1/255.0 alpha:0.35]];
    NSUInteger pageCount=[_dataArray count];
    float pageControlWidth=(pageCount-2)*10.0f+40.f;
    float pagecontrolHeight=20.0f;
    noteTitle=[[UILabel alloc] initWithFrame:CGRectMake(5, 6, self.frame.size.width-pageControlWidth-15, pagecontrolHeight)];
    noteTitle.textColor = [UIColor whiteColor];
    [noteTitle setText:[[_dataArray objectAtIndex:0] objectForKey:@"title"]];
//    NSLog(@"%@",[[_dataArray objectAtIndex:0] objectForKey:@"title"]);
    [noteTitle setBackgroundColor:[UIColor clearColor]];
    [noteTitle setFont:[UIFont systemFontOfSize:13]];
    [noteView addSubview:noteTitle];
    
    [self addSubview:noteView];
    
    _pageControl = [[UIPageControl alloc]initWithFrame:CGRectMake(noteTitle.frame.size.width+noteTitle.frame.origin.x+5, 6, pageControlWidth, pagecontrolHeight)];
//    _pageControl.center = CGPointMake(self.frame.size.width / 2, self.frame.size.height - 20);
    _pageControl.numberOfPages = _dataArray.count;
    [noteView addSubview:_pageControl];
    _pageControl.userInteractionEnabled = NO;
}

- (void)imgClick:(UITapGestureRecognizer *)tap{
//    NSLog(@"%@",_dataArray[tap.view.tag - 101][@"title"]);

    if ([self.delegate respondsToSelector:@selector(ImageWallClick:commonCount:)]) {
        [self.delegate performSelector:@selector(ImageWallClick:commonCount:) withObject:_dataArray[tap.view.tag - 101][@"url"] withObject:_dataArray[tap.view.tag-101][@"id"]];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [self scrollViewChange:scrollView];
}
- (void)scrollViewChange:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.x == 0) {
        scrollView.contentOffset = CGPointMake(self.frame.size.width * (_dataArray.count), 0);
        _pageControl.currentPage = _dataArray.count - 1;
        return;
    }
    if (scrollView.contentOffset.x > self.frame.size.width * (_dataArray.count)) {
        scrollView.contentOffset = CGPointMake(self.frame.size.width, 0);
        _pageControl.currentPage = 0;
        return;
    }
    
    _pageControl.currentPage = scrollView.contentOffset.x / self.frame.size.width - 1;
}

//- (void)loadSuccess:(ZWDAFNETWorking *)request{
//    if (request.downloadData) {
//        NSMutableArray *dict = [NSJSONSerialization JSONObjectWithData:request.downloadData options:NSJSONReadingMutableContainers error:nil];
//        if (dict) {
//            _dataArray = dict;
//            [self UIConfig];
//        }
//    }
//}
//- (void)loadFaild:(ZWDAFNETWorking *)request{
//    
//}

- (void)guidangWithDic:(NSArray *)arr {
    NSMutableData *mData = [NSMutableData data];
    //创建对象
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:mData];
    //归档
    [archiver encodeObject:arr forKey:@"adArr"];
    //3.结束归档.结束归档之后在归档无效
    [archiver finishEncoding];
    BOOL isSucess = [mData writeToFile:[self getFilePath] atomically:YES];
//    NSLog(@"%d", isSucess);
    
}

- (NSArray *)handleReadData {
    //1.从本地读取数据
    NSData *mData = [NSData dataWithContentsOfFile:[self getFilePath]];
    //2.创建反归档对象
    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:mData];
    //3.反归档
    NSArray *adArr = [unarchiver decodeObjectForKey:@"adArr"];
    //4.结束反规档
    [unarchiver finishDecoding];
    return adArr;
}

- (NSString *)getFilePath {
    //1.获取Documents文件夹路径
    NSString *filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    //2.拼接上文件路径
    NSString *newFilePath = [filePath stringByAppendingPathComponent:@"adArr.data"];
    return newFilePath;
}

-(void)startRequest:(NSString *)url{
    
    _dataArray = [self handleReadData];
//    NSLog(@"%@",_dataArray);
//    NSLog(@"%@", url);
    if (_dataArray.count>0) {
        [self UIConfig];
    }
    
    [[HXHttpClient shareInstance]grabURL:url success:^(id responseObject) {
        if (responseObject) {
            
            _dataArray = responseObject[@"Variables"][@"plaza_ad"];
            
            [self UIConfig];
        }
        
        [self guidangWithDic:_dataArray];
        
    } fail:^(NSError *error) {
//        NSLog(@"下载失败");
    }];

    //[ZWDAFNETWorking ZWDAFNETWorkingWithRequestUrl:url Target:self successAction:@selector(loadSuccess:) faildAction:@selector(loadFaild:)];
}

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    CGFloat pageWidth = _scrollView.frame.size.width;
    int page = floor((_scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    _pageControl.currentPage=(page-1);
    int titleIndex=page-1;
    if (titleIndex==[_dataArray count]) {
        titleIndex=0;
    }
    if (titleIndex<0) {
        titleIndex = (int)_dataArray.count-1;
    }
    [noteTitle setText:[[_dataArray objectAtIndex:titleIndex] objectForKey:@"title"]];
}


@end
