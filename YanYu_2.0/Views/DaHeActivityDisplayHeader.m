//
//  DaHeActivityDisplayHeader.m
//  yanyu
//
//  Created by dahe on 15/4/30.
//  Copyright (c) 2015年 dahe.cn. All rights reserved.
//

#import "DaHeActivityDisplayHeader.h"
#import "UIImageView+WebCache.h"

#define kSideToLeft     16
#define kHeightOfImage  (KDeviceSizeWidth - 2.0 * kSideToLeft) / 1.8



@implementation DaHeActivityDisplayHeader

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)showData:(NSDictionary *)dict{
    self.backgroundColor = [Common colorWithHex:@"#F3F3F3" alpha:1];
    UIView *whiteView = [[UIView alloc]initWithFrame:CGRectMake(10, 10, self.bounds.size.width - 20, self.bounds.size.height - 10)];
    whiteView.backgroundColor = [UIColor whiteColor];
    [self insertSubview:whiteView atIndex:0];
    
    
    self.titleLable.text = dict[@"thread"][@"subject"];
//    self.titleLable.text = @"大家好大家好大家好大家好啊后恒大华府都撒谎返回冬奥会发到后发点哈佛大红色发达送饭哈达送话费辅导书发达送话费";
    if ([dict[@"special_activity"][@"class"] length] > 0) {
        self.typeLable.text = dict[@"special_activity"][@"class"];
    }else{
        self.typeLable.text = @"大河活动";
    }
    self.typeLable.textColor = [UIColor whiteColor];
    self.typeLable.layer.masksToBounds = YES;
    self.typeLable.layer.cornerRadius = 5;
    
    NSInteger startTime = [dict[@"special_activity"][@"starttimefrom"] integerValue];
    NSInteger toTime = [dict[@"special_activity"][@"starttimeto"] integerValue];
    NSString *startTimeStr = [[self class] formatDate:startTime withFormat:@"yyyy.MM.dd"];
    NSString *toTimeStr = [[self class] formatDate:toTime withFormat:@"yyyy.MM.dd"];
    
    if (toTime <= 0) {
        
        self.startTimeLable.text = [NSString stringWithFormat:@"活动时间: %@开始", startTimeStr];
    } else {
        self.startTimeLable.text = [NSString stringWithFormat:@"活动时间: %@ - %@", startTimeStr, toTimeStr];
    }
    self.startTimeLable.textColor= [UIColor lightGrayColor];
    
    /*
    NSMutableString *statStr = [NSMutableString stringWithString: dict[@"special_activity"][@"starttimefrom"]];
    NSMutableString *toStr = [NSMutableString stringWithString: dict[@"special_activity"][@"starttimeto"]];
    
    NSRange range =[statStr rangeOfString:@"&nbsp;"];
    if (range.length > 0) {
        [statStr replaceCharactersInRange:range withString:@" "];
    }
    range = [toStr rangeOfString:@"&nbsp;"];
    if (range.length > 0) {
        [toStr replaceCharactersInRange:range withString:@" "];
    }
    if ([toStr isEqualToString:@"0"]) {
        self.startTimeLable.text = [NSString stringWithFormat:@"活动时间:%@开始",statStr];
    }else{
        self.startTimeLable.text = [NSString stringWithFormat:@"活动时间:%@-%@",statStr,toStr];
    }
    */
    
    
    
    [self.imgV sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kBaseURL,dict[@"special_activity"][@"thumb"]]] placeholderImage:[UIImage imageNamed:@"bg_imageloader"]];
    
    
    NSInteger countof = 0;
    NSInteger countOf = 0;
    if ([dict[@"special_activity"][@"applylistverified"] isKindOfClass:[NSArray class]]) {
         countof = [dict[@"special_activity"][@"applylistverified"]count];
    }
    if ([dict[@"special_activity"][@"applylist"] isKindOfClass:[NSArray class]]) {
         countOf = [dict[@"special_activity"][@"applylist"] count];
    }
    NSInteger allCount = countOf + countof;

    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString: [NSString stringWithFormat:@"已有%d人感兴趣，已有%d报名", [ dict[@"thread"][@"views"] integerValue], allCount]];
    int index = [((NSString *)[NSString stringWithFormat:@"%d",[dict[@"thread"][@"views"] integerValue]]) length];
    int index2 = [((NSString *)[NSString stringWithFormat:@"%d",allCount]) length];
    
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor orangeColor] range:NSMakeRange(2,index)];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(2 + index + 7,index2)];
    self.countLable.attributedText = str;
    
    if (![dict[@"special_activity"][@"activityclose"] boolValue]) {//0未关闭
        [self.joinButton setImage:[UIImage imageNamed:@"立即参加"] forState:UIControlStateNormal];
        self.rightImageV.image = [UIImage imageNamed:@"进行中"];

        
    }else{
        [self.joinButton setImage:[UIImage imageNamed:@"已结束立即参加"] forState:UIControlStateNormal];
        self.rightImageV.image = [UIImage imageNamed:@"已结束"];

    }
    
    if ([self detailTextHeight:self.titleLable.text] > 22.0) {
        self.titleLable.textAlignment = NSTextAlignmentLeft;
    }else{
        self.titleLable.textAlignment = NSTextAlignmentCenter;
    }
    
    //改变frame
    self.titleLable.frame = CGRectMake(self.titleLable.frame.origin.x, self.titleLable.frame.origin.y + 20, self.titleLable.frame.size.width, [self detailTextHeight:self.titleLable.text]);
    self.typeLable.frame = CGRectMake(self.typeLable.frame.origin.x, self.titleLable.frame.origin.y + self.titleLable.frame.size.height + 10, self.typeLable.frame.size.width, self.typeLable.frame.size.height);
    self.startTimeLable.frame = CGRectMake(self.startTimeLable.frame.origin.x, self.typeLable.frame.origin.y, self.startTimeLable.frame.size.width, self.startTimeLable.frame.size.height);
    self.imgV.frame = CGRectMake(self.imgV.frame.origin.x, self.typeLable.frame.origin.y + self.typeLable.frame.size.height + 10, KDeviceSizeWidth - 2* kSideToLeft, kHeightOfImage);
//    self.countLable.frame = CGRectMake(self.countLable.frame.origin.x, self.imgV.frame.origin.y + self.imgV.frame.size.height + 10, self.countLable.frame.size.width, self.countLable.frame.size.height);
    
//    UILabel *joinedCount = [[UILabel alloc] initWithFrame:CGRectMake(self.countLable.frame.origin.x + 20 + self.countLable.frame.size.width, self.countLable.frame.origin.y, , self.countLable.frame.size.height)];
//    [self addSubview:joinedCount];
    
    [self.joinButton removeFromSuperview];
    
}
+ (NSString *)formatDate:(NSTimeInterval)timeInterval withFormat:(NSString*)format {
    NSDate *myDate = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.locale = [NSLocale currentLocale];
    df.dateFormat = format;
    NSString *date = [df stringFromDate:myDate];
    return date;
}
- (CGFloat)detailTextHeight:(NSString *)text {
    CGRect rectToFit = [text boundingRectWithSize:CGSizeMake(KDeviceSizeWidth - 2 * kSideToLeft - 15, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:17.0f]} context:nil];
//    NSLog(@"rectToFit:%f", rectToFit.size.height);
    return rectToFit.size.height;
}

@end
