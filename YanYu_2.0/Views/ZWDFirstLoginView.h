//
//  ZWDFirstLoginView.h
//  yanyu
//
//  Created by Case on 15/4/23.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZWDFirstLoginViewDelegate <NSObject>

- (void)ZWDFirstLoginViewBtnClick:(NSDictionary *)dict;



@end

@interface ZWDFirstLoginView : UIView

@property (nonatomic,weak) id<ZWDFirstLoginViewDelegate>delegate;
@property (nonatomic,strong) UIButton *button;

- (id)initWithFrame:(CGRect)frame withDict:(NSDictionary *)dict;


@end
