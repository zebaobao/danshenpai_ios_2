//
//  HYHActivityDetailFirstCell.h
//  yanyu
//
//  Created by caiyee on 15/4/15.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol HYHActivityDetailFirstCellDelegate <NSObject>
- (void)handleApplyBtn:(UIButton *)sender;
- (void)handleZanBtn:(UIButton *)sender;
@end


@interface HYHActivityDetailFirstCell : UITableViewCell<DTAttributedTextContentViewDelegate>
//@property (weak, nonatomic) IBOutlet UILabel *subjectLabel;
@property (weak, nonatomic) IBOutlet UIButton *BMButton;
@property (weak, nonatomic) IBOutlet DTAttributedTextView *contentViewLabel;

@property (nonatomic,retain) NSArray *dataSource;


@property (assign, nonatomic) id<HYHActivityDetailFirstCellDelegate>delegate;

- (void)showData:(NSDictionary *)dict index:(NSIndexPath *)index data:(NSArray *)datasource;


@end
