//
//  ZWDShowLayOut.m
//  yanyu
//
//  Created by Case on 15/4/16.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "ZWDShowLayOut.h"


@implementation ZWDShowLayOut
{
    NSMutableArray *_HeightArr;
    
    NSCache *_cache;
}
- (void)cache{
    if (!_cache) {
        _cache = [NSCache new];
    }
}
- (NSDictionary *)getImageDict:(NSDictionary *)dict{

    NSDictionary *retDict = dict[@"attachments"];
    NSDictionary *imgDict = retDict[[[retDict allKeys] firstObject]];
    return imgDict;
}
//返回整个collectionView的大小
-(CGSize)collectionViewContentSize
{
    float heightLeft = 0.0;
    float heightRight = 0.0;
    if (heightLeft < 0.2 && heightRight < 0.2) {
        if (heightRight > heightLeft) {
            
            while (1) {
//                NSLog(@"bug");
            }
        }
    }
    for (int i = 0; i < self.dataArr.count; i++) {
        NSDictionary *imgDict = [self getImageDict:self.dataArr[i]];
        float height = [imgDict[@"height"] floatValue]/[imgDict[@"width"] floatValue] * KDeviceSizeWidth/2;
//        if (heightRight > heightLeft) {
//            heightLeft += height;
//        }else{
//            heightRight += height;
//        }
        if (heightRight < heightLeft) {
            heightRight += height;
        }
        else
        {
            heightLeft += height;
        }
        
    }
    
    return CGSizeMake(KDeviceSizeWidth, heightLeft>heightRight?heightLeft:heightRight);
}

//返回对应indexPath的cell的属性
-(UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self cache];
    
    UICollectionViewLayoutAttributes *attr = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
    float heightLeft = 0.0;
    float heightRight = 0.0;
    
    
    if (heightLeft < 0.2 && heightRight < 0.2) {
        if (heightRight > heightLeft) {
            
            while (1) {
//                NSLog(@"bug");
            }
        }
    }
    NSString *key = [NSString stringWithFormat:@"%d--%d",indexPath.section,indexPath.row];
    if ([_cache objectForKey:key]) {
        return [_cache objectForKey:key];
    }else{
        for (int i = 0; i<= indexPath.row; i++) {
            NSDictionary *dict = [self getImageDict:self.dataArr[i]];
            float height = [dict[@"height"] floatValue]/[dict[@"width"] floatValue] * KDeviceSizeWidth/2;
            
            if (heightRight < heightLeft) {
                heightRight += height;
                attr.center = CGPointMake(KDeviceSizeWidth / 4  + KDeviceSizeWidth/2, heightRight - height/2);
            }
            else
            {
                heightLeft += height;
                attr.center = CGPointMake(KDeviceSizeWidth/4, heightLeft - height/2);
            }
            attr.size = CGSizeMake(KDeviceSizeWidth/2, height);
        }
        [_cache setObject:attr forKey:key];
        return attr;
    }
    
}

//返回所有的cell的属性的数组
-(NSArray*)layoutAttributesForElementsInRect:(CGRect)rect
{
    
//    if (!_HeightArr || _HeightArr.count != self.dataArr.count) {
//        _HeightArr =[[NSMutableArray alloc]init];
//        for (int i=0; i<[self.collectionView numberOfItemsInSection:0]; i++) {
//            [_HeightArr addObject:[self layoutAttributesForItemAtIndexPath:[NSIndexPath indexPathForItem:i inSection:0]]];
//        }
//        return _HeightArr;
//    }
//    return _HeightArr;
    
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    for (int i=0; i< [self.collectionView numberOfItemsInSection:0]; i++) {
        [arr addObject:[self layoutAttributesForItemAtIndexPath:[NSIndexPath indexPathForItem:i inSection:0]]];
    }
    return arr;
}


@end
