//
//  ZWDLoadMoreCell.h
//  yanyu
//
//  Created by Case on 15/4/14.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZWDLoadMoreCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *contentLable;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activty;

@end
