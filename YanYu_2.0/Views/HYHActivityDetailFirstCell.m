//
//  HYHActivityDetailFirstCell.m
//  yanyu
//
//  Created by caiyee on 15/4/15.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "HYHActivityDetailFirstCell.h"

@implementation HYHActivityDetailFirstCell
{
    id mainData;
    
    CGFloat _cellPadding;
    
}

- (void)awakeFromNib {
    // Initialization code
    /*
     UIViewAutoresizingNone                 = 0,
     UIViewAutoresizingFlexibleLeftMargin   = 1 << 0,
     UIViewAutoresizingFlexibleWidth        = 1 << 1,
     UIViewAutoresizingFlexibleRightMargin  = 1 << 2,
     UIViewAutoresizingFlexibleTopMargin    = 1 << 3,
     UIViewAutoresizingFlexibleHeight       = 1 << 4,
     UIViewAutoresizingFlexibleBottomMargin = 1 << 5

     */
//    self.contentView = [[DTAttributedTextContentView alloc]init];
    //self.contentView.autoresizingMask =

    self.contentViewLabel.shouldDrawImages = YES;
    self.contentViewLabel.shouldDrawLinks = YES;
    self.contentViewLabel.backgroundColor = [UIColor clearColor];
    _cellPadding = 7.0f;
    self.contentViewLabel.bounces = NO;
    
    
    [self.BMButton addTarget:self action:@selector(BMButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.BMButton setImage:[UIImage imageNamed:@"点击报名"] forState:UIControlStateNormal];
}
- (void)BMButtonClick{
    if ([self.delegate respondsToSelector:@selector(handleApplyBtn:)]) {
        [self.delegate handleApplyBtn:nil];
    }
}
- (void)showData:(NSDictionary *)dict index:(NSIndexPath *)index data:(NSArray *)datasource{
    mainData = dict;

//    self.subjectLabel.text = dict[@"thread"][@"subject"];
//    NSLog(@"%@----%@",dict[@"thread"][@"subject"],dict[@"postlist"][0][@"message"]);
//    self.subjectLabel.backgroundColor = [UIColor whiteColor];
    
    self.dataSource = datasource;
    
    NSString *message = [NSString stringWithFormat:@"%@",self.dataSource[index.section][index.row][@"message"]];
    NSString *message0 = [NSString stringWithFormat:@"%@",message];
    
    NSAttributedString *attStr =[self formatHTMLString:message0 atIndexPath:index];
    self.contentViewLabel.attributedString = attStr;
    self.contentViewLabel.backgroundColor = [UIColor whiteColor];
    
//    [self.BMButton setTintColor:[UIColor whiteColor]];
    //self.BMButton.backgroundColor = RGBACOLOR(38, 112, 198, 1.0);
//    self.BMButton.backgroundColor = RGBACOLOR(251, 152, 152, 1.0);
    self.BMButton.layer.masksToBounds = YES;
    self.BMButton.layer.cornerRadius = 8;
    
    self.contentViewLabel.frame = CGRectMake(10, 40, KDeviceSizeWidth - 10 * 2, [self heightForContentAtIndexPath:index] + 45);
    self.BMButton.frame = CGRectMake(17, self.contentViewLabel.frame.origin.y+self.contentViewLabel.frame.size.height + 20, KDeviceSizeWidth - 17 * 2, 40);
    [self.BMButton setTintColor:[UIColor whiteColor]];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (NSAttributedString *)formatHTMLString:(NSString *)html atIndexPath:(NSIndexPath *)indexPath
{
    if (html == nil) {
        return nil;
    }
    
//    NSLog(@"html--%@",html);
    
    NSMutableString *str = [NSMutableString stringWithString:html];
    
    NSRange range;// = NSMakeRange(0, str.length);
    //[str replaceOccurrencesOfString:@"<br />" withString:@"" options:NSCaseInsensitiveSearch range:range];
    range = NSMakeRange(0, str.length);
    [str replaceOccurrencesOfString:@"blockquote" withString:@"quote" options:NSCaseInsensitiveSearch range:range];
    //匹配出附件 [attach]852156[/attach]
//    NSLog(@"str--%@",str);
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"\\[attach\\](\\d+)\\[/attach\\]" options:NSRegularExpressionCaseInsensitive error:NULL];
    
    NSDictionary *attachements = self.dataSource[indexPath.section][indexPath.row][@"attachments"];
//    NSLog(@"dict--%@",attachements);
    __block NSInteger offset = 0;
    NSMutableString *replaceStr = [NSMutableString stringWithFormat:@"%@",str];
    [regex enumerateMatchesInString:str options:0 range:NSMakeRange(0, str.length) usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
        
        if (result.numberOfRanges == 2) {
            
            NSString *attachid = [str substringWithRange:[result rangeAtIndex:1]];
            //NSLog(@"%@",attachid);
            NSDictionary *attachData = attachements[attachid];
            if (attachData && [attachData[@"url"] length] > 0 && [attachData[@"attachment"] length] > 0) {
                CGFloat width = [attachData[@"width"] floatValue];
                CGFloat height = [attachData[@"height"] floatValue];
                NSString *attachStr;
                
                if (width <= 30) {
                    attachStr = [NSString stringWithFormat:@"<img src='%@%@' width='%f' height='%f'>",attachData[@"url"],attachData[@"attachment"],width / 2,height / 2];
                } else {
                    attachStr = [NSString stringWithFormat:@"<div><img src='%@%@' width='%f' height='%f'></div>",attachData[@"url"],attachData[@"attachment"],width,height];
                }
                
                
                NSRange range = NSMakeRange(result.range.location + offset, result.range.length);
                
                [replaceStr replaceCharactersInRange:range withString:attachStr];
                offset += attachStr.length - result.range.length;
            }
        }
    }];
    //NSLog(@"%@",str);
    NSData *data = [replaceStr dataUsingEncoding:NSUTF8StringEncoding];
    
    void (^callBackBlock)(DTHTMLElement *element) = ^(DTHTMLElement *element) {
        
        // the block is being called for an entire paragraph, so we check the individual elements
        
        for (DTHTMLElement *oneChildElement in element.childNodes)
        {
            // if an element is larger than twice the font size put it in it's own block
            if (oneChildElement.displayStyle == DTHTMLElementDisplayStyleInline && oneChildElement.textAttachment.displaySize.height > 2.0 * oneChildElement.fontDescriptor.pointSize)
            {
                oneChildElement.displayStyle = DTHTMLElementDisplayStyleBlock;
                oneChildElement.paragraphStyle.minimumLineHeight = element.textAttachment.displaySize.height;
                oneChildElement.paragraphStyle.maximumLineHeight = element.textAttachment.displaySize.height;
            }
        }
    };
    
    
    NSMutableDictionary *options = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    [NSValue valueWithCGSize:CGSizeMake(306, 2000)], DTMaxImageSize,
                                    [NSNumber numberWithFloat:1.5], DTDefaultLineHeightMultiplier,
                                    [NSNumber numberWithFloat:16], DTDefaultFontSize,
                                    //@"Arial", DTDefaultFontFamily,
                                    @"Blue", DTDefaultLinkColor,
                                    @"none", DTDefaultLinkDecoration,
                                    [[DTCSSStylesheet alloc] initWithStyleBlock:@"quote{display:block;color:#666666;font-size:12px;padding:5px;}"], DTDefaultStyleSheet,
                                    callBackBlock, DTWillFlushBlockCallBack,
                                    nil];
    
    NSAttributedString *string = [[NSAttributedString alloc] initWithHTMLData:data options:options documentAttributes:NULL];
//    NSLog(@"%@",string);
    return string;
}
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    UIColor *strokeColor = [UIColor colorWithWhite:200.0f/255.0f alpha:.5];
    UIColor *fillColor = [UIColor whiteColor];
    
    CGContextSaveGState(context);
    [fillColor setFill];
    
    //设置阴影
    //    CGContextSetShadowWithColor(context, CGSizeMake(0, 0), 2, [UIColor colorWithWhite:0 alpha:.15].CGColor);
    
    CGContextFillRect(context, rect);
    
    //描边
    //    [strokeColor setStroke];
    //    CGContextSetLineWidth(context, 1);
    //    CGContextStrokeRect(context, rect);
    //    CGContextRestoreGState(context);
    
    //上面的分割线
    //draw1PxStroke(context, CGPointMake(_cellPadding, CGRectGetMinY(rect) + 45), CGPointMake(CGRectGetWidth(rect) - _cellPadding, CGRectGetMinY(rect) + 45), strokeColor.CGColor);
    
    //下面的分割线
    draw1PxStroke(context, CGPointMake(_cellPadding, CGRectGetMaxY(rect)-1), CGPointMake(CGRectGetWidth(rect) - _cellPadding, CGRectGetMaxY(rect)-1), strokeColor.CGColor);
    
    //按钮分割线
    //    draw1PxStroke(context, CGPointMake((int)(CGRectGetWidth(rect) / 3) + .5, CGRectGetMaxY(rect) - 25), CGPointMake((int)(CGRectGetWidth(rect) / 3) + .5, CGRectGetMaxY(rect) - 5), strokeColor.CGColor);
    //    draw1PxStroke(context, CGPointMake((int)(CGRectGetWidth(rect) * 2 / 3) + .5, CGRectGetMaxY(rect) - 25), CGPointMake((int)(CGRectGetWidth(rect) * 2 / 3) + .5, CGRectGetMaxY(rect) - 5), strokeColor.CGColor);
}
- (CGFloat)heightForContentAtIndexPath:(NSIndexPath *)indexPath
{
//    NSLog(@"%d---%d",indexPath.section,indexPath.row);
    //NSLog(@"_data:%@",_dataSource);
    NSString *message = [NSString stringWithFormat:@"%@",_dataSource[indexPath.section][indexPath.row][@"message"]];
//    NSLog(@"%@",message);
    DTAttributedTextView *dtv = [[DTAttributedTextView alloc] initWithFrame:CGRectMake(0, 0, KDeviceSizeWidth - 20, 10)];
//    NSLog(@"%@", message);
    dtv.attributedString = [self formatHTMLString:message atIndexPath:indexPath];
    CGRect rect = dtv.attributedTextContentView.layoutFrame.frame;
//    NSLog(@"%f",CGRectGetMaxY(rect));
    return CGRectGetMaxY(rect);
}
@end
