//
//  ZWDShareButton.m
//  yanyu
//
//  Created by dahe on 15/4/28.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "ZWDShareButton.h"

@implementation ZWDShareButton{
    NSString *_name;
}
- (instancetype)initWithFrame:(CGRect)frame name:(NSString *)name imageName:(NSString *)imgName{
    
    if (self = [super initWithFrame:frame]) {
        _name = name;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapClick)];
        [self addGestureRecognizer:tap];
        
        UIImageView *imageV = [[UIImageView alloc]initWithFrame:CGRectMake(20, 20, self.bounds.size.width - 40, self.bounds.size.width - 40)];
        imageV.image = [UIImage imageNamed:imgName];
        imageV.userInteractionEnabled = YES;
        [self addSubview:imageV];
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(5, imageV.frame.origin.y+imageV.frame.size.height + 5, self.bounds.size.width - 10, self.bounds.size.height - (imageV.frame.origin.y+imageV.frame.size.height + 5))];
        label.text = _name;
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor whiteColor];
        [self addSubview:label];
        label.font = [UIFont systemFontOfSize:15];
        label.userInteractionEnabled = YES;
        self.userInteractionEnabled = YES;
    }
    return self;
    
}
- (void)tapClick{
    if ([self.delegate respondsToSelector:@selector(ZWDShareButtonClick:)]) {
        [self.delegate performSelector:@selector(ZWDShareButtonClick:) withObject:_name];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
