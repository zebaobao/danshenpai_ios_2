//
//  ZWDCommRightCell.m
//  yanyu
//
//  Created by Case on 15/4/8.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "ZWDCommRightCell.h"
#import "UIImageView+WebCache.h"

@interface ZWDCommRightCell ()
{
    NSDictionary *_dict;
}
@end

@implementation ZWDCommRightCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)showData:(NSDictionary *)dict isGuZhu:(BOOL)isGuZhu{
    _dict = [NSDictionary dictionaryWithDictionary:dict];
    self.nameLable.text = dict[@"name"];
    self.nameLable.font = [UIFont systemFontOfSize:14];
    self.countLable.text = [NSString stringWithFormat:@"今日新帖:%@",dict[@"todayposts"]];
    self.countLable.font = [UIFont systemFontOfSize:13];
    self.countLable.textColor = RGBACOLOR(144, 144, 144, 1.0);
    self.iconImgView.layer.masksToBounds = YES;
    self.iconImgView.layer.cornerRadius = 25;
    [self.iconImgView.layer setBorderWidth:1];
    [self.iconImgView.layer setBorderColor:RGBACOLOR(230, 230, 230, 1.0).CGColor];
    NSArray *arr = [dict allKeys];
    
    if ([arr containsObject:@"icon"]) {
        [self.iconImgView sd_setImageWithURL:[NSURL URLWithString:dict[@"icon"]] placeholderImage:[UIImage imageNamed:@"河宝娃"]];
    }else{
        [self.iconImgView setImage:[UIImage imageNamed:@"河宝娃"]];
    }
    self.guanZhuButton.titleLabel.font = [UIFont systemFontOfSize:15];
    self.guanZhuButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.guanZhuButton.layer.masksToBounds = YES;
    self.guanZhuButton.layer.cornerRadius = 10;
    [self.guanZhuButton.layer setBorderWidth:0.5];
    [self.guanZhuButton.layer setBorderColor:RGBACOLOR(230, 230, 230, 1.0).CGColor];
    if (isGuZhu) {
        [self.guanZhuButton setTitle:@"取消关注" forState:UIControlStateNormal];
    }else{
        [self.guanZhuButton setTitle:@"关注" forState:UIControlStateNormal];
    }
    [self.guanZhuButton addTarget:self action:@selector(guanZhuButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
}
- (void)guanZhuButtonClick:(UIButton *)button{
    //判断是否登陆
    if (![CDFMe shareInstance].isLogin) {
        [SVProgressHUD showWithStatus:@"请登录"];
        UIStoryboard *user = [UIStoryboard storyboardWithName:@"welcome" bundle:nil];
        [(UIViewController *)self.delegate presentViewController:user.instantiateInitialViewController animated:YES completion:^{
            [SVProgressHUD dismiss];
        }];
        //[(UIViewController *)self.delegate presentModalViewController:user.instantiateInitialViewController animated:YES];
        return;
    }
    //http://svnbang.dahe.cn/api/mobile/?version=3&module=favforum&id=571
    
   // 收藏时候后面拼接参数 &id= 板块的fid
    //    NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=forumindex",kApiVersion];
    button.userInteractionEnabled = NO;
    if ([button.titleLabel.text isEqualToString:@"关注"]) {
        NSString *url = [NSString stringWithFormat:@"api/mobile/?version=%@&module=favforum&id=%@&formhash=%@",kApiVersion,_dict[@"fid"],[CDFMe shareInstance].formHash];
        
        [SVProgressHUD showWithStatus:@"正在关注"];
        
        [[HXHttpClient shareInstance]grabURL:url success:^(id responseObject) {
            button.userInteractionEnabled = YES;
//            NSLog(@"%@",responseObject);
            if (responseObject[@"Message"]) {
                if ([responseObject[@"Message"][@"messageval"] isEqualToString:@"favorite_do_success"]) {
                    //关注成功
                    [SVProgressHUD showSuccessWithStatus:@"关注成功" duration:1.5];
                    [button setTitle:@"取消关注" forState:UIControlStateNormal];
                    [[NSNotificationCenter defaultCenter] postNotificationName:FAV_SUCCESS object:self userInfo:_dict];
                }else{
                    [SVProgressHUD showSuccessWithStatus:responseObject[@"Message"][@"messagestr"] duration:2];
                }
            }
        } fail:^(NSError *error) {
            [button setTitle:@"关注" forState:UIControlStateNormal];

            button.userInteractionEnabled = YES;
            [SVProgressHUD showSuccessWithStatus:@"关注失败" duration:2];
        }];
    }else{
        //根据fid得到favid
//        NSLog(@"%@",_dict);
//        NSLog(@"%@",self.dataDict);
        NSString *favid;
        for (NSDictionary *newDict in self.dataDict) {
            if ([newDict[@"id"]isEqualToString:_dict[@"fid"]]) {
                favid = newDict[@"favid"];
                break;
            }
        }
        
        NSString *url =[NSString stringWithFormat:@"api/mobile/?version=%@&module=delfav&formhash=%@&favid=%@",kApiVersion,[CDFMe shareInstance].formHash,favid];
//        NSLog(@"%@",url);
        
        [SVProgressHUD showWithStatus:@"正在取消关注"];
        
        [[HXHttpClient shareInstance] grabURL:url success:^(id responseObject) {
            button.userInteractionEnabled = YES;
//            NSLog(@"%@",responseObject);
            if (responseObject[@"Message"]) {
                if ([responseObject[@"Message"][@"messageval"] isEqualToString:@"do_success"]) {
                    //取消关注成功
                    [SVProgressHUD showSuccessWithStatus:@"取消关注成功" duration:1.5];
                    [button setTitle:@"关注" forState:UIControlStateNormal];
                    [[NSNotificationCenter defaultCenter] postNotificationName:FAV_DELETESUCCESS object:self userInfo:_dict];
                }else{
                    [button setTitle:@"取消关注" forState:UIControlStateNormal];

                    [SVProgressHUD showSuccessWithStatus:responseObject[@"Message"][@"messagestr"] duration:2];
                }
            }
        } fail:^(NSError *error) {
            button.userInteractionEnabled = YES;
            [SVProgressHUD showSuccessWithStatus:@"关注失败" duration:2];
        }];
        
    }
    
}
@end
