//
//  ZWDActivityResultCell.m
//  yanyu
//
//  Created by Case on 15/4/22.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "ZWDActivityResultCell.h"

@implementation ZWDActivityResultCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)showdata:(NSDictionary *)dict{
    self.iconImg.layer.masksToBounds = YES;
    self.iconImg.layer.cornerRadius = 15;
    [self.iconImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://bang.dahe.cn/uc_server/avatar.php?uid=%@&size=big",dict[@"uid"]]] placeholderImage:nil];
    
    self.nameLable.text = dict[@"username"];
    //&nbsp;
    NSMutableString *str = [NSMutableString stringWithString:dict[@"dateline"]];
    
    NSRange range = [str rangeOfString:@"&nbsp;"];
    if (range.length>0) {
        [str replaceCharactersInRange:range withString:@" "];

    }
    self.dateLable.text =str;
    
}
@end
