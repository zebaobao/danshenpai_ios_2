//
//  ZBFlowView.m
//  
//
//  Created by zhangbin on 14-7-28.
//  Copyright (c) 2014年 OneStore. All rights reserved.
//

#import "ZBFlowView.h"
#import "UIButton+WebCache.h"

@interface ZBFlowView()
{
    UIButton *btn;
    UILabel *_titleLabel;
}
@end

@implementation ZBFlowView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag = self.tag;
        btn.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
        btn.autoresizingMask = UIViewAutoresizingFlexibleWidth
        |UIViewAutoresizingFlexibleHeight;
   
        [btn addTarget:self action:@selector(pressed:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        
        
//        UIView *grayView = [[UIView alloc]initWithFrame:CGRectMake(0, btn.frame.size.height - 20, (KDeviceSizeWidth - 15)/2, 20)];
//        grayView.backgroundColor = [UIColor blackColor];
//        //grayView.alpha = 0.6;
//        [self addSubview:grayView];
        
        
        
        _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
        _titleLabel.font = [UIFont systemFontOfSize:12];
        _titleLabel.numberOfLines = 0;
        //[self addSubview:_titleLabel];
        return self;
    }
    return self;
}

- (void)pressed:(id)sender
{
    if (self) {
        if ([_flowViewDelegate respondsToSelector:@selector(pressedAtFlowView:)]) {
            [_flowViewDelegate pressedAtFlowView:self];
        }
    }
}

- (void)showData:(NSDictionary *)dict{
    NSDictionary *newDict = [self getImageDict:dict];
    [btn setBackgroundImage:nil forState:UIControlStateNormal];
    
    [btn sd_setBackgroundImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",newDict[@"url"],newDict[@"attachment"]]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"bg_imageloader"]];
    
    //_replyButton.titleEdgeInsets = UIEdgeInsetsMake(0, 7, 0, 0);

//    [btn setTitle:dict[@"subject"] forState:UIControlStateNormal];
//    btn.titleLabel.numberOfLines = 0;
//    btn.titleLabel.font = [UIFont systemFontOfSize:12];
//    btn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, -(btn.bounds.size.height), 0);
//    NSLog(@"%f",-(btn.bounds.size.height));
    
    
    
    _titleLabel.frame = CGRectMake(0, self.bounds.size.height - 20, btn.bounds.size.width,10);
    _titleLabel.text = dict[@"subject"];
    
}
- (NSDictionary *)getImageDict:(NSDictionary *)dict{
    
    NSDictionary *retDict = dict[@"attachments"];
    NSDictionary *imgDict = retDict[[[retDict allKeys] firstObject]];
    return imgDict;
}
- (float) heightForString:(NSString *)value fontSize:(float)fontSize andWidth:(float)width
{
    CGSize sizeToFit = [value sizeWithFont:[UIFont systemFontOfSize:fontSize]
                         constrainedToSize:CGSizeMake(width -16.0, CGFLOAT_MAX)
                             lineBreakMode:NSLineBreakByWordWrapping];
    //此处的换行类型（lineBreakMode）可根据自己的实际情况进行设置
    return sizeToFit.height + 16.0;
}
@end
