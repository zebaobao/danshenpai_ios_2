//
//  CellView.m
//  PSCollectionViewDemo
//
//  Created by Eric on 12-6-15.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "CellView.h"
#define MARGIN 4.0

@implementation CellView
@synthesize viewsLable;
@synthesize picView;
@synthesize titleLabel;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
- (void)showData:(NSDictionary *)item width:(CGFloat)columnWidth{
    //UIViewContentModeScaleToFill,
    //UIViewContentModeScaleAspectFit,      // contents scaled to fit with fixed aspect. remainder is transparent
    //UIViewContentModeScaleAspectFill
    NSDictionary *dict = [self getImageDict:item];
    
    self.picView.contentMode = UIViewContentModeScaleAspectFill;

    
    CGFloat height = 0.0;
    CGFloat width = columnWidth - MARGIN * 2;
//    NSLog(@"%f",width);
    height += MARGIN;
    
    
    CGFloat objectWidth = [[dict objectForKey:@"width"] floatValue];
    CGFloat objectHeight = [[dict objectForKey:@"height"] floatValue];
    CGFloat scaledHeight = floorf(objectHeight / (objectWidth / width));
    height += scaledHeight;
    
    
    
    
    NSString *caption = [item objectForKey:@"subject"]!=[NSNull null]? [item objectForKey:@"subject"]:@"";
//    NSLog(@"%@",caption);
    CGSize labelSize = CGSizeZero;
    UIFont *labelFont = [UIFont systemFontOfSize:12.0];
    labelSize = [caption sizeWithFont:labelFont constrainedToSize:CGSizeMake(width, INT_MAX) lineBreakMode:UILineBreakModeWordWrap];
    
    
//    NSLog(@"%@",NSStringFromCGSize(labelSize));
    height += labelSize.height;
    
    height += MARGIN;
    
    
    CGRect frame = self.picView.frame;
    frame.size.height = scaledHeight;
    self.picView.frame = frame;
    
    //[self isContain];
    
    
}
- (BOOL)isContain{
    
    while (CGRectIntersectsRect(self.picView.frame, self.titleLabel.frame)) {
        CGRect frame = self.titleLabel.frame;
        frame.origin.y += 1;
        self.titleLabel.frame = frame;
    }
    return YES;
}
-(NSDictionary *)getImageDict:(NSDictionary *)dict{
    
    NSDictionary *retDict = dict[@"attachments"];
    NSDictionary *imgDict = retDict[[[retDict allKeys] firstObject]];
    return imgDict;
}

- (void)dealloc {
    [picView release];
    [super dealloc];
}
@end
