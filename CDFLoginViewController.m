//
//  CDFLoginViewController.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-20.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFLoginViewController.h"
#import "UMSocial.h"
#import "LoginWebViewController.h"
#import "CDFIMViewController.h"
@interface loginFormSubmitButton : UIButton
@end

@implementation loginFormSubmitButton

- (void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    
    CGColorRef startColor = (self.highlighted ? [Common colorWithHex:@"#ECE9E1" alpha:1].CGColor : [UIColor whiteColor].CGColor);
    CGColorRef endColor = (self.highlighted ? [Common colorWithHex:@"#EAE7DF" alpha:1].CGColor : [Common colorWithHex:@"#E0DBCF" alpha:1].CGColor);
    CGPoint startPoint = CGPointMake(CGRectGetMinX(rect), CGRectGetMinY(rect));
    CGPoint endPoint = CGPointMake(CGRectGetMinX(rect), CGRectGetMaxY(rect));
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGMutablePathRef path = createRoundedRectForRect(CGRectInset(rect, 2, 2), 5);
    CGContextSaveGState(context);
    
    CGContextAddPath(context, path);
    CGContextSetShadowWithColor(context, CGSizeMake(0, 0), 2, [UIColor colorWithWhite:0 alpha:.6].CGColor);
    [[UIColor whiteColor] setFill];
    CGContextFillPath(context);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGFloat locations[] = { 0.0, 1.0 };
    
    NSArray *colors = [NSArray arrayWithObjects:(__bridge id)(startColor), (__bridge id)(endColor), nil];
    
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef) colors, locations);
    
    CGContextAddPath(context, path);
    CGContextClip(context);
    CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
    CGColorSpaceRelease(colorSpace);
    CGGradientRelease(gradient);
    CGContextRestoreGState(context);
    CGPathRelease(path);
}

@end

/**
 *  登录表单背景
 */
@interface loginFormView : UIView

@end

@implementation loginFormView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];
}

- (void)setup
{
    self.backgroundColor = [UIColor clearColor];
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    CGMutablePathRef path = createRoundedRectForRect(CGRectInset(rect, 2, 2), 5);
    CGContextSetShadowWithColor(context, CGSizeMake(0, 0), 2, [Common colorWithHex:@"#8C0000" alpha:.6].CGColor);
    CGContextAddPath(context, path);
    [[UIColor whiteColor] setFill];
    CGContextFillPath(context);
    CGPathRelease(path);
    CGContextRestoreGState(context);
    
    //密码与帐号中间的分割线
    draw1PxStroke(context, CGPointMake(CGRectGetMinX(rect) + 10, CGRectGetMidY(rect)), CGPointMake(CGRectGetMaxX(rect) - 10, CGRectGetMidY(rect)), [UIColor colorWithWhite:204.0/255.0f alpha:1].CGColor);
}

@end

@interface CDFLoginViewController ()<UITextFieldDelegate,UIWebViewDelegate>{

    LoginWebViewController *_qqController;
}
@property (strong, nonatomic) IBOutlet UIImageView *bgImageView;
@property (strong, nonatomic) IBOutlet loginFormView *myloginFormView;
@property (strong, nonatomic) IBOutlet UITextField *userNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UnderLineLabel *registerButton;
@property (strong, nonatomic) IBOutlet UIButton *wbloginButton;
@property (strong, nonatomic) IBOutlet UIButton *qqloginButton;
@property (strong, nonatomic) IBOutlet loginFormSubmitButton *loginButton;

@end

@implementation CDFLoginViewController {
    
    CGFloat startOffsetY;
    NSString *loginURL;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

//huanxin--sendMessage
- (void)sendMessage {
    //环信test
    CDFIMViewController *imVC = [[CDFIMViewController alloc] init];
    [imVC viewDidLoad];
}

+ (NSString *) md5:(NSString *)str {
    const char *cStr = [str UTF8String];
    unsigned char result[16];
    CC_MD5( cStr, strlen(cStr), result );
    return [NSString stringWithFormat:@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}

//统计IM用户未读消息数
- (void)countOfUnreadMessages {
    int indexICareAbout = 3;
    NSArray *conversations = [[[EaseMob sharedInstance] chatManager] conversations];
    NSInteger unreadCount = 0;
    for (EMConversation *conversation in conversations) {
        unreadCount += conversation.unreadMessagesCount;
    }
    NSString *badgeValue = [NSString stringWithFormat:@"%ld", (long)unreadCount];
    if (unreadCount > 0) {
        [[[[[self tabBarController] viewControllers] objectAtIndex: indexICareAbout] tabBarItem] setBadgeValue:badgeValue];
    }else{
        [[[[[self tabBarController] viewControllers] objectAtIndex: indexICareAbout] tabBarItem] setBadgeValue:nil];
    }
}

- (IBAction)loginAction:(id)sender {
    
    if ([sender isEqual:self.loginButton]) {
        [MobClick event:@"Login" label:@"普通登录"];
        if ([self.userNameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length <= 0) {
            [SVProgressHUD showErrorWithStatus:@"请输入您的登录名"];
            return;
        }
        if ([self.passwordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length <= 0) {
            [SVProgressHUD showErrorWithStatus:@"请输入密码"];
            return;
        }

//        loginURL = [NSString stringWithFormat:@"api/mobile/?version=%@&module=login&loginsubmit=yes&loginfield=auto&submodule=checkpost&lssubmit=yes&username=%@&password=%@",kApiVersion ,[self.userNameTextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[self.passwordTextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];

        //password = (NSString *)CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef) password,NULL,(CFStringRef) @"!*'();:@&=+$,/?%#[]",kCFStringEncodingUTF8);
        //处理用户名密码中的特殊字符
        loginURL = [NSString stringWithFormat:@"api/mobile/?version=%@&module=login&loginsubmit=yes&loginfield=auto&submodule=checkpost&lssubmit=yes&username=%@&password=%@&mobiletype=2",kApiVersion ,(NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef) self.userNameTextField.text,NULL,(CFStringRef) @"!*'();:@&=+$,/?%#[]",kCFStringEncodingUTF8)),(NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef) self.passwordTextField.text,NULL,(CFStringRef) @"!*'();:@&=+$,/?%#[]",kCFStringEncodingUTF8))];

        //loginURL = [NSString stringWithFormat:@"api/mobile/?version=%@&module=login&loginsubmit=yes&loginfield=auto&submodule=checkpost&lssubmit=yes&username=%@&password=%@",kApiVersion ,[self.userNameTextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[self.passwordTextField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
//        NSLog(@"%@",loginURL);

        
//        NSLog(@"%d",[[Common unarchiveForKey:kOnlineInfo] allKeys].count);
//        NSLog(@"%@--%@",[Common unarchiveForKey:kOnlineInfo][@"data"][@"member_uid"],[Common unarchiveForKey:kOnlineInfo][@"data"][@"member_username"]);
        [XGPush delTag:[Common unarchiveForKey:kOnlineInfo][@"data"][@"member_uid"]];

        [Common archive:nil withKey:kOnlineInfo];

        //登录前先退出登录
        [Common logout];

        [SVProgressHUD showWithStatus:@"登录中..." maskType:SVProgressHUDMaskTypeGradient];
        [[HXHttpClient shareInstance] grabURL:loginURL success:^(id responseObject) {
            
//            [XGPush setTag:[NSString stringWithFormat:@"%@", responseObject[@"Variables"][@"member_uid"]]];
            [XGPush setAccount:[NSString stringWithFormat:@"%@", responseObject[@"Variables"][@"member_uid"]]];
            //  Variables  member_uid  groupid
//            NSLog(@"meber_uid:%@ groupid:%@", responseObject[@"Variables"][@"member_uid"], responseObject[@"Variables"][@"groupid"]);
//            
//            NSLog(@"dicdic:%@", responseObject);
#pragma mark - 保存是否为第一次登陆
            [CDFMe shareInstance].isFirstLogin = ![responseObject[@"Variables"][@"firstlogin"] boolValue];//0代表是第一次 1代表不是
//            NSLog(@"huyahui%@", [NSString stringWithFormat:@"dahe_%@",responseObject[@"Variables"][@"member_uid"]]);
            
            NSString *mima = [CDFLoginViewController md5:responseObject[@"Variables"][@"member_uid"]];
//            NSLog(@"mima:%@", mima);
            NSString *NewMima = [mima substringWithRange:NSMakeRange(0, 10)];
//            NSLog(@"NewMima:%@",NewMima);
            
            if ([[EaseMob sharedInstance].chatManager isLoggedIn]) {
//                NSLog(@"huyahui1122-0");
                [[EaseMob sharedInstance].chatManager asyncLogoffWithCompletion:^(NSDictionary *info, EMError *error) {
                    if (error) {
//                        NSLog(@"error:%@", error);
//                        NSLog(@"huyahui11-0");
                    } else {
//                        NSLog(@"huyahui11-1");
//                        
//                        NSLog(@"dic:%@", info);
                        [[EaseMob sharedInstance].chatManager asyncLoginWithUsername:[NSString stringWithFormat:@"dahe_%@",responseObject[@"Variables"][@"member_uid"]] password:NewMima completion:^(NSDictionary *loginInfo, EMError *error) {
                            if (!error && loginInfo) {
//                                NSLog(@"登陆成功");
                                [self countOfUnreadMessages];
                            }else {
//                                NSLog(@"huyahui11-3");
                                
//                                NSLog(@"登陆失败:%@", error);
//                                NSLog(@"error.errorCode:%u", error.errorCode);
                            }
                        } onQueue:nil];
                    }
                } onQueue:nil];
            }else {
//                NSLog(@"huyahui1122-0");
                [[EaseMob sharedInstance].chatManager asyncLoginWithUsername:[NSString stringWithFormat:@"dahe_%@",responseObject[@"Variables"][@"member_uid"]] password:NewMima completion:^(NSDictionary *loginInfo, EMError *error) {
                    if (!error && loginInfo) {
//                        NSLog(@"登陆成功");
//                        NSLog(@"huyahui11-2");
                    }else {
//                        NSLog(@"huyahui11-3");
//                        
//                        NSLog(@"登陆失败:%@", error);
//                        NSLog(@"error.errorCode:%u", error.errorCode);
                    }
                } onQueue:nil];
                


            }
            
            //            [[EaseMob sharedInstance].chatManager asyncLogoffWithUnbindDeviceToken:YES];
            //环信
            
            //NSLog(@"%@",[Common dictionaryToJSON:responseObject]);
            if (responseObject[@"Message"]) {
                NSString *message = [NSString stringWithFormat:@"%@",responseObject[@"Message"][@"messagestr"]];
                //NSLog(@"%@",responseObject);
                if (![responseObject[@"Message"][@"messageval"] isEqualToString:@"login_succeed"]) {
                    [SVProgressHUD dismiss];
                    if ([responseObject[@"Message"][@"messageval"] isEqualToString:@"location_login"]) {
                        [self performSegueWithIdentifier:@"pushToQuestion" sender:nil];
                    } else {
                        [SVProgressHUD showErrorWithStatus:message];
                    }
                } else {
                    [SVProgressHUD showSuccessWithStatus:message];
                    
                    [Common login:responseObject];
                    
                    [self dismissModalViewControllerAnimated:YES];
                }
            }
        } fail:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
        }];
    } else if ([sender isEqual:self.wbloginButton]) {
        //        NSLog(@"weibo login");
        //        [MobClick event:@"Login" label:@"微博登录"];
        //        UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToSina];
        //        snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response) {
        //            NSLog(@"response is %@",[Common dictionaryToJSON:response.data]);
        //
        //            [self onekeyLogin:response platform:@"wb"];
        //        });
        
        UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToSina];
        snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
            
            [self onekeyLogin:response platform:@"wb"];
        });
        //设置回调对象
//        [UMSocialControllerService defaultControllerService].socialUIDelegate = self;
    } else if ([sender isEqual:self.qqloginButton]) {

        _qqController = [[LoginWebViewController alloc] init];
        [self.navigationController pushViewController:_qqController animated:YES];
//        [MobClick event:@"Login" label:@"QQ登录"];
//        UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToQQ];
//        snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response) {
//            
//            NSLog(@"response is %@",[Common dictionaryToJSON:response.data]);
//            [self onekeyLogin:response platform:@"qq"];
//        });
    }
}

/**
 *  用友盟的第三方登录取到数据以后执行此方法登录
 *  @param response 第三方登录返回的数据
 *  @param platform 平台，微博登录为sina，qq登录为qq
 */
- (void)onekeyLogin:(UMSocialResponseEntity *)response platform:(NSString *)platform
{
    [Common logout];
    //判断登录平台
    NSString *key = [platform isEqualToString:@"wb"] ? @"sina" : @"qq";

    NSString * access_Token = nil;
    if (response.data[key][@"accessToken"] != nil) {
        
        access_Token = response.data[key][@"accessToken"];
    }else{
    
        access_Token = response.data[key][@"usid"];
    }
    //若返回了值，则进行登录
    if ([response.data isKindOfClass:[NSDictionary class]]) {
        NSString *url = [NSString stringWithFormat:@"sso/%@/callback2.php?access_token=%@&uid=%@&screen_name=%@", platform,access_Token, response.data[key][@"usid"], [response.data[key][@"username"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
//        NSLog(@"%@", url);
        [[HXHttpClient shareInstance] grabURL:url success:^(id responseObject) {
            
//            NSLog(@"%@",[Common dictionaryToJSON:responseObject]);
            if ([responseObject[@"err"] integerValue] != 0) {
                //登录失败
                [SVProgressHUD showErrorWithStatus:responseObject[@"msg"]];
            } else {
                //登录成功
                [SVProgressHUD showSuccessWithStatus:responseObject[@"msg"]];
                
                /**
                 *  此处返回的数据和普通登录返回的数据一致，执行普通登录的登录后方法就行了
                 */
                [Common login:responseObject];
                [self dismissModalViewControllerAnimated:YES];
            }
        } fail:^(NSError *error) {
            //登录出错
//            NSLog(@"%@", error);
        }];
    } else {
        [SVProgressHUD dismiss];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController respondsToSelector:@selector(loginURL)]) {
        [segue.destinationViewController setValue:loginURL forKey:@"loginURL"];
    }
}

- (void)registerButtonPressed:(UIButton *)sender{

    [self performSegueWithIdentifier:@"pushToRegister" sender:sender];
}

- (void)registerAction:(id)sender
{
//    [self performSegueWithIdentifier:@"pushToRegister" sender:sender];
}

- (void)handlePan:(UIPanGestureRecognizer *)recognizer
{
    CGPoint point = [recognizer locationInView:self.view];
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        startOffsetY = point.y;
    }
    if (recognizer.state == UIGestureRecognizerStateChanged) {
        //NSLog(@"%f",point.y - startOffsetY);
        if (point.y - startOffsetY >= 50.0f) {
            
            [self dismissModalViewControllerAnimated:YES];
        }
    }
}

#pragma mark UIViewController Super Methods
- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.title = @"登录";
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(login) name:@"isLogin" object:nil];
    self.registerButton.shouldUnderline = YES;
    self.registerButton.highlightedColor = [UIColor colorWithWhite:0 alpha:.1];
    [self.registerButton addTarget:self action:@selector(registerAction:)];

    UIButton *registerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    registerButton.frame = CGRectMake(0, self.view.frame.size.height - 90, self.view.frame.size.width, 50);
    registerButton.backgroundColor = [UIColor clearColor];
    [registerButton addTarget:self
                       action:@selector(registerButtonPressed:)
             forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:registerButton];
    
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    [self.view addGestureRecognizer:pan];
    
    [_qqloginButton removeFromSuperview];
    [_wbloginButton removeFromSuperview];
#pragma mark - 忘记密码？
    UIButton *resetButton = [UIButton buttonWithType:UIButtonTypeSystem];
    resetButton.frame = CGRectMake(registerButton.frame.origin.x, registerButton.frame.origin.y+registerButton.frame.size.height + 3 , registerButton.frame.size.width, 20);
    [resetButton setTitle:@"忘记密码？" forState:UIControlStateNormal];
    [resetButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    resetButton.backgroundColor = [UIColor clearColor];
    [self.view addSubview:resetButton];
    [resetButton addTarget:self action:@selector(resetButtonClick) forControlEvents:UIControlEventTouchUpInside];
}
- (void)resetButtonClick{
    ZWDResetPasswordViewController  *vc = [ZWDResetPasswordViewController new];
    
    [self presentViewController:vc animated:YES completion:nil];
}
- (void)login{

    [self dismissModalViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
//    NSLog(@"%@",NSHomeDirectory());
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"登录"];
    [self addKeyBoardNotification];
    self.bgImageView.image = [[UIImage imageNamed:@"bg_welcome"] stretchableImageWithLeftCapWidth:0 topCapHeight:500];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"登录"];
    [self removeKeyBoardNotification];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UIKeyboardController Listener

//监听键盘隐藏和显示事件
- (void)addKeyBoardNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowOrHide:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowOrHide:) name:UIKeyboardWillHideNotification object:nil];
}
#pragma mark - 弹出键盘时修改坐标
static CGFloat offsetHeight = 0;
static NSInteger isFirst = 1;
static CGRect firstRect;

-(void)keyboardWillShowOrHide:(NSNotification *)notification {
    
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    BOOL isShow = [[notification name] isEqualToString:UIKeyboardWillShowNotification] ? YES : NO;
    if (isFirst == 1) {
        //记录下输入框最初的位置
        firstRect = self.myloginFormView.frame;
        isFirst = 2;
    }
    CGRect formRect = self.myloginFormView.frame;
    
    if (isShow) {
        
        if (offsetHeight == 0) {
            offsetHeight = keyboardSize.height - (CGRectGetHeight(self.view.frame) - CGRectGetMaxY(formRect));
        }
        
        formRect.origin.y -= keyboardSize.height - (CGRectGetHeight(self.view.frame) - CGRectGetMaxY(formRect));
    } else {
        
        //formRect.origin.y += ABS(offsetHeight);
        offsetHeight = 0;
        [UIView animateWithDuration:.25 animations:^{
            self.myloginFormView.frame = firstRect;
        }];
        return;
    }
    [UIView animateWithDuration:.25 animations:^{
        self.myloginFormView.frame = formRect;
    }];
}

//注销监听事件
- (void)removeKeyBoardNotification {
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isEqual:self.userNameTextField]) {
        [self.passwordTextField becomeFirstResponder];
        return NO;
    }
    [self.passwordTextField resignFirstResponder];
    return YES;
}

@end
