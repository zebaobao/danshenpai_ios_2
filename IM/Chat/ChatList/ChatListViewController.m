/************************************************************
 *  * EaseMob CONFIDENTIAL
 * __________________
 * Copyright (C) 2013-2014 EaseMob Technologies. All rights reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of EaseMob Technologies.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from EaseMob Technologies.
 */

#import "ChatListViewController.h"
#import "SRRefreshView.h"
#import "ChatListCell.h"
#import "EMSearchBar.h"
#import "NSDate+Category.h"
#import "RealtimeSearchUtil.h"
#import "ChatViewController.h"
#import "EMSearchDisplayController.h"
#import "ConvertToCommonEmoticonsHelper.h"
#import "CDFIMCell.h"
#import "CDFMessageViewController.h"
#import "MessageModel.h"

@interface ChatListViewController ()<UITableViewDelegate,UITableViewDataSource, UISearchDisplayDelegate,SRRefreshDelegate, UISearchBarDelegate, IChatManagerDelegate>

@property (strong, nonatomic) NSMutableArray        *dataSource;

@property (nonatomic, strong) EMSearchBar           *searchBar;
@property (nonatomic, strong) SRRefreshView         *slimeView;
@property (nonatomic, strong) UIView                *networkStateView;

@property (strong, nonatomic) EMSearchDisplayController *searchController;

@end

@implementation ChatListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _dataSource = [NSMutableArray array];
    }
    return self;
}
- (void)handleNotificationFromTieziCountChange {
    [self.tableView reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.rightBarButtonItem.tintColor = TITLECOLOR;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNotificationFromTieziCountChange) name:kTieziCount_Change object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNotificationFromTieziCountChange) name:kNewLike object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNotificationFromTieziCountChange) name:kNewLessonUser object:nil];
    
//    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 30)];
//    label.text = @"消息";
//    label.textAlignment = NSTextAlignmentCenter;
//    label.textColor = TITLEGRAY;
//    self.navigationItem.titleView = label;
    UILabel *titleLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    titleLable.text = @"消息";
    titleLable.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    titleLable.textAlignment = NSTextAlignmentCenter;
    titleLable.textColor = RGBACOLOR(250, 66, 4, 1.0);
    self.navigationItem.titleView = titleLable;

    
    
    [self.view addSubview:self.searchBar];
    [self.view addSubview:self.tableView];
    [self.tableView addSubview:self.slimeView];
    [self networkStateView];
    
    [self searchController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
+ (NSString *) md5:(NSString *)str {
    const char *cStr = [str UTF8String];
    unsigned char result[16];
    CC_MD5( cStr, strlen(cStr), result );
    return [NSString stringWithFormat:@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}

- (void)countOfUnreadMessages {
    int indexICareAbout = 3;
    NSArray *conversations = [[[EaseMob sharedInstance] chatManager] conversations];
    NSInteger unreadCount = 0;
    for (EMConversation *conversation in conversations) {
        unreadCount += conversation.unreadMessagesCount;
    }
    unreadCount = unreadCount + [CDFMe shareInstance].tiezeCount + [CDFMe shareInstance].newLikeCount + [CDFMe shareInstance].newTingZhongCount;
    NSString *badgeValue = [NSString stringWithFormat:@"%ld", (long)unreadCount];
    
    if (unreadCount > 0) {
        [[[[[self tabBarController] viewControllers] objectAtIndex: indexICareAbout] tabBarItem] setBadgeValue:badgeValue];
    }else{
        [[[[[self tabBarController] viewControllers] objectAtIndex: indexICareAbout] tabBarItem] setBadgeValue:nil];
    }
}

static bool ret = YES;
- (void)logoutNot{
    //推出账号时发送通知
    ret = YES;
}
-(void)viewWillAppear:(BOOL)animated {
    if (ret) {
        if (![CDFMe shareInstance].isLogin) {
            UIStoryboard *s = [UIStoryboard storyboardWithName:@"welcome" bundle:nil];
            [self presentViewController:s.instantiateInitialViewController animated:YES completion:nil];
            ret = NO;
            return;
        }
    }
    
    
    
    NSString *mima = [ChatListViewController md5:[CDFMe shareInstance].uid];
//    NSLog(@"mima:%@", mima);
    NSString *NewMima = [mima substringWithRange:NSMakeRange(0, 10)];
//    NSLog(@"NewMima:%@",NewMima);
    
    NSDictionary *userInfoDic = [[EaseMob sharedInstance].chatManager loginInfo];
    NSString *easeUserName = [userInfoDic[@"username"] substringFromIndex:5]; //
    
    [super viewWillAppear:animated];
    if (![CDFMe shareInstance].isLogin && [[EaseMob sharedInstance].chatManager isLoggedIn]) {
        //yanyu未登录, 但是环信登录了, 这时退出环信
        [[EaseMob sharedInstance].chatManager asyncLogoffWithCompletion:^(NSDictionary *info, EMError *error) {
            if (error) {
//                NSLog(@"error:%@", error);
            } else {
                
            }
        } onQueue:nil];
    } else if ([CDFMe shareInstance].isLogin && ![[CDFMe shareInstance].uid isEqualToString:easeUserName]) {
        //yanyu登录了, 但是环信账号和yanyu账号不是同一个, 这时退出环信登录, 进行重新登录环信
        [[EaseMob sharedInstance].chatManager asyncLogoffWithCompletion:^(NSDictionary *info, EMError *error) {
            if (error) {
//                NSLog(@"error:%@", error);
//                NSLog(@"huyahui11-0");
            } else {
//                NSLog(@"huyahui11-1");
//                
//                NSLog(@"dic:%@", info);
                [[EaseMob sharedInstance].chatManager asyncLoginWithUsername:[NSString stringWithFormat:@"dahe_%@",[CDFMe shareInstance].uid] password:NewMima completion:^(NSDictionary *loginInfo, EMError *error) {
                    if (!error && loginInfo) {
//                        NSLog(@"登陆成功");
                        [self countOfUnreadMessages];
                    }else {
//                        NSLog(@"登陆失败:%@", error);
//                        NSLog(@"error.errorCode:%u", error.errorCode);
                    }
                } onQueue:nil];
            }
        } onQueue:nil];
    } else if([CDFMe shareInstance].isLogin) {
        
        [self unregisterNotifications];
//        NSLog(@"me.name = %@", [CDFMe shareInstance].userName);
        NSString *userID = [CDFMe shareInstance].uid;
//        NSLog(@"huyahui%@", [NSString stringWithFormat:@"dahe_%@", userID]);
        
        NSString *mima = [ChatListViewController md5:userID];
//        NSLog(@"mima:%@", mima);
        NSString *NewMima = [mima substringWithRange:NSMakeRange(0, 10)];
//        NSLog(@"NewMima:%@",NewMima);
        //环信
        [[EaseMob sharedInstance].chatManager asyncLoginWithUsername:[NSString stringWithFormat:@"dahe_%@",userID] password:NewMima completion:^(NSDictionary *loginInfo, EMError *error) {
            if (!error && loginInfo) {
//                NSLog(@"登陆成功");
            }else {
//                NSLog(@"登陆失败");
            }
        } onQueue:nil];
    }
    
    if (self.tableView) {
        [self.tableView reloadData];
    }
    
    
    [self refreshDataSource];
    [self registerNotifications];
    
    //    int indexICareAbout = 3;
    //    NSString *badgeValue = nil;
    //    [[[[[self tabBarController] viewControllers] objectAtIndex: indexICareAbout] tabBarItem] setBadgeValue:badgeValue];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self unregisterNotifications];
}

#pragma mark - getter

- (SRRefreshView *)slimeView
{
    if (!_slimeView) {
        _slimeView = [[SRRefreshView alloc] init];
        _slimeView.delegate = self;
        _slimeView.upInset = 0;
        _slimeView.slimeMissWhenGoingBack = YES;
        _slimeView.slime.bodyColor = [UIColor grayColor];
        _slimeView.slime.skinColor = [UIColor grayColor];
        _slimeView.slime.lineWith = 1;
        _slimeView.slime.shadowBlur = 4;
        _slimeView.slime.shadowColor = [UIColor grayColor];
        _slimeView.backgroundColor = [UIColor whiteColor];
    }
    return _slimeView;
}

- (UISearchBar *)searchBar
{
    if (!_searchBar) {
        _searchBar = [[EMSearchBar alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width, 0)];
        _searchBar.delegate = self;
        _searchBar.placeholder = NSLocalizedString(@"search", @"Search");
        _searchBar.backgroundColor = [UIColor colorWithRed:0.747 green:0.756 blue:0.751 alpha:1.000];
    }
    
    return _searchBar;
}

- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, self.searchBar.frame.size.height, self.view.frame.size.width, self.view.frame.size.height - self.searchBar.frame.size.height) style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [[UIView alloc] init];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerClass:[ChatListCell class] forCellReuseIdentifier:@"chatListCell"];
    }
    
    return _tableView;
}

- (EMSearchDisplayController *)searchController
{
    if (_searchController == nil) {
        _searchController = [[EMSearchDisplayController alloc] initWithSearchBar:self.searchBar contentsController:self];
        _searchController.delegate = self;
        _searchController.searchResultsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        __weak ChatListViewController *weakSelf = self;
        [_searchController setCellForRowAtIndexPathCompletion:^UITableViewCell *(UITableView *tableView, NSIndexPath *indexPath) {
            static NSString *CellIdentifier = @"ChatListCell";
            ChatListCell *cell = (ChatListCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            // Configure the cell...
            if (cell == nil) {
                cell = [[ChatListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            
            EMConversation *conversation = [weakSelf.searchController.resultsSource objectAtIndex:indexPath.row];
            cell.name = conversation.chatter;
            if (!conversation.isGroup) {
                cell.placeholderImage = [UIImage imageNamed:@"chatListCellHead.png"];
            }
            else{
                NSString *imageName = @"groupPublicHeader";
                NSArray *groupArray = [[EaseMob sharedInstance].chatManager groupList];
                for (EMGroup *group in groupArray) {
                    if ([group.groupId isEqualToString:conversation.chatter]) {
                        cell.name = group.groupSubject;
                        imageName = group.isPublic ? @"groupPublicHeader" : @"groupPrivateHeader";
                        break;
                    }
                }
                cell.placeholderImage = [UIImage imageNamed:imageName];
            }
            cell.detailMsg = [weakSelf subTitleMessageByConversation:conversation];
            cell.time = [weakSelf lastMessageTimeByConversation:conversation];
            cell.unreadCount = [weakSelf unreadMessageCountByConversation:conversation];
            if (indexPath.row % 2 == 1) {
                cell.contentView.backgroundColor = RGBACOLOR(246, 246, 246, 1);
            }else{
                cell.contentView.backgroundColor = [UIColor whiteColor];
            }
            return cell;
        }];
        
        [_searchController setHeightForRowAtIndexPathCompletion:^CGFloat(UITableView *tableView, NSIndexPath *indexPath) {
            return [ChatListCell tableView:tableView heightForRowAtIndexPath:indexPath];
        }];
        
        [_searchController setDidSelectRowAtIndexPathCompletion:^(UITableView *tableView, NSIndexPath *indexPath) {
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            [weakSelf.searchController.searchBar endEditing:YES];
            
            EMConversation *conversation = [weakSelf.searchController.resultsSource objectAtIndex:indexPath.row];
#warning hutest150304_
            ChatViewController *chatVC = [[ChatViewController alloc] initWithChatter:conversation.chatter isGroup:conversation.isGroup userName:nil];
            chatVC.title = conversation.chatter;
            [weakSelf.navigationController pushViewController:chatVC animated:YES];
        }];
    }
    
    return _searchController;
}

- (UIView *)networkStateView
{
    if (_networkStateView == nil) {
        _networkStateView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 44)];
        _networkStateView.backgroundColor = [UIColor colorWithRed:255 / 255.0 green:199 / 255.0 blue:199 / 255.0 alpha:0.5];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, (_networkStateView.frame.size.height - 20) / 2, 20, 20)];
        imageView.image = [UIImage imageNamed:@"messageSendFail"];
        [_networkStateView addSubview:imageView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(imageView.frame) + 5, 0, _networkStateView.frame.size.width - (CGRectGetMaxX(imageView.frame) + 15), _networkStateView.frame.size.height)];
        label.font = [UIFont systemFontOfSize:15.0];
        label.textColor = [UIColor grayColor];
        label.backgroundColor = [UIColor clearColor];
        label.text = NSLocalizedString(@"network.disconnection", @"Network disconnection");
        [_networkStateView addSubview:label];
    }
    
    return _networkStateView;
}

#pragma mark - private

- (NSMutableArray *)loadDataSource
{
    NSMutableArray *ret = nil;
    NSArray *conversations = [[EaseMob sharedInstance].chatManager conversations];
    NSArray* sorte = [conversations sortedArrayUsingComparator:
                      ^(EMConversation *obj1, EMConversation* obj2){
                          EMMessage *message1 = [obj1 latestMessage];
                          EMMessage *message2 = [obj2 latestMessage];
                          if(message1.timestamp > message2.timestamp) {
                              return(NSComparisonResult)NSOrderedAscending;
                          }else {
                              return(NSComparisonResult)NSOrderedDescending;
                          }
                      }];
    ret = [[NSMutableArray alloc] initWithArray:sorte];
    return ret;
}

// 得到最后消息时间
-(NSString *)lastMessageTimeByConversation:(EMConversation *)conversation
{
    NSString *ret = @"";
    EMMessage *lastMessage = [conversation latestMessage];
    if (lastMessage) {
        ret = [NSDate formattedTimeFromTimeInterval:lastMessage.timestamp];
    }
    return ret;
}

// 得到未读消息条数
- (NSInteger)unreadMessageCountByConversation:(EMConversation *)conversation
{
    NSInteger ret = 0;
    ret = conversation.unreadMessagesCount;
    
    return  ret;
}

// 得到最后消息文字或者类型
-(NSString *)subTitleMessageByConversation:(EMConversation *)conversation
{
    NSString *ret = @"";
    EMMessage *lastMessage = [conversation latestMessage];
    if (lastMessage) {
        id<IEMMessageBody> messageBody = lastMessage.messageBodies.lastObject;
        switch (messageBody.messageBodyType) {
            case eMessageBodyType_Image:{
                ret = NSLocalizedString(@"message.image1", @"[image]");
            } break;
            case eMessageBodyType_Text:{
                // 表情映射。
                NSString *didReceiveText = [ConvertToCommonEmoticonsHelper
                                            convertToSystemEmoticons:((EMTextMessageBody *)messageBody).text];
                ret = didReceiveText;
            } break;
            case eMessageBodyType_Voice:{
                ret = NSLocalizedString(@"message.voice1", @"[voice]");
            } break;
            case eMessageBodyType_Location: {
                ret = NSLocalizedString(@"message.location1", @"[location]");
            } break;
            case eMessageBodyType_Video: {
                ret = NSLocalizedString(@"message.vidio1", @"[vidio]");
            } break;
            default: {
            } break;
        }
    }
    return ret;
}

#pragma mark - TableViewDelegate & TableViewDatasource

-(UITableViewCell *)tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {//帖子回复
        CDFIMCell *cell = [[CDFIMCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"nil"];
        //        NSInteger newpmCount = [CDFMe shareInstance].newpm;
        NSInteger newpmCount = [CDFMe shareInstance].tiezeCount;
//        NSLog(@"newpmcount:%d", newpmCount);
        if (newpmCount > 0 && newpmCount <= 99) {
            UIImageView *imageView = (UIImageView *)[cell viewWithTag:100];
            [imageView removeFromSuperview];
            cell.badgeLabel.backgroundColor = [UIColor redColor];
            cell.badgeLabel.text = [NSString stringWithFormat:@"%d", newpmCount]; //+1是为了测试
        } else if (newpmCount > 99) {
            UIImageView *imageView = (UIImageView *)[cell viewWithTag:100];
            [imageView removeFromSuperview];
            cell.badgeLabel.text = @"99+";
            cell.badgeLabel.font = [UIFont systemFontOfSize:11];
        } else {
            cell.badgeLabel.backgroundColor = [UIColor whiteColor];
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width - 30, 20, 5, 10)];
            imageView.image = [UIImage imageNamed:@"btn_nxt@2x"];
            imageView.tag = 100;
            [cell addSubview:imageView];
        }
        cell.titleLable.text = @"回复";
        cell.iconView.image = [UIImage imageNamed:@"postReply"];
        cell.iconView.layer.masksToBounds = YES;
        cell.iconView.layer.cornerRadius = 20;
        return cell;
    } else if (indexPath.row == 1) {//
        CDFIMCell *cell = [[CDFIMCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"nil"];
        //NSInteger newpmCount = [CDFMe shareInstance].newpm;
        NSInteger newpmCount = [CDFMe shareInstance].newTingZhongCount;
//        NSLog(@"newpmcount:%d", newpmCount);
        if (newpmCount > 0 && newpmCount <= 99) {
            UIImageView *imageView = (UIImageView *)[cell viewWithTag:100];
            [imageView removeFromSuperview];
            cell.badgeLabel.backgroundColor = [UIColor redColor];
            cell.badgeLabel.text = [NSString stringWithFormat:@"%d", newpmCount]; //+1是为了测试
        } else if (newpmCount > 99) {
            UIImageView *imageView = (UIImageView *)[cell viewWithTag:100];
            [imageView removeFromSuperview];
            cell.badgeLabel.text = @"99+";
            cell.badgeLabel.font = [UIFont systemFontOfSize:11];
        } else {
            cell.badgeLabel.backgroundColor = [UIColor whiteColor];
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width - 30, 20, 5, 10)];
            imageView.image = [UIImage imageNamed:@"btn_nxt@2x"];
            imageView.tag = 100;
            [cell addSubview:imageView];
        }
        cell.titleLable.text = @"新听众";
        cell.iconView.image = [UIImage imageNamed:@"myNewFriends"];
        cell.iconView.layer.masksToBounds = YES;
        cell.iconView.layer.cornerRadius = 20;
        return cell;
    }else if (indexPath.row == 2){//喜欢
        CDFIMCell *cell = [[CDFIMCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"nil"];
        //        NSInteger newpmCount = [CDFMe shareInstance].newpm;
        NSInteger newpmCount = [CDFMe shareInstance].newLikeCount;
//        NSLog(@"newpmcount:%d", newpmCount);
        if (newpmCount > 0 && newpmCount <= 99) {
            UIImageView *imageView = (UIImageView *)[cell viewWithTag:100];
            [imageView removeFromSuperview];
            cell.badgeLabel.backgroundColor = [UIColor redColor];
            cell.badgeLabel.text = [NSString stringWithFormat:@"%d", newpmCount]; //+1是为了测试
        } else if (newpmCount > 99) {
            UIImageView *imageView = (UIImageView *)[cell viewWithTag:100];
            [imageView removeFromSuperview];
            cell.badgeLabel.text = @"99+";
            cell.badgeLabel.font = [UIFont systemFontOfSize:11];
        } else {
            cell.badgeLabel.backgroundColor = [UIColor whiteColor];
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width - 30, 20, 5, 10)];
            imageView.image = [UIImage imageNamed:@"btn_nxt@2x"];
            imageView.tag = 100;
            [cell addSubview:imageView];
        }
        cell.titleLable.text = @"赞";
        cell.iconView.image = [UIImage imageNamed:@"myLike"];
        cell.iconView.layer.masksToBounds = YES;
        cell.iconView.layer.cornerRadius = 20;
        return cell;
    }else {
        static NSString *identify = @"chatListCell";
        ChatListCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
        
        if (!cell) {
            cell = [[ChatListCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identify];
        }
        EMConversation *conversation = [self.dataSource objectAtIndex:indexPath.row - 3];
        NSString *uid = [conversation.chatter substringFromIndex:5];
        NSString *authorFaceURL = [Common formatAvatarWithUID:uid type:@"big"];
        cell.imageURL = [NSURL URLWithString:authorFaceURL];
//        NSLog(@"conversationID:%@", conversation.chatter);
        NSString *firstUserName = conversation.latestMessage.ext[@"to_nickname"];
        NSString *secondUserName = conversation.latestMessage.ext[@"from_nickname"];
        CDFMe *me = [CDFMe shareInstance];
        NSString *nameToShow;
        if ([firstUserName isEqualToString:me.userName]) {
            nameToShow = secondUserName;
        } else {
            nameToShow = firstUserName;
        }
//        NSLog(@"fromName:%@", nameToShow);
        //        cell.name = conversation.chatter;
        cell.name = nameToShow;
        if (!conversation.isGroup) {
            cell.placeholderImage = [UIImage imageNamed:@"chatListCellHead.png"];
        } else{
            NSString *imageName = @"groupPublicHeader";
            NSArray *groupArray = [[EaseMob sharedInstance].chatManager groupList];
            for (EMGroup *group in groupArray) {
                if ([group.groupId isEqualToString:conversation.chatter]) {
                    cell.name = group.groupSubject;
                    imageName = group.isPublic ? @"groupPublicHeader" : @"groupPrivateHeader";
                    break;
                }
            }
            cell.placeholderImage = [UIImage imageNamed:imageName];
        }
        cell.detailMsg = [self subTitleMessageByConversation:conversation];
        cell.time = [self lastMessageTimeByConversation:conversation];
        cell.unreadCount = [self unreadMessageCountByConversation:conversation];
        if (indexPath.row % 2 == 1) {
            cell.contentView.backgroundColor = [UIColor whiteColor];
        }else{
//            cell.contentView.backgroundColor = RGBACOLOR(246, 246, 246, 1);
            cell.contentView.backgroundColor = [UIColor whiteColor];
        }
        return cell;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  self.dataSource.count + 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return 51;
    } else if (indexPath.row == 1) {
        return 51;
    }else if (indexPath.row == 2){
        return 51;
    }
    else {
        return [ChatListCell tableView:tableView heightForRowAtIndexPath:indexPath];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        //        [CDFMe shareInstance].newpm = 0;
        [CDFMe shareInstance].tiezeCount = 0;
        NSArray *conversations = [[[EaseMob sharedInstance] chatManager] conversations];
        NSInteger unreadCount = 0;
        for (EMConversation *conversation in conversations) {
            unreadCount += conversation.unreadMessagesCount;
        }
        unreadCount = unreadCount + [CDFMe shareInstance].newLikeCount + [CDFMe shareInstance].newTingZhongCount;
        if (unreadCount > 0) {
            self.navigationController.tabBarItem.badgeValue = [NSString stringWithFormat:@"%d", unreadCount];
        } else {
            self.navigationController.tabBarItem.badgeValue = nil;
        }
//        [self performSegueWithIdentifier:@"hahahaha" sender:nil];
        UIStoryboard *s = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        CDFMessageViewController *vc = [s instantiateViewControllerWithIdentifier:@"CDFMessageViewController"];
        vc.type = reply;
        vc.navigationItem.title = @"帖子回复";
        [self.navigationController pushViewController:vc animated:YES];
        
    } else if (indexPath.row == 1) {
        //新听众
        [CDFMe shareInstance].newTingZhongCount = 0;
        NSArray *conversations = [[[EaseMob sharedInstance] chatManager] conversations];
        NSInteger unreadCount = 0;
        for (EMConversation *conversation in conversations) {
            unreadCount += conversation.unreadMessagesCount;
        }
        unreadCount = unreadCount + [CDFMe shareInstance].newLikeCount + [CDFMe shareInstance].tiezeCount;
        if (unreadCount > 0) {
            self.navigationController.tabBarItem.badgeValue = [NSString stringWithFormat:@"%d", unreadCount];
        } else {
            self.navigationController.tabBarItem.badgeValue = nil;
        }
        UIStoryboard *s = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        CDFMessageViewController *vc = [s instantiateViewControllerWithIdentifier:@"CDFMessageViewController"];
        vc.type = newListen;
        vc.navigationItem.title = @"我的听众";
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.row == 2) {
        //喜欢
        [CDFMe shareInstance].newLikeCount = 0;
        NSArray *conversations = [[[EaseMob sharedInstance] chatManager] conversations];
        NSInteger unreadCount = 0;
        for (EMConversation *conversation in conversations) {
            unreadCount += conversation.unreadMessagesCount;
        }
        unreadCount = unreadCount + [CDFMe shareInstance].tiezeCount + [CDFMe shareInstance].newTingZhongCount;
        if (unreadCount > 0) {
            self.navigationController.tabBarItem.badgeValue = [NSString stringWithFormat:@"%d", unreadCount];
        } else {
            self.navigationController.tabBarItem.badgeValue = nil;
        }
        UIStoryboard *s = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        CDFMessageViewController *vc = [s instantiateViewControllerWithIdentifier:@"CDFMessageViewController"];
        vc.type = like;
        vc.navigationItem.title = @"赞";
        [self.navigationController pushViewController:vc animated:YES];
    }
    else {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        EMConversation *conversation = [self.dataSource objectAtIndex:indexPath.row -3];
        
        ChatViewController *chatController;
        NSString *title = conversation.chatter;
        if (conversation.isGroup) {
            NSArray *groupArray = [[EaseMob sharedInstance].chatManager groupList];
            for (EMGroup *group in groupArray) {
                if ([group.groupId isEqualToString:conversation.chatter]) {
                    title = group.groupSubject;
                    break;
                }
            }
        }
        NSString *firstUserName = conversation.latestMessage.ext[@"to_nickname"];
        NSString *secondUserName = conversation.latestMessage.ext[@"from_nickname"];
        CDFMe *me = [CDFMe shareInstance];
        NSString *nameToShow;
        if ([firstUserName isEqualToString:me.userName]) {
            nameToShow = secondUserName;
        } else {
            nameToShow = firstUserName;
        }
//        NSLog(@"nametoShwo = %@", nameToShow);
        
        NSString *chatter = conversation.chatter;
        chatController = [[ChatViewController alloc] initWithChatter:chatter isGroup:conversation.isGroup userName:nameToShow];
        
//        NSLog(@"%@----%@",[self.dataSource[0] chatter],[CDFMe shareInstance].uid);
        chatController.uid =[[self.dataSource[0] chatter] componentsSeparatedByString:@"_"][1];
        
        chatController.title = nameToShow;
        chatController.hidesBottomBarWhenPushed = YES; //隐藏tabbar
        [self.navigationController pushViewController:chatController animated:YES];
    }
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row >= 3) {
        return YES;
    }
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >=3) {
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            EMConversation *converation = [self.dataSource objectAtIndex:indexPath.row - 3];
            [[EaseMob sharedInstance].chatManager removeConversationByChatter:converation.chatter deleteMessages:YES];
            [self.dataSource removeObjectAtIndex:indexPath.row - 3];
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
    }
}

#pragma mark - UISearchBarDelegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [[RealtimeSearchUtil currentUtil] realtimeSearchWithSource:self.dataSource searchText:(NSString *)searchText collationStringSelector:@selector(chatter) resultBlock:^(NSArray *results) {
        if (results) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.searchController.resultsSource removeAllObjects];
                [self.searchController.resultsSource addObjectsFromArray:results];
                [self.searchController.searchResultsTableView reloadData];
            });
        }
    }];
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    searchBar.text = @"";
    [[RealtimeSearchUtil currentUtil] realtimeSearchStop];
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
}
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//    if ([segue.destinationViewController respondsToSelector:@selector(touid)] && [sender isKindOfClass:[UITableViewCell class]]) {
//        [MobClick event:@"SendPMFromMessage"];
//        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
//        [segue.destinationViewController setValue:self.dataSource[indexPath.row][@"touid"] forKey:@"touid"];
//        [segue.destinationViewController setTitle:self.dataSource[indexPath.row][@"tousername"]];
//    }
//    if ([sender isKindOfClass:[NSDictionary class]]) {
//        if ([sender[@"type"] isEqualToString:@"user"]) {
//
//            if ([segue.destinationViewController respondsToSelector:@selector(uid)]) {
//                [segue.destinationViewController setValue:sender[@"uid"] forKey:@"uid"];
//            }
//        }
//        if ([sender[@"type"] isEqualToString:@"post"]) {
//
//            if ([segue.destinationViewController respondsToSelector:@selector(tid)]) {
//                [segue.destinationViewController setValue:sender[@"tid"] forKey:@"tid"];
//            }
//
//            if ([segue.destinationViewController respondsToSelector:@selector(pid)]) {
//                [segue.destinationViewController setValue:sender[@"pid"] forKey:@"pid"];
//            }
//        }
//    }
//}

#pragma mark - scrollView delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [_slimeView scrollViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [_slimeView scrollViewDidEndDraging];
}

#pragma mark - slimeRefresh delegate
//刷新消息列表
- (void)slimeRefreshStartRefresh:(SRRefreshView *)refreshView
{
    [self refreshDataSource];
    [_slimeView endRefresh];
}

#pragma mark - IChatMangerDelegate

-(void)didUnreadMessagesCountChanged
{
    [self refreshDataSource];
}

- (void)didUpdateGroupList:(NSArray *)allGroups error:(EMError *)error
{
    [self refreshDataSource];
}

#pragma mark - registerNotifications
-(void)registerNotifications{
    [self unregisterNotifications];
    [[EaseMob sharedInstance].chatManager addDelegate:self delegateQueue:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(logoutNot) name:kLOGOUT_NOTIFICATION object:nil];
}

-(void)unregisterNotifications{
    [[EaseMob sharedInstance].chatManager removeDelegate:self];
}

- (void)dealloc{
    [self unregisterNotifications];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTieziCount_Change object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNewLessonUser object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNewLike object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLOGOUT_NOTIFICATION object:nil];
}

#pragma mark - public

-(void)refreshDataSource
{
    self.dataSource = [self loadDataSource];
    [_tableView reloadData];
    //    [self hideHud];
}

- (void)isConnect:(BOOL)isConnect{
    if (!isConnect) {
        _tableView.tableHeaderView = _networkStateView;
    }
    else{
        _tableView.tableHeaderView = nil;
    }
}

- (void)networkChanged:(EMConnectionState)connectionState
{
    if (connectionState == eEMConnectionDisconnected) {
        _tableView.tableHeaderView = _networkStateView;
    }
    else{
        _tableView.tableHeaderView = nil;
    }
}

- (void)willReceiveOfflineMessages{
//    NSLog(NSLocalizedString(@"message.beginReceiveOffine", @"Begin to receive offline messages"));
}

- (void)didFinishedReceiveOfflineMessages:(NSArray *)offlineMessages{
//    NSLog(NSLocalizedString(@"message.endReceiveOffine", @"End to receive offline messages"));
    [self refreshDataSource];
}

@end
