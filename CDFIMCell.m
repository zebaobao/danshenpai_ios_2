//
//  CDFIMCell.m
//  yanyu
//
//  Created by caiyee on 15/3/2.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "CDFIMCell.h"

@implementation CDFIMCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.iconView = [[UIImageView alloc] initWithFrame:CGRectMake(12, 5, 40, 40)];
        [self addSubview:self.iconView];
        self.badgeLabel = [[UILabel alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width - 40, 15, 20, 20)];
        _badgeLabel.tag = 999;
        _badgeLabel.textAlignment = NSTextAlignmentCenter;
        _badgeLabel.textColor = [UIColor whiteColor];
        _badgeLabel.layer.masksToBounds = YES;
        _badgeLabel.layer.cornerRadius = 10;
        _badgeLabel.font = [UIFont systemFontOfSize:15];
        [self addSubview:_badgeLabel];
        self.titleLable = [[UILabel alloc] initWithFrame:CGRectMake(65, 10, 100, 30)];
        [self addSubview:self.titleLable];
        
        UIView *grayView = [[UIView alloc]initWithFrame:CGRectMake(0, 50, self.bounds.size.width, 1)];
        grayView.backgroundColor = RGBACOLOR(246, 246, 246, 1);
        [self.contentView addSubview:grayView];
    }
    return self;
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
