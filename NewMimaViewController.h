//
//  NewMimaViewController.h
//  yanyu
//
//  Created by caiyee on 15/4/24.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewMimaViewController : UIViewController
@property (retain, nonatomic)  UITextField *firstTextField;
@property (retain, nonatomic)  UITextField *secondTextField;
@property (retain, nonatomic)  UITextField *thirdTextField;
@property (retain, nonatomic)  UIButton *btn;

@end
