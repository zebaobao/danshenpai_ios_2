//
//  CDFLocationManager.m
//  yanyu
//
//  Created by caiyee on 15/2/16.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "CDFLocationManager.h"
static CDFLocationManager *locationManager = nil;
@implementation CDFLocationManager
- (NSString *)locationURL {
    [self getLocationInfo];
    if (self.isAllowLocation) {
//        NSLog(@"locationURL=%@",locationURL);
        return locationURL;
    }
    return nil;
}
+ (CDFLocationManager *)sharedInstance
{
    if (!locationManager) {
//        NSLog(@"jiaxin");
        locationManager = [[CDFLocationManager alloc] init];
        //locationManager.isAllowLocation = YES;
    }
    return locationManager;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    [locationManager stopUpdatingLocation];
    
    NSString *strLat = [NSString stringWithFormat:@"%.4f",newLocation.coordinate.latitude + 0.004865];//纬度
//    NSLog(@"weidu:%f", newLocation.coordinate.latitude);
//    NSLog(@"jingdu:%f", newLocation.coordinate.longitude);
    NSString *strLng = [NSString stringWithFormat:@"%.4f",newLocation.coordinate.longitude + 0.012630];//经度
    CLLocation *locationhu = [[CLLocation alloc] initWithLatitude:(newLocation.coordinate.latitude-0.000635) longitude:(newLocation.coordinate.longitude+0.005450)];
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:locationhu completionHandler:^(NSArray *placemarks, NSError *error)
     {
         /*
         @property (nonatomic, readonly, copy) NSDictionary *addressDictionary;
         
         // address dictionary properties
         @property (nonatomic, readonly, copy) NSString *name; // eg. Apple Inc.
         @property (nonatomic, readonly, copy) NSString *thoroughfare; // street address, eg. 1 Infinite Loop
         @property (nonatomic, readonly, copy) NSString *subThoroughfare; // eg. 1
         @property (nonatomic, readonly, copy) NSString *locality; // city, eg. Cupertino
         @property (nonatomic, readonly, copy) NSString *subLocality; // neighborhood, common name, eg. Mission District
         @property (nonatomic, readonly, copy) NSString *administrativeArea; // state, eg. CA
         @property (nonatomic, readonly, copy) NSString *subAdministrativeArea; // county, eg. Santa Clara
         @property (nonatomic, readonly, copy) NSString *postalCode; // zip code, eg. 95014
         @property (nonatomic, readonly, copy) NSString *ISOcountryCode; // eg. US
         @property (nonatomic, readonly, copy) NSString *country; // eg. United States
         @property (nonatomic, readonly, copy) NSString *inlandWater; // eg. Lake Tahoe
         @property (nonatomic, readonly, copy) NSString *ocean; // eg. Pacific Ocean
         @property (nonatomic, readonly, copy) NSArray *areasOfInterest; // eg. Golden Gate Park
         @end
         
         */
         if(error == nil && [placemarks count]>0)
         {
             CLPlacemark *placemark = [placemarks objectAtIndex:0];
             /*
             NSString *formattedaddresslines = placemark.addressDictionary[@"FormattedAddressLines"];
             NSString *name = placemark.addressDictionary[@"Name"];
             NSString *state = placemark.addressDictionary[@"State"];
             NSString *street = placemark.addressDictionary[@"Street"];
             NSString *sublocality = placemark.addressDictionary[@"SubLocality"];
             NSString *Thoroughfare = placemark.addressDictionary[@"Thoroughfare"];
             NSLog(@"dic:%@", placemark.addressDictionary);
             NSLog(@"dic,formattedaddressLines%@", formattedaddresslines);
             NSLog(@"dic,name%@", name);
             NSLog(@"dic,state%@", state);
             NSLog(@"dic,street%@", street);
             NSLog(@"dic,SubLocality%@", sublocality);
             NSLog(@"dic,thoroughfare%@", Thoroughfare);
             */
             /*
             NSLog(@"name:%@", placemark.name);
             NSLog(@"thoroughfare:%@", placemark.thoroughfare);
             NSLog(@"subThoroughfare:%@", placemark.subThoroughfare);
             NSLog(@"locality:%@", placemark.locality);
             
             NSLog(@"subLocality:%@", placemark.subLocality);
             NSLog(@"administrativeArea:%@", placemark.administrativeArea);
             NSLog(@"subAdministrativeArea:%@", placemark.subAdministrativeArea);
             NSLog(@"postalCode:%@", placemark.postalCode);
             NSLog(@"ISOcountryCode:%@", placemark.ISOcountryCode);
             NSLog(@"country:%@", placemark.country);
             NSLog(@"inlandWater:%@", placemark.inlandWater);
             NSLog(@"ocean:%@", placemark.ocean);
             NSLog(@"areasOfInterest:%@", placemark.areasOfInterest);
             
             NSLog(@"alladdress%@", placemark.location);
             NSLog(@"Country = %@", placemark.country);
             NSLog(@"Postal Code = %@", placemark.postalCode);
             NSLog(@"Locality = %@", placemark.locality);
             NSLog(@"address = %@",placemark.name);
             NSLog(@"administrativeArea = %@",placemark.administrativeArea);
             NSLog(@"subAdministrativeArea = %@",placemark.subAdministrativeArea);
             NSLog(@"locality = %@", placemark.locality);
             NSLog(@"thoroughfare = %@", placemark.thoroughfare);
             */
             NSString *newStr;
             if ([placemark.administrativeArea isEqualToString:@"河南省"]) {
                 NSString *locality = placemark.locality ? placemark.locality : @"";
                 NSString *subLocality = placemark.subLocality ? placemark.subLocality : @"";
                 NSString *thoroughfare = placemark.thoroughfare ? placemark.thoroughfare : @"";
//                 newStr = placemark.subLocality ? [NSString stringWithFormat:@"%@%@%@", placemark.locality, placemark.subLocality, nil] : [NSString stringWithFormat:@"%@%@", placemark.locality, nil];
                 newStr = [NSString stringWithFormat:@"%@%@%@", locality, subLocality, thoroughfare];
             } else {
                 NSString *administrativeArea = placemark.administrativeArea ? placemark.administrativeArea : @"";
                 NSString *locality = placemark.locality ? placemark.locality : @"";
                 NSString *subLocality = placemark.subLocality ? placemark.subLocality : @"";
                 NSString *thoroughfare = placemark.thoroughfare ? placemark.thoroughfare : @"";
//                 NSString *locality = placemark.locality ? placemark.locality : @"";
//                 NSString *subLocality = placemark.subLocality ? placemark.subLocality : @"";
//                 NSString *thoroughfare = placemark.thoroughfare ? placemark.thoroughfare : @"";
//                 newStr = placemark.subLocality ? [NSString stringWithFormat:@"%@%@%@%@",placemark.administrativeArea, placemark.locality, placemark.subLocality, placemark.thoroughfare] : [NSString stringWithFormat:@"%@%@%@", placemark.administrativeArea, placemark.locality, placemark.thoroughfare];
                 newStr = [NSString stringWithFormat:@"%@%@%@%@", administrativeArea, locality, subLocality, thoroughfare];
             }
           
             /*
             if (placemark.name.length > 20 ) {
                 newStr = [NSString stringWithFormat:@"%@%@", placemark.location, placemark.thoroughfare];
             } else if ([[placemark.name substringWithRange:NSMakeRange(0, 2)] isEqualToString:@"中国"]) {
                 if ([[placemark.name substringWithRange:NSMakeRange(2, 2)] isEqualToString:@"河南"]) {
                     newStr = [placemark.name substringWithRange:NSMakeRange(5, placemark.name.length-5)];
                 }else {
                     newStr = [placemark.name substringWithRange:NSMakeRange(2, placemark.name.length-2)];
                 }
             } else {
                 newStr = placemark.name;
             }
              */
             
             locationURL = [NSString stringWithFormat:@"location=%@|%@|%@", strLng, strLat, newStr];
//             NSLog(@"789789%@", locationURL);
             
         }
         else if(error==nil && [placemarks count]==0){
//             NSLog(@"No results were returned.");
         }
         else if(error != nil) {
//             NSLog(@"An error occurred = %@", error);
         }
     }];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"locError:%@", error);
}
-(void)getLocationInfo {
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    if([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
//               [locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization];
    }
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    locationManager.distanceFilter = 10.0f;
    [locationManager startUpdatingLocation];
//    NSLog(@"wohense");
}

@end
