//
//  CDFUserManagerViewController.m
//  NewChengduFaner
//
//  Created by dev@huaxi100.com on 13-11-25.
//  Copyright (c) 2013年 www.huaxi100.com. All rights reserved.
//

#import "CDFUserManagerViewController.h"
//#import "APService.h"

@interface CDFUserManagerViewController ()
@property (strong, nonatomic) NSMutableArray *dataSource;
@end

@implementation CDFUserManagerViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (NSMutableArray *)dataSource
{
    if (!_dataSource) {
        _dataSource = [NSMutableArray new];
        for (id key in [[Common unarchiveForKey:kLoginInfo] allKeys]) {
            [_dataSource addObject:[Common unarchiveForKey:kLoginInfo][key]];
        }
    }
    
    return _dataSource;
}

- (void)reloadDataSource
{
    [self.dataSource removeAllObjects];
    for (id key in [[Common unarchiveForKey:kLoginInfo] allKeys]) {
        [self.dataSource addObject:[Common unarchiveForKey:kLoginInfo][key]];
    }
    [self.tableView reloadData];
}

- (IBAction)addUserAction:(id)sender {
    UIStoryboard *user = [UIStoryboard storyboardWithName:@"welcome" bundle:nil];
    [self presentModalViewController:user.instantiateInitialViewController animated:YES];
}

- (void)handleLogNotification:(NSNotification *)notification
{
    [self reloadDataSource];
}

- (void)addNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLogNotification:) name:kLOGIN_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLogNotification:) name:kLOGOUT_NOTIFICATION object:nil];
}

- (void)removeNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLOGIN_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLOGOUT_NOTIFICATION object:nil];
}

#pragma mark UIViewController Super Methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([Common systemVersion] < 7.0) {
        UIView *bg = [[UIView alloc] initWithFrame:self.tableView.bounds];
        bg.backgroundColor = bgColor;
        self.tableView.backgroundView = bg;
        self.navigationItem.rightBarButtonItem.tintColor = self.navigationController.navigationBar.tintColor;
        self.navigationItem.backBarButtonItem.tintColor = self.navigationController.navigationBar.tintColor;
    } else {
        self.view.backgroundColor = bgColor;
        self.navigationItem.rightBarButtonItem.tintColor = TITLECOLOR;
    }
    [self addNotification];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 30)];
    label.text = @"帐号管理";
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = TITLEGRAY;
    self.navigationItem.titleView = label;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"账号管理"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"账号管理"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.textLabel.text = self.dataSource[indexPath.row][@"data"][@"member_username"];
    //删除按钮
    UIButton *deleBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    deleBtn.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 50, 7, 30, 30);
    [deleBtn setTitle:@"X" forState:UIControlStateNormal];
    [deleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    deleBtn.layer.masksToBounds = YES;
    deleBtn.layer.cornerRadius = 15;
    [deleBtn setBackgroundColor:[UIColor grayColor]];
    [deleBtn addTarget:self action:@selector(deleteUser:) forControlEvents:UIControlEventTouchUpInside];
    deleBtn.tag = indexPath.row;
    [cell addSubview:deleBtn];
    
    return cell;
}
#pragma mark - 删除账号
//删除账号
- (void)deleteUser:(UIButton *)button{
    if ([[CDFMe shareInstance].uid isEqualToString:self.dataSource[button.tag][@"data"][@"member_uid"] ]) {
        [SVProgressHUD showErrorWithStatus:@"不可删除当前登陆用户！"];
        return;
    }
    //NSLog(@"%@",self.dataSource[button.tag][@"data"][@"member_username"]);
    
    NSDictionary * s = [Common unarchiveForKey:kLoginInfo];
    NSMutableDictionary *mDict = [NSMutableDictionary dictionaryWithDictionary:s];
    
//    for (NSString *key in [s allKeys]) {
//        NSLog(@"%@",key);
//        if ([key isEqualToString:self.dataSource[button.tag][@"data"][@"member_uid"] ]) {
//            
//        }
//    }
    [mDict removeObjectForKey:self.dataSource[button.tag][@"data"][@"member_uid"]];
    [Common archive:mDict withKey:kLoginInfo];
    [self.dataSource removeObjectAtIndex:button.tag];
    [self.tableView reloadData];
    [SVProgressHUD showSuccessWithStatus:@"删除成功！" duration:2.f];
   // NSLog(@"%@",s);
}
+ (NSString *) md5:(NSString *)str {
    const char *cStr = [str UTF8String];
    unsigned char result[16];
    CC_MD5( cStr, strlen(cStr), result );
    return [NSString stringWithFormat:@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *mima = [CDFUserManagerViewController md5:self.dataSource[indexPath.row][@"data"][@"member_uid"]];
//    NSLog(@"mima:%@", mima);
    NSString *NewMima = [mima substringWithRange:NSMakeRange(0, 10)];
//    NSLog(@"NewMima:%@",NewMima);
    
    [[EaseMob sharedInstance].chatManager asyncLogoffWithCompletion:^(NSDictionary *info, EMError *error) {
        if (error && error.errorCode != EMErrorServerNotLogin) {
            
        }
        else{
            
            [[EaseMob sharedInstance].chatManager asyncLoginWithUsername:[NSString stringWithFormat:@"dahe_%@",self.dataSource[indexPath.row][@"data"][@"member_uid"]] password:NewMima completion:^(NSDictionary *loginInfo, EMError *error) {
                if (!error && loginInfo) {
//                    NSLog(@"登陆成功");
                    
                }else {
                    
//                    NSLog(@"登陆失败:%@---username : %@", error, self.dataSource[indexPath.row][@"data"]);
//                    NSLog(@"error.errorCode:%u", error.errorCode);
                }
            } onQueue:nil];
        }
        
    } onQueue:nil];
    
   
  //环信

    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.navigationController popToRootViewControllerAnimated:YES];
    //[XGPush delTag:[Common unarchiveForKey:kOnlineInfo][@"data"][@"member_uid"]];

    [Common logout];
    
    [Common archive:self.dataSource[indexPath.row] withKey:kOnlineInfo];

//    [APService setAlias:self.dataSource[indexPath.row][@"data"][@"member_uid"] callbackSelector:nil object:nil];

    [[NSNotificationCenter defaultCenter] postNotificationName:kLOGIN_NOTIFICATION object:self.dataSource[indexPath.row]];
    
//    NSLog(@"%@ login", self.dataSource[indexPath.row][@"data"][@"member_uid"]);
//    NSLog(@"--------");
//
//    NSLog(@"%@",self.dataSource);
//    NSLog(@"--------");
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
