//
//  CDFIMCell.h
//  yanyu
//
//  Created by caiyee on 15/3/2.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDFIMCell : UITableViewCell
@property (nonatomic, retain) UILabel *badgeLabel;
@property (nonatomic, retain) UILabel *titleLable;
@property (nonatomic, retain) UIImageView *iconView;
@end
