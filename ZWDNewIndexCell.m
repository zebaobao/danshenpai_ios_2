//
//  ZWDNewIndexCell.m
//  yanyu
//
//  Created by Case on 15/3/27.
//  Copyright (c) 2015年 www.huaxi100.com. All rights reserved.
//

#import "ZWDNewIndexCell.h"

@implementation ZWDNewIndexCell

/**
 *  刷新附件view
 */
- (void)reloadAttachmentView
{
    
    CGRect frame = self.likeAndViewsControl.frame;
    frame.origin.y = self.bounds.size.height - 20;
    self.likeAndViewsControl.frame = frame;
    
    
    //清空容器
    
    [self.attachmentsView.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [obj removeFromSuperview];
    }];
    
    
    
    for (int i = 0; i < 1; i++) {
        
        NSString *attachmentURL = [NSString stringWithFormat:@"%@%@",self.attachmentsData[i][@"url"],self.attachmentsData[i][@"attachment"]];
//        NSLog(@"1290:%@", attachmentURL);
        //        CDFClickableImageView *attachment = [[CDFClickableImageView alloc] initWithFrame:CGRectMake(i * (50 + 8.5), 0, 50, 50)];
        //        attachment.backgroundColor = [UIColor colorWithWhite:.92 alpha:1];
        //        [self.attachmentsView addSubview:attachment];
        //        attachment.hqURL = attachmentURL;
        //        [attachment setImageWithURL:[NSURL URLWithString:[Common getThumbImage:attachmentURL size:CGSizeMake(50, 50) withRetinaDisplayDetect:YES]]];
//        NSLog(@"%f",(self.attachmentsView.bounds.size.width - 2*8.5)/3);
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(8.5, 0, 200, 200)];
        imageView.tag = 100 + i;
        imageView.userInteractionEnabled = YES;
        //        [imageView setImageWithURL:[NSURL URLWithString:[Common getThumbImage:attachmentURL size:CGSizeMake(50, 50) withRetinaDisplayDetect:YES]]];
        [imageView setImageWithURL:[NSURL URLWithString:[Common getThumbImage:attachmentURL size:CGSizeMake(200, 200) withRetinaDisplayDetect:YES]] placeholderImage:[UIImage imageNamed:@"bg_imageloader"]];
        
        
        [self.attachmentsView addSubview:imageView];
        
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(imageViewPressed:)];
        [imageView addGestureRecognizer: singleTap];
    }
    
    
    //    if (self.attachmentsData.count > 4) {
    //
    //        UIButton *totalAttachmentButton = [self createTotalAttachmentButton:self.attachmentsData.count];
    //        totalAttachmentButton.frame = CGRectMake(4 * (50 + 8.5), 0, 50, 50);
    //        [self.attachmentsView addSubview:totalAttachmentButton];
    //    }
}

@end
